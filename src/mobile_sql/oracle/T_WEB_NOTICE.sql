drop table t_web_notice;
drop trigger web_notice_trigger;
drop sequence web_notice_sequence; 


CREATE TABLE "T_WEB_NOTICE"
(	"NID" NUMBER(20,0),
	"TITLE" VARCHAR2(128 BYTE),
	"SUBJECT" CLOB,
	"CREATOR" VARCHAR2(32 BYTE) DEFAULT NULL,
	"CREATE_DATE" TIMESTAMP (6),
	"UPDATOR" VARCHAR2(32 BYTE) DEFAULT NULL,
	"UPDATE_DATE" TIMESTAMP (6),
	"IS_OPEN" NUMBER(1,0) DEFAULT 1,
	"RESERVED_DATE" DATE DEFAULT SYSDATE
);


CREATE SEQUENCE web_notice_sequence
START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE TRIGGER web_notice_trigger
BEFORE INSERT ON T_WEB_NOTICE
FOR EACH ROW
WHEN (new.nid IS NULL)
BEGIN
 SELECT web_notice_sequence.NEXTVAL
 INTO :NEW.NID
 FROM dual;
END;
/

select * from user_sequences;
select * from user_triggers;





delete from T_WEB_NOTICE;

