

DROP TABLE T_SERVER_INFO3;

CREATE TABLE T_SERVER_INFO3
 (
    "SERVER_ID" VARCHAR2(20 BYTE) NOT NULL ENABLE,
	  "SERVER_NAME" VARCHAR2(32 BYTE) NOT NULL ENABLE,
	  "PROTOCOL" VARCHAR2(8 BYTE) NOT NULL ENABLE,
	  "HOST_IP" VARCHAR2(24 BYTE) NOT NULL ENABLE,
	  "HOST_PORT" NUMBER NOT NULL ENABLE,
	  "NOTE" VARCHAR2(150 BYTE),
	  CONSTRAINT "T_SERVER_INFO3_PK" PRIMARY KEY ("SERVER_ID", "HOST_IP", "HOST_PORT")
) ;


insert into T_SERVER_INFO3
(SERVER_ID, SERVER_NAME, PROTOCOL, HOST_IP, HOST_PORT, NOTE)
VALUES
('0000001', 'APACHE', 'https', '211.115.15.244', 8443, null);


insert into T_SERVER_INFO3
(SERVER_ID, SERVER_NAME, PROTOCOL, HOST_IP, HOST_PORT, NOTE)
VALUES
('0000002', 'FCS', 'tcp', '211.115.15.244', 11555, null);


