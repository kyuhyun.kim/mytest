drop table T_WEB_HELP;
drop sequence web_help_sequence;
drop trigger web_HELP_trigger;


CREATE TABLE "T_WEB_HELP"
(
  "NID" NUMBER(20,0) NOT NULL ENABLE,
	"TITLE" VARCHAR2(128 BYTE) NOT NULL ENABLE,
	"SUBJECT" CLOB NOT NULL ENABLE,
	"CREATOR" VARCHAR2(32 BYTE) DEFAULT NULL,
	"CREATE_DATE" TIMESTAMP (6) NOT NULL ENABLE,
	"UPDATOR" VARCHAR2(32 BYTE) DEFAULT NULL,
	"UPDATE_DATE" TIMESTAMP (6) NOT NULL ENABLE,
	"CATEGORY" NUMBER(3,0) DEFAULT 0,
	 PRIMARY KEY ("NID")
);

CREATE SEQUENCE web_help_sequence
START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE TRIGGER web_HELP_trigger
BEFORE INSERT ON T_WEB_HELP
FOR EACH ROW
WHEN (new.nid IS NULL)
BEGIN
 SELECT web_HELP_sequence.NEXTVAL
 INTO :NEW.NID
 FROM dual;
END;
/

commit;

insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말 티아이스퀘어', '도움말 테스트 가나다라마바사아자차카타파하', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말1 Tisquare', '도움말  테스트 abcdefghijklmnopqrstuwvxyz', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말2', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말3', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말4', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말5', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말6', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말7', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말8', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말9', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말10', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말11', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말12', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말13', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말14', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말15', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말16', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말17', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말18', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말19', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말20', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);
insert into T_WEB_HELP (title, subject, creator, create_date, updator, update_date)
values ('도움말21', '도움말  테스트', '홍길동', SYSTIMESTAMP, '홍길동', SYSTIMESTAMP);

select * from T_WEB_HELP;

delete from t_WEB_HELP;


