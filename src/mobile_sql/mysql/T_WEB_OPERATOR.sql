
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cntdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cntdb`;
DROP TABLE IF EXISTS `T_WEB_OPERATOR`;
CREATE TABLE `T_WEB_OPERATOR` (
  `OP_ID` varchar(32) NOT NULL DEFAULT '',
  `NAME` varchar(16) DEFAULT NULL,
  `EMAIL` varchar(32) DEFAULT NULL,
  `PHONE` varchar(32) DEFAULT NULL,
  `STATUS` varchar(2) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `CREATOR` varchar(32) DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `UPDATOR` varchar(32) DEFAULT NULL,
  `OP_GRADE` varchar(16) DEFAULT NULL,
  `PASSWORD` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`OP_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

insert  into `T_WEB_OPERATOR`(`OP_ID`,`NAME`,`EMAIL`,`PHONE`,`STATUS`,`CREATE_DATE`,`CREATOR`,`UPDATE_DATE`,`UPDATOR`,`OP_GRADE`,`PASSWORD`) values ('admin','admin user','a@tisquare.com','010-1111-2223','1','2012-12-04 00:00:00','admin','2012-12-04 18:09:43','admin','ROLE_ADMIN','admin'),('test1','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:27:53','admin','2012-12-04 17:27:53','admin','ROLE_ADMIN','1111'),('test2','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:27:57','admin','2012-12-04 17:27:57','admin','ROLE_ADMIN','1111'),('test3','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:00','admin','2012-12-04 17:28:00','admin','ROLE_ADMIN','1111'),('test4','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:02','admin','2012-12-04 17:28:02','admin','ROLE_ADMIN','1111'),('test5','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:03','admin','2012-12-04 17:28:03','admin','ROLE_ADMIN','1111'),('test6','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:05','admin','2012-12-04 17:28:05','admin','ROLE_ADMIN','1111'),('test7','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:07','admin','2012-12-04 17:28:07','admin','ROLE_ADMIN','1111'),('test8','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:09','admin','2012-12-04 17:28:09','admin','ROLE_ADMIN','1111'),('test9','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:11','admin','2012-12-04 17:28:11','admin','ROLE_ADMIN','1111'),('test10','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:13','admin','2012-12-04 17:28:13','admin','ROLE_ADMIN','1111'),('test11','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:15','admin','2012-12-04 17:28:15','admin','ROLE_ADMIN','1111'),('test12','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:17','admin','2012-12-04 17:28:17','admin','ROLE_ADMIN','1111'),('test14','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:19','admin','2012-12-04 17:28:19','admin','ROLE_ADMIN','1111'),('test16','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:22','admin','2012-12-04 17:28:22','admin','ROLE_ADMIN','1111'),('test17','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:23','admin','2012-12-04 17:28:23','admin','ROLE_ADMIN','1111'),('test18','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:24','admin','2012-12-04 17:28:24','admin','ROLE_ADMIN','1111'),('test19','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:26','admin','2012-12-04 17:28:26','admin','ROLE_ADMIN','1111'),('test20','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:29','admin','2012-12-04 17:28:29','admin','ROLE_ADMIN','1111'),('test21','NAME','b@tisquare.com','000-1111-2222','1','2012-12-04 17:28:30','admin','2012-12-04 17:28:30','admin','ROLE_ADMIN','1111'),('test24','test24','1t@tisquare.com','010-1111-1256','1','2012-12-07 11:36:07','admin','2012-12-07 11:36:07','admin','ROLE_USER','test24'),('test23','test23 한글유저','asdf@tisquare.com','000-1111-2222','1','2012-12-04 19:14:05','admin','2012-12-04 19:14:05','admin','ROLE_ADMIN','1111');
