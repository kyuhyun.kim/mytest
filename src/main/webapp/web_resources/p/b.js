/*global window, $*/

function getArgs(queryString) {
  var args = {};
  var a = '';

  var prop;
  var val;
  var arg;

  try {
    if(queryString === undefined) {
      a = window.location.search.split('?')[1].split('&');
    } else {
      a = queryString.split('?')[1].split('#')[0].split('&');
    }
    for (prop in a) {
      if (a.hasOwnProperty(prop)) {
        console.log("prop: " + prop + " value: " + a[prop]);
        val = a[prop];
        arg = val.split('=');
        args[arg[0]] = arg[1];
      }
    }
  } catch (e) {
    console.log('Error getArgs window.location.search('+window.location.search+')');
  }

  console.log(JSON.stringify(args));
  return args;
}

//https://xxx.xxx.xxx.xxx:xxxx/mcs/p/b.html?sid=1&passwd=bite2058111%40%40%40
//Intent://[host명]?파라미터=파리미터값#Intent;scheme=callMyApp;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.test.myapp;end


$(function(){
  function toApp() {
    var o = {};
    o.protocol = "sktptt";
    o.host = "sktptt.com";
    o.appLink = 'Intent;scheme=' + o.protocol + ';action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.skt.ptt;end';
    o.param = args.referrer;
    var scheme = o.protocol + '://' + o.host + '?referrer=' + o.param + '#' + o.appLink;
    //window.location.href = scheme;
    //console.log("toApp clicked");
    return scheme;
  }

  function toMarket() {
    //var referrerData = encodeURIComponent('inviteId=' + args.token);
    //var referrerData = args.token;
    var referrerData = args.referrer;
    var scheme = "market://details?id=com.skt.ptt&referrer=" + referrerData;
    //window.location.href = scheme
    //console.log("toMarket clicked");
    return scheme;
  }

  function jump() {
    try {
      var scheme = toApp();
      var fallbackLink = toMarket();
      console.log("scheme: " + scheme);
      console.log('fallbackLink: ' + fallbackLink);

      var f;
      f = document.createElement("iframe");
      f.style.border = "none";
      f.style.width = "0px";
      f.style.height = "0px";
      f.src = scheme;
      document.body.appendChild(f);
    } catch (e) {
      window.location.replace(fallbackLink);
      console.log('fallbackLink catch');
    }

    var startDate = new Date();
    window.setTimeout(function (){
      if((new Date() - startDate) < 1600) {
        window.location.replace(fallbackLink);
        console.log('fallbackLink timeout');
      } else {
        //document.getElementById("loadingBar").style.display = 'none';
      }
    }, 1500);
  }

  var args = getArgs();
  if(args.referrer) {
    console.log(args.referrer);
  } else {
    console.log("no referrer");
  }
  /*
   *$(".toApp").click(function () {
   *  toApp();
   *});
   *$(".toMarket").click(function () {
   *  toMarket();
   *});
   */
  jump();

});
