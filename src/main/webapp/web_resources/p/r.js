/*global window, document*/

var scheme;

function getArgs(queryString) {
  var args = {};
  var a = '';

  // for loop
  var prop;
  var val;
  var arg;

  try {
    if(queryString === undefined) {
      a = window.location.search.split('?')[1].split('&');
    } else {
      a = queryString.split('?')[1].split('#')[0].split('&');
    }
    for (prop in a) {
      if (a.hasOwnProperty(prop)) {
        console.log("prop: " + prop + " value: " + a[prop]);
        val = a[prop];
        arg = val.split('=');
        args[arg[0]] = arg[1];
      }
    }
  } catch (e) {
    console.log('Error getArgs window.location.search('+window.location.search+')');
  }

  console.log(JSON.stringify(args));
  return args;
}

var o = {
  protocol: 'tphonetestservicekeypicture',
  //protocol: 'intent',
  host: 'message',
  fallbackLink: '',
  param: '',
  //appLink: 'Intent;scheme=tphonetestservicekeypicture;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.skt.topping;end'
  appLink: ''
};

function jump(o, args) {

  if(args.t) {
    if(args.t === 'i') {
      o.protocol = 'tphonetestservicekeypicture';
    } else if(args.t === 'd') {
      o.protocol ='tphonetestservicekeydocument';
    } else if(args.t === 'm') {
      o.protocol = 'tphonetestservicekeylocation';
    } else if(args.t === 'w') {
      o.protocol = 'tphonetestservicekeyweb';
    } else {
      console.log("args.t is invalid.");
    }
  }

  if(args.a) {
    o.param = args.a;
  } else {
    console.log("args.a is invalid.");
  }

  if(args.w) {
    o.fallbackLink = decodeURIComponent(args.w);
  } else {
    console.log("args.w is invalid.");
  }

  console.log(JSON.stringify(o));

  if(navigator.userAgent.indexOf('TPhone') > 0){
    document.getElementById("loadingBar").style.display = 'none';
    if(args.w && args.w.length > 0) {
      window.location.replace(o.fallbackLink);
    }
  } else {
    document.getElementById("loadingBar").style.display = '';
  try {
      scheme = o.protocol + '://' + o.host + '?value=' + o.param + '#' + o.appLink;

      console.log("scheme: " + scheme);

      var f;
      f = document.createElement("iframe");
      f.style.border = "none";
      f.style.width = "0px";
      f.style.height = "0px";
      f.src = scheme;
      document.body.appendChild(f);
    } catch (e) {
      if(args.w && args.w.length > 0) {
        window.location.replace(o.fallbackLink);
      }
      //document.body.style.backgroundColor = 'red';
      console.log('fallbackLink catch');
    }

    var startDate = new Date();
    window.setTimeout(function (){
      if((new Date() - startDate) < 1600) {
        //document.body.style.backgroundColor = 'yellow';
        //window.location.replace(scheme);
        //setTimeout(function() {
          //document.body.style.backgroundColor = 'blue';
          if(args.w && args.w.length > 0) {
            window.location.replace(o.fallbackLink);
          }
          console.log('fallbackLink timeout');
        //}, 3000);
      } else {
        document.getElementById("loadingBar").style.display = 'none';
      }
    }, 1500);
  }
}

window.onload = function () {
  document.body.style.backgroundColor = 'black';
  jump(o, getArgs());
};
