$(document).ready(function () {
    window.type = 'help';
    if (window.getQueryVariable === undefined) {
        window.getQueryVariable = function (variable) {
            try {
                var query = window.location.search.substring(1);
                console.log('query(emergency getQueryVariable): ' + query);
                var vars = query.split('&');
                for (var i = 0; i < vars.length; i++) {
                    var pair = vars[i].split('=');
                    if (decodeURIComponent(pair[0]) === variable) {
                        console.log(JSON.stringify(pair));
                        console.log(decodeURIComponent(pair[0]));
                        console.log(decodeURIComponent(pair[1]));
                        return decodeURIComponent(pair[1]);
                    }
                }
            } catch (e) {
                console.log(e.message);
                console.log('emergency getQueryVariable: ' + e.message);
                return false;
            }
            console.log('emergency Query variable %s not found', variable);
            return false;
        };
    }

    var option = {start_sid: 0, last_read_id: false == window.getQueryVariable("last_read_id") ? 0 : window.getQueryVariable("last_read_id")};
    getFromHelpList(option);
    $(document).scroll(function () {
        if (window.autoLoading === true) {
            if (window.innerHeight >= document.body.scrollHeight - window.scrollY) {
                if (isEnd == false && endCheckFlag === false) {
                    endCheckFlag = true;
                    var option = {start_sid: endNum, last_read_id: window.getQueryVariable("last_read_id")};
                    getFromHelpList(option);
                }
            }
        }
    });

    //debug start
    //addClickHandler();
    //window.appInterface.openUrl("http://naver.com");
    //debug end
});
