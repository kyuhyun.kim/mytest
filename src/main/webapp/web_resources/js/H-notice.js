var NOTICE_ADDR = "/plus/list/noticeJson.do";
var HELP_ADDR = "/plus/list/helpJson.do";
var isEnd = false;

$(document).ready( function(){
    $(".notice-list li button").click( showContent );

    $(".select select").change( function() {
        $(this).parent().find("span").html( $(this).find(":selected").text() );
    });

    $("#btnShowTop").click( function(){
        $(document).scrollTop(0);
    });
    
    $(".backward").click( function(){
    	//try{
    		window.CallApp.goPrevious();
    	/*}
    	catch( e )
    	{
    		console.log("App에서 구동되지 않았습니다.");
    	}*/
    	
    });
});

function showContent(){
    $(this).parents("li").toggleClass("on");

    if( true == $(this).parents("li").hasClass("on") )
    {
        $(this).parents("li").find("p").css("display", "block");
    }
    else
    {
        $(this).parents("li").find("p").css("display", "none");
    }
}

function showLoading( bo ){
	if( true == bo && false == $(".diff").hasClass("showloading") )
	{
		$(".diff").addClass("showloading");
	}
	else
	{
		$(".diff").removeClass("showloading");
	}
}

function getData( addr, option, result, error )
{
	showLoading(true);
    $.ajax( addr, {
        contentType : "application/json",
        data: option,
        dataType: "json",
        type: "GET",
        success: function( data, textStatus, jqXHR )
        {
        	showLoading(false);
        	if( data.result == 1 )
    		{
        		if( null != result && undefined != result && result instanceof Function )
                {
                    result( data, textStatus, jqXHR );
                }
    		}
        	else
    		{
        		alert("데이터를 불러오지 못하였습니다. 다시한번 시도해주세요.");
    		}
        },
        error: function( jqXHR, textStatus, errorThrown )
        {
        	showLoading(false);
            if( null != error && undefined != error && result instanceof Function )
            {
                error( jqXHR, textStatus, errorThrown );
            }
        }
    });
}

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    
    return false;
    console.log('Query variable %s not found', variable);
}