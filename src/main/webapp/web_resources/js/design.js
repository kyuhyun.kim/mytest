
$(function(){

	$(".customwrap .show_list>li>a").click(function(){
		$(this).toggleClass("on");
		$(this).next("div").slideToggle(200);
		$(".customwrap .show_list>li>a").not(this).removeClass("on").next("div").slideUp(200);
		return false;
	});

});
