var endNum = 0;
var isEnd = false;
var endCheckFlag = false;
window.autoLoading = true;
window.s_sid = 100000;
window.prevLastNID = -1;
window.lastNID = -1;

var G = {};

function getArgs(queryString) {
    var args = {};
    var a = '';
    try {
        if (queryString === undefined) {
            a = window.location.search.split('?')[1].split('&');
        } else {
            a = queryString.split('?')[1].split('#')[0].split('&');
        }
        $.each(a, function (key, val) {
            var arg = val.split('=');
            args[arg[0]] = arg[1];
        });
    } catch (e) {
        console.log('Error getArgs window.location.search(' + window.location.search + ')');
    }
    console.log(JSON.stringify(args));
    return args;
}

function whichNative() {
    var c = getArgs().app;
    switch (c) {
        case 'android':
            break;
        case 'ios':
            break;
        default:
            c = 'unknown';
            break;
    }

    G.app = c;
    return c;
}

function isAndroid() {
    return whichNative() === 'android';
}

function isIOS() {
    return whichNative() === 'ios';
}

function beforeAfter(s_sid_a, s_sid_b) {
    return s_sid_a > s_sid_b;
}

function clickHandler(e) {
    var desc = $(this).parent().siblings('.list_notice_detail_txt');
    var badge = $(this).find('.badge');
    var allDesc = $('.list_notice_detail_txt');
    var allTitle = $('.btn-list');
    var prevDesc = $('.btn-list.activated.on').parent().siblings('.list_notice_detail_txt');
    var prevDescHeight = 0;
    var t = 0;
    var current_s_sid = 0;

    if (prevDesc.length === 1 && prevDesc.outerHeight() !== null) {
        prevDescHeight = prevDesc.outerHeight();
    } else {
        prevDescHeight = -1;
    }
    console.log("prevDescHeight is " + prevDescHeight);

    allDesc.css({display: 'none'});
    desc.css({display: 'block'});
    badge.css({display: 'none'});
    $(this).addClass('on');

    if ($(this).hasClass('activated')) {
        console.log('close>');
        allDesc.css({display: 'none'});
        allTitle.removeClass('on');
        $(this).removeClass('activated');
    } else {
        console.log('open>');

        allTitle.removeClass('activated');
        allTitle.removeClass('on');

        $(this).addClass('activated');
        $(this).addClass('on');

        if (prevDescHeight === -1 || prevDescHeight === null) {
            //window.scrollTo(0, e.pageY - e.offsetY);
        } else {
            current_s_sid = Number($(this).attr('count'));
            console.log("s_sid: " + window.s_sid + ' ' + current_s_sid);
            if (window.s_sid === current_s_sid) {
                console.log("s_sid: same");
                t = e.pageY - e.offsetY - prevDescHeight;
            } else if (beforeAfter(window.s_sid, current_s_sid)) {
                t = e.pageY - e.offsetY - prevDescHeight;
                //window.scrollTo(0, t);
            } else {
                t = e.pageY - e.offsetY;
                console.log("t:" + t + ' ' + 'UPPER');
                //window.scrollTo(0, e.pageY - e.offsetY);
            }
        }
        window.s_sid = +($(this).attr('count'));
    }
    e.preventDefault();
}

if (window.appInterface === undefined) {
    window.appInterface = {};
    console.log("web mode");
} else {
    console.log("app mode");
}

if (window.appInterface.openUrl !== undefined) {
    console.log("openUrl ok");
} else {
    console.log("debug: window.appInterface.openUrl is null.");
    console.log("openUrl debug ok");
    window.appInterface.openUrl = function (url) {
        console.log("debug: " + url);
    };
}

if (window.appInterface.commandToWebview !== undefined) {
    console.log("commandToWebview ok");
} else {
    console.log("debug: window.appInterface.commandToWebview is null.");
    console.log("commandToWebview debug ok");
    window.appInterface.commandToWebview = function (cmd, data) {
        console.log("debug.cmd: " + cmd);
        console.log("debug.data: " + data);
    };
}

function appClickHandler(e) {
    var url = '';
    e.preventDefault();
    e.stopPropagation();
    url = $(this).attr('href');
    window.appInterface.openUrl(url);
    console.log("openUrl: " + url);
}

window.updateLastNID = function (id) {
    console.log('updateLastNID: ' + id);
    if (window.type === 'notice') {
        window.appInterface.commandToWebview('SetLastNID', id);
    }
};

function addClickHandler() {
    //$('.btn-list').off('click', clickHandler);
    //$('.btn-list').on('click', clickHandler);
    //$('a.app-interface').off('click', appClickHandler);
    //$('a.app-interface').on('click', appClickHandler);

    /*
     *  var contentNumber = $('.btn-list').size();
     *  var i = 0;
     *  for(i=0;i<contentNumber;i++){
     *    var contentArray = $('.atTxt'+i).width();
     *    var documentWidth = $(document).width();
     *    var result = (contentArray * 100) / documentWidth;
     *
     *    console.log('result:',result);
     *
     *    if(result > 81){
     *      $('.atTxt'+i).addClass('texts');
     *      $('.texts').addClass('ellipsis');
     *    }else{
     *      $('.atTxt'+i).removeClass('texts');
     *    }
     *  }
     */

    $.each($("[class^='atTxt']"), function (key, val) {
        var item = $(val);
        var contentArray = item.width();
        var documentWidth = $(document).width();
        var result = (contentArray * 100) / documentWidth;

        /*
         *item.removeClass('ellipsis');
         *if(result > 60){
         *  item.addClass('ellipsis');
         *}
         */
    });

}

var FormatHelpListItem = ' \
    <li> \
        <a href="">{0}</a> \
        <div>{1}</div> \
    </li>';

function createHelpListItem(json) {
    //console.log(json);
    var item = String.format(FormatHelpListItem, json.title, json.content);
    item = $(item);

    var aTag = item.find('a');

    aTag.click(function() {
        $(this).toggleClass("on");
        $(this).next("div").slideToggle(200);
        $(".customwrap .show_list>li>a").not(this).removeClass("on").next("div").slideUp(200);
        return false;
    });

    return item;
}

function makeS(sid, title, date, content, new_flag) {
    var s_sid;
    var s_title;
    var s_date;
    var s_content;
    var s_new_image = '';

    console.log("makeS", sid, title, date, content);

    if (sid === undefined) {
        s_sid = 0;
    } else {
        s_sid = sid;
    }

    console.log("checking s_sid, lastNID: " + s_sid + ' > ' + window.lastNID);
    if (Number(s_sid) > window.lastNID) {
        window.lastNID = Number(s_sid);
        console.log("checking lastNID: ok " + window.lastNID);
    }

    if (title === undefined) {
        s_title = '오픈 시니어 서비스';
    } else {
        s_title = title;
    }

    if (date === undefined) {
        s_date = '08월 06일';
    } else {
        var d = date.split('-');
        s_date = d[1] + "월 " + d[2] + "일"
    }

    if (content === undefined) {
        s_content =
            '                        스마트폰을 더 스마트하게!<br>' +
            '                        시니어 서비스를 소개합니다^^<br>' +
            '                        <br>' +
            '                        다들 스마트폰 어렵게 사용하시느라 많이 힘드셨었죠?<br>' +
            '                        이제 스마트폰을 더 스마트하고 편리하게<br>' +
            '                        사용할 수 있는 T 시니어 서비스가 나왔습니다!<br>';
    } else {
        s_content = content;
    }

    if (new_flag !== undefined) {
        if (new_flag === 1) {
            s_new_image = '<img src="../img/btNew.png" style="width:19px; height:19px;">';
        }
    }

    var li_help = '<li class="list_notice">';
    var s_date_line = '                        <p class="date">' + s_date + '</p>';
    if (window.type === 'notice') {
        li_help = '<li class="list_notice">';
        s_date_line = '                        <p class="date">' + s_date + '</p>';
    } else {
        li_help = '<li class="list_notice list_notice_noDate">';
        s_date_line = '';
    }

    var ss =
        '            <ul>' +
        '                    ' + li_help +
        '                    <a class="btn-list" count="' + s_sid + '">' +
        '                        <p class="atTxt">' + s_title + '<span class="badge">' + s_new_image + '</span></p><br />' +
        '                    ' + s_date_line +
        '                    </a>' +
        '                </li>' +
        '                <li class="list_notice_detail_txt" style="display:none;">' +
        '                    <div class="txtDetail">' +
        '                    ' + s_content +
        '                    </div>' +
        '                </li>' +
        '            </ul>';
    return ss;
}

function getNoticeResult(data) {
    if (1 == data.isEnd) {
        isEnd = true;
    }

    endNum = data.EndNum;

    /*
     *  $.tmpl("list-item", data.items ).each( function(i){
     *    $(this).find("h3 > button").click( showContent );
     *  }).appendTo("#list");
     *
     *  $("#btnShowMore").text("더 보기(" + $("#list li").length + "/" + data.TotCnt + ")");
     */
}

function getFromServer(url, option) {
    console.log("getFromServer");
    //console.log(url, option);
    $.ajax({
        type: "GET",
        url: url,
        data: option,
        dataType: "json",
        success: function (data) {
            //console.log("ok:" + JSON.stringify(data));
            getNoticeResult(data);
            $.each(data.items, function (key, val) {
                //console.log(JSON.stringify(val));
                //$('.contents').append(makeS(val.sid, val.title, val.date, val.content, val.isNew));
                $('.contents').append(createHelpListItem(val));
            });

            console.log("checking lastNID, prevLastNID: " + window.lastNID + ' > ' + window.prevLastNID);
            if (Number(window.lastNID) > window.prevLastNID) {
                window.prevLastNID = Number(window.lastNID);
                console.log("checking prevLastNID: ok " + window.prevLastNID);
                window.updateLastNID(String(window.prevLastNID));
            }

            addClickHandler();
            endCheckFlag = false;
        },
        error: function (e) {
            console.log("error : " + JSON.stringify(e));
            endCheckFlag = false;
            /*
             *$.each(mock_data, function(key, val) {
             *  console.log(JSON.stringify(val));
             *  $('.contents').append(makeS(val.title, val.date, val.content));
             *});
             *var i = 0;
             *for(i = 0; i < 5; i++) {
             *  $('.contents').append(makeS());
             *}
             */
        }
    });
}

function getFromNoticeList(option) {
    if (isAndroid()) {
        getFromServer("/mcs/list/noticeJson.do", option);
    } else if (isIOS()) {
        getFromServer("/mcs/list/noticeJson.do", option);
    } else {
        getFromServer("/mcs/list/noticeJson.do", option);
    }
}

function getFromHelpList(option) {
    if (isAndroid()) {
        getFromServer("/mcs/list/helpJson.do", option);
    } else if (isIOS()) {
        getFromServer("/mcs/list/helpJson.do", option);
    } else {
        getFromServer("/mcs/list/helpJson.do", option);
    }
}

function getQueryVariable(variable) {
    try {
        var query = window.location.search.substring(1);
        console.log('query(normal getQueryVariable): ' + query);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) === variable) {
                console.log(JSON.stringify(pair));
                console.log(decodeURIComponent(pair[0]));
                console.log(decodeURIComponent(pair[1]));
                return decodeURIComponent(pair[1]);
            }
        }
    } catch (e) {
        console.log('normal getQueryVariable: ' + e.message);
        return false;
    }
    console.log('normal Query variable %s not found', variable);
    return false;
}


// -- String format function, String.format("{0} ABCD", "English") -> "English ABCD"
if (!String.format) {
    String.format = function(format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}