<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<title>[오키토키 PRO]</title>
<meta charset="utf-8">
<!--<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>-->
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/reset.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/common.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/style.css" />

<%--
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/app.css" />
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/skt/hservice.css?v=0.8.9"/>
--%>

<style>
    form {
        border: 3px solid #f1f1f1;
    }

    input[type=text], input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
    }

    button:hover {
        opacity: 0.8;
    }

    .cancelbtn {
        width: auto;
        padding: 10px 18px;
        background-color: #f44336;
    }

    .imgcontainer {
        text-align: center;
        margin: 24px 0 12px 0;
    }

    img.avatar {
        width: 40%;
        border-radius: 50%;
    }

    .container {
        padding: 16px;
    }

    span.psw {
        float: right;
        padding-top: 16px;
    }

    /* Change styles for span and cancel button on extra small screens */
    @media screen and (max-width: 300px) {
        span.psw {
            display: block;
            float: none;
        }
        .cancelbtn {
            width: 100%;
        }
    }
</style>
<body>
<%--<div class="header" style='position:absolute;z-index:30 ;'>
    <h1 class="title_bar">[오키토키 PRO] 앱 다운로드</h1>
</div>--%>
<div class="error-msg-body">


<form form id="command" action="/acs/download_auth" method="POST">

    <div class="imgcontainer">
        <img src="/acs/img/h/logo_dark.png" <%--alt="Avatar" class="avatar"--%>>
    </div>

    <div class="container">
        <label><b>기업코드</b></label>
        <input type="text" placeholder="Enter 기업코드" name="name" onfocus="this.value=''" required>

        <button type="submit">확인</button>

       <%-- <input id="name" name="name" style=";text-align:center; width: 320px !important; height: 30px" value="인증코드 입력." class="form-control" type="text" onfocus="this.value=''" value=""/><BR/><BR/>

        <button type="submit" class="css3button" style="width: 40px !important; height: 30px" >확인</button>
--%>

    </div>

</form>
</div>
</body>
</html>
