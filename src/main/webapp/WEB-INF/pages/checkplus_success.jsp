<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.springframework.web.bind.annotation.RequestParam" %>
<%@ page language="java" contentType="text/html;charset=euc-kr" %>

<%	//인증 후 결과값이 null로 나오는 부분은 관리담당자에게 문의 바랍니다.
    NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();

    String sEncodeData = requestReplace(request.getParameter("EncodeData"), "encodeData");

    /*String sSiteCode = "BE525";				// NICE로부터 부여받은 사이트 코드
    String sSitePassword = "jQWlJZX1noBG";			// NICE로부터 부여받은 사이트 패스워드
    */
    String sSiteCode = request.getAttribute("nice_site_code").toString();			// NICE로부터 부여받은 사이트 코드
    String sSitePassword = request.getAttribute("nice_site_pwd").toString();		// NICE로부터 부여받은 사이트 패스워드

    String sCipherTime = "";			// 복호화한 시간
    String sRequestNumber = "ti2";			// 요청 번호
    String sResponseNumber = "";		// 인증 고유번호
    String sAuthType = "";				// 인증 수단
    String sName = "";					// 성명
    String sDupInfo = "";				// 중복가입 확인값 (DI_64 byte)
    String sConnInfo = "";				// 연계정보 확인값 (CI_88 byte)
    String sBirthDate = "";				// 생년월일(YYYYMMDD)
    String sGender = "";				// 성별
    String sNationalInfo = "";			// 내/외국인정보 (개발가이드 참조)
	String sMobileNo = "";				// 휴대폰번호
	String sMobileCo = "";				// 통신사
    String sMessage = "";
    String sPlainData = "";
    String sResultInfo = "";
    int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);
    String sRseult = "n";
    String sUrlInfo = "";
    Long iuid = Long.parseLong(request.getAttribute("iuid").toString());



    if( iReturn == 0 )
    {
        sPlainData = niceCheck.getPlainData();
        sCipherTime = niceCheck.getCipherDateTime();
        
        // 데이타를 추출합니다.
        java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);
        
        sRequestNumber  = (String)mapresult.get("REQ_SEQ");
        sResponseNumber = (String)mapresult.get("RES_SEQ");
        sAuthType		= (String)mapresult.get("AUTH_TYPE");
        //sName			= (String)mapresult.get("NAME");
		sName			= (String)mapresult.get("UTF8_NAME"); //charset utf8 사용시 주석 해제 후 사용
        sBirthDate		= (String)mapresult.get("BIRTHDATE");
        sGender			= (String)mapresult.get("GENDER");
        sNationalInfo  	= (String)mapresult.get("NATIONALINFO");
        sDupInfo		= (String)mapresult.get("DI");
        sConnInfo		= (String)mapresult.get("CI");
        sMobileNo		= (String)mapresult.get("MOBILE_NO");
        sMobileCo		= (String)mapresult.get("MOBILE_CO");


        String session_sRequestNumber = (String)session.getAttribute("REQ_SEQ");
        if(!sRequestNumber.equals(session_sRequestNumber))
        {
            sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
            sResponseNumber = "";
            sAuthType = "";
        }

        sRseult = "y";
        sResultInfo = "&nm=" + sName+"&gd="+ sGender +"&mn="+ sMobileNo +"&bt="+ sBirthDate;
    }
    else if( iReturn == -1)
    {
        sMessage = "복호화 시스템 에러입니다.";
    }    
    else if( iReturn == -4)
    {
        sMessage = "복호화 처리오류입니다.";
    }    
    else if( iReturn == -5)
    {
        sMessage = "복호화 해쉬 오류입니다.";
    }    
    else if( iReturn == -6)
    {
        sMessage = "복호화 데이터 오류입니다.";
    }    
    else if( iReturn == -9)
    {
        sMessage = "입력 데이터 오류입니다.";
    }    
    else if( iReturn == -12)
    {
        sMessage = "사이트 패스워드 오류입니다.";
    }    
    else
    {
        sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
    }

    if(StringUtils.equals(request.getAttribute("iuid").toString(), "0"))
        sUrlInfo = "auth://register?result="+sRseult + "&nm=" +sName + "&bt=" + sBirthDate + "&mn=" + sMobileNo + "&gd=" + sGender;
    else
        sUrlInfo = "auth://join?result="+sRseult + "&nm=" +sName + "&bt=" + sBirthDate + "&mn=" + sMobileNo + "&gd=" + sGender;

%>
<%!

	public String requestReplace (String paramValue, String gubun) {

        String result = "";
        
        if (paramValue != null) {
        	
        	paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

        	paramValue = paramValue.replaceAll("\\*", "");
        	paramValue = paramValue.replaceAll("\\?", "");
        	paramValue = paramValue.replaceAll("\\[", "");
        	paramValue = paramValue.replaceAll("\\{", "");
        	paramValue = paramValue.replaceAll("\\(", "");
        	paramValue = paramValue.replaceAll("\\)", "");
        	paramValue = paramValue.replaceAll("\\^", "");
        	paramValue = paramValue.replaceAll("\\$", "");
        	paramValue = paramValue.replaceAll("'", "");
        	paramValue = paramValue.replaceAll("@", "");
        	paramValue = paramValue.replaceAll("%", "");
        	paramValue = paramValue.replaceAll(";", "");
        	paramValue = paramValue.replaceAll(":", "");
        	paramValue = paramValue.replaceAll("-", "");
        	paramValue = paramValue.replaceAll("#", "");
        	paramValue = paramValue.replaceAll("--", "");
        	paramValue = paramValue.replaceAll("-", "");
        	paramValue = paramValue.replaceAll(",", "");
        	
        	if(gubun != "encodeData"){
        		paramValue = paramValue.replaceAll("\\+", "");
        		paramValue = paramValue.replaceAll("/", "");
            paramValue = paramValue.replaceAll("=", "");
        	}
        	
        	result = paramValue;
            
        }
        return result;
  }
%>

<html>
<head>
    <title>NICE평가정보</title>
</head>
<body>
    <center>
    <p><p><p><p>본인인증이 완료 되었습니다.<br>
        <%--<p><p><p><p><%=sUrlInfo%><br>
        <p><p><p><p><%=iuid%><br>--%>
   <%-- <table border=1>
      &lt;%&ndash;  <tr>
            <td>복호화한 시간</td>
            <td><%= sCipherTime %> (YYMMDDHHMMSS)</td>
        </tr>
        <tr>
            <td>요청 번호</td>
            <td><%= sRequestNumber %></td>
        </tr>            
        <tr>
            <td>NICE응답 번호</td>
            <td><%= sResponseNumber %></td>
        </tr>            
        <tr>
            <td>인증수단</td>
            <td><%= sAuthType %></td>
        </tr>
		<tr>
            <td>성명</td>
            <td><%= sName %></td>
        </tr>
		<tr>
            <td>중복가입 확인값(DI)</td>
            <td><%= sDupInfo %></td>
        </tr>
		<tr>
            <td>연계정보 확인값(CI)</td>
            <td><%= sConnInfo %></td>
        </tr>
		<tr>
            <td>생년월일(YYYYMMDD)</td>
            <td><%= sBirthDate %></td>
        </tr>
		<tr>
            <td>성별</td>
            <td><%= sGender %></td>
        </tr>
				<tr>
            <td>내/외국인정보</td>
            <td><%= sNationalInfo %></td>
        </tr>
        </tr>
			<td>휴대폰번호</td>
            <td><%= sMobileNo %></td>
        </tr>
		<tr>
			<td>통신사</td>
			<td><%= sMobileCo %></td>
        </tr>
		<tr>
			<td colspan="2">인증 후 결과값은 내부 설정에 따른 값만 리턴받으실 수 있습니다. <br>
			일부 결과값이 null로 리턴되는 경우 관리담당자 또는 계약부서(02-2122-4615)로 문의바랍니다.</td>
		</tr>&ndash;%&gt;
		</table>--%><br><br>
            <%= sMessage %><br>
            <%= iuid %><br>

    </center>
    <script>
        var iuid = <%=iuid%>;
        if (iuid == -1)
        {
            window.location = "auth://camp.make?result=<%=sRseult%>&nm=<%=sName%>&bt=<%=sBirthDate%>&mn=<%=sMobileNo%>&gd=<%=sGender%>";
        }
        else if(iuid == 0)
        {
            window.location = "auth://join?result=<%=sRseult%>&nm=<%=sName%>&bt=<%=sBirthDate%>&mn=<%=sMobileNo%>&gd=<%=sGender%>";
        }
        else if(iuid > 0)
        {
            window.location = "auth://register?result=<%=sRseult%>&nm=<%=sName%>&bt=<%=sBirthDate%>&mn=<%=sMobileNo%>&gd=<%=sGender%>";
        }

    </script>
</body>
</html>