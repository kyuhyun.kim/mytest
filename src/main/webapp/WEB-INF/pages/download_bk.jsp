<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <meta charset="utf-8">
    <!--<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>-->
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/reset.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/common.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/custom.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/style.css" />



    <title>[오키토키 PRO]</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/app.css" />
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/acs/css/skt/hservice.css?v=0.8.9"/>
    <style type="text/css">
        .classname {
            -moz-box-shadow:inset 0px 1px 0px 0px #fce2c1;
            -webkit-box-shadow:inset 0px 1px 0px 0px #fce2c1;
            box-shadow:inset 0px 1px 0px 0px #fce2c1;
            background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ffc477), color-stop(1, #fb9e25) );
            background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
            background-color:#ffc477;
            -webkit-border-top-left-radius:0px;
            -moz-border-radius-topleft:0px;
            border-top-left-radius:0px;
            -webkit-border-top-right-radius:0px;
            -moz-border-radius-topright:0px;
            border-top-right-radius:0px;
            -webkit-border-bottom-right-radius:0px;
            -moz-border-radius-bottomright:0px;
            border-bottom-right-radius:0px;
            -webkit-border-bottom-left-radius:0px;
            -moz-border-radius-bottomleft:0px;
            border-bottom-left-radius:0px;
            text-indent:0;
            border:1px solid #eeb44f;
            display:inline-block;
            color:#ffffff;
            font-family:Arial;
            font-size:15px;
            font-weight:bold;
            font-style:normal;
            height:40px;
            line-height:40px;
            width:100px;
            text-decoration:none;
            text-align:center;
            text-shadow:1px 1px 0px #cc9f52;
        }
        .classname:hover {
            background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fb9e25), color-stop(1, #ffc477) );
            background:-moz-linear-gradient( center top, #fb9e25 5%, #ffc477 100% );
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#fb9e25', endColorstr='#ffc477');
            background-color:#fb9e25;
        }.classname:active {
             position:relative;
             top:1px;
         }</style>



</head>
<body>
<div class="header" style='position:absolute;z-index:30 ;'>
    <h1 class="title_bar">[오키토키 PRO] 앱 다운로드</h1>
</div>
<div class="error-msg-body">
<%--<h2>[오키토키 PRO] 설치 인증</h2>--%>
    <div class="msg">
        <img src="${pageContext.request.contextPath}/acs/img/h/logo_dark.png"  style="width: 300px !important; height: 200px"/>
<form:form method="POST" action="${pageContext.request.contextPath}/acs/download_auth">


    </div>

        <tr>

            <div class="form-group has-success">
                <label class="control-label" for="inputSuccess1">인증 코드 입력</label>
                <input type="text" style="width: 50px !important; height: 30px" class="form-control" id="inputSuccess1">
            </div>

        <%--<p>인증 코드 입력.<BR></p>--%>
           <%-- <form:input path="name" value="인증코드 입력." style=";text-align:center; width: 250px !important; height: 50px " class="form-control" onfocus="this.value=''"  /><BR/><BR/>--%>
                <%--<button type="submit"> <img src="${pageContext.request.contextPath}/acs/img/h/verify_right.png" style="width: 50px !important; height: 30px" ></button>--%>
                <button type="submit" value="확인" class="css3button" style="width: 40px !important; height: 30px" >확인</button>






            <td>


            </td>


        </tr>

        <%--<tr>
            <td colspan="2">
                <input type="submit" value="확인"/>
            </td>
        </tr>--%>


</div>
</form:form>
</div>
</body>
</html>