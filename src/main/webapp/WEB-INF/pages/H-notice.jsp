<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="ko">
<head>
	<title>...</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/reset.css"/>
	<link rel="stylesheet" href="css/common.css"/>
	<link rel="stylesheet" href="css/custom.css"/>
	<meta name="format-detection" content="telephone=no"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>
<section class="customwrap" id="notice">
	<ul class="show_list contents">
	</ul>
</section>
<script type="text/javascript" src="js/app_rd.js"></script>
<script type="text/javascript" src="js/notice.js"></script>
</body>
</html>
