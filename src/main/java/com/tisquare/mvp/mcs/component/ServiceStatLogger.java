package com.tisquare.mvp.mcs.component;

import com.tisquare.mvp.mcs.model.ServerWatchStatLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 표준화 OMC - 가입 관리 통계 로그
 *

 */
@Component
public class ServiceStatLogger {
    private static final Logger statLogger = LoggerFactory.getLogger("STAT_LOGGER");

    public void writeLog(ServerWatchStatLog serverWatchStatLog) {
        statLogger.info(serverWatchStatLog.toString());
    }
}
