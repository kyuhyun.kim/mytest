package com.tisquare.mvp.mcs.urlshort;

import com.tisquare.mvp.mcs.db.UrlMapDao;
import com.tisquare.mvp.mcs.config.InitConfig;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by asus on 2015-03-01.
 */
@Service
public class UrlShortService {
    private static Logger logger = LoggerFactory.getLogger(UrlShortService.class);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    @Autowired
    UrlMapDao urlMapDao;

    @Autowired
    InitConfig initConfig;

    public String getExpireDate(String svcType){
        Calendar cal =Calendar.getInstance();
        int period = 1;

        if(initConfig.getUrlDefExpireVal() != null){
            period = initConfig.getUrlDefExpireVal();
        } else
            logger.info("InitConfig getUrlDefExpireVal is null. val[{}]", initConfig.getUrlDefExpireVal());

        /* 서비스 타입에 따라 분기가 필요함. */
        /* 현재는 1일 고정으로... */
        cal.add(Calendar.DAY_OF_YEAR, period);
//        cal.add(Calendar.MINUTE, 1);

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(cal.getTime());
    }

    public long strExpireDateToLong(String dateFormat) throws ParseException{
        return sdf.parse(dateFormat).getTime();
    }

    public String getCurrentDateFormat(){
        Calendar cal = Calendar.getInstance();
        return sdf.format(cal.getTime());
    }

    public synchronized String makeRandom10Cahr(){
        return RandomStringUtils.randomAlphabetic(10);
    }

    public synchronized String makeRandom9Cahr(){
        return RandomStringUtils.randomAlphabetic(9);
    }

//    @Scheduled(fixedDelay = 60000)
    private void deleteExpiredUrlMap(){
        String currentTime = getCurrentDateFormat();

        try {
            int delCount = urlMapDao.deleteExpiredUrlMap(currentTime);
            logger.debug("URL MAP DELETE COUNT[{}] CURRENT TIME[{}]", delCount, currentTime);
        } catch (Exception e){
            logger.debug("deleteExpiredUrlMap DB Error. : ", e);
            return;
        }
    }

}
