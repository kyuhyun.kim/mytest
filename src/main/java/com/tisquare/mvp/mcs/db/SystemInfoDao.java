package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.type.SUBS_TYPE;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tisquare.mvp.mcs.entity.EventPopup;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class SystemInfoDao {

    private static final Logger logger = LoggerFactory.getLogger(SystemInfoDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }


    public AppUpdatetInfo getAppUpdate(Integer appMarketType, int substype) {

        if(substype == SUBS_TYPE.COMMANDER.getCode())
            substype = SUBS_TYPE.B2B.getCode();

        if (getSqlSession() == null) {
            logger.error("SystemInfoDao is null");
            return null;
        }

        AppUpdatetInfo tmp = new AppUpdatetInfo(appMarketType, substype);

        return _selectAppUpdate(tmp);
    }

    public EventPopup getEventPopup() {
        EventPopup event;

        try {
            event = getReadSqlSession().selectOne(SystemInfoDao.class.getName() + ".selectEventPopupNoti");
        } catch (Throwable t) {
            logger.error("[getEventPopup] failed : " + t);
            return null;
        }

        return event;
    }


    public List<EventPopup> getEventPopupList() {
        List<EventPopup> event;

        try {
            event = getReadSqlSession().selectList(SystemInfoDao.class.getName() + ".selectEventPopupNotiList");
        } catch (Throwable t) {
            logger.error("[getEventPopup] failed : " + t);
            return null;
        }

        return event;
    }

    private AppUpdatetInfo _selectAppUpdate(AppUpdatetInfo tmp) {
        AppUpdatetInfo appUpdate;

        logger.debug("[getAppUpdate] query condition : " + tmp);

        Map<String, Object> map = new HashMap<>();

        map.put("substype", tmp.getSubstype());
        map.put("storeType", tmp.getStoreType());
        map.put("osType", tmp.getOsType());

        try {

            appUpdate = getReadSqlSession().selectOne(SystemInfoDao.class.getName() + ".selectAppUpdate", tmp);
        } catch (Throwable t) {
            logger.error("[getAppUpdate] failed : " + t);
            return null;
        }

        logger.debug("[getAppUpdate] found : " + appUpdate);

        return appUpdate;
    }

    public int isSystemUnavailable() {
        EventPopup event;

        try {
            event = getReadSqlSession().selectOne(SystemInfoDao.class.getName() +".selectEventPopupWhenSystemUnavailable");
            if(event == null)
                return 0;
            if(event.getForceStop() == EventPopup.FORCE_UPDATE)
                return 1;
            else
                return 0;

        } catch (Throwable t) {
            logger.error("[isSystemUnavailable] failed : " + t);
            return 0;
        }
    }

    public int isSystemUnavailableList() {
        EventPopup event;

        try {
            event = getReadSqlSession().selectOne(SystemInfoDao.class.getName() +".selectEventPopupListSystemUnavailable");
            if(event == null)
                return 0;
            if(event.getForceStop() == EventPopup.FORCE_UPDATE)
                return 1;
            else
                return 0;

        } catch (Throwable t) {
            logger.error("[isSystemUnavailableList] failed : " + t);
            return 0;
        }
    }


}
