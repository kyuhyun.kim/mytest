package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Created by kimkyuhyun on 2017-12-06.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class EventNotice {

    @JsonProperty("epid")
    private Long epid;

    @JsonProperty("event-stime")
    private String eventStime;

    @JsonProperty("event-etime")
    private String eventEtime;

    @JsonProperty("img")
    private String img;

    @JsonProperty("title")
    private String title;

    @JsonProperty("text")
    private String text;

    @JsonProperty("promotion-type")
    private String promotion_type;

    @JsonProperty("priority")
    private Integer priority;

    @JsonProperty("landing-type")
    private String landing_type;

    @JsonProperty("landing-menu")
    private String landing_menu;

    @JsonProperty("click-url")
    private String click_url;

    @JsonProperty("landing-img")
    private String landing_img;

    @JsonProperty("landing-url")
    private String landing_url;

    @JsonIgnore
    private Long iuid;

    @JsonIgnore
    private String regDate;

    @JsonIgnore
    private String uptDate;


    public void setLanding_img(String landing_img) {
        this.landing_img = landing_img;
    }

    public void setLanding_url(String landing_url) {
        this.landing_url = landing_url;
    }

    public String getLanding_img() {
        return landing_img;
    }

    public String getLanding_url() {
        return landing_url;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public void setEpid(Long epid) {
        this.epid = epid;
    }

    public void setEventStime(String eventStime) {
        this.eventStime = eventStime;
    }

    public void setEventEtime(String eventEtime) {
        this.eventEtime = eventEtime;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPromotion_type(String promotion_type) {
        this.promotion_type = promotion_type;
    }

    public void setLanding_type(String landing_type) {
        this.landing_type = landing_type;
    }

    public void setLanding_menu(String landing_menu) {
        this.landing_menu = landing_menu;
    }

    public void setClick_url(String click_url) {
        this.click_url = click_url;
    }


    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public void setUptDate(String uptDate) {
        this.uptDate = uptDate;
    }

    public Long getEpid() {
        return epid;
    }

    public String getEventStime() {
        return eventStime;
    }

    public String getEventEtime() {
        return eventEtime;
    }

    public String getImg() {
        return img;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getPromotion_type() {
        return promotion_type;
    }

    public String getLanding_type() {
        return landing_type;
    }

    public String getLanding_menu() {
        return landing_menu;
    }

    public String getClick_url() {
        return click_url;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getRegDate() {
        return regDate;
    }

    public String getUptDate() {
        return uptDate;
    }
}
