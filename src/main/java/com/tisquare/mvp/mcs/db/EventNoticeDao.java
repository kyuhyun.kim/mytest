package com.tisquare.mvp.mcs.db;

import com.tisquare.mvp.mcs.db.domain.EventNotice;
import com.tisquare.mvp.mcs.model.AuthInfoBody;
import com.tisquare.mvp.mcs.model.ProfileDetail;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class EventNoticeDao {

    private static final Logger logger = LoggerFactory.getLogger(EventNoticeDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }


    public List<EventNotice> selectEventNotice() {


        try {
            List<EventNotice> eventNotices = getReadSqlSession().selectList(EventNoticeDao.class.getName() + ".selectEventNotice");

            if(eventNotices == null || eventNotices.size() == 0)
                return null;
            else
                return eventNotices;

        } catch (Exception e) {
            logger.error("[selectEventNotice] failed : " + e.getMessage());
            return null;
        }

    }



    public List<EventNotice> selectWebEventNoticeType(AuthInfoBody authInfoBody) {


        try {
            List<EventNotice> eventNotices = getReadSqlSession().selectList(EventNoticeDao.class.getName() + ".selectWebEventNoticeType", authInfoBody.getProfileDetail().getType());

            if(eventNotices == null || eventNotices.size() == 0)
                return null;
            else
                return eventNotices;

        } catch (Exception e) {
            logger.error("[selectEventNotice] failed : " + e.getMessage());
            return null;
        }

    }

    public List<EventNotice> selectEventNotice(AuthInfoBody authInfoBody) {


        try {
            List<EventNotice> eventNotices = getReadSqlSession().selectList(EventNoticeDao.class.getName() + ".selectEventNoticeType", authInfoBody.getProfileDetail().getType());

            if(eventNotices == null || eventNotices.size() == 0)
                return null;
            else
                return eventNotices;

        } catch (Exception e) {
            logger.error("[selectEventNotice] failed : " + e.getMessage());
            return null;
        }

    }

    public List<EventNotice> selectEventNotice(ProfileDetail profileDetail) {


        try {
            List<EventNotice> eventNotices = getReadSqlSession().selectList(EventNoticeDao.class.getName() + ".selectEventNoticeType", profileDetail.getType());

            if(eventNotices == null || eventNotices.size() == 0)
                return null;
            else
                return eventNotices;

        } catch (Exception e) {
            logger.error("[selectEventNotice] failed : " + e.getMessage());
            return null;
        }

    }

    public List<EventNotice> selectWebEventNotice(ProfileDetail profileDetail) {


        try {
            List<EventNotice> eventNotices = getReadSqlSession().selectList(EventNoticeDao.class.getName() + ".selectWebEventNoticeType", profileDetail.getType());

            if(eventNotices == null || eventNotices.size() == 0)
                return null;
            else
                return eventNotices;

        } catch (Exception e) {
            logger.error("[selectEventNotice] failed : " + e.getMessage());
            return null;
        }

    }

}
