package com.tisquare.mvp.mcs.db.domain;

/**
 * Created by jhkim on 14. 3. 20.
 */
public class SvcFeature {
    int fid;
    String fname;

    public int getFid() { return fid; }
    public void setFid(int fid) { this.fid = fid; }
    public String getFname() { return fname; }
    public void setFname(String fname) { this.fname = fname; }

    public String toString(){
        return String.format("FID=%d,FNAME=%s", fid, fname);
    }
}
