package com.tisquare.mvp.mcs.db.domain;

import com.tisquare.mvp.mcs.model.Statistic;

/**
 * Created by jhkim on 14. 3. 20.
 */
public class
        SessionInfo {


    private String reg_dt;
    private String reg_tm;

    private int svc_type;
    private int share_type;
    private int session_cnt;
    private int svc_req_cnt;
    private int svc_accept_cnt;


    public SessionInfo(){}

    public void setReg_dt(String reg_dt) {
        this.reg_dt = reg_dt;
    }

    public void setReg_tm(String reg_tm) {
        this.reg_tm = reg_tm;
    }

    public void setSvc_type(int svc_type) {
        this.svc_type = svc_type;
    }

    public void setShare_type(int share_type) {
        this.share_type = share_type;
    }

    public void setSession_cnt(int session_cnt) {
        this.session_cnt = session_cnt;
    }

    public void setSvc_req_cnt(int svc_req_cnt) {
        this.svc_req_cnt = svc_req_cnt;
    }

    public void setSvc_accep_cnt(int svc_accept_cnt) {
        this.svc_accept_cnt = svc_accept_cnt;
    }

    public String getReg_dt() {

        return reg_dt;
    }

    public String getReg_tm() {
        return reg_tm;
    }

    public int getSvc_type() {
        return svc_type;
    }

    public int getShare_type() {
        return share_type;
    }

    public int getSession_cnt() {
        return session_cnt;
    }

    public int getSvc_req_cnt() {
        return svc_req_cnt;
    }

    public int getSvc_accept_cnt() {
        return svc_accept_cnt;
    }

    public SessionInfo(Statistic stat){





        if( stat.getservice_type().equals("picture")) {
            setSvc_type(1);
        }
        else if( stat.getservice_type().equals("location")) {
            setSvc_type(2);
        }
        else if( stat.getservice_type().equals("web")) {
            setSvc_type(3);
        }
        else if( stat.getservice_type().equals("document")) {
            setSvc_type(4);
        }
        else
            setSvc_type(0);


        if( stat.getShare_type().equals("share")) {
            setShare_type(1);
            if( stat.getStart_time() > 0 )
                setSvc_accep_cnt(1);
            else
                setSvc_accep_cnt(0);

        }
        else if( stat.getShare_type().equals("transmit")) {
            setShare_type(2);
            setSvc_accep_cnt(1);
        }
        else
            setShare_type(0);

        setSession_cnt(1);

        if( stat.getRequest_time() > 0)
            setSvc_req_cnt(1);
        else
            setSvc_req_cnt(0);


    }
}






