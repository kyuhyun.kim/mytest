package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.model.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class BssEmpDao {

    private static final Logger logger = LoggerFactory.getLogger(BssEmpDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }


    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }



    public BssEmp selectEmp(UserInfo userInfo) {
        return getReadSqlSession().selectOne(BssEmpDao.class.getName() + ".selectBssEmp", userInfo);

    }

    public BssEmp selectBssCommander(UserInfo userInfo) {
        return getReadSqlSession().selectOne(BssEmpDao.class.getName() + ".selectBssCommander", userInfo);

    }

    public Long selectBssCommanderPhone(Bssinfo bssinfo) {
        return getReadSqlSession().selectOne(BssEmpDao.class.getName() + ".selectBssCommanderPhone", bssinfo);

    }

    public BssEmp selectBssEmpCpCode(NoticeReqHeaders noticeReqHeaders)
    {
        return getReadSqlSession().selectOne(BssEmpDao.class.getName() + ".selectBssEmpCpCode", noticeReqHeaders);
    }

    public void updateLoginDateEmp(UserInfo bssinfo) {

        if(bssinfo.getBssEmp().getFirst_login() == null)
            getSqlSession().update(BssEmpDao.class.getName() + ".updateFirstLoginDateEmp", bssinfo.getProfile().getIuid());
        else
            getSqlSession().update(BssEmpDao.class.getName() + ".updateLoginDateEmp", bssinfo.getProfile().getIuid());
    }

    public void updateLoginDateCommander(UserInfo bssinfo) {

        if(bssinfo.getBssEmp().getFirst_login() == null)
            getSqlSession().update(BssEmpDao.class.getName() + ".updateFirstLoginDateCommander", bssinfo.getProfile().getIuid());
        else
            getSqlSession().update(BssEmpDao.class.getName() + ".updateLoginDateCommander", bssinfo.getProfile().getIuid());
    }

    public void updateBssEmp(Bssinfo bssinfo, Long iuid)
    {
        bssinfo.setIuid(iuid);
        getSqlSession().update(BssEmpDao.class.getName() + ".updateChgPwd", bssinfo);
    }

    public void updateDeviceChgF(long iuid)
    {
        getSqlSession().update(BssEmpDao.class.getName() + ".updateDeviceChgF", iuid);
    }


}
