package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.model.VoiceHist;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class RoomNoInfo {

    private String cp_code;

    private Long room_no;

    private Long sid;

    private String occupied;

    private String chgtime;

    public void setOccupied(String occupied) {
        this.occupied = occupied;
    }

    public void setChgtime(String chgtime) {
        this.chgtime = chgtime;
    }

    public String getOccupied() {
        return occupied;
    }

    public String getChgtime() {
        return chgtime;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getSid() {
        return sid;
    }

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public void setRoom_no(Long room_no) {
        this.room_no = room_no;
    }

    public String getCp_code() {
        return cp_code;
    }

    public Long getRoom_no() {
        return room_no;
    }
}