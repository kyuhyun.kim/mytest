package com.tisquare.mvp.mcs.db;

import com.tisquare.mvp.mcs.db.domain.HelpNoticeCnt;
import com.tisquare.mvp.mcs.model.HelpBody;
import com.tisquare.mvp.mcs.model.HelpJh;
import com.tisquare.mvp.mcs.model.NoticeBody;
import com.tisquare.mvp.mcs.model.NoticeJh;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jhkim on 14. 3. 5.
 */
@Repository
public class BssNoticeDao extends DaoSupport{

    private SqlSession sqlSession;

    private boolean externalSqlSession;

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {
    }

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession() {
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }



    public List<NoticeJh> selectNoticeMoreList(java.util.Map <String, Object> param){
        return getReadSqlSession().selectList(BssNoticeDao.class.getName() + ".moreList", param);
    }

    public List<NoticeJh> selectNoticeMoreList_SKT(java.util.Map <String, Object> param){
        return getReadSqlSession().selectList(BssNoticeDao.class.getName() + ".moreListSKT", param);
    }

    public List<NoticeJh> selectNoticeMoreList_KT(java.util.Map <String, Object> param){
        return getReadSqlSession().selectList(BssNoticeDao.class.getName() + ".moreListKT", param);
    }

    public List<NoticeJh> selectNoticeMoreList_LGT(java.util.Map <String, Object> param){
        return getReadSqlSession().selectList(BssNoticeDao.class.getName() + ".moreListLGT", param);
    }

    public HelpNoticeCnt selectNoticeTotCnt(java.util.Map <String, Object> param){
        return getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getCount", param);
    }


    public List<HelpJh> selectHelpeMoreList(java.util.Map <String, Object> param){
        return getReadSqlSession().selectList(BssNoticeDao.class.getName() + ".HelpMore", param);
    }

    public HelpNoticeCnt selectHelpTotCnt(){
        return getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getHelpCount");
    }

    public int selectHelpNewCnt(Long hid){
        return  getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getNewHelpCount", hid);
    }

    public Long selectHelpLastHid(){
        String sLastHid =  getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getNewHelpLastHid");
        if(sLastHid == null)
            return 0L;
        else
            return Long.parseLong( sLastHid );
    }

    public List<NoticeJh> selectNoticeList(java.util.Map <String, Object> param){
        return getReadSqlSession().selectList(BssNoticeDao.class.getName() + ".getNoticeList", param);
    }


    public int selectNoticeRemainCnt(java.util.Map <String, Object> param){
        return (Integer)getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getRemainCount", param);
    }


    public NoticeBody selectNotice(int sid){
        return getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getNotice", sid);
    }


    public int selectHelpRemainCnt(int hid){
        return (Integer)getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getRemainCountHelp", hid);
    }

    public List<HelpJh> selectHelpList(java.util.Map <String, Object> param){
        return getReadSqlSession().selectList(BssNoticeDao.class.getName() + ".getHelpList", param);
    }

    public HelpBody selectHelpHid(int hid){
        return getReadSqlSession().selectOne(BssNoticeDao.class.getName() + ".getHelpHid", hid);
    }


}
