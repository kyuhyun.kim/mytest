package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

/**
 * Created by kkh on 16. 03. 02.
 */
public class CosCmmntyPenalty {


    @JsonProperty("penalty-type")
    private Integer penalty_type;

    @JsonProperty("status")
    private Integer status;

    @JsonProperty("cause")
    private String cause;

    @JsonProperty("start-date")
    private String start_date;


    @JsonProperty("end-date")
    private String end_date;

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getCause() {
        return cause;
    }

    public void setPenalty_type(Integer penalty_type) {
        this.penalty_type = penalty_type;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public Integer getPenalty_type() {
        return penalty_type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }
}
