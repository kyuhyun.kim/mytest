package com.tisquare.mvp.mcs.db;

import com.tisquare.mvp.mcs.db.domain.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jhkim on 14. 3. 6.
 */
@Repository
public class StatDao extends DaoSupport{
    private SqlSession statSqlSession;

    private boolean externalStatSqlSession;

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {

    }

    @Autowired
    @Qualifier("factory1")
    public void setStatSqlSessionFactory(SqlSessionFactory statSqlSessionFactory){

        if(!this.externalStatSqlSession){
            this.statSqlSession = new SqlSessionTemplate(statSqlSessionFactory);
        }
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

/*    public int insertStat(StatLog stat){
        return getSqlSession().insert(StatDao.class.getName() + ".insert", stat);
    }
*/
//    public List<?> selectSvcFeature(){
//        return getSqlSession().selectList(StatDao.class.getName() + ".selectFeatureCode");
//    }

    public  int selectMaxSessionCnt(SessionInfo svc ){
        return getReadSqlSession().selectOne( StatDao.class.getName() + ".selectMaxSessionCnt", svc);
    }

    public int updateTotCnt(ServiceTotal svc){
        return getSqlSession().update(StatDao.class.getName() + ".updateTotCnt", svc);
    }

    public int updateToSessCnt(SessionInfo sessionInfo) {
        return getSqlSession().update(StatDao.class.getName() + ".updateSessonInfo", sessionInfo);
    }

//    public int updateMusicalCnt(ServiceMusical svc){
//        return getSqlSession().update(StatDao.class.getName() + ".updateMusicalCnt", svc);
//    }

//    public int updateSmallCall(ServiceSmallCall svc){
//        return getSqlSession().insert(StatDao.class.getName() + ".updateSmallCall", svc);
//    }

//    public  int updateServiceThankQ(ServiceThank svc){
//        return getSqlSession().update(StatDao.class.getName() + ".updateServiceThankQ", svc);
//    }

//    public  int updateServiceContact(ShareContact svc){
//        return getSqlSession().update(StatDao.class.getName() + ".updateServiceContact", svc);
//    }


    public  int updateServiceStat(StatSvcInfo svc){
        return getSqlSession().update(StatDao.class.getName() + ".updateServiceStat", svc);
    }

    public  int updateServiceStat_day(StatSvcInfo svc){
        return getSqlSession().update(StatDao.class.getName() + ".updateServiceStat_day", svc);
    }
    public  int updateServiceStat_month(StatSvcInfo svc){
        return getSqlSession().update(StatDao.class.getName() + ".updateServiceStat_month", svc);
    }

    public  int updateServiceStatSession(SessionInfo svc){
        return getSqlSession().update(StatDao.class.getName() + ".updateServiceStatSession", svc);
    }

    public  int updateServiceStatSession_ext(SessionInfo svc){
        return getSqlSession().update(StatDao.class.getName() + ".updateServiceStatSession_ext", svc);
    }



    protected final SqlSession getSqlSession() {
        return this.statSqlSession;
    }
}
