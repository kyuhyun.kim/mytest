package com.tisquare.mvp.mcs.db.domain;

/**
 * Created by 상기 on 2015-04-06.
 */
public class AuthTocken {
    private String mdn;
    private String phone_no;    // 단말에서 주는 가공하지 않은 번호
    private String iuid;
    private String cauth_key;
    private String app_version;

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getIuid() {
        return iuid;
    }

    public void setIuid(String iuld) {
        this.iuid = iuld;
    }

    public String getCauth_key() {
        return cauth_key;
    }

    public void setCauth_key(String cauth_key) {
        this.cauth_key = cauth_key;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    @Override
    public String toString() {
        return "AuthTocken{" +
                "mdn='" + mdn + '\'' +
                ", phone_no='" + phone_no + '\'' +
                ", iuld='" + iuid + '\'' +
                ", cauth_key='" + cauth_key + '\'' +
                ", app_version='" + app_version + '\'' +
                '}';
    }
}
