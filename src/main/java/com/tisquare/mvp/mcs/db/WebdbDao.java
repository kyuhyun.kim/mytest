package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.entity.Client;
import com.tisquare.mvp.mcs.entity.IndividualClient;
import com.tisquare.mvp.mcs.entity.SubscriberType;
import com.tisquare.mvp.mcs.model.AuthBody;
import com.tisquare.mvp.mcs.model.AuthInfoBody;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.UserInfo;
import com.tisquare.mvp.mcs.type.ClientType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class WebdbDao {

    private static final Logger logger = LoggerFactory.getLogger(WebdbDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public Integer newNoticeCnt(Long nid) {
        return getReadSqlSession().selectOne(WebdbDao.class.getName() + ".selectNewNoticeCnt", nid);
    }

    public Integer newBssNoticeCnt(UserInfo userInfo, ClientAuthReqParams clientAuthReqParams) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sid", clientAuthReqParams.getSid());
        return getReadSqlSession().selectOne(WebdbDao.class.getName() + ".selectBssNoticeCnt", map);
    }

//    public Integer newNoticeCntSKT(Long nid) {
//        return getSqlSession().selectOne(WebdbDao.class.getName() + ".selectNewNoticeCntSKT", nid);
//    }
//
//    public Integer newNoticeCntKT(Long nid) {
//        return getSqlSession().selectOne(WebdbDao.class.getName() + ".selectNewNoticeCntKT", nid);
//    }
//
//    public Integer newNoticeCntLGT(Long nid) {
//        return getSqlSession().selectOne(WebdbDao.class.getName() + ".selectNewNoticeCntLGT", nid);
//    }

    public String getLastNoticeTime() {
        return getReadSqlSession().selectOne(WebdbDao.class.getName() + ".selectLastNoticeTime");
    }

    public String getLastBssNoticeTime(UserInfo userInfo) {
        return getReadSqlSession().selectOne(WebdbDao.class.getName() + ".selectLastBssNoticeTime", userInfo.getBssEmp().getCp_code());
    }

//    public String getLastNoticeTimeSKT() {
//        return getSqlSession().selectOne(WebdbDao.class.getName() + ".selectLastNoticeTimeSKT");
//    }
//
//    public String getLastNoticeTimeKT() {
//        return getSqlSession().selectOne(WebdbDao.class.getName() + ".selectLastNoticeTimeKT");
//    }
//
//    public String getLastNoticeTimeLGT() {
//        return getSqlSession().selectOne(WebdbDao.class.getName() + ".selectLastNoticeTimeLGT");
//    }

    public Long getLastNid() {
        return getReadSqlSession().selectOne(WebdbDao.class.getName() +".selectLastNid");
    }

    public Long getLastBssNid(UserInfo userInfo) {
        return getReadSqlSession().selectOne(WebdbDao.class.getName() +".selectLastBssNid", userInfo.getBssEmp().getCp_code());
    }
//    public Long getLastNidSKT() {
//        return getSqlSession().selectOne(WebdbDao.class.getName() +".selectLastNidSKT");
//    }
//    public Long getLastNidKT() {
//        return getSqlSession().selectOne(WebdbDao.class.getName() +".selectLastNidKT");
//    }
//    public Long getLastNidLGT() {
//        return getSqlSession().selectOne(WebdbDao.class.getName() +".selectLastNidLGT");
//    }
}
