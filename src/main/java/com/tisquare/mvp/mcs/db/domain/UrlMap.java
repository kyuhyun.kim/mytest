package com.tisquare.mvp.mcs.db.domain;

/**
 * Created by asus on 2015-03-02.
 */
public class UrlMap {

    String shortUrl;

    String targetUrl;

    Long sid;

    String svcType;

    String expired_Date;

    public String getShortUrl() { return shortUrl; }
    public void setShortUrl(String shortUrl) { this.shortUrl = shortUrl; }

    public String getTargetUrl() { return targetUrl; }
    public void setTargetUrl(String targetUrl) { this.targetUrl = targetUrl; }

    public String getSvcType() { return svcType; }
    public void setSvcType(String svcType) { this.svcType = svcType; }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getExpired_Date() { return expired_Date; }
    public void setExpired_Date(String expired_Date) { this.expired_Date = expired_Date; }

    @Override
    public String toString() {
        return "UrlMap{" +
                "shortUrl='" + shortUrl + '\'' +
                ", targetUrl='" + targetUrl + '\'' +
                ", sid=" + sid +
                ", svcType='" + svcType + '\'' +
                ", expired_Date='" + expired_Date + '\'' +
                '}';
    }
}
