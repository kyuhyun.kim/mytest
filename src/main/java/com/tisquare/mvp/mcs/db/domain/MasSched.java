package com.tisquare.mvp.mcs.db.domain;

/**
 * Created by kkh on 16. 03. 02.
 */
public class MasSched {

    private long iuid;
    private Integer SCHEDULE_TYPE;
    private String SCHEDULE_DATE;
    private Integer ADVANCE_TYPE;
    private Integer OPEN_TYPE;

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public void setSCHEDULE_TYPE(Integer SCHEDULE_TYPE) {
        this.SCHEDULE_TYPE = SCHEDULE_TYPE;
    }

    public void setSCHEDULE_DATE(String SCHEDULE_DATE) {
        this.SCHEDULE_DATE = SCHEDULE_DATE;
    }

    public void setADVANCE_TYPE(Integer ADVANCE_TYPE) {
        this.ADVANCE_TYPE = ADVANCE_TYPE;
    }

    public void setOPEN_TYPE(Integer OPEN_TYPE) {
        this.OPEN_TYPE = OPEN_TYPE;
    }

    public long getIuid() {
        return iuid;
    }

    public Integer getSCHEDULE_TYPE() {
        return SCHEDULE_TYPE;
    }

    public String getSCHEDULE_DATE() {
        return SCHEDULE_DATE;
    }

    public Integer getADVANCE_TYPE() {
        return ADVANCE_TYPE;
    }

    public Integer getOPEN_TYPE() {
        return OPEN_TYPE;
    }
}
