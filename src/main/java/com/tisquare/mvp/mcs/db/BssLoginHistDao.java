package com.tisquare.mvp.mcs.db;

import com.tisquare.mvp.mcs.db.domain.BssLoginHist;
import com.tisquare.mvp.mcs.db.domain.LoginUserinfo;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Repository;

/**
 * Created by jhkim on 14. 3. 5.
 */
@Repository
public class BssLoginHistDao extends DaoSupport{

    private SqlSession sqlSession;

    private boolean externalSqlSession;

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {
    }

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    public void insertLoginUser(BssLoginHist bssLoginHist){
        getSqlSession().insert(BssLoginHistDao.class.getName() + ".insertBssLoginHist", bssLoginHist);
    }



    protected final SqlSession getSqlSession() {
        return this.sqlSession;
    }
}
