package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SnsStat {


    private String reg_dt;

    private String reg_tm;

    private long sid;

    private int sns;

    private int cnt;

    public void setReg_dt(String reg_dt) {
        this.reg_dt = reg_dt;
    }

    public void setReg_tm(String reg_tm) {
        this.reg_tm = reg_tm;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public void setSns(int sns) {
        this.sns = sns;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public String getReg_dt() {
        return reg_dt;
    }

    public String getReg_tm() {
        return reg_tm;
    }

    public long getSid() {
        return sid;
    }

    public int getSns() {
        return sns;
    }

    public int getCnt() {
        return cnt;
    }
}
