package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.db.domain.AuthTocken;
import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.entity.Client;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.entity.IndividualClient;
import com.tisquare.mvp.mcs.entity.SubscriberType;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.tisquare.mvp.mcs.type.ClientType;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class ClientDao {

    private static final Logger logger = LoggerFactory.getLogger(ClientDao.class);

    private SqlSession statSqlSession;
    private boolean externalStatSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory statSqlSessionFactory){
        if(!this.externalStatSqlSession){
            this.statSqlSession = new SqlSessionTemplate(statSqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.statSqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }


    public UserInfo selectUserInfo(String phoneNo) throws IOException {
        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectClient", phoneNo);

//        logger.error("============================================================================");
//        logger.error(client.toString());
//        logger.error("============================================================================");
        if (client == null)
            return null;

        return getUserInfoFromDao(client.getIuid(), client);
    }

    public UserInfo selectUserInfo(Bssinfo bssinfo) throws IOException {
        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectBssClient", bssinfo);

//        logger.error("============================================================================");
//        logger.error(client.toString());
//        logger.error("============================================================================");
        if (client == null)
            return null;

        return getUserInfoFromDao(client.getIuid(), client);
    }

    public String selectIndividualClientIuid(Presence presence) throws IOException
    {
        return getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectIndividualClientIuid", presence);
    }

    public int selectProd(String prod_id) throws IOException {
        if( prod_id == null)
            return 0;
        return getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectProd", prod_id);
    }

    public UserInfo selectUserInfo(Long iuid) throws IOException {
        Client client = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectClientByIuid", iuid);

        if (client == null)
            return null;

        return getUserInfoFromDao(iuid, client);
    }

    public void updateUserCert(java.util.Map <String, Object> param) throws IOException {
        getSqlSession().selectOne(ClientDao.class.getName() + ".updateClientCertDt", param);
    }


    public void updateLoginStatus(UserInfo userInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws IOException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("iuid", String.valueOf(userInfo.getIuid()));
        map.put("phone_no", authReqHeaders.getPhoneNo());
        getSqlSession().update(ClientDao.class.getName() + ".updateLoginStatus", map);
    }



    public void updateUserInfo(java.util.Map <String, Object> param) throws IOException {
        getSqlSession().selectOne(ClientDao.class.getName() + ".updateIndividualInfo", param);
    }


    public void updateBssUserInfo(UserInfo userInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("iuid", String.valueOf(userInfo.getIuid()));
        map.put("phone_no", authReqHeaders.getPhoneNo());
        map.put("app_version", authReqHeaders.getAppVersion());
        map.put("os_type", authReqParams.getOsType());
        map.put("os_version", authReqParams.getOsVersion());
        map.put("device_id", authReqParams.getDeviceId());
        map.put("store_type", authReqHeaders.getStoreType());
        map.put("telecom_str", authReqParams.getTelecomStr());
        map.put("telecom_code", authReqParams.getTelecomCode());
        map.put("device_model", authReqParams.getDeviceModel());
        map.put("device_chg_f", "0");
        getSqlSession().update(ClientDao.class.getName() + ".updateBssIndividual", map);

    }

    public void updatePcIndividualClient(UserInfo userInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("iuid", String.valueOf(userInfo.getIuid()));

        getSqlSession().update(ClientDao.class.getName() + ".updatePcIndividualClient", map);

    }

    public void updatePasswd(UsInfo param) throws IOException {

        getSqlSession().update(ClientDao.class.getName() + ".updatePasswd", param);
    }


    public void updateBssUserInfo(java.util.Map <String, Object> param) throws IOException {

        getSqlSession().selectOne(ClientDao.class.getName() + ".updateBssIndividual", param);
        getSqlSession().selectOne(ClientDao.class.getName() + ".updateClientInfo", param);
    }

    public void updateIndividualClientLogout(long iuid) throws IOException {
        getSqlSession().selectOne(ClientDao.class.getName() + ".updateIndividualClientLogout", iuid);
    }

    public void updateIndividualTwtMode(java.util.Map <String, Object> param) throws IOException {
        getSqlSession().selectOne(ClientDao.class.getName() + ".updateIndividualTwtMode", param);
    }

    public UserInfo selectUserCert(ClientAuthReqHeaders authReqHeaders) throws IOException {

        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectAuthClient", authReqHeaders);

        if (client == null)
            return null;

        return getUserCertFromDao(client.getIuid(), client, authReqHeaders);
    }

    public UserInfo selectUserCertIUID(ClientAuthReqHeaders authReqHeaders) throws IOException {

        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectAuthClientIUID", authReqHeaders);

        if (client == null)
            return null;

        return getUserCertFromDao(client.getIuid(), client, authReqHeaders);
    }


    public UserInfo selectIUID(Long iuid) throws IOException {

        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectAuthIUID", iuid);

        if (client == null)
            return null;

        return getUserIuidCertFromDao(client.getIuid(), client);
    }

    public UserInfo selectPhoneNo(ClientAuthReqHeaders authReqHeaders) throws IOException {

        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selecClientPhoneNo", authReqHeaders);

        if (client == null)
            return null;

        return getUserCertFromDao(client.getIuid(), client, authReqHeaders);
    }


    public UserInfo selectIUID(ClientAuthReqHeaders authReqHeaders) throws IOException {

        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectAuthIUID", authReqHeaders);

        if (client == null)
            return null;

        return getUserCertFromDao(client.getIuid(), client, authReqHeaders);
    }



    public UserInfo selectUserID(ClientAuthReqHeaders authReqHeaders, UsInfo usInfo) throws IOException {

        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectUserID", usInfo);

        if (client == null)
            return null;

        return getUserCertFromDao(client.getIuid(), client, authReqHeaders);
    }

/*    public Integer selectAuthIndividualClient(Map<String, Object> map) throws IOException {
        Integer is_exist = getSqlSession().selectOne( ClientDao.class.getName()+ ".selectAuthIndividualClient", map);
        return is_exist;
    }*/

    public UserInfo selectUserPwdID(ClientAuthReqHeaders authReqHeaders, UsInfo usInfo) throws IOException {

        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectUserID", usInfo);

        if (client == null)
            return null;

        return getUserPwdCertFromDao(client.getIuid(), client, authReqHeaders);
    }

/*    public Integer selectAuthIndividualClient(Map<String, Object> map) throws IOException {
        Integer is_exist = getSqlSession().selectOne( ClientDao.class.getName()+ ".selectAuthIndividualClient", map);
        return is_exist;
    }*/

    public UserInfo selectUserCert(String phoneNo) throws IOException {
        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectClient", phoneNo);

        if (client == null)
            return null;

        return getUserCertFromDao(client.getIuid(), client);
    }

    public UserInfo selectUserCert(Bssinfo bssinfo) throws IOException {
        Client client = getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectBssClient", bssinfo);

        if (client == null)
            return null;

        return getUserCertFromDao(client.getIuid(), client);
    }

    public void updateChgMdnClient(Bssinfo bssinfo, long iuid) throws IOException {
        bssinfo.setIuid(iuid);
        getSqlSession().update( ClientDao.class.getName()+ ".updateChgMdnClient", bssinfo);
        getSqlSession().update( ClientDao.class.getName()+ ".updateChgMdnIndividualClient", bssinfo);
    }

    public void updateChgLoginMdn(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws IOException {


        getSqlSession().update( ClientDao.class.getName()+ ".updateChgLoginMdn", authReqHeaders);
    }

    public Integer selectExistIuid(Long iuid) throws IOException {

        return getReadSqlSession().selectOne( ClientDao.class.getName()+ ".selectExistIuid", iuid);
    }

    private UserInfo getUserInfoFromDao(Long iuid, Client client)
            throws IOException {

        UserInfo userInfo = new UserInfo();
        try {

            userInfo.setIuid(client.getIuid());
            userInfo.setPhoneNo(client.getPhoneNo());
            userInfo.setCountryCode(client.getCountryCode());
            userInfo.setRegionCode(client.getRegionCode());
            userInfo.setNationalFmt(client.getNationalFmt());
            userInfo.setClientType(ClientType.fromInt(client.getSubsType()));
            userInfo.setBaseSvcDir(client.getBaseSvcDir());
            userInfo.setRegDate(client.getRegDate());
            userInfo.setSuid(client.getSuid());
            userInfo.setSuidPw(client.getSuidPw());
            userInfo.setTuid(null);
            userInfo.setfTest(client.getFTest());
            userInfo.setfActive(client.getFActive());

            IndividualClient individualClient = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectIndividualClientByIuid", iuid);
            userInfo.setSubsType(SubscriberType.fromInt(individualClient.getSubsType()));
            userInfo.setAppVersion(individualClient.getAppVersion());
            userInfo.setCauthKey(individualClient.getCAuthKey());
            userInfo.setPnotiToken(individualClient.getPNotiToken());
            userInfo.setOsType(individualClient.getOsType());
            userInfo.setOsVersion(individualClient.getOsVersion());
            userInfo.setTelecomStr(individualClient.getTelecomStr());
            userInfo.setTelecomCode(individualClient.getTelecomCode());
            userInfo.setDeviceId(individualClient.getDeviceID());
            userInfo.setDevice_chg_f(individualClient.getDevice_chg_flag());
            userInfo.setSvc_net_type(0);
            userInfo.setHome_sched_iuid(individualClient.getHome_sched_iuid());
            userInfo.setAlarm_setup(individualClient.getAlarm_setup());
            userInfo.setAlarm_setup_gr(individualClient.getAlarm_setup_gr());
            userInfo.setAlarm_setup_sched(individualClient.getAlarm_setup_sched());
            userInfo.setAlarm_setup_msg(individualClient.getAlarm_setup_msg());


            Profile profile = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectProfile", iuid);

            if (profile != null) {
                userInfo.setProfile(profile);

            }


            userInfo.setStoreType(individualClient.getStoreType());

//        if(individualClient.getNickname() != null )
//            userInfo.getProfile().setNickname(individualClient.getNickname());
//        if(individualClient.getStatusMessage() != null )
//            userInfo.getProfile().setStatusMessage(individualClient.getStatusMessage());
//        if( individualClient.getPresence() != null)
//            userInfo.getProfile().setPresence(individualClient.getPresence());

//        userInfo.setStoreType(individualClient.getStoreType());

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return null;
        }

        return userInfo;
    }


//    private UserInfo getUserCertFromDao(Long iuid, Client client)
//            throws IOException {
//
//
//        UserInfo userInfo = new UserInfo();
//        userInfo.setIuid(client.getIuid());
//        userInfo.setPhoneNo(client.getPhoneNo());
//        userInfo.setCountryCode(client.getCountryCode());
//        userInfo.setRegionCode(client.getRegionCode());
//        userInfo.setNationalFmt(client.getNationalFmt());
//        userInfo.setClientType(ClientType.fromInt(client.getSubsType()));
//        userInfo.setBaseSvcDir(client.getBaseSvcDir());
//        userInfo.setRegDate(client.getRegDate());
//        userInfo.setSuid(client.getSuid());
//        userInfo.setSuidPw(client.getSuidPw());
//        userInfo.setTuid(null);
//        userInfo.setfTest(client.getFTest());
//        userInfo.setfActive(client.getFActive());
//
//        IndividualClient individualClient = getSqlSession().selectOne(ClientDao.class.getName() + ".selectIndividualClientByIuidCert", iuid);
//        userInfo.setSubsType(SubscriberType.fromInt(individualClient.getSubsType()));
//        userInfo.setAppVersion(individualClient.getAppVersion());
//        userInfo.setCauthKey(individualClient.getCAuthKey());
//        userInfo.setPnotiToken(individualClient.getPNotiToken());
//        userInfo.setOsType(individualClient.getOsType());
//        userInfo.setOsVersion(individualClient.getOsVersion());
//        userInfo.setTelecomStr(individualClient.getTelecomStr());
//        userInfo.setTelecomCode(individualClient.getTelecomCode());
//        userInfo.setDeviceId(individualClient.getDeviceID());
//        userInfo.setProfileTag(individualClient.getProfileTag());
//        userInfo.setStatusMessage(individualClient.getStatusMessage());
//        userInfo.setNickname(individualClient.getNickname());
//        userInfo.setSvc_net_type(0);
//
//
//        if (individualClient.getProfile() != null) {
//            userInfo.setProfileFromString(individualClient.getProfile());
//        }
//
//
//        userInfo.setStoreType(individualClient.getStoreType());
//        userInfo.setDeviceModel(individualClient.getDeviceModel());
//        userInfo.setStoreType(individualClient.getStoreType());
//        userInfo.getProfile().setNickname(individualClient.getNickname());
//        userInfo.getProfile().setStatusMessage(individualClient.getStatusMessage());
//        userInfo.getProfile().setPresence(individualClient.getPresence());
//
//
//        return userInfo;
//    }

    private UserInfo getUserCertFromDao(Long iuid, Client client)
            throws IOException {


        UserInfo userInfo = new UserInfo();
        userInfo.setIuid(client.getIuid());
        userInfo.setPhoneNo(client.getPhoneNo());
        userInfo.setCountryCode(client.getCountryCode());
        userInfo.setRegionCode(client.getRegionCode());
        userInfo.setNationalFmt(client.getNationalFmt());
        userInfo.setClientType(ClientType.fromInt(client.getSubsType()));
        userInfo.setBaseSvcDir(client.getBaseSvcDir());
        userInfo.setRegDate(client.getRegDate());
        userInfo.setSuid(client.getSuid());
        userInfo.setSuidPw(client.getSuidPw());
        userInfo.setTuid(client.getTuid());
        userInfo.setfTest(client.getFTest());
        userInfo.setfActive(client.getFActive());



        IndividualClient individualClient = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectIndividualClientByIuidCert", iuid);

        userInfo.setSubsType(SubscriberType.fromInt(individualClient.getSubsType()));
        userInfo.setAppVersion(individualClient.getAppVersion());
        userInfo.setCauthKey(individualClient.getCAuthKey());
        userInfo.setPnotiToken(individualClient.getPNotiToken());
        userInfo.setOsType(individualClient.getOsType());
        userInfo.setOsVersion(individualClient.getOsVersion());
        userInfo.setTelecomStr(individualClient.getTelecomStr());
        userInfo.setTelecomCode(individualClient.getTelecomCode());
        userInfo.setDeviceId(individualClient.getDeviceID());
        userInfo.setLogin_status(individualClient.getLogin_status());


        Profile profile = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectProfile", iuid);

        if(profile != null) {
            userInfo.setProfileTag(profile.getProfile_tag());
            userInfo.setProfile(profile);
        }



        userInfo.setStoreType(individualClient.getStoreType());
        userInfo.setDeviceModel(individualClient.getDeviceModel());
        userInfo.setStoreType(individualClient.getStoreType());

        userInfo.setSvc_net_type(0);

        return userInfo;
    }

    private UserInfo getUserIuidCertFromDao(Long iuid, Client client)
            throws IOException {


        UserInfo userInfo = new UserInfo();
        userInfo.setIuid(client.getIuid());
        userInfo.setPhoneNo(client.getPhoneNo());
        userInfo.setCountryCode(client.getCountryCode());
        userInfo.setRegionCode(client.getRegionCode());
        userInfo.setNationalFmt(client.getNationalFmt());
        userInfo.setClientType(ClientType.fromInt(client.getSubsType()));
        userInfo.setBaseSvcDir(client.getBaseSvcDir());
        userInfo.setRegDate(client.getRegDate());
        userInfo.setSuid(client.getSuid());
        userInfo.setSuidPw(client.getSuidPw());
        userInfo.setTuid(client.getTuid());
        userInfo.setfTest(client.getFTest());
        userInfo.setfActive(client.getFActive());
        userInfo.setUser_type(client.getSubsType());



        IndividualClient individualClient = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectIndividualClientByIuidCert", iuid);

        if(individualClient == null)
        {
            logger.error("T_INDIVIDUAL_CLIENT 정보에 해당 가입자 없음!! [{}]", iuid);
            return null;
        }

        /*if(individualClient.getSubsType() != authReqHeaders.getSubs_type())
        {
            logger.error("가입자 유형 불일치!! iuid[{}] db-subs_type[{}] req-subs_type[{}]", iuid, individualClient.getSubsType(), authReqHeaders.getSubs_type());
            return null;
        }

        if(authReqHeaders.getcAuthKey() != null || authReqHeaders.getIuid() != null)
        {
            if(authReqHeaders.getIuid() != iuid || !StringUtils.equals(authReqHeaders.getCAuthKey(), individualClient.getCAuthKey())) {
                logger.error("가입자 iuid/auth-key 불일치!! iuid[{}][{}] auth-key[{}][{}] ",authReqHeaders.getIuid(), iuid, authReqHeaders.getCAuthKey(), individualClient.getCAuthKey());
                return null;
            }
        }*/

        userInfo.setCh_auth(individualClient.getCh_auth());
        userInfo.setSubsType(SubscriberType.fromInt(individualClient.getSubsType()));
        userInfo.setAppVersion(individualClient.getAppVersion());
        userInfo.setCauthKey(individualClient.getCAuthKey());
        userInfo.setPnotiToken(individualClient.getPNotiToken());
        userInfo.setOsType(individualClient.getOsType());
        userInfo.setOsVersion(individualClient.getOsVersion());
        userInfo.setTelecomStr(individualClient.getTelecomStr());
        userInfo.setTelecomCode(individualClient.getTelecomCode());
        userInfo.setDeviceId(individualClient.getDeviceID());
        userInfo.setLogin_status(individualClient.getLogin_status());
        userInfo.setHome_sched_iuid(individualClient.getHome_sched_iuid());
        userInfo.setHome_sched_iuid(individualClient.getHome_sched_iuid());
        userInfo.setAlarm_setup(individualClient.getAlarm_setup());
        userInfo.setAlarm_setup_gr(individualClient.getAlarm_setup_gr());
        userInfo.setAlarm_setup_sched(individualClient.getAlarm_setup_sched());
        userInfo.setAlarm_setup_msg(individualClient.getAlarm_setup_msg());
        userInfo.setAlarm_setup_camp(individualClient.getAlarm_setup_camp());


        Profile profile = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectProfile", iuid);

        if(profile != null) {
            userInfo.setProfileTag(profile.getProfile_tag());
            userInfo.setProfile(profile);
        }



        userInfo.setStoreType(individualClient.getStoreType());
        userInfo.setDeviceModel(individualClient.getDeviceModel());
        userInfo.setStoreType(individualClient.getStoreType());

        userInfo.setSvc_net_type(0);

        return userInfo;
    }


    private UserInfo getUserCertFromDao(Long iuid, Client client, ClientAuthReqHeaders authReqHeaders)
            throws IOException {


        UserInfo userInfo = new UserInfo();
        userInfo.setIuid(client.getIuid());
        userInfo.setPhoneNo(client.getPhoneNo());
        userInfo.setCountryCode(client.getCountryCode());
        userInfo.setRegionCode(client.getRegionCode());
        userInfo.setNationalFmt(client.getNationalFmt());
        userInfo.setClientType(ClientType.fromInt(client.getSubsType()));
        userInfo.setBaseSvcDir(client.getBaseSvcDir());
        userInfo.setRegDate(client.getRegDate());
        userInfo.setSuid(client.getSuid());
        userInfo.setSuidPw(client.getSuidPw());
        userInfo.setTuid(client.getTuid());
        userInfo.setfTest(client.getFTest());
        userInfo.setfActive(client.getFActive());
        userInfo.setUser_type(client.getSubsType());



        IndividualClient individualClient = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectIndividualClientByIuidCert", iuid);

        if(individualClient == null)
        {
            logger.error("T_INDIVIDUAL_CLIENT 정보에 해당 가입자 없음!! [{}]", iuid);
            return null;
        }

        /*if(individualClient.getSubsType() != authReqHeaders.getSubs_type())
        {
            logger.error("가입자 유형 불일치!! iuid[{}] db-subs_type[{}] req-subs_type[{}]", iuid, individualClient.getSubsType(), authReqHeaders.getSubs_type());
            return null;
        }

        if(authReqHeaders.getcAuthKey() != null || authReqHeaders.getIuid() != null)
        {
            if(authReqHeaders.getIuid() != iuid || !StringUtils.equals(authReqHeaders.getCAuthKey(), individualClient.getCAuthKey())) {
                logger.error("가입자 iuid/auth-key 불일치!! iuid[{}][{}] auth-key[{}][{}] ",authReqHeaders.getIuid(), iuid, authReqHeaders.getCAuthKey(), individualClient.getCAuthKey());
                return null;
            }
        }*/

        userInfo.setLogin_phoneNo(individualClient.getPhoneNo());
        userInfo.setCh_auth(individualClient.getCh_auth());
        userInfo.setSubsType(SubscriberType.fromInt(individualClient.getSubsType()));
        userInfo.setAppVersion(individualClient.getAppVersion());
        userInfo.setCauthKey(individualClient.getCAuthKey());
        userInfo.setPnotiToken(individualClient.getPNotiToken());
        userInfo.setOsType(individualClient.getOsType());
        userInfo.setOsVersion(individualClient.getOsVersion());
        userInfo.setTelecomStr(individualClient.getTelecomStr());
        userInfo.setTelecomCode(individualClient.getTelecomCode());
        userInfo.setDeviceId(individualClient.getDeviceID());
        userInfo.setLogin_status(individualClient.getLogin_status());
        userInfo.setHome_sched_iuid(individualClient.getHome_sched_iuid());
        userInfo.setHome_sched_iuid(individualClient.getHome_sched_iuid());
        userInfo.setAlarm_setup(individualClient.getAlarm_setup());
        userInfo.setAlarm_setup_gr(individualClient.getAlarm_setup_gr());
        userInfo.setAlarm_setup_sched(individualClient.getAlarm_setup_sched());
        userInfo.setAlarm_setup_msg(individualClient.getAlarm_setup_msg());
        userInfo.setAlarm_setup_camp(individualClient.getAlarm_setup_camp());


        Profile profile = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectProfile", iuid);

        if(profile != null) {
            userInfo.setProfileTag(profile.getProfile_tag());
            userInfo.setProfile(profile);
        }



        userInfo.setStoreType(individualClient.getStoreType());
        userInfo.setDeviceModel(individualClient.getDeviceModel());
        userInfo.setStoreType(individualClient.getStoreType());

        userInfo.setSvc_net_type(0);

        return userInfo;
    }



    private UserInfo getUserPwdCertFromDao(Long iuid, Client client, ClientAuthReqHeaders authReqHeaders)
            throws IOException {


        UserInfo userInfo = new UserInfo();
        userInfo.setIuid(client.getIuid());
        userInfo.setPhoneNo(client.getPhoneNo());
        userInfo.setCountryCode(client.getCountryCode());
        userInfo.setRegionCode(client.getRegionCode());
        userInfo.setNationalFmt(client.getNationalFmt());
        userInfo.setClientType(ClientType.fromInt(client.getSubsType()));
        userInfo.setBaseSvcDir(client.getBaseSvcDir());
        userInfo.setRegDate(client.getRegDate());
        userInfo.setSuid(client.getSuid());
        userInfo.setSuidPw(client.getSuidPw());
        userInfo.setTuid(client.getTuid());
        userInfo.setfTest(client.getFTest());
        userInfo.setfActive(client.getFActive());
        userInfo.setUser_type(client.getSubsType());



        IndividualClient individualClient = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectIndividualClientByIuidCert", iuid);

        if(individualClient == null)
        {
            logger.error("T_INDIVIDUAL_CLIENT 정보에 해당 가입자 없음!! [{}]", iuid);
            return null;
        }


        userInfo.setCh_auth(individualClient.getCh_auth());
        userInfo.setSubsType(SubscriberType.fromInt(individualClient.getSubsType()));
        userInfo.setAppVersion(individualClient.getAppVersion());
        userInfo.setCauthKey(individualClient.getCAuthKey());
        userInfo.setPnotiToken(individualClient.getPNotiToken());
        userInfo.setOsType(individualClient.getOsType());
        userInfo.setOsVersion(individualClient.getOsVersion());
        userInfo.setTelecomStr(individualClient.getTelecomStr());
        userInfo.setTelecomCode(individualClient.getTelecomCode());
        userInfo.setDeviceId(individualClient.getDeviceID());
        userInfo.setLogin_status(individualClient.getLogin_status());

        Profile profile = getReadSqlSession().selectOne(ClientDao.class.getName() + ".selectProfile", iuid);

        if(profile != null) {
            userInfo.setProfileTag(profile.getProfile_tag());
            userInfo.setProfile(profile);
        }



        userInfo.setStoreType(individualClient.getStoreType());
        userInfo.setDeviceModel(individualClient.getDeviceModel());
        userInfo.setStoreType(individualClient.getStoreType());


        return userInfo;
    }

    public List<MdnUpdateList> selectIndividualDeviceDuplicate(String device_id) {
        return getReadSqlSession().selectList(ClientDao.class.getName() + ".selectIndividualDeviceDuplicate", device_id);
    }

    public boolean isAuthUser(AuthTocken authTocken) {
        int count = getReadSqlSession().selectOne(ClientDao.class.getName() + ".countIndividualClient", authTocken);

        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isAuthUserKey(AuthTocken authTocken) {
        int count = getReadSqlSession().selectOne(ClientDao.class.getName() + ".countIndividualClientKey", authTocken);

        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void updateIndividualClientAlarm(AlarmInfo alarmInfo)
    {
        getSqlSession().update(ClientDao.class.getName() + ".updateIndividualClientAlarm", alarmInfo);
    }

    public void updateIndividualClientHomeSchedIuid(HomeSchedIuid homeSchedIuid)
    {
        getSqlSession().update(ClientDao.class.getName() + ".updateIndividualClientHomeSchedIuid", homeSchedIuid);
    }
}
