package com.tisquare.mvp.mcs.db.domain;

import com.tisquare.mvp.mcs.model.StatLog;

/**
 * Created by jhkim on 14. 3. 20.
 */
public class ServiceTotal {
    String year;
    String month;
    String day;
    String hour;
    String minute;
    int svr_id=0;
    int svc_id=0;
    int sub_svc_id=0;
    int part_id=0;
    int used_cnt=0;

    public ServiceTotal(){}
    public ServiceTotal(StatLog node){
        this.year = node.getYear();
        this.month = node.getMonth();
        this.day = node.getDay();
        this.hour = node.getHour();
        this.minute = node.getMin();
        this.used_cnt = node.getUsed_cnt();
    }

    public String getYear() { return year; }
    public void setYear(String year) { this.year = year; }
    public String getMonth() { return month; }
    public void setMonth(String month) { this.month = month; }
    public String getDay() { return day; }
    public void setDay(String day) { this.day = day; }
    public String getHour() { return hour; }
    public void setHour(String hour) { this.hour = hour; }
    public String getMinute() { return minute; }
    public void setMinute(String minute) { this.minute = minute; }
    public int getSvr_id() { return svr_id; }
    public void setSvr_id(int svr_id) { this.svr_id = svr_id; }
    public int getSvc_id() { return svc_id; }
    public void setSvc_id(int svc_id) { this.svc_id = svc_id; }
    public int getSub_svc_id() { return sub_svc_id; }
    public void setSub_svc_id(int sub_svc_id) { this.sub_svc_id = sub_svc_id; }
    public int getPart_id() { return part_id; }
    public void setPart_id(int part_id) { this.part_id = part_id; }
    public int getUsed_cnt() { return used_cnt; }
    public void setUsed_cnt(int used_cnt) { this.used_cnt = used_cnt; }
}
