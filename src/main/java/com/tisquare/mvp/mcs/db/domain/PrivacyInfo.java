package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tisquare.mvp.mcs.model.AlarmInfo;
import com.tisquare.mvp.mcs.model.UserInfo;

/**
 * Created by kkh on 16. 03. 02.
 */
public class PrivacyInfo {


    @JsonProperty("home_sched_iuid")
    private Long home_sched_iuid;

    @JsonProperty("alarm-info")
    private AlarmInfo alarmInfo;

    public PrivacyInfo( UserInfo userInfo)
    {
        this.home_sched_iuid = userInfo.getHome_sched_iuid();
        this.alarmInfo = new AlarmInfo(userInfo);
    }

    public void setHome_sched_iuid(Long home_sched_iuid) {
        this.home_sched_iuid = home_sched_iuid;
    }

    public Long getHome_sched_iuid() {
        return home_sched_iuid;
    }

    public AlarmInfo getAlarmInfo() {
        return alarmInfo;
    }

    public void setAlarmInfo(AlarmInfo alarmInfo) {
        this.alarmInfo = alarmInfo;
    }
}
