package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kkh on 16. 03. 02.
 */
public class NiceInfo {



    long iuid;

    int result;

    String enc_data;

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public void setResult(int result) {
        this.result = result;
    }



    public long getIuid() {
        return iuid;
    }

    public int getResult() {
        return result;
    }


    public void setEnc_data(String enc_data) {
        this.enc_data = enc_data;
    }

    public String getEnc_data() {
        return enc_data;
    }
}
