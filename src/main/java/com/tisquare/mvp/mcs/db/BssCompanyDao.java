package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.AuthTocken;
import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.entity.Client;
import com.tisquare.mvp.mcs.entity.IndividualClient;
import com.tisquare.mvp.mcs.entity.SubscriberType;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.type.ClientType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class BssCompanyDao {

    private static final Logger logger = LoggerFactory.getLogger(BssCompanyDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }



    public BssCompany selectBssCompany(UserInfo userInfo) {
        return getReadSqlSession().selectOne(BssCompanyDao.class.getName() + ".selectBssCompany", userInfo);

    }

    public BssCompany selectDownloadFlagBssCompany(Download download) {
        return getReadSqlSession().selectOne(BssCompanyDao.class.getName() + ".selectDownloadFlagBssCompany", download);

    }

    public BssCompany selectCommanderBssCompany(String cp_code) {
        return getReadSqlSession().selectOne(BssCompanyDao.class.getName() + ".selectCommanderBssCompany", cp_code);

    }
}
