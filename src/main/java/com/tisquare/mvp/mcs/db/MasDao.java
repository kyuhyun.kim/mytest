package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.MasSched;
import com.tisquare.mvp.mcs.db.domain.MasTrainee;
import com.tisquare.mvp.mcs.model.AuthInfoBody;
import com.tisquare.mvp.mcs.model.BtInfo;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class MasDao {

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public MasTrainee selectMasTraniee(Long iuid){

        return getReadSqlSession().selectOne(MasDao.class.getName() + ".selectMasTraniee", iuid);
    }

    public List<MasSched> selectMasSched(Long iuid, int type){

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("iuid", iuid);
        map.put("SCHEDULE_TYPE", type);
        return getReadSqlSession().selectList(MasDao.class.getName() + ".selectMasSched", map);
    }
}
