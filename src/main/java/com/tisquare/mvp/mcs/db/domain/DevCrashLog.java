package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by jhkim on 14. 11. 10.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DevCrashLog {

    @JsonProperty("mdn")
    String mdn;
    @JsonProperty("iuid")
    String iuid;

    @JsonProperty("app-version")
    String app_version;
    @JsonProperty("os-ver")
    String os_version;
    @JsonProperty("dev-model")
    String dev_model;
    @JsonProperty("category")
    String category;

    @JsonProperty("stack-trace")
    String stack_trace;



    public String getMdn() { return mdn; }
    public void setMdn(String mdn) { this.mdn = mdn; }
    public String getIuid() { return iuid; }
    public void setIuid(String iuid) { this.iuid = iuid; }
    public String getApp_version() { return app_version; }
    public void setApp_version(String app_version) { this.app_version = app_version; }

    public String getOs_ver() { return os_version; }
    public void setOs_ver(String os_ver) { this.os_version = os_ver; }
    public String getDev_model() { return dev_model; }
    public void setDev_model(String dev_model) { this.dev_model = dev_model; }
    public String getCategory() { return category; }
    public void setCategory(String category) { this.category = category; }
    public String getStack_trace() { return stack_trace; }
    public void setStack_trace(String stack_trace) { this.stack_trace = stack_trace; }
}
