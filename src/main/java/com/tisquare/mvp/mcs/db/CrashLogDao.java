package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.domain.DevCrashLog;
import com.tisquare.mvp.mcs.model.CrashLog;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class CrashLogDao {

    private SqlSession sqlSession;
    private boolean externalSqlSession;


    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    public int insertCrashLog(CrashLog crashLog){
        return getSqlSession().insert(CrashLogDao.class.getName() + ".insertCrashLog", crashLog);
    }
}
