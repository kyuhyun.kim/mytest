package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.entity.Client;
import com.tisquare.mvp.mcs.entity.IndividualClient;
import com.tisquare.mvp.mcs.entity.SubscriberType;
import com.tisquare.mvp.mcs.model.ServerConnInfo;
import com.tisquare.mvp.mcs.model.UserInfo;
import com.tisquare.mvp.mcs.type.ClientType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import com.tisquare.mvp.mcs.db.domain.ServerInfoRec;
import com.tisquare.mvp.mcs.model.ConnectionInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class ServerDao {

    private static final Logger logger = LoggerFactory.getLogger(ServerDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    InitConfig initConfig;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }


    public List<ServerConnInfo> findServers(String tnVersion) throws DataAccessException {
        if (tnVersion == null)
            tnVersion = "1.0.0";

        DefaultArtifactVersion clientVersion = new DefaultArtifactVersion(tnVersion);
        DefaultArtifactVersion minVersion = new DefaultArtifactVersion("2.0.0");

        if (clientVersion != null && minVersion != null) {
            /**
             * a.compareTo(b) < 0 : a<b, >0 : a>b, 0 : a=b
             */
            if (clientVersion.compareTo(minVersion) >= 0)
                return this.findServers2();
        }

        return getReadSqlSession().selectList(ServerDao.class.getName() + ".selectServers");
    }

    public List<ServerConnInfo> findServers2() throws DataAccessException {
        List<ServerInfoRec> serverRecs = getReadSqlSession().selectList(ServerDao.class.getName() + ".selectServers2");

        if (serverRecs == null || serverRecs.size() <= 0)
            return null;

        List<ServerConnInfo> serverConnInfos = new ArrayList<>();

        for (ServerInfoRec item : serverRecs) {
            String protocol = item.getProtocol();

            ConnectionInfo connection;

            if (protocol.equalsIgnoreCase("url")) {
                connection = new ConnectionInfo(protocol, item.getUrl(), item.getTag());
            } else
                connection = new ConnectionInfo(protocol, item.getIpAddr(), item.getPort(), item.getTag());

            ServerConnInfo findItem = new ServerConnInfo(item.getId(), item.getName());
            ServerConnInfo newItem;

            if (!serverConnInfos.contains(findItem)) {
                newItem = new ServerConnInfo(item.getId(), item.getName());

                serverConnInfos.add(newItem);
            } else {
                int idx = serverConnInfos.indexOf(findItem);
                newItem = serverConnInfos.get(idx);
            }

            newItem.addConnection(connection);
        }

        return serverConnInfos;
    }

    public List<ServerConnInfo> findServers3(int fcs_count) throws DataAccessException {
        List<ServerInfoRec> serverRecs = getReadSqlSession().selectList(ServerDao.class.getName()+ ".selectServers3");

        if (serverRecs == null || serverRecs.size() <= 0)
            return null;

        List<ServerConnInfo> serverConnInfos = new ArrayList<>();
        int idx = 0;
        for (ServerInfoRec item : serverRecs) {

            String protocol = item.getProtocol();


            ConnectionInfo connection = new ConnectionInfo(protocol, item.getIpAddr(), item.getPort(), item.getTag());


            ServerConnInfo findItem = new ServerConnInfo(item.getId(), item.getName());
            findItem.addConnection(connection);


            if(!protocol.toLowerCase().equals("tcp") )
                serverConnInfos.add(findItem);
            else
            {
                if(serverConnInfos.size() < 2 )
                    serverConnInfos.add(findItem);
                if(idx++ == fcs_count ) {
                    serverConnInfos.remove(serverConnInfos.size() -1);
                    serverConnInfos.add(findItem);
                }
            }



        }



        ServerInfoRec serverinfo3Scf = getReadSqlSession().selectOne(ServerDao.class.getName()+ ".selectServers3ToSCF");

        if(serverinfo3Scf != null) {
            ServerConnInfo findItem = new ServerConnInfo(serverinfo3Scf.getId(), serverinfo3Scf.getName());
            ConnectionInfo connection = new ConnectionInfo(serverinfo3Scf.getProtocol(), serverinfo3Scf.getIpAddr(), serverinfo3Scf.getPort(), serverinfo3Scf.getTag());
            findItem.addConnection(connection);

            serverConnInfos.add(findItem);
        }
        return serverConnInfos;
    }


    /*public List<ServerConnInfo> findServers3(int fcs_count) throws DataAccessException {

        int ServerInfoFcsCnt = 0;
        int idx = 1;
        int fcs_idx = fcs_count;
        int scf_idx = fcs_idx;
        List<ServerInfoRec> serverRecs = getSqlSession().selectList(ServerDao.class.getName()+ ".selectServers3");

        if (serverRecs == null || serverRecs.size() <= 0)
            return null;

        for (ServerInfoRec item : serverRecs) {
            if(item.getName().toLowerCase().equals("fcs"))
                ServerInfoFcsCnt++;
        }

        if(fcs_count > ServerInfoFcsCnt )
            fcs_idx = 1;

        List<ServerConnInfo> serverConnInfos = new ArrayList<>();


        for (ServerInfoRec item : serverRecs) {

            String protocol = item.getProtocol();


            ConnectionInfo connection = new ConnectionInfo(protocol, item.getIpAddr(), item.getPort(), item.getTag());


            ServerConnInfo findItem = new ServerConnInfo(item.getId(), item.getName());
            findItem.addConnection(connection);


            if(!protocol.toLowerCase().equals("tcp") )
                serverConnInfos.add(findItem);
            else
            {
                if (idx++ == fcs_idx) {
                    serverConnInfos.add(findItem);
                }

            }



        }


        idx = 1;
        List<ServerInfoRec> ScfserverRecs = getSqlSession().selectList(ServerDao.class.getName()+ ".selectServersSCF3");


        if(fcs_count > ScfserverRecs.size() )
            scf_idx = 1;

        for (ServerInfoRec item : ScfserverRecs) {

            String protocol = item.getProtocol();


            ConnectionInfo connection = new ConnectionInfo(protocol, item.getIpAddr(), item.getPort(), item.getTag());


            ServerConnInfo findItem = new ServerConnInfo(item.getId(), item.getName());
            findItem.addConnection(connection);


            if (idx++ == scf_idx) {
                serverConnInfos.add(findItem);
            }




        }
        return serverConnInfos;
    }*/



    public List<ServerConnInfo> findServers3() throws DataAccessException {
        List<ServerInfoRec> serverRecs = getReadSqlSession().selectList(ServerDao.class.getName()+ ".selectServers3");

        if (serverRecs == null || serverRecs.size() <= 0)
            return null;

        List<ServerConnInfo> serverConnInfos = new ArrayList<>();
        int idx = 0;
        for (ServerInfoRec item : serverRecs) {

            String protocol = item.getProtocol();

            ConnectionInfo connection = new ConnectionInfo(protocol, item.getIpAddr(), item.getPort(), item.getTag());

            ServerConnInfo findItem = new ServerConnInfo(item.getId(), item.getName());
            findItem.addConnection(connection);

            serverConnInfos.add(findItem);


        }

        return serverConnInfos;
    }
}
