package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.Presence;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.IOException;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class PresenceDao {

    private static final Logger logger = LoggerFactory.getLogger(PresenceDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }


    public void updatePresence(Presence presence) throws IOException {
        getSqlSession().update(PresenceDao.class.getName() + ".updatePresence", presence);
    }

    public Integer selectPresence(Presence presence) throws IOException {
        return getReadSqlSession().selectOne(PresenceDao.class.getName() + ".selectPresence", presence);
    }



}
