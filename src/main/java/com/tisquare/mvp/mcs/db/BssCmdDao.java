package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.model.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class BssCmdDao {

    private static final Logger logger = LoggerFactory.getLogger(BssCmdDao.class);

    private SqlSession sqlSession;
    private boolean externalSqlSession;




    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }


    public BssCommander selectCommanderUserPwd(Bssinfo bssinfo) {
        return getReadSqlSession().selectOne(BssCmdDao.class.getName() + ".selectCommanderUserPwd", bssinfo);

    }

    public void updateCommanderUserPwd(Bssinfo bssinfo) {
        getSqlSession().update(BssCmdDao.class.getName() + ".updateCommanderUserPwd", bssinfo);

    }


    public String selectCommanderAuth(ClientAuthReqHeaders authReqHeaders) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("iuid", authReqHeaders.getIuid());
        map.put("cauth_key", authReqHeaders.getcAuthKey());


        return getReadSqlSession().selectOne(BssCmdDao.class.getName() + ".selectCommanderAuth", map);
    }





}
