package com.tisquare.mvp.mcs.db;

import com.tisquare.mvp.mcs.model.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jhkim on 14. 3. 5.
 */
@Repository
public class TSbsDao extends DaoSupport{

    private SqlSession sqlSession;

    private boolean externalSqlSession;

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {
    }

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession() {
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public GradeInfo selectSbsAdmin(UserInfo userInfo) throws DataAccessException {
        return getReadSqlSession().selectOne(TSbsDao.class.getName() + ".selectSbsAdmin", userInfo);
    }

    public  List<GradeInfo> selectCampMember(UserInfo userInfo) throws DataAccessException {
        return getReadSqlSession().selectList(TSbsDao.class.getName() + ".selectCampMember", userInfo);
    }
}
