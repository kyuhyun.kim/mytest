package com.tisquare.mvp.mcs.db.domain;


import com.tisquare.mvp.mcs.model.StatLog;

/**
 * Created by jhkim on 14. 3. 21.
 */
public class ServiceSmallCall {
    String year;
    String month;
    String day;
    String hour;
    String minute;
    int receive_call_cnt;
    int display_cnt;
    int accept_call_cnt;
    int reject_call_cnt;
    int reject_msg_cnt;

    public ServiceSmallCall(){}
    public ServiceSmallCall(StatLog node){
        this.year = node.getYear();
        this.month = node.getMonth();
        this.day = node.getDay();
        this.hour = node.getHour();
        this.minute = node.getMin();
    }

    public String getYear() { return year; }
    public void setYear(String year) { this.year = year; }
    public String getMonth() { return month; }
    public void setMonth(String month) { this.month = month; }
    public String getDay() { return day; }
    public void setDay(String day) { this.day = day; }
    public String getHour() { return hour; }
    public void setHour(String hour) { this.hour = hour; }
    public String getMinute() { return minute; }
    public void setMinute(String minute) { this.minute = minute; }
    public int getReceive_call_cnt() { return receive_call_cnt; }
    public void setReceive_call_cnt(int receive_call_cnt)
        { this.receive_call_cnt = receive_call_cnt; }
    public int getDisplay_cnt() { return display_cnt; }
    public void setDisplay_cnt(int display_cnt)
        { this.display_cnt = display_cnt; }
    public int getAccept_call_cnt() { return accept_call_cnt; }
    public void setAccept_call_cnt(int accept_call_cnt)
        { this.accept_call_cnt = accept_call_cnt; }
    public int getReject_call_cnt() { return reject_call_cnt; }
    public void setReject_call_cnt(int reject_call_cnt)
        { this.reject_call_cnt = reject_call_cnt; }
    public int getReject_msg_cnt() { return reject_msg_cnt; }
    public void setReject_msg_cnt(int reject_msg_cnt)
        { this.reject_msg_cnt = reject_msg_cnt; }
}
