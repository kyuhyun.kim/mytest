package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.entity.ActiveUser;
import com.tisquare.mvp.mcs.model.CrashLog;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class ActiveUserDao {

    private SqlSession sqlSession;


    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public ActiveUser select(Long iuid, String phoneNo) throws DataAccessException {
        Map<String, Object> condition = new HashMap<>();
        condition.put("iuid", iuid);
        if (StringUtils.isNotBlank(phoneNo)) {
            condition.put("phone_no", phoneNo);
        }
        return getReadSqlSession().selectOne(ActiveUserDao.class.getName() +".find", condition);
    }


    public void insert(ActiveUser activeUser) throws DataAccessException {
        getSqlSession().insert(ActiveUserDao.class.getName() + ".insert", activeUser);
    }

    public void delete(ActiveUser activeUser) throws DataAccessException {
        getSqlSession().delete(ActiveUserDao.class.getName() +".delete", activeUser);
    }
}
