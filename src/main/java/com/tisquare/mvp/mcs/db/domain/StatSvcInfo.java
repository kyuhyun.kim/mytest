package com.tisquare.mvp.mcs.db.domain;

import com.tisquare.mvp.mcs.model.Statistic;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by jhkim on 14. 3. 20.
 */
public class StatSvcInfo {

    private String reg_dt;
    private String reg_tm;
    private int svc_type;
    private int sub_type;
    private int share_type;
    private int call_state;
    private String start_path;
    private int req_cnt;
    private int accept_cnt;
    private long svc_dur_tm;
    private int file_cnt;
    private long file_size;


    public StatSvcInfo(){}
    public StatSvcInfo(Statistic stat){
        SimpleDateFormat Regdt_format = new SimpleDateFormat("yyyyMMddHH", Locale.KOREA); //


        Timestamp currentTime = new Timestamp( stat.getRequest_time());

        String sDateTime = Regdt_format.format(currentTime);
        setReg_dt(sDateTime.substring(0, 8));
        setReg_tm( sDateTime.substring(8, 10) );

        if( stat.getservice_type().equals("picture")) {
            setSvc_type(1);
            if( stat.getService_sub_type().equals("album"))
                setSub_type(1);
            else if( stat.getService_sub_type().equals("camera"))
                setSub_type(2);
            else if( stat.getService_sub_type().equals("sketch"))
                setSub_type(3);
            else
                setSub_type(0);
        }
        else if( stat.getservice_type().equals("location")) {
            setSvc_type(2);
            if( stat.getService_sub_type().equals("position"))
                setSub_type(1);
            else if( stat.getService_sub_type().equals("route"))
                setSub_type(2);
            else if( stat.getService_sub_type().equals("tracking"))
                setSub_type(3);
            else
                setSub_type(0);
        }
        else if( stat.getservice_type().equals("web")) {
            setSvc_type(3);
        }
        else if( stat.getservice_type().equals("document")) {
            setSvc_type(4);
        }
        else
            setSvc_type(0);


        if( stat.getShare_type().equals("share")) {
            setShare_type(1);
            if(stat.getStart_time() != 0 ) {
                setAccept_cnt(1);
                if( stat.getEnd_time() != 0 )
                    setSvc_dur_tm(  stat.getEnd_time() - stat.getStart_time() );
                else
                    setSvc_dur_tm(0);
            }
            else {
                setAccept_cnt(0);
                setSvc_dur_tm(0);
            }

        }
        else if( stat.getShare_type().equals("transmit")) {
            setShare_type(2);
            setAccept_cnt(1);
            setSvc_dur_tm(0);
        }
        else
            setShare_type(0);

        if(stat.getCall_state().equals("calling"))
            setCall_state(1);
        else if(stat.getCall_state().equals("idle"))
            setCall_state(2);
        else
            setCall_state(0);

        setStart_path(stat.getStart_path());
        setReq_cnt(1);




        setFile_cnt(stat.getFile_count());
        setFile_size(stat.getFile_size());
    }

    public void setReg_dt(String reg_dt) {
        this.reg_dt = reg_dt;
    }

    public void setReg_tm(String reg_tm) {
        this.reg_tm = reg_tm;
    }

    public void setSvc_type(int svc_type) {
        this.svc_type = svc_type;
    }

    public void setSub_type(int sub_type) {
        this.sub_type = sub_type;
    }

    public void setShare_type(int share_type) {
        this.share_type = share_type;
    }

    public void setCall_state(int call_state) {
        this.call_state = call_state;
    }

    public void setReq_cnt(int req_cnt) {
        this.req_cnt = req_cnt;
    }

    public void setAccept_cnt(int accept_cnt) {
        this.accept_cnt = accept_cnt;
    }

    public void setSvc_dur_tm(long svc_dur_tm) {
        this.svc_dur_tm = svc_dur_tm;
    }

    public void setFile_cnt(int file_cnt) {
        this.file_cnt = file_cnt;
    }

    public void setFile_size(long file_size) {
        this.file_size = file_size;
    }

    public String getReg_dt() {

        return reg_dt;
    }

    public String getReg_tm() {
        return reg_tm;
    }

    public int getSvc_type() {
        return svc_type;
    }

    public int getSub_type() {
        return sub_type;
    }

    public int getShare_type() {
        return share_type;
    }

    public int getCall_state() {
        return call_state;
    }

    public int getReq_cnt() {
        return req_cnt;
    }

    public int getAccept_cnt() {
        return accept_cnt;
    }

    public long getSvc_dur_tm() {
        return svc_dur_tm;
    }

    public int getFile_cnt() {
        return file_cnt;
    }

    public long getFile_size() {
        return file_size;
    }

    public void setStart_path(String start_path) {
        this.start_path = start_path;
    }
    public String getStart_path() {
        return start_path;
    }


}
