package com.tisquare.mvp.mcs.db;

import com.tisquare.mvp.mcs.model.ShortUrlMap;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Repository;

/**
 * Created by asus on 2015-03-01.
 */
@Repository
public class TDao extends DaoSupport{

    private SqlSession tSqlSession;

    private boolean externalTSqlSession;

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {

    }

    @Autowired
    @Qualifier("factory1")
    public void setDaoSqlSession(SqlSessionFactory sqlSessionFactory){
        if(!this.externalTSqlSession){
            this.tSqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession() {
        return this.tSqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public Boolean existIuid(Long iuid){
        Long value = getReadSqlSession().selectOne(TDao.class.getName() + ".selectIuid", iuid);
        if(value != null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public String existSid(Long sid){
        String value = getReadSqlSession().selectOne(TDao.class.getName() + ".selectSid", sid);

        return value;
    }

    public ShortUrlMap existSidTargetUrl(String short_url){
        return  getReadSqlSession().selectOne(TDao.class.getName() + ".existSidTargetUrl", short_url);
    }


}
