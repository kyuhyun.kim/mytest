package com.tisquare.mvp.mcs.db.domain;

import com.tisquare.mvp.mcs.model.*;

/**
 * Created by hsna on 14. 8. 7.
 * SKT ICAS 시스템으로 부터 고객정보 및 에러코드를 전달하는 클래스
 */
public class BssLoginHist {


    String cp_code;
    String id;
    int login_type;
    String mdn;
    String app_version;
    String app_market;
    String os_type;
    String os_version;
    String telecom_code;
    String device_model;
    long iuid;

    public BssLoginHist(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {


        this.mdn = authReqHeaders.getPhoneNo();
        this.app_version = authReqHeaders.getAppVersion();
        this.app_market = String.valueOf(authReqHeaders.getStoreType());
        this.os_type = authReqParams.getOsType();
        this.os_version = authReqParams.getOsVersion();
        this.telecom_code = authReqParams.getTelecomCode();
        this.device_model = authReqParams.getDeviceModel();
        this.iuid = authInfoBody.getProfile().getIuid();


    }


    @Override
    public String toString() {
        return "BssLoginHist{" +
                ", cp_code='" + this.cp_code + '\'' +
                ", id='" + this.id + '\'' +
                ", iuid='" + this.iuid + '\'' +
                ", login_type='" + this.login_type + '\'' +
                ", mdn='" + this.mdn +'\'' +
                ", app_version='" + this.app_version +'\'' +
                ", app_market='" + this.app_market +'\'' +
                ", os_type='" + this.os_type +'\'' +
                ", os_version='" + this.os_version +'\'' +
                ", telecom_code='" + this.telecom_code +'\'' +
                ", device_model='" + this.device_model +'\'' +
                '}';
    }
}
