package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.PromotionSamsung;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.type.CampackRegResult;
import com.tisquare.mvp.mcs.type.CampackResult;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class PromotionDao {

    private static final Logger logger = LoggerFactory.getLogger(PromotionDao.class);
    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public PromotionSamsung selectPromotionSamsungInfo( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
    {
        return getReadSqlSession().selectOne(PromotionDao.class.getName() + ".selectPromotionSamsungInfo", authReqHeaders);
    }


    public void insertPromotionSamsung( PromotionSamsung promotionSamsung )
    {
        getSqlSession().update(PromotionDao.class.getName() + ".insertPromotionSamsung", promotionSamsung);
    }

    public PromotionCampPackInfo selectPromotionSamsungCampPackInfo( PromotionCampPackInfo promotionCampPackInfo)
    {
        return getReadSqlSession().selectOne(PromotionDao.class.getName() + ".selectPromotionSamsungCampPackInfo", promotionCampPackInfo);
    }

    public PromotionCampPackInfo selectPromotionSamsungCampPackInfoSoldier( PromotionCampPackInfo promotionCampPackInfo)
    {
        return getReadSqlSession().selectOne(PromotionDao.class.getName() + ".selectPromotionSamsungCampPackInfoSoldier", promotionCampPackInfo);
    }

    public PromotionCampPackInfo selectCampackInfo( PromotionCampPackInfo promotionCampPackInfo)
    {
        return getSqlSession().selectOne(PromotionDao.class.getName() + ".selectCampackInfo", promotionCampPackInfo);
    }

    public PromotionCampPackInfo selectPromotionCampPackInfo( PromotionCampPackInfo promotionCampPackInfo)
    {
        return getReadSqlSession().selectOne(PromotionDao.class.getName() + ".selectPromotionCampPackInfo", promotionCampPackInfo);
    }

    public PromotionCampPackInfo selectPromotionCampPackInfoSoldier( PromotionCampPackInfo promotionCampPackInfo)
    {
        return getReadSqlSession().selectOne(PromotionDao.class.getName() + ".selectPromotionCampPackInfoSoldier", promotionCampPackInfo);
    }



    public void insertPromotionCampPackInfo( PromotionCampPackInfo promotionCampPackInfo)
    {
        getSqlSession().selectOne(PromotionDao.class.getName() + ".insertPromotionCampPackInfo", promotionCampPackInfo);
    }

    public PromotionCampPackInfo CampackHistoryValidCheck(PromotionCampPackInfo promotionCampPackInfo)
    {
        /* 삼성 프로모션 테이블에서 이력 정보 확인 */
/*        PromotionCampPackInfo result =  selectPromotionSamsungCampPackInfo(promotionCampPackInfo);


        if( result != null)
        {
            logger.info("삼성 프로모션 테이블 신청자 이력 확인 [{}]", promotionCampPackInfo.getMdn());
            result.setResult(CampackResult.SUCCESS.getCode());
            result.setResult_msg("캠프팩 신청 정보가 존재 함");
            return result;
        }
*/

 /*       PromotionCampPackInfo  result =  selectPromotionSamsungCampPackInfoSoldier(promotionCampPackInfo);

        if( result != null)
        {
            logger.info("삼성 프로모션 테이블 군인 신청 이력 확인 [{}, {}, {}]", promotionCampPackInfo.getName(), promotionCampPackInfo.getEnter_date(), promotionCampPackInfo.getEnter_unit());
            result.setResult(CampackResult.OTHER_FOUND.getCode());
            result.setResult_msg("다른 가족이 신청한 이력이 존재 함.");
            return result;
        }*/

        /* 캠프팩 테이블에서 이력 정보 확인 */

        /*result = selectPromotionCampPackInfo(promotionCampPackInfo);
        if( result != null)
        {
            logger.info("캠프팩 테이블 신청 이력 확인 [{}]", promotionCampPackInfo.getMdn());
            result.setResult(CampackResult.SUCCESS.getCode());
            result.setResult_msg("캠프팩 신청 정보가 존재 함");
            return result;
        }*/

        PromotionCampPackInfo result = selectPromotionCampPackInfoSoldier(promotionCampPackInfo);
        if( result != null)
        {
            logger.info("캠프팩 테이블 군인 신청 이력 확인 [{}, {}, {}]", promotionCampPackInfo.getName(), promotionCampPackInfo.getEnter_date(), promotionCampPackInfo.getEnter_unit());
            result.setResult(CampackResult.OTHER_FOUND.getCode());
            result.setResult_msg("다른 가족이 신청한 이력이 존재 함.");
            return result;
        }



        return result;
    }

    public PromotionCampPackInfo CampackRegHistoryValidCheck(PromotionCampPackInfo promotionCampPackInfo)
    {
        /* 삼성 프로모션 테이블에서 이력 정보 확인 */
      /*  PromotionCampPackInfo result = selectPromotionSamsungCampPackInfo(promotionCampPackInfo);

        /*if( result != null)
        {
            logger.info("삼성 프로모션 테이블 신청자 이력 확인 [{}]", promotionCampPackInfo.getMdn());
            result.setResult(CampackRegResult.EXISTS.getCode());
            result.setResult_msg("신청 정보가 존재 함(신청자)");
            return result;
        }
*/
        PromotionCampPackInfo result =  selectPromotionSamsungCampPackInfoSoldier(promotionCampPackInfo);

        if( result != null)
        {
            logger.info("삼성 프로모션 테이블 군인 신청 이력 확인 [{}, {}, {}]", promotionCampPackInfo.getName(), promotionCampPackInfo.getEnter_date(), promotionCampPackInfo.getEnter_unit());
            result.setResult(CampackRegResult.SOLDIER_EXISTS.getCode());
            result.setResult_msg("신청 정보가 존재 함(군인).");
            return result;
        }

        /* 캠프팩 테이블에서 이력 정보 확인 */

        /*result = selectPromotionCampPackInfo(promotionCampPackInfo);
        if( result != null)
        {
            logger.info("캠프팩 테이블 신청 이력 확인 [{}]", promotionCampPackInfo.getMdn());
            result.setResult(CampackRegResult.EXISTS.getCode());
            result.setResult_msg("신청 정보가 존재 함(신청자)");
            return result;
        }*/

        result = selectPromotionCampPackInfoSoldier(promotionCampPackInfo);
        if( result != null)
        {
            logger.info("캠프팩 테이블 군인 신청 이력 확인 [{}, {}, {}]", promotionCampPackInfo.getName(), promotionCampPackInfo.getEnter_date(), promotionCampPackInfo.getEnter_unit());
            result.setResult(CampackRegResult.SOLDIER_EXISTS.getCode());
            result.setResult_msg("신청 정보가 존재 함(군인)");
            return result;
        }

        return result;
    }

}
