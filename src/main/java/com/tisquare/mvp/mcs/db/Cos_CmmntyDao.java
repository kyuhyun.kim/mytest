package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.CosCmmntyPenalty;
import com.tisquare.mvp.mcs.db.domain.Terms;
import com.tisquare.mvp.mcs.model.UserInfo;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class Cos_CmmntyDao {

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }
    public List<CosCmmntyPenalty> selectCosCmmntyPenalty(UserInfo userInfo){
        return getReadSqlSession().selectList(Cos_CmmntyDao.class.getName() + ".selectCosCmmntyPenalty", userInfo);
    }
}
