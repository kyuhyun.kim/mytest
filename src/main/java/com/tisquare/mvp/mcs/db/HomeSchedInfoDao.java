package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.model.BtInfo;
import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.HomeSched;
import com.tisquare.mvp.mcs.model.HomeSchedInfo;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class HomeSchedInfoDao {

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }


    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public Long selectDelHomeSchedInfo(HomeSchedInfo homeSchedInfo){

        return getReadSqlSession().selectOne(HomeSchedInfoDao.class.getName() + ".selectDelHomeSchedInfo", homeSchedInfo);
    }

    public Long selectUpdateHomeSchedInfo(HomeSchedInfo homeSchedInfo){

        return getReadSqlSession().selectOne(HomeSchedInfoDao.class.getName() + ".selectUpdateHomeSchedInfo", homeSchedInfo);
    }

    public Integer selectCntHomeSchedInfo( ClientAuthReqHeaders authReqHeaders){

        return getReadSqlSession().selectOne(HomeSchedInfoDao.class.getName() + ".selectCntHomeSchedInfo", authReqHeaders);
    }

    public void updateHomeSchedInfo(HomeSchedInfo homeSchedInfo){

        getSqlSession().update(HomeSchedInfoDao.class.getName() + ".updateHomeSchedInfo", homeSchedInfo);
    }

    public void deleteHomeSchedInfo(HomeSchedInfo homeSchedInfo){

        getSqlSession().delete(HomeSchedInfoDao.class.getName() + ".deleteHomeSchedInfo", homeSchedInfo);
    }

    public List<HomeSchedInfo> selectHomeSchedInfo(ClientAuthReqHeaders authReqHeaders){

        return getReadSqlSession().selectList(HomeSchedInfoDao.class.getName() + ".selectHomeSchedInfo", authReqHeaders);
    }

    public int selectHomeSchedInfoDuplicate(HomeSchedInfo homeSchedInfo){

        return getReadSqlSession().selectOne(HomeSchedInfoDao.class.getName() + ".selectHomeSchedInfoDuplicate", homeSchedInfo);
    }


    public void insertHomeSchedInfo(HomeSchedInfo homeSchedInfo){
        getSqlSession().insert(HomeSchedInfoDao.class.getName() + ".insertHomeSchedInfo", homeSchedInfo);
    }

    public void updateSeqHomeSchedInfo(HomeSchedInfo homeSchedInfo){

        getSqlSession().update(HomeSchedInfoDao.class.getName() + ".updateSeqHomeSchedInfo", homeSchedInfo);
    }
}
