package com.tisquare.mvp.mcs.db.domain;

public class ServerInfoRec {
	private String id;
	private String name;
	private String protocol;
	private String ipAddr;
	private Integer port;
	private String url;
	private String tag;
    private long diff_time;

    public void setDiff_time(long diff_time) {
        this.diff_time = diff_time;
    }

    public long getDiff_time() {
        return diff_time;
    }

    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	@Override
	public String toString() {
		return "ServerInfoRec [id=" + id + ", name=" + name + ", protocol="
				+ protocol + ", ipAddr=" + ipAddr + ", port=" + port + ", url="
				+ url + ", tag=" + tag + "]";
	}
}
