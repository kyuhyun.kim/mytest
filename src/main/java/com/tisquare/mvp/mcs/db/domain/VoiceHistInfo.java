package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.model.VoiceHist;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class VoiceHistInfo {

    @JsonProperty("sid")
    private Long sid;

    @JsonProperty("stime")
    private Long stime;

    @JsonProperty("etime")
    private Long etime;

    @JsonProperty("recordfile-list")
    private List<VoiceHistFileInfo> voiceHistFileInfoList;

    public VoiceHistInfo(VoiceHist voiceHist)
    {
        this.sid = voiceHist.getSid();
        this.stime = voiceHist.getStime();
        this.etime = voiceHist.getEtime();
    }

    @JsonProperty("zip-download")
    private String zip_download;

    @JsonProperty("zip-size")
    private Long zip_size;

    public void setZip_size(Long zip_size) {
        this.zip_size = zip_size;
    }

    public Long getZip_size() {
        return zip_size;
    }

    public void setZip_download(String zip_download) {
        this.zip_download = zip_download;
    }

    public String getZip_download() {
        return zip_download;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public void setStime(Long stime) {
        this.stime = stime;
    }

    public void setEtime(Long etime) {
        this.etime = etime;
    }

    public void setVoiceHistFileInfoList(List<VoiceHistFileInfo> voiceHistFileInfoList) {
        this.voiceHistFileInfoList = voiceHistFileInfoList;
    }

    public Long getSid() {
        return sid;
    }

    public Long getStime() {
        return stime;
    }

    public Long getEtime() {
        return etime;
    }

    public List<VoiceHistFileInfo> getVoiceHistFileInfoList() {
        return voiceHistFileInfoList;
    }
}