package com.tisquare.mvp.mcs.db.domain;

import com.tisquare.mvp.mcs.model.*;

/**
 * Created by hsna on 14. 8. 7.
 * SKT ICAS 시스템으로 부터 고객정보 및 에러코드를 전달하는 클래스
 */
public class LoginUserinfo {


    long iuid;
    String phone_no;
    String app_version;
    String telecom_code;
    String os_type;
    String device_model;

    public LoginUserinfo(CertBody certBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        this.iuid = certBody.getProfile().getIuid();
        this.phone_no = authReqHeaders.getPhoneNo();
        this.app_version = authReqHeaders.getAppVersion();
        this.telecom_code = authReqParams.getTelecomCode();
        this.os_type = authReqParams.getOsType();
        this.device_model = authReqParams.getDeviceModel();

    }

    public LoginUserinfo(BssCertBody certBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        this.iuid = certBody.getProfile().getIuid();
        this.phone_no = authReqHeaders.getPhoneNo();
        this.app_version = authReqHeaders.getAppVersion();
        this.telecom_code = authReqParams.getTelecomCode();
        this.os_type = authReqParams.getOsType();
        this.device_model = authReqParams.getDeviceModel();

    }

    public LoginUserinfo(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        this.iuid = authInfoBody.getProfile().getIuid();
        this.phone_no = authReqHeaders.getPhoneNo();
        this.app_version = authReqHeaders.getAppVersion();
        this.telecom_code = authReqParams.getTelecomCode();
        this.os_type = authReqParams.getOsType();
        this.device_model = authReqParams.getDeviceModel();

    }


    @Override
    public String toString() {
        return "LoginUserinfo{" +
                ", iuid='" + iuid + '\'' +
                ", phone_no='" + phone_no + '\'' +
                ", app_version='" + app_version + '\'' +
                ", telecom_code='" + telecom_code +'\'' +
                ", os_type='" + os_type +'\'' +
                ", device_model='" + device_model +'\'' +
                '}';
    }
}
