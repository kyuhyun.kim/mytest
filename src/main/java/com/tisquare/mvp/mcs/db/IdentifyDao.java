package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.db.domain.NiceInfo;
import com.tisquare.mvp.mcs.model.BtInfo;
import com.tisquare.mvp.mcs.model.UserInfo;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class IdentifyDao {

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    @Qualifier("factory1")
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public NiceInfo selectNiceInfo(UserInfo userinfo){
        return getReadSqlSession().selectOne(IdentifyDao.class.getName() + ".selectNiceInfo", userinfo);
    }

    public void insertNiceInfo(Long iuid, String enc_data){

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("iuid", iuid);
        map.put("result", 0);
        map.put("enc_data", enc_data);
        getSqlSession().insert(IdentifyDao.class.getName() + ".insertNiceInfo", map);
    }

    public void deleteNiceInfo(UserInfo userinfo){
        getSqlSession().delete(IdentifyDao.class.getName() + ".deleteNiceInfo", userinfo);
    }

    public void updateSuccNiceStat(){
        getSqlSession().update(IdentifyDao.class.getName() + ".updateSuccNiceStat");
    }

    public void updateFailNiceStat(){
        getSqlSession().update(IdentifyDao.class.getName() + ".updateFailNiceStat");
    }
}
