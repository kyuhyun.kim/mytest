package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.utils.DateUtil;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class VoiceHistFileInfo {

    @JsonProperty("sid")
    private Long sid;

    @JsonProperty("iuid")
    private Long iuid;

    @JsonProperty("download")
    private String download;

    @JsonProperty("mdn")
    private String mdn;

    @JsonProperty("seq")
    private Long seq;

    @JsonProperty("length")
    private Long length;

    @JsonProperty("cri-time")
    private Long crt_time;

    @JsonProperty("conference-id")
    private Long conference_id;

    @JsonProperty("start-time")
    private String start_time;

    @JsonProperty("stop-time")
    private String stop_time;

    @JsonProperty("duration-time")
    private Long duration_time;

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public void setStop_time(String stop_time) {
        this.stop_time = stop_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getStop_time() {
        return stop_time;
    }

    public void setDuration_time(Long duration_time) {
        this.duration_time = duration_time;
    }

    public String getMdn() {
        return mdn;
    }

    public Long getDuration_time() {
        return duration_time;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }

    public Long getSeq() {
        return seq;
    }

    public void setCrt_time(Long crt_time) {
        this.crt_time = crt_time;
    }

    public Long getCrt_time() {
        return crt_time;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public void setConference_id(Long conference_id) {
        this.conference_id = conference_id;
    }

    public Long getLength() {
        return length;
    }

    public Long getConference_id() {
        return conference_id;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public Long getSid() {
        return sid;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getDownload() {
        return download;
    }

    @Override
    public String toString() {
        if(this == null)
            return null;
        final StringBuilder sb = new StringBuilder("VoiceHistInfo:{");
        sb.append("sid='").append(this.sid).append('\'');
        sb.append("iuid='").append(this.iuid).append('\'');
        sb.append(", download='").append(this.download).append('\'');
        sb.append('}');
        return sb.toString();
    }
}