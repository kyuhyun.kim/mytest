package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kkh on 16. 03. 02.
 */
public class MasTrainee {

    private long iuid;
    private String tr_name;
    private String tr_entrance;
    private String tr_birth;
    private String tr_entrance_day;
    private String tr_edu_unit;
    private Long tr_edu_start_day;
    private Long tr_edu_end_day;
    private String tr_sh_edu_unit;
    private Long tr_sh_edu_start_day;
    private Long tr_sh_edu_start_end;
    private String tr_arr_unit;
    private Long tr_arr_day;

    private Long curr_date;

    public void setCurr_date(Long curr_date) {
        this.curr_date = curr_date;
    }

    public Long getCurr_date() {
        return curr_date;
    }

    public void setTr_edu_start_day(Long tr_edu_start_day) {
        this.tr_edu_start_day = tr_edu_start_day;
    }

    public void setTr_edu_end_day(Long tr_edu_end_day) {
        this.tr_edu_end_day = tr_edu_end_day;
    }

    public void setTr_sh_edu_unit(String tr_sh_edu_unit) {
        this.tr_sh_edu_unit = tr_sh_edu_unit;
    }

    public void setTr_sh_edu_start_day(Long tr_sh_edu_start_day) {
        this.tr_sh_edu_start_day = tr_sh_edu_start_day;
    }

    public void setTr_sh_edu_start_end(Long tr_sh_edu_start_end) {
        this.tr_sh_edu_start_end = tr_sh_edu_start_end;
    }

    public void setTr_arr_unit(String tr_arr_unit) {
        this.tr_arr_unit = tr_arr_unit;
    }

    public void setTr_arr_day(Long tr_arr_day) {
        this.tr_arr_day = tr_arr_day;
    }

    public Long getTr_edu_start_day() {
        return tr_edu_start_day;
    }

    public Long getTr_edu_end_day() {
        return tr_edu_end_day;
    }

    public String getTr_sh_edu_unit() {
        return tr_sh_edu_unit;
    }

    public Long getTr_sh_edu_start_day() {
        return tr_sh_edu_start_day;
    }

    public Long getTr_sh_edu_start_end() {
        return tr_sh_edu_start_end;
    }

    public String getTr_arr_unit() {
        return tr_arr_unit;
    }

    public Long getTr_arr_day() {
        return tr_arr_day;
    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public void setTr_name(String tr_name) {
        this.tr_name = tr_name;
    }

    public void setTr_entrance(String tr_entrance) {
        this.tr_entrance = tr_entrance;
    }

    public void setTr_birth(String tr_birth) {
        this.tr_birth = tr_birth;
    }

    public void setTr_entrance_day(String tr_entrance_day) {
        this.tr_entrance_day = tr_entrance_day;
    }

    public void setTr_edu_unit(String tr_edu_unit) {
        this.tr_edu_unit = tr_edu_unit;
    }

    public long getIuid() {
        return iuid;
    }

    public String getTr_name() {
        return tr_name;
    }

    public String getTr_entrance() {
        return tr_entrance;
    }

    public String getTr_birth() {
        return tr_birth;
    }

    public String getTr_entrance_day() {
        return tr_entrance_day;
    }

    public String getTr_edu_unit() {
        return tr_edu_unit;
    }
}
