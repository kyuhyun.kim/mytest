package com.tisquare.mvp.mcs.db;


import com.tisquare.mvp.mcs.model.BtInfo;
import com.tisquare.mvp.mcs.model.CrashLog;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jhkim on 14. 11. 10.
 */
@Repository
public class BtinfoDao {

    private SqlSession sqlSession;
    private boolean externalSqlSession;

    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSession){
            this.sqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession(){
        return this.sqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }


    public List<BtInfo> selectBTinfo(Integer store_type, String pre_url){

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("store_type", store_type);
        map.put("pre_url", pre_url);
        return getReadSqlSession().selectList(BtinfoDao.class.getName() + ".selectBTinfo", map);
    }
}
