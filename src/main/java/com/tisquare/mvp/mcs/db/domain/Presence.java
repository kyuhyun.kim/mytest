package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kkh on 16. 03. 02.
 */
public class Presence {


    @JsonProperty("iuid")
    long iuid;

    @JsonProperty("presence")
    int presence;

    @JsonIgnore
    String cAuthKey;

    public Presence(long iuid, String cAuthKey, int presence)
    {
        this.iuid = iuid;
        this.presence = presence;
        this.cAuthKey = cAuthKey;
    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public void setPresence(int presence) {
        this.presence = presence;
    }

    public void setcAuthKey(String cAuthKey) {
        this.cAuthKey = cAuthKey;
    }

    public long getIuid() {
        return iuid;
    }

    public int getPresence() {
        return presence;
    }

    public String getcAuthKey() {
        return cAuthKey;
    }
}
