package com.tisquare.mvp.mcs.db;

import com.tisquare.mvp.mcs.db.domain.SnsStat;
import com.tisquare.mvp.mcs.db.domain.UrlMap;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Repository;

/**
 * Created by asus on 2015-03-02.
 */
@Repository
public class UrlMapDao extends DaoSupport{

    private SqlSession uSqlSession;

    private boolean externalTSqlSession;

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {

    }

    @Autowired
    @Qualifier("factory1")
    public void setDaoSqlSession(SqlSessionFactory sqlSessionFactory){
        if(!this.externalTSqlSession){
            this.uSqlSession = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getSqlSession() {
        return this.uSqlSession;
    }

    private SqlSession sqlSessionRead;
    private boolean externalSqlSessionRead;

    @Autowired
    @Qualifier("factory2")
    public void setReadSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        if(!this.externalSqlSessionRead){
            this.sqlSessionRead = new SqlSessionTemplate(sqlSessionFactory);
        }
    }

    protected final SqlSession getReadSqlSession(){
        return this.sqlSessionRead;
    }

    public Boolean existUrlCheck(String redirectUrl){
        String value = getReadSqlSession().selectOne(UrlMapDao.class.getName() + ".existUrlCheck", redirectUrl);
        if(value != null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public void updateUrlMap(UrlMap data) throws Exception{
        getSqlSession().update(UrlMapDao.class.getName() + ".updateUrlMap", data);
    }

    public void insertUrlMap(UrlMap data) throws Exception{
        getSqlSession().insert(UrlMapDao.class.getName() + ".insertUrlMap", data);
    }

    public UrlMap selectTargetUrl(String shortUrl) throws Exception{
        return getSqlSession().selectOne(UrlMapDao.class.getName() + ".targetUrl", shortUrl);
    }

    public int deleteExpiredUrlMap(String currentTime) throws Exception{
        return getSqlSession().delete(UrlMapDao.class.getName() + ".deleteExpiredUrlMap", currentTime);
    }

    public void updateSnsStat(SnsStat snsStat) throws Exception{
        getSqlSession().update(UrlMapDao.class.getName() + ".updateSnsStat", snsStat);
    }

}
