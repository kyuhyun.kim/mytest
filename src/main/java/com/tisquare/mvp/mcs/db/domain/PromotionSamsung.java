package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by jhkim on 14. 11. 10.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PromotionSamsung {

    @JsonIgnore
    private String phone_no;

    @JsonProperty("rel-phone-no")
    private String rel_phone_no;

    @JsonProperty("rel-iuid")
    private Long rel_iuid;

    @JsonProperty("relationship")
    private String relationship;

    @JsonIgnore
    private Integer user_type;

    @JsonProperty("name")
    private String rel_name;

    @JsonProperty("reg-date")
    private String reg_date;

    @JsonProperty("rel-addr")
    private String rel_addr;

    @JsonProperty("rel-addr-no")
    private String rel_addr_no;

    @JsonProperty("result")
    private Integer result;

    @JsonIgnore
    private Long iuid;

    @JsonIgnore
    private String s_id;

    @JsonIgnore
    private Long birth;

    @JsonIgnore
    private Integer gender;

    @JsonIgnore
    private String name;

    @JsonIgnore
    private Long rel_birth;

    @JsonIgnore
    private int gift_yn;

    @JsonIgnore
    private Integer rel_gender;

    @JsonIgnore
    private Long enter_date;

    @JsonIgnore
    private int ad_yn;

    @JsonIgnore
    private int rel_ad_yn;

    @JsonIgnore
    private int insurance;

    public void setInsurance(int insurance) {
        this.insurance = insurance;
    }

    public int getInsurance() {
        return insurance;
    }

    public void setRel_addr_no(String rel_addr_no) {
        this.rel_addr_no = rel_addr_no;
    }

    public String getRel_addr_no() {
        return rel_addr_no;
    }

    public void setRel_addr(String rel_addr) {
        this.rel_addr = rel_addr;
    }

    public String getRel_addr() {
        return rel_addr;
    }

    public void setRel_ad_yn(int rel_ad_yn) {
        this.rel_ad_yn = rel_ad_yn;
    }

    public int getRel_ad_yn() {
        return rel_ad_yn;
    }

    public void setEnter_date(Long enter_date) {
        this.enter_date = enter_date;
    }

    public void setAd_yn(int ad_yn) {
        this.ad_yn = ad_yn;
    }

    public Long getEnter_date() {
        return enter_date;
    }

    public int getAd_yn() {
        return ad_yn;
    }

    public void setRel_gender(Integer rel_gender) {
        this.rel_gender = rel_gender;
    }

    public Integer getRel_gender() {
        return rel_gender;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setGift_yn(int gift_yn) {
        this.gift_yn = gift_yn;
    }

    public int getGift_yn() {
        return gift_yn;
    }

    public void setRel_birth(Long rel_birth) {
        this.rel_birth = rel_birth;
    }

    public void setRel_name(String rel_name) {
        this.rel_name = rel_name;
    }

    public String getRel_name() {
        return rel_name;
    }

    public Long getRel_birth() {
        return rel_birth;
    }

    public void setBirth(Long birth) {
        this.birth = birth;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getGender() {
        return gender;
    }

    public Long getBirth() {
        return birth;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    public Integer getUser_type() {
        return user_type;
    }

    public void setRel_iuid(Long rel_iuid) {
        this.rel_iuid = rel_iuid;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public Long getRel_iuid() {
        return rel_iuid;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setRel_phone_no(String rel_phone_no) {
        this.rel_phone_no = rel_phone_no;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getRel_phone_no() {
        return rel_phone_no;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Integer getResult() {
        return result;
    }
}
