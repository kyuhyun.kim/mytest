package com.tisquare.mvp.mcs.db.domain;

/**
 * Created by asus on 2015-03-02.
 */
public class HelpNoticeCnt {

    int cnt;

    int max_cnt;

    int min_cnt;


    public int getCnt() {
        return cnt;
    }

    public int getMax_cnt() {
        return max_cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public void setMax_cnt(int max_cnt) {
        this.max_cnt = max_cnt;
    }

    public int getMin_cnt() {
        return min_cnt;
    }

    public void setMin_cnt(int min_cnt) {
        this.min_cnt = min_cnt;
    }

    @Override
    public String toString() {
        return "HelpNoticeCnt{" +
                "cnt='" + cnt + '\'' +
                ", max_cnt='" + max_cnt + '\'' +
                ", min_cnt='" + min_cnt + '\'' +
                '}';
    }
}
