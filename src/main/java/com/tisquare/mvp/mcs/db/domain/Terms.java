package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kkh on 16. 03. 02.
 */
public class Terms {


    @JsonProperty("name")
    private String name;

    @JsonProperty("value")
    private int value;

    @JsonProperty("chg-date")
    private String chg_date;

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void setChg_date(String chg_date) {
        this.chg_date = chg_date;
    }

    public String getChg_date() {
        return chg_date;
    }
}
