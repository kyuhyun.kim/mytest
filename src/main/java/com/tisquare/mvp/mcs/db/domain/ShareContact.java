package com.tisquare.mvp.mcs.db.domain;

import com.tisquare.mvp.mcs.model.StatLog;

/**
 * Created by jhkim on 14. 3. 21.
 */
public class ShareContact {
    String year;
    String month;
    String day;
    String hour;
    String minute;
    int used_cnt;

    public ShareContact(){}
    public ShareContact(StatLog node){
        this.year = node.getYear();
        this.month = node.getMonth();
        this.day = node.getDay();
        this.hour = node.getHour();
        this.minute = node.getMin();
        this.used_cnt = node.getUsed_cnt();
    }

    public String getYear() { return year; }
    public void setYear(String year) { this.year = year; }
    public String getMonth() { return month; }
    public void setMonth(String month) { this.month = month; }
    public String getDay() { return day; }
    public void setDay(String day) { this.day = day; }
    public String getHour() { return hour; }
    public void setHour(String hour) { this.hour = hour; }
    public String getMinute() { return minute; }
    public void setMinute(String minute) { this.minute = minute; }
    public int getUsed_cnt() { return used_cnt; }
    public void setUsed_cnt(int used_cnt) { this.used_cnt = used_cnt; }
}
