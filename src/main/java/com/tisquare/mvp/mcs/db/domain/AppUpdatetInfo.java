package com.tisquare.mvp.mcs.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AppUpdatetInfo {

    private  static String DEF_OS_TYPE_STR = "android";
    public static final int FORCE_UPDATE = 1; //강제 업데이트

    @JsonProperty("store-type")
    private Integer storeType;

    @JsonProperty("os-type")
    private String osType;

    @JsonProperty("app-version")
    private String appVersion;

    @JsonProperty("download-url")
    private String downloadUrl;

    @JsonProperty("description")
    private String description;

    @JsonProperty("force-update")
    private Integer forceUpdate;

    @JsonIgnore
    private String lowestVersion;

    @JsonIgnore
    private String regDate;

    @JsonIgnore
    private String uptDate;


    @JsonIgnore
    private int substype;


    public AppUpdatetInfo() {
    }

    public void setSubstype(int substype) {
        this.substype = substype;
    }

    public int getSubstype() {
        return substype;
    }

    public static void setDefOsTypeStr(String defOsTypeStr) {
        DEF_OS_TYPE_STR = defOsTypeStr;
    }

    public void setStoreType(Integer storeType) {
        this.storeType = storeType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setForceUpdate(Integer forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public void setLowestVersion(String lowestVersion) {
        this.lowestVersion = lowestVersion;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public void setUptDate(String uptDate) {
        this.uptDate = uptDate;
    }

    public static String getDefOsTypeStr() {

        return DEF_OS_TYPE_STR;
    }

    public Integer getStoreType() {
        return storeType;
    }

    public String getOsType() {
        return osType;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getDescription() {
        return description;
    }


    public Integer getForceUpdate() {
        return forceUpdate;
    }

    public String getLowestVersion() {
        return lowestVersion;
    }

    public String getRegDate() {
        return regDate;
    }

    public String getUptDate() {
        return uptDate;
    }

    public AppUpdatetInfo(Integer appMarketType, int substype) {
        this();
        this.setStoreType(appMarketType);
        this.substype = substype;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("event-notice:{");
        sb.append("store-type='").append(storeType).append('\'');
        sb.append(", os-type='").append(osType).append('\'');
        sb.append(", app-version='").append(appVersion).append('\'');
        sb.append('}');
        return sb.toString();
    }


    public boolean needForceUpdate(String appVersion) {
        DefaultArtifactVersion clientVersion = new DefaultArtifactVersion(appVersion);

        if (this.lowestVersion != null) {
            DefaultArtifactVersion minVersion = new DefaultArtifactVersion(this.lowestVersion);

            /**
             * a.compareTo(b) < 0 : a<b, >0 : a>b, 0 : a=b
             */
            if (clientVersion.compareTo(minVersion) < 0)
                return true;
        }

        if (this.forceUpdate.intValue() == 1) {
            DefaultArtifactVersion maxVersion = new DefaultArtifactVersion(this.appVersion);

            /**
             * a.compareTo(b) < 0 : a<b, >0 : a>b, 0 : a=b
             */
            if (clientVersion.compareTo(maxVersion) < 0)
                return true;
        }


        return false;
    }

    public boolean needUpdateIs(String appVersion) {
        DefaultArtifactVersion clientVersion = new DefaultArtifactVersion(appVersion);

        DefaultArtifactVersion maxVersion = new DefaultArtifactVersion(this.appVersion);



            /**
             * a.compareTo(b) < 0 : a<b, >0 : a>b, 0 : a=b
             */
        int a = clientVersion.compareTo(maxVersion);
        if (clientVersion.compareTo(maxVersion) >= 0)
            return true;


        return false;
    }
}
