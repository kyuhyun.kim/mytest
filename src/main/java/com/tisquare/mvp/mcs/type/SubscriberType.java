package com.tisquare.mvp.mcs.type;

public enum SubscriberType {

    PAID_REGULAR_MEMBER(1),
	FREE_REGULAR_MEMBER(2),
	ASSOCIATE_MEMBER(3),
	COMMANDER(4),
    B2B_ADMIN(5);
	
	private final int value;
	private SubscriberType(int value) { this.value = value; }

	public int value() {
		return this.value;
	}
	
	public String toString() {
		return Integer.toString(value);
	}
	
	public static SubscriberType fromInt(int x) {
        switch(x) {
        case 1:
            return PAID_REGULAR_MEMBER;
        case 2:
            return FREE_REGULAR_MEMBER;
        case 3:
        	return ASSOCIATE_MEMBER;
        case 4:
        	return COMMANDER;
        case 5:
            return B2B_ADMIN;
        }
        return null;
    }
}