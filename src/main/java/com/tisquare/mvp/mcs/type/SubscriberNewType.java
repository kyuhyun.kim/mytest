package com.tisquare.mvp.mcs.type;

public enum SubscriberNewType {

    IS_SUB_NEW(0),            //신규 서비스 가입자
    IS_SUB_REINSTALL(1);     //재 설치

	private final int value;
	private SubscriberNewType(int value) { this.value = value; }

	public int value() {
		return this.value;
	}
	
	public String toString() {
		return Integer.toString(value);
	}

}