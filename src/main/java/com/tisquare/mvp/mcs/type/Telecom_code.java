package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public class Telecom_code {

    public static final String TELECOM_SKT = "45005";
    public static final String TELECOM_KT = "45008";
    public static final String TELECOM_LGT = "45006";
    public static final String TELECOM_CODE = "450";

}
