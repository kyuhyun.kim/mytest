package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Cp_status {

    DEFAULT(0),
    REG(1),
    CONTACT_ING(2),
    USE(3),
    USE_EXPIERD(4),
    DELETE(9);

    private Integer code;

    Cp_status(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
