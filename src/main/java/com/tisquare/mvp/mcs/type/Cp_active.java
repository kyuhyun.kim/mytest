package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Cp_active {

    IMACTIVE(0),
    ACTIVE(1);

    private Integer code;

    Cp_active(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
