package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Login_status {

    LOGIN(0),
    LOGOUT(1);
    private Integer code;

    Login_status(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
