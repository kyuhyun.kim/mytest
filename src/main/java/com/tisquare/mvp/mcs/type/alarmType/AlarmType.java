package com.tisquare.mvp.mcs.type.alarmType;

/**
 * 프로필 부분 편집 요청을 식별하기 위한 타입
 *
 * @author leejs
 * @since 2015-03-08
 */
public enum AlarmType {
    CHG_MDN("chgmdn");


    private String code;

    AlarmType(String code) {
        this.code = code;
    }

    public static AlarmType getProfileType(String code) {
        for (AlarmType type : values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        throw new IllegalArgumentException(code);
    }

    public String getCode() {
        return code;
    }
}
