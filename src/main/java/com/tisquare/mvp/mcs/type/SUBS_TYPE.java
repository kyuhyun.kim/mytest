package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum SUBS_TYPE {

    B2C(0),
    B2B(1),
    B2B_RENT(2),
    B2B_IOT(3),
    COMMANDER(4),
    B2B_ADMIN(5);
    private Integer code;

    SUBS_TYPE(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
