package com.tisquare.mvp.mcs.type.pc;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum SenderType {

    DEFAULT(0),
    SMS(1),
    EMAIL(2);


    private Integer code;

    SenderType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
