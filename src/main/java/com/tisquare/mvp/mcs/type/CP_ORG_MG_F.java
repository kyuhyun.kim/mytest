package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum CP_ORG_MG_F {

    DEFAULT(0),
    NOT_REG(1),
    REG(2);

    private Integer code;

    CP_ORG_MG_F(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
