package com.tisquare.mvp.mcs.type.mas;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Advance_type {

    DEFAULT(0),
    이병(1),
    일병(2),
    상병(3),
    병장(4),
    하사(5),
    중사(6),
    상사(7),
    원사(8),
    준위(9),
    소위(10),
    중위(11),
    대위(12),
    소령(13),
    중령(14),
    대령(15),
    준장(16),
    소장(17),
    중장(18),
    대장(19);


    private Integer code;

    Advance_type(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
