package com.tisquare.mvp.mcs.type.mas;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Sched_type {

    DEFAULT(0),
    전역(1),
    진급(2),
    정기휴가(3),
    포상휴가(4),
    외박(5),
    외출(6),
    면회(7),
    기타(8);

    private Integer code;

    Sched_type(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
