package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum UserSearchType {

    NICKNAME(0),
    CALL_NUMBER(1),
    E_164(2);

    private Integer code;

    UserSearchType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
