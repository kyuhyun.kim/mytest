package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum USER_TYPE {

    NORMAL(0),
    MILITARY(1),
    ETC(2);
    private Integer code;

    USER_TYPE(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
