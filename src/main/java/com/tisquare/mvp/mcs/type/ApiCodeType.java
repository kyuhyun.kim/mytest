package com.tisquare.mvp.mcs.type;

import org.apache.http.HttpStatus;

public class ApiCodeType {

    public final static String 로그인_URL = "/acs/authinfo";
    public final static String 로그아웃_URL = "/acs/logout";
    public final static String ID찾기_URL = "/acs/0301/id";
    public final static String 계정_E_MAIL_휴대폰_번호_인증_URL = "/acs/passwd/auth";
    public final static String 계정_비밀번호_변경_URL = "/acs/passwd";
    public final static String 본인인증요청_URL = "/acs/Identify";
    public final static String 본인인증확인_URL = "/acs/Identify_check";
    public final static String 서비스_공지사항_리스트_요청_URL = "/acs/notice/list";
    public final static String 서비스_공지_사항_요청_URL = "/acs/notice/sid";
    public final static String 전체_도움말_리스트_요청_URL = "/acs/help/list";
    public final static String 도움말_요청_URL = "/acs/help/hid";
    public final static String 단말_로그_수집_URL = "/acs/log/crash";
    public final static String 삼성생명_보험가족_등록_URL = "/acs/promotion/samsung/reg";
    public final static String 삼성생명_보험가족_조회_URL = "/acs/promotion/samsung/info";
    public final static String 삼성생명_보험_OEM_주소록_가입자_조회_URL = "/acs/promotion/samsung/oem";
    public final static String 홈_일정_대상_등록_URL = "/acs/home/sched/reg";
    public final static String 알림_설정_관리_URL = "/acs/setup/alarm";
    public final static String 로그인_정보_조회_URL = "/acs/mvpinfo";
    public final static String 사용단말_번호변경_URL = "/acs/chg_mdn";
    public final static String 홈화면_대상자_조회_URL = "/acs/0701/home/sched/info";
    public final static String 홈화면_대상자_등록_URL = "/acs/0702/home/sched/reg";
    public final static String 홈화면_대상자_편집_URL = "/acs/0703/home/sched/edit";
    public final static String 홈화면_대상자_삭제_URL = "/acs/0704/home/sched/del";
    public final static String 홈화면_대상자_재정렬_URL = "/acs/0704/home/sched/seq";
    public final static String 일반인_캠프팩_등록_조회_URL = "/acs/promotion/campack/info";
    public final static String 일반인_캠프팩_등록_URL = "/acs/promotion/campack/reg";

    public final static String 로그인 = "ACS0000";
    public final static String 로그아웃 = "ACS0001";
    public final static String 로그인_정보_조회 = "ACS0002";
    public final static String ID찾기 = "ACS0301";
    public final static String 계정_E_MAIL_휴대폰_번호_인증 = "ACS0302";
    public final static String 계정_비밀번호_변경 = "ACS0303";
    public final static String 사용_단말_번호_변경 = "ACS0304";
    public final static String 본인인증요청 = "ACS0501";
    public final static String 본인인증확인 = "ACS0502";
    public final static String 서비스_공지사항_리스트_요청 = "ACS0101";
    public final static String 서비스_공지_사항_요청 = "ACS0102";
    public final static String 전체_도움말_리스트_요청 = "ACS0103";
    public final static String 도움말_요청 = "ACS0104";
    public final static String 단말_로그_수집 = "ACS0201";
    public final static String 삼성생명_보험가족_등록 = "ACS0401";
    public final static String 삼성생명_보험가족_조회 = "ACS0403";
    public final static String 삼성생명_보험_OEM_주소록_가입자_조회 = "ACS0404";
    public final static String 일반인_캠프팩_등록_조회 = "ACS0406";
    public final static String 일반인_캠프팩_등록 = "ACS0407";
    public final static String 홈_일정_대상_등록 = "ACS0601";
    public final static String 알림_설정_관리 = "ACS0602";
    public final static String 홈화면_대상자_조회 = "ACS0701";
    public final static String 홈화면_대상자_등록 = "ACS0702";
    public final static String 홈화면_대상자_편집 = "ACS0703";
    public final static String 홈화면_대상자_삭제 = "ACS0704";
    public final static String 홈화면_대상자_재정렬 = "ACS0705";





    public final static String DESC_200 = "성공";
    public final static String DESC_400 = "요청오류";
    public final static String DESC_403 = "권한없음";
    public final static String DESC_404 = "가입자 없음";
    public final static String DESC_405 = "가입정보 불일치";
    public final static String DESC_요청자_군인아님_405 = "요청자 군인아님";
    public final static String DESC_가족관계_설정_가입자_없음_405 = "가족관계 설정 가입자 없음";
    public final static String DESC_일정_등_대상자_없음_405 = "일정 등록 대상 가입자 없음";
    public final static String DESC_406 = "App 강제 업데이트";
    public final static String DESC_429 = "요청 과부하";
    public final static String DESC_500 = "서버오류";
    public final static String DESC_503 = "시스템 작업";


    public final static int STATUS_200 = 200;
    public final static int STATUS_400 = 400;
    public final static int STATUS_403 = 403;
    public final static int STATUS_404 = 404;
    public final static int STATUS_405 = 405;
    public final static int STATUS_406 = 406;
    public final static int STATUS_429 = 429;
    public final static int STATUS_500 = 500;
    public final static int STATUS_503 = 503;



    public static String getApiCode(String ApiName)
    {
        switch (ApiName)
        {
            case 로그인_URL:
                return 로그인;
            case  로그아웃_URL:
                return 로그아웃;
            case  계정_E_MAIL_휴대폰_번호_인증_URL:
                return 계정_E_MAIL_휴대폰_번호_인증;
            case  계정_비밀번호_변경_URL:
                return 계정_비밀번호_변경;
            case  본인인증요청_URL:
                return 본인인증요청;
            case  본인인증확인_URL:
                return 본인인증확인;
            case  서비스_공지사항_리스트_요청_URL:
                return 서비스_공지사항_리스트_요청;
            case  서비스_공지_사항_요청_URL:
                return 서비스_공지_사항_요청;
            case  전체_도움말_리스트_요청_URL:
                return 전체_도움말_리스트_요청;
            case  도움말_요청_URL:
                return 도움말_요청;
            case  단말_로그_수집_URL:
                return 단말_로그_수집;
            case  삼성생명_보험가족_등록_URL:
                return 삼성생명_보험가족_등록;
            case  삼성생명_보험가족_조회_URL:
                return 삼성생명_보험가족_조회;
            case  삼성생명_보험_OEM_주소록_가입자_조회_URL:
                return 삼성생명_보험_OEM_주소록_가입자_조회;
            case  홈_일정_대상_등록_URL:
                return 홈_일정_대상_등록;
            case  알림_설정_관리_URL:
                return 알림_설정_관리;
            case  로그인_정보_조회_URL:
                return 로그인_정보_조회;
            case  사용단말_번호변경_URL:
                return 사용_단말_번호_변경;
            case ID찾기_URL:
                return ID찾기;
            case 홈화면_대상자_조회_URL:
                return 홈화면_대상자_조회;
            case 홈화면_대상자_등록_URL:
                return 홈화면_대상자_등록;
            case 홈화면_대상자_편집_URL:
                return 홈화면_대상자_편집;
            case 홈화면_대상자_삭제_URL:
                return 홈화면_대상자_삭제;
            case 홈화면_대상자_재정렬_URL:
                return 홈화면_대상자_재정렬;
            case 일반인_캠프팩_등록_URL:
                return 일반인_캠프팩_등록;
            case 일반인_캠프팩_등록_조회_URL:
                return 일반인_캠프팩_등록_조회;
        }

        return null;
    }

    public static String getSatusDesc(String ApiName, int status)
    {
        switch (status)
        {
            case HttpStatus.SC_OK:
                return DESC_200;
            case HttpStatus.SC_BAD_GATEWAY:
                return DESC_400;
            case HttpStatus.SC_FORBIDDEN:
                return DESC_403;
            case HttpStatus.SC_NOT_FOUND:
                return DESC_404;
            case HttpStatus.SC_METHOD_NOT_ALLOWED:
                if(org.apache.commons.lang3.StringUtils.equals(ApiName, 삼성생명_보험가족_등록_URL))
                    return DESC_요청자_군인아님_405;
                else if(org.apache.commons.lang3.StringUtils.equals(ApiName, 삼성생명_보험가족_조회_URL))
                    return DESC_가족관계_설정_가입자_없음_405;
                else if(org.apache.commons.lang3.StringUtils.equals(ApiName, 삼성생명_보험_OEM_주소록_가입자_조회_URL))
                    return DESC_요청자_군인아님_405;
                else if(org.apache.commons.lang3.StringUtils.equals(ApiName, 홈_일정_대상_등록_URL))
                    return DESC_일정_등_대상자_없음_405;
                else if(org.apache.commons.lang3.StringUtils.equals(ApiName,알림_설정_관리_URL ))
                    return DESC_일정_등_대상자_없음_405;
                else
                    return DESC_405;
            case HttpStatus.SC_NOT_ACCEPTABLE:
                return DESC_406;
            case 429:
                return DESC_429;
            case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                return DESC_500;
            case HttpStatus.SC_SERVICE_UNAVAILABLE:
                return DESC_503;
        }
        return null;
    }

    public static String getResultMsg(String ApiName, int status) {
        return getSatusDesc(ApiName, status);
    }
}
