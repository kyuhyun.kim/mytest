package com.tisquare.mvp.mcs.type;

public enum ClientType {
    INDIVIDUAL_CLIENT(0),
    FIRST_RESERVE(1),
    SOLDIER(2);
	
	private final int value;
	private ClientType(int value) { this.value = value; }

	public int value() {
		return this.value;
	}
	
	public String toString() {
		return Integer.toString(value);
	}
	
	public static ClientType fromInt(int x) {
        switch(x) {
        case 0:
            return INDIVIDUAL_CLIENT;
        case 1:
            return FIRST_RESERVE;
        case 2:
            return SOLDIER;
        }
        return null;
    }
}