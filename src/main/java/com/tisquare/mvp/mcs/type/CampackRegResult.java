package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum CampackRegResult {

    SUCCESS(0),
    EXISTS(1),
    SOLDIER_EXISTS(2),
    OTHER_FOUND(3),
    SOLDIER_GENDER_ERR(4),
    NOT_FOUND_SOLDEIR(5);

    private Integer code;

    CampackRegResult(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
