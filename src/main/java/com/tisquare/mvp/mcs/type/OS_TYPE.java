package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public class OS_TYPE {

    public static final String OS_TYPE_IOS = "ios";
    public static final String OS_TYPE_ANDROID = "android";
    public static final String OS_TYPE_WIN32 = "win32";
    public static final String OS_TYPE_WIN7 = "win7";
    public static final String OS_TYPE_UNKNOWN = "unknown";


}
