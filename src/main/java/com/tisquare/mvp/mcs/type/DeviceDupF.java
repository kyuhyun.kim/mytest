package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum DeviceDupF {

    UNIQUE_DEVICE(0),
    DUPLICATE_DEVICE(1);
    private Integer code;

    DeviceDupF(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
