package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum LoginType {

    DEFAULT(0),
    NORMAL(1),
    NAVER(2),
    GOOGLE(3),
    KAKAO(4),
    FACEBOOK(5);
    private Integer code;

    LoginType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
