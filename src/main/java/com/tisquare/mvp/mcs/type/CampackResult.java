package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum CampackResult {

    SUCCESS(0),
    NOT_FOUND(1),
    OTHER_FOUND(2),
    AGE_LIMIT(3);

    private Integer code;

    CampackResult(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
