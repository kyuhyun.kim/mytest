package com.tisquare.mvp.mcs.type.mas;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Open_type {

    OPEN_NOT(0),
    OPEN(1);



    private Integer code;

    Open_type(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
