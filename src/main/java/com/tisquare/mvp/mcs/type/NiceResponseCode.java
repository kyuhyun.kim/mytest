package com.tisquare.mvp.mcs.type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

public class NiceResponseCode implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(NiceResponseCode.class);

    /* 나이스 연동 코트 */
    public static String getReturnMsg(String ResponseCode) {

		String ReturnMsg = null;
		logger.debug("NICE GET RESPONSE CODE["+ ResponseCode +"]");

        try {

            switch (ResponseCode)
            {

                case "0000": ReturnMsg = "인증성공";
                    break;
                case "0001": ReturnMsg = "인증불일치(사용자인증, 인증번호 불일치)";
                    break;
                case "0011": ReturnMsg = "유효하지 않은 응답 SEQ";
                    break;
                case "0012": ReturnMsg = "유효하지 않은 인증정보(주민번호, 휴대폰번호, 이통사)";
                    break;
                case "0013": ReturnMsg = "암호화 데이터 처리오류";
                    break;
                case "0014": ReturnMsg = "암호화 프로세스 오류";
                    break;
                case "0015": ReturnMsg = "암호화 데이터 오류";
                    break;
                case "0016": ReturnMsg = "복호화 프로세스 오류";
                    break;
                case "0017": ReturnMsg = "복호화 데이터 오류";
                    break;
                case "0018": ReturnMsg = "이통사 통신오류";
                    break;
                case "0020": ReturnMsg = "유효하지 않은 제휴사 코드";
                    break;
                case "0021": ReturnMsg = "중단된 제휴사 코드";
                    break;
                case "0022": ReturnMsg = "휴대폰인증 사용이 불가한 제휴사 코드";
                    break;
                case "0031": ReturnMsg = "인증번호 확인 실패(해당 데이터 없음)";
                    break;
                case "0032": ReturnMsg = "인증번호 확인 실패(주민번호 불일치)";
                    break;
                case "0033": ReturnMsg = "인증번호 확인 실패(요청SEQ 불일치)";
                    break;
                case "0034": ReturnMsg = "인증번호 확인 실패(기 처리건)";
                    break;
                case "9998": ReturnMsg = "본인인증 결과값 전달 실패";
                    break;
                case "9999": ReturnMsg = "정의되지 않은 오류";
                    break;
                default: ReturnMsg = "정의 되이 않은 오류 발생";
                    break;

            }

		} catch (Exception e) {
			logger.error(e.toString());
			return null;
		}
    	
        return ReturnMsg;
    }
}