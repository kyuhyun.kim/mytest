package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Device_chg_code {

    UTIL(0),
    CHANGE(1);
    private Integer code;

    Device_chg_code(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
