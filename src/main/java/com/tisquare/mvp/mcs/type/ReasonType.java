package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum ReasonType {

    IMEI_MISMATCH(0);

    private Integer code;

    ReasonType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
