package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum TYPE_ACTIVE_COMMANDER {

    DISABLE(0),
    ENABLE(1);


    private Integer code;

    TYPE_ACTIVE_COMMANDER(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
