package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum FindIDType {

    NOT_DEFINE(0),
    MDN(1),
    BIRTH(2);

    private Integer code;

    FindIDType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
