package com.tisquare.mvp.mcs.type.pc;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum MailType {

    DEFAULT(0),
    JOIN(1),
    ID(2);

    private Integer code;

    MailType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
