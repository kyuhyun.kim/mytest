package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum Cp_authtype {

    AUTO_LOGIN(0),
    ID_LOGIN(1);

    private Integer code;

    Cp_authtype(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
