package com.tisquare.mvp.mcs.type;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public enum SKIP_TYPE {

    FALSE(0),
    TRUE(1);

    private Integer code;

    SKIP_TYPE(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
