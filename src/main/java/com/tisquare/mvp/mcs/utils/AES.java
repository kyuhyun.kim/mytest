package com.tisquare.mvp.mcs.utils;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AES {
	private AES(){
		super();
	}

	public static SecretKey getSecretKey(byte[] key) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] digestKey = md.digest(key);
		//System.out.println(NumberUtils.toHexString(digestKey));
		return new SecretKeySpec(digestKey,"AES");
	}

	public static byte[] encrypt(SecretKey key, byte[] input) throws GeneralSecurityException {
		if(key==null||input==null) return null;
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE,key);
		return cipher.doFinal(input);
	}

	public static byte[] decrypt(SecretKey key, byte[] input) throws GeneralSecurityException {
		if(key==null||input==null) return null;
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE,key);
		return cipher.doFinal(input);
	}

	public static String encryptPassword(int room_no, String password) throws Exception {
		SecretKey key = getSecretKey(("526F6F6D5F4E6F2E"+room_no).getBytes("UTF-8"));
		byte[] enc_bytes = encrypt(key,password.getBytes("UTF-8"));
		//System.out.println(NumberUtils.toHexString(enc_bytes));
		return Base64.encodeBase64String(enc_bytes);
	}

	public static String decryptPassword(int room_no, String password) throws Exception {
		SecretKey key = getSecretKey(("526F6F6D5F4E6F2E"+room_no).getBytes("UTF-8"));
		byte[] enc_bytes = Base64.decodeBase64(password);
		byte[] str_bytes = decrypt(key,enc_bytes);
		return new String(str_bytes,"UTF-8");
	}


}