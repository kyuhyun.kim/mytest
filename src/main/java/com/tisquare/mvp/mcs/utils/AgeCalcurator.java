package com.tisquare.mvp.mcs.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kimkyuhyun on 2016-01-25.
 */
public class AgeCalcurator {


    public static int calculateManAge(String YYYYMMDD){

        if(YYYYMMDD.length() < 7){ //7자리 보다 작으면
            return 0;
        }

        int manAge = 0; //만 나이

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        String today = formatter.format(new Date()); //시스템 날짜를 가져와서 yyyyMMdd 형태로 변환


        int todayYear = Integer.parseInt( today.substring(0, 4) );
        int todayMonth = Integer.parseInt( today.substring(4, 6) );
        int todayDay = Integer.parseInt( today.substring(6, 8) );

        int ssnYear = Integer.parseInt( YYYYMMDD.substring(0, 2) );
        int ssnMonth = Integer.parseInt( YYYYMMDD.substring(2, 4) );
        int ssnDay = Integer.parseInt( YYYYMMDD.substring(4, 6) );

        if( YYYYMMDD.charAt( 6 ) == '0' || YYYYMMDD.charAt( 6 ) == '9' ){
            ssnYear += 1800;
        }else if( YYYYMMDD.charAt( 6 ) == '1' || YYYYMMDD.charAt( 6 ) == '2' ||
                YYYYMMDD.charAt( 6 ) == '5' || YYYYMMDD.charAt( 6 ) == '6' ){
            ssnYear += 1900;
        }else { //3, 4, 7, 8
            ssnYear += 2000;
        }

        manAge = todayYear - ssnYear;

        if( todayMonth < ssnMonth ){ //생년월일 "월"이 지났는지 체크
            manAge--;
        }else if( todayMonth == ssnMonth ){ //생년월일 "일"이 지났는지 체크
            if( todayDay < ssnDay ){
                manAge--; //생일 안지났으면 (만나이 - 1)
            }
        }

        return manAge;
    }


    public static int calculateYYYYMMDD(String ssn){

        if(ssn.length() != 7){ //7자리 보다 작으면
            return 0;
        }

        int ssnYear = Integer.parseInt( ssn.substring(0, 2) );
        int ssnMonth = Integer.parseInt( ssn.substring(2, 4) );
        int ssnDay = Integer.parseInt( ssn.substring(4, 6) );

        if( ssn.charAt( 6 ) == '0' || ssn.charAt( 6 ) == '9' ){
            ssnYear += 1800;
        }else if( ssn.charAt( 6 ) == '1' || ssn.charAt( 6 ) == '2' ||
                ssn.charAt( 6 ) == '5' || ssn.charAt( 6 ) == '6' ){
            ssnYear += 1900;
        }else { //3, 4, 7, 8
            ssnYear += 2000;
        }

        return (ssnYear * 10000) + (ssnMonth* 100) + ssnDay;
    }

    public static int getAge(String birth) {

        int birthYear = Integer.parseInt( birth.substring(0, 4) );
        int birthMonth = Integer.parseInt( birth.substring(4, 6) );
        int birthDay = Integer.parseInt( birth.substring(6, 8) );

        Calendar current = Calendar.getInstance();
        int currentYear = current.get(Calendar.YEAR);
        int currentMonth = current.get(Calendar.MONTH) + 1;
        int currentDay = current.get(Calendar.DAY_OF_MONTH);

        int age = currentYear - birthYear;
        // 생일 안 지난 경우 -1
        if (birthMonth * 100 + birthDay > currentMonth * 100 + currentDay)
            age--;

        return age;
    }
}
