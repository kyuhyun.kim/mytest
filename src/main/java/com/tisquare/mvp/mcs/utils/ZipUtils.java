package com.tisquare.mvp.mcs.utils;
import java.io.*;
import java.util.List;
import java.util.zip.ZipEntry;

import java.util.zip.ZipOutputStream;

import com.tisquare.mvp.mcs.db.domain.VoiceHistFileInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ZipUtils {

    private static final Logger logger = LoggerFactory.getLogger(AsyncProcess.class);
    private static final int COMPRESSION_LEVEL = 8;

    private static final int BUFFER_SIZE = 1024 * 2;

    /**
     * 지정된 폴더를 Zip 파일로 압축한다.
     *
     * @param sourcePath - 압축 대상 디렉토리
     * @param output     - 저장 zip 파일 이름
     * @throws Exception
     */
    public static void zip(String sourcePath, String output, List<VoiceHistFileInfo> voiceHistFileInfos) throws Exception {


        // 파일을 읽기위한 버퍼
        byte[] buf = new byte[1024];

        try {
            // 압축파일명
            String prefix = "D:\\task\\pro\\";
            String zipName = "D:\\test\\ab.zip";
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipName));

            // 파일 압축
            for (int i=0; i< voiceHistFileInfos.size(); i++) {

                String fileInfo = prefix  +  voiceHistFileInfos.get(i).getDownload().replace("cs", "data" );

                if(new File(fileInfo).exists()) {

                    FileInputStream in = new FileInputStream(fileInfo);

                    // 압축 항목추가
                    out.putNextEntry(new ZipEntry(fileInfo));

                    // 바이트 전송
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }

                    out.closeEntry();
                    in.close();
                }
            }

            // 압축파일 작성
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}

