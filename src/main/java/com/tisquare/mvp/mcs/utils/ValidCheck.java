package com.tisquare.mvp.mcs.utils;

import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.type.DeviceDupF;
import com.tisquare.mvp.mcs.type.Login_status;
import com.tisquare.mvp.mcs.type.SubscriberType;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.omg.CORBA.INTERNAL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidCheck {

    private static final Logger logger = LoggerFactory.getLogger(ValidCheck.class);
    /**
     * 고객 DB와 요청 건에 대해서 비교 한다.

     *
     * @param userInfo 최종 고객 관리 DB 저장 내용
     * @return 같으면 true, 다르면 false
     */
    public static boolean InputParamChgCheck(UserInfo userInfo, String phoneNo,
                                             Long iuid,
                                             String authKey,
                                             String tnVersion,
                                             UserDevice userDevice,
                                             Long nid,
                                             Long hid,
                                             Integer storeType,
                                             String deviceModel,
                                             String telecomCode,
                                             String deviceId) {
        try {

            if(!StringUtils.equals(userInfo.getAppVersion(), tnVersion))
                return false;
            if(!StringUtils.equals(userInfo.getDeviceId(), deviceId))
                return false;
            if(!StringUtils.equals(userInfo.getOsType(), userDevice.getOsType()))
                return false;
            if(!StringUtils.equals(userInfo.getOsVersion(), userDevice.getOsVersion()))
                return false;
            if(!StringUtils.equals(userInfo.getPhoneNo(), phoneNo))
                return false;
        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return false;
        }
        return true;

    }

    public static boolean InputParamChgCheck(UserInfo userInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Bssinfo bssinfo) {
        try {

            if(!StringUtils.equals(userInfo.getAppVersion(), authReqHeaders.getAppVersion()))
                return false;
            if(!StringUtils.equals(userInfo.getOsType(), authReqParams.getOsType()))
                return false;
            if(!StringUtils.equals(userInfo.getOsVersion(), authReqParams.getOsVersion()))
                return false;
            if(userInfo.getLogin_status() != Login_status.LOGIN.getCode())
                return false;
            if(userInfo.getDeviceId() == null)
                return false;

            /* 지령대 계정에 대한 DEVICE를 확인 한다. */
            if(userInfo.getSubsType().value() == SubscriberType.COMMANDER.value())
            {
                if(!StringUtils.equals(userInfo.getDeviceId(), authReqParams.getDeviceId()))
                    return false;
            }
        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return false;
        }
        return true;

    }


    public static boolean InputParamChgCheck(UserInfo userInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) {
        try {

            if(userInfo.getStoreType() != authReqHeaders.getStoreType())
                return false;
            if(!StringUtils.equals(userInfo.getAppVersion(), authReqHeaders.getAppVersion()))
                return false;
            if(!StringUtils.equals(userInfo.getOsType(), authReqParams.getOsType()))
                return false;
            if(!StringUtils.equals(userInfo.getOsVersion(), authReqParams.getOsVersion()))
                return false;
            if(!StringUtils.equals(userInfo.getPhoneNo(), authReqHeaders.getPhoneNo()))
                return false;
            if(userInfo.getDeviceId() == null)
                return false;

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return false;
        }
        return true;

    }


    public static boolean PcInputParamChgCheck(UserInfo userInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) {
        try {

            if(userInfo.getStoreType() != authReqHeaders.getStoreType())
                return false;
            if(!StringUtils.equals(userInfo.getAppVersion(), authReqHeaders.getAppVersion()))
                return false;
            if(!StringUtils.equals(userInfo.getOsType(), authReqParams.getOsType()))
                return false;
            if(!StringUtils.equals(userInfo.getOsVersion(), authReqParams.getOsVersion()))
                return false;

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return false;
        }
        return true;

    }

    /**
     * 고객 DB와 요청 건에 대해서 비교 한다.

     *
     * @param userInfo 최종 고객 관리 DB 저장 내용
     * @return 같으면 true, 다르면 false
     */
    public static org.springframework.http.HttpStatus MisMatchCheck(UserInfo userInfo, String phoneNo,
                                             Long iuid,
                                             String authKey,
                                             String telecomCode) {
        try {

            if (userInfo.getPhoneNo() == null || !StringUtils.equals(userInfo.getPhoneNo(), phoneNo)) {
                logger.error("Request Mdn AND DB Mdn MisMatch");
                return org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
            }
           /* else if (!org.apache.commons.lang.StringUtils.equals(userInfo.getTelecomCode(), telecomCode)) {
                logger.error("Request Telecome_code AND DB Telecom_code MisMatch");
                return org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
            }*/
            else  if(!userInfo.getIuid().equals(iuid) )
            {
                logger.error("Request iuid AND DB iuid MisMatch");
                return org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
            }
            else if(!org.apache.commons.lang.StringUtils.equals(authKey, userInfo.getCauthKey()))
            {
                logger.error("Request AuthKey MisMatch");
                return org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
            }
        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return org.springframework.http.HttpStatus.OK;

    }

    public static int juminGeneration( String birth, Integer nativ, Integer gender ) {
        try {

            int result = 0;
            if(birth == null || nativ == null || gender ==null) {

                logger.error("필수 데이터에 null 값 포함 됨");
                return -1;
            }
            switch (nativ)
            {
                case 0:
                case 1:
                    break;
                default:
                    logger.error("내/외국인 유형에 정의 되지 않은 내용 입력 [{}]", nativ);
                    return -1;
            }

            switch (gender)
            {
                case 0:
                case 1:
                    break;
                default:
                    logger.error("성별에 정의 되지 않은 내용 입력 [{}]", gender);
                    return -1;
            }

            long birt_yyyy = Long.parseLong( birth.substring(0, 4));


            /* 1800 년대 생 */
            if( birt_yyyy < 1900 )
            {
                /* 성별 확인 */
                if(gender == 0) /* 남자 */
                {
                    result = 9;
                }
                else
                {
                    result = 0;
                }
            }
            /* 1900 년대 생 */
            else if( birt_yyyy < 2000)
            {
                /* 성별 확인 */
                if(gender == 0) /* 남자 */
                {
                    if( nativ == 0 ) /* 내국인 */
                        result = 1;
                    else
                        result = 5;
                }
                else
                {
                    if( nativ == 0 ) /* 내국인 */
                        result = 2;
                    else
                        result = 6;
                }

            }
            else /* 2000 년대 생 */
            {
                if(gender == 0) /* 남자 */
                {
                    if( nativ == 0 ) /* 내국인 */
                        result = 3;
                    else
                        result = 7;
                }
                else
                {
                    if( nativ == 0 ) /* 내국인 */
                        result = 4;
                    else
                        result = 8;
                }
            }

            return result;
        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return -1;
        }
    }

    public static boolean CheckSearch(String msg) {
        try {

           return msg.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣|a-z|A-Z]+.*");

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return false;
        }
    }

    public static boolean CheckE164Search(String msg) {
        try {

            /*String regex = "^\\+[0-9](?:[- ]?[0-9]){3,14}$";*/
            String regex = "^\\+[0-9](?:[- ]?[0-9])$";
            return msg.matches(regex);

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return false;
        }
    }

    public static boolean CheckMdnSearch(String msg) {
        try {

            /*String regex = "^\\+[0-9](?:[- ]?[0-9]){3,14}$";*/
            String regex = "^[0-9]*$";
            return msg.matches(regex);

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return false;
        }
    }


}
