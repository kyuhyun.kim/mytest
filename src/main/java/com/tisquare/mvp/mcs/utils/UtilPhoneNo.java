package com.tisquare.mvp.mcs.utils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

public class UtilPhoneNo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8363356027289869609L;
	private static final String DEFAULT_COUNTRY = "KR";
	private static PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
	private static final Logger logger = LoggerFactory.getLogger(UtilPhoneNo.class);
	PhoneNumber number;
	
	public UtilPhoneNo(String phoneNo, String regionCode) throws NumberParseException {
		// TODO Auto-generated constructor stub
		if (regionCode == null)
			regionCode = DEFAULT_COUNTRY;
		
		this.number =  phoneUtil.parseAndKeepRawInput(phoneNo, regionCode);
	}


	//Get E164 Format
    public static String getE164Fmt(String phoneNo) {
		String E164Fmt = null;
		
		logger.debug("phoneNo(" + phoneNo + ")");
		
		try {
			PhoneNumber number =  phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);
			//phoneUtil.truncateTooLongNumber(number);
			E164Fmt = phoneUtil.format(number, PhoneNumberFormat.E164);
			
			logger.info("phoneNo(" + phoneNo + ") --> E164(" + E164Fmt + ")");
		} catch (NumberParseException e) {
			logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
			return null;
		}
    	
        return E164Fmt;
    }
    
  //Get E164 Format
    public static String getE164Fmt(String phoneNo, String region) {
		String E164Fmt = null;
		
		logger.debug("phoneNo(" + phoneNo + ")");
		
		try {
			PhoneNumber number =  phoneUtil.parseAndKeepRawInput(phoneNo, region);
			//phoneUtil.truncateTooLongNumber(number);
			E164Fmt = phoneUtil.format(number, PhoneNumberFormat.E164);
			
			logger.info("phoneNo(" + phoneNo + ") --> E164(" + E164Fmt + ")");
		} catch (NumberParseException e) {
			logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
			return null;
		}
    	
        return E164Fmt;
    }
    
    //Get E164 Format
    public static String getNationalFmt(String phoneNo) {
 		String nationalNumber = null;
		
		try {
			PhoneNumber number =  phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);

			nationalNumber = phoneUtil.format(number, PhoneNumberFormat.NATIONAL).replaceAll("-", "").replaceAll(" ", "");
			
			logger.info("phoneNo(" + phoneNo + ") --> nationalNumber(" + nationalNumber + ")");
		} catch (NumberParseException e) {
			logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
			return null;
		}
    	
        return nationalNumber;
    }
    
  //Get Country Code
    public static String getCountryCode(String phoneNo) {
  		String countryCode = null;
	
		try {
			PhoneNumber number =  phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);
			
			countryCode = Integer.toString(number.getCountryCode());
			logger.info("phoneNo(" + phoneNo + ") --> Country code(" + countryCode + ")");
		} catch (NumberParseException e) {
			logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
			return null;
		}
    	
        return countryCode;
    }

	public static String getRegionCode(String phoneNo) {
		// TODO Auto-generated method stub
		String regionCode = null;
		
		try {
			PhoneNumber number =  phoneUtil.parseAndKeepRawInput(phoneNo, DEFAULT_COUNTRY);
			
			regionCode = phoneUtil.getRegionCodeForNumber(number);
			logger.info("phoneNo(" + phoneNo + ") --> Region code(" + regionCode + ")");
		} catch (NumberParseException e) {
			logger.error("Invalid phone number:" + phoneNo + ":" + e.toString());
			return null;
		}
    	
        return regionCode;
	}

	public static void convert(String phoneNo, String regionCode,
			String e164Fmt, String nationalFmt) throws NumberParseException {
		// TODO Auto-generated method stub
		if (regionCode == null) 
			regionCode = DEFAULT_COUNTRY;
		
		PhoneNumber number = null;
		
		number =  phoneUtil.parseAndKeepRawInput(phoneNo, regionCode);
		e164Fmt = phoneUtil.format(number, PhoneNumberFormat.E164);
		nationalFmt = phoneUtil.format(number, PhoneNumberFormat.NATIONAL).replaceAll("-", "").replaceAll(" ", "");
	}

	public String getRegionCode() {
		// TODO Auto-generated method stub
		return phoneUtil.getRegionCodeForNumber(number);
	}

	public String getNationalFmt() {
		// TODO Auto-generated method stub
		return phoneUtil.format(number, PhoneNumberFormat.NATIONAL).replaceAll("-", "").replaceAll(" ", "");
	}

	public String getE164Fmt() {
		// TODO Auto-generated method stub
		return phoneUtil.format(number, PhoneNumberFormat.E164);
	}

	public String getCountryCode() {
		// TODO Auto-generated method stub
		return Integer.toString(number.getCountryCode());
	}

    /**
     * 전화번호 포맷 변경
     * @param mdn 포맷 변경할 전화번호
     * @return 포맷 변경 한 전화번호
     */
    public static String convertFormat(String mdn) {
        String replaceMDN = mdn.replace("-", "").replace(" ", "");

        // 국제 전화 번호 형식
        if(replaceMDN.startsWith("+")) {
            if(replaceMDN.startsWith("+82")) replaceMDN = "0" + replaceMDN.substring(3, replaceMDN.length()); // 대한민국

            // 대한민국 이외 나라에 대한 포맷 변경이 필요?
            // 국제전화 나라번호 : 1 ~ 9 로 시작 되며 1 ~ 4 자리수
        }

        // + 를 붙이지 않은 국제 전화 형식의 번호가 들어올 경우?
        if(replaceMDN.startsWith("82")) replaceMDN = "0" + replaceMDN.substring(2, replaceMDN.length()); // 대한민국

        return replaceMDN;
    }
}