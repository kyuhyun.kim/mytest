package com.tisquare.mvp.mcs.utils;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

public class VersionUtil {

    /**
     * 버전 체크 Util. 체크하고 싶은 버전이 a이고 b가 기준이다.
     * a.compareTo(b) < 0 : a<b, >0 : a>b, 0 : a=b
     *
     * @param aVersionStr 단말 버전
     * @param bVersionStr 타켓 버전
     * @return 작으면 -1, 같으면 0, 크면 1
     */
    public static int checkVersion(String aVersionStr, String bVersionStr) {
        DefaultArtifactVersion aVersion = new DefaultArtifactVersion(aVersionStr);
        DefaultArtifactVersion bVersion = new DefaultArtifactVersion(bVersionStr);
        return aVersion.compareTo(bVersion);
    }
}
