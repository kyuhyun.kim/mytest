package com.tisquare.mvp.mcs.utils;


import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import java.io.FileOutputStream;
import java.net.InetAddress;

/**
 * Created by kimkyuhyun on 2015-09-24.
 */
@Controller
public class OmcWrite {

    private static final Logger logger = LoggerFactory.getLogger(OmcWrite.class);
    private static final String LINE = "\r\n";
    private static final String REALTIME = "_REALTIME_";
    private static final String TRACE = "_TRACE_";
    private static final String EVENT = "_EVENT_";

    @Autowired
    InitConfig initConfig;

    /*public void OmcStatLog(OmcLog omcLog)
    {

        *//*InitConfig initConfig = new InitConfig();*//*
        FileOutputStream fileOutputStream = null;

        long time = System.currentTimeMillis();

        try {

            String OmcEcgFName =  omcLog.getOmc_log_base() +"/" + InetAddress.getLocalHost().getHostName().toString() + REALTIME + initConfig.getEms_log_svc_svrkey() +"_" + DateUtil.toFormatString(time, "yyyyMMddHH") + ".log";

            fileOutputStream = new FileOutputStream (OmcEcgFName, true);

            fileOutputStream.write((omcLog.toString() + LINE).getBytes());

            fileOutputStream.close();

        }
        catch (Exception e)
        {
            logger.error(e.toString());
        }
    }*/

    public void EmsRealStatLog( EmsRealStat emsRealStat) {

        FileOutputStream fileOutputStream = null;
        String hostName = null;

        try {

            hostName = InetAddress.getLocalHost().getHostName().toString();

            emsRealStat.setHostName(hostName);

            emsRealStat.setSvcKey(initConfig.getEms_log_svc_svrkey());

            String OmcEcgFName = initConfig.getRealtime_log_base() + "/" + hostName + REALTIME + initConfig.getEms_log_svc_svrkey() + DateUtil.toFormatString(System.currentTimeMillis(), "_yyyyMMdd") + ".log";

            fileOutputStream = new FileOutputStream(OmcEcgFName, true);

            fileOutputStream.write((emsRealStat.toString() + LINE).getBytes());

            fileOutputStream.close();

        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public void EmsTraceLog( EmsTrace emsTrace) {

        FileOutputStream fileOutputStream = null;
        String hostName = null;

        try {

            hostName = InetAddress.getLocalHost().getHostName().toString();

            emsTrace.setHostName(hostName);

            emsTrace.setSvcKey(initConfig.getEms_log_svc_svrkey());

            String OmcEcgFName = initConfig.getTrace_log_base() + "/" + hostName + TRACE + initConfig.getEms_log_svc_svrkey() + DateUtil.toFormatString(System.currentTimeMillis(), "_yyyyMMdd") + ".log";

            fileOutputStream = new FileOutputStream(OmcEcgFName, true);

            fileOutputStream.write((emsTrace.toString() + LINE).getBytes());

            fileOutputStream.close();

        } catch (Exception e) {
            logger.error(e.toString());
        }
    }


    public void EmsEventLog( EmsEvent emsEvent) {

        FileOutputStream fileOutputStream = null;
        String hostName = null;

        try {

            hostName = InetAddress.getLocalHost().getHostName().toString();

            emsEvent.setHostName(hostName);

            emsEvent.setSvcKey(initConfig.getEms_log_svc_svrkey());

            String OmcEcgFName = initConfig.getEventlog_base() + "/" + hostName + EVENT + initConfig.getEms_log_svc_svrkey() + DateUtil.toFormatString(System.currentTimeMillis(), "_yyyyMMdd") + ".log";

            fileOutputStream = new FileOutputStream(OmcEcgFName, true);

            fileOutputStream.write((emsEvent.toString() + LINE).getBytes());

            fileOutputStream.close();

        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

}
