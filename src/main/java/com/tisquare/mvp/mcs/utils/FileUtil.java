package com.tisquare.mvp.mcs.utils;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);
    public static final  String underbar = "_" ;
    public static final  String slash = "/" ;

    //파일을 복사하는 메소드
    public static void fileCopy(String sourcePath, String inFileName, String outFileName) {

        try {
            FileInputStream fis = new FileInputStream(sourcePath + inFileName);
            FileOutputStream fos = new FileOutputStream( outFileName);

            int data = 0;
            while((data=fis.read())!=-1) {
                fos.write(data);
            }
            fis.close();
            fos.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error(e.toString());
            e.printStackTrace();
        }
    }


}
