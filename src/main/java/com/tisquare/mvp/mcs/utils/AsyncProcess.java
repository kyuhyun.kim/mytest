package com.tisquare.mvp.mcs.utils;

import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.db.BssLoginHistDao;
import com.tisquare.mvp.mcs.db.LoginUserDao;
import com.tisquare.mvp.mcs.db.domain.BssLoginHist;
import com.tisquare.mvp.mcs.db.domain.LoginUserinfo;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.model.pc.NsSendSmsInfo;
import com.tisquare.mvp.mcs.model.pc.PcAuthInfo;
import com.tisquare.mvp.mcs.service.IPCControl;
import com.tisquare.mvp.mcs.type.OS_TYPE;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by kimkyuhyun on 2016-01-25.
 */
@Service
public class AsyncProcess {

    private static final Logger logger = LoggerFactory.getLogger(AsyncProcess.class);

    @Autowired
    LoginUserDao loginUserDao;

    @Autowired
    BssLoginHistDao bssLoginHistDao;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    IPCControl ipcControl;


    @Async
    public void processLoginUserStat(CertBody certBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) {

        try {

            if( loginUserDao.selectLoginUser() != null) {
                loginUserDao.insertLoginUser(new LoginUserinfo(certBody, authReqHeaders, authReqParams));
                logger.info("======= cert log collect ok =======");
            }
        }
        catch (Exception e)
        {
            logger.error("======= cert log collect error!! ========" + e.toString());
        }
    }

    @Async
    public void NsInterFaceLogout(UserInfo userInfo, String uri)
    {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        logger.info("URL" + uri);
        HttpPost httpPost = new HttpPost(uri);

        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(10000).build();
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-type", "application/json;charset=UTF-8");
        httpPost.addHeader("TN-REQ-USER",    userInfo.getPhoneNo());
        httpPost.addHeader("TN-IUID",        String.valueOf( userInfo.getIuid()));
        httpPost.addHeader("TN-AUTH-KEY",    userInfo.getCauthKey());
        httpPost.addHeader("TN-SOURCE", mcsConfig.getProperty("ems.log.svc.svrkey"));
        httpPost.addHeader("TN-ACK-TYPE", "1");

        String[] registration_ids = new String[1];
        registration_ids[0] = userInfo.getPnotiToken();



        try {

            String data = null;

            if(org.apache.commons.lang.StringUtils.equals( userInfo.getOsType(), OS_TYPE.OS_TYPE_ANDROID))
                data = String.format("{\"noti_title\" : \"타 기기 로그인 알림\",\"noti_body\" : \"중복 로그인되어 로그아웃 처리.\",\"push_type\" : 101}");
            else
                data = String.format("{\"title\" : \"타 기기 로그인 알림\",\"body\" : \"중복 로그인되어 로그아웃 처리.\",\"push_type\" : 101}");

            String responseBody = Json.toStringJson((Object)new IPCNsFcm( registration_ids, data));

            httpPost.setEntity(new StringEntity(responseBody, HTTP.UTF_8));



            CloseableHttpResponse response = httpClient.execute(httpPost);
            InputStreamReader is = new InputStreamReader(response.getEntity().getContent());
            BufferedReader br = new BufferedReader(is);
            String read = null;
            StringBuffer sb = new StringBuffer();
            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            is.close();
            response.close();
            httpPost.releaseConnection();

            String Message = System.lineSeparator() + "--------------------------------------------------------------------------------" + response.toString()+ System.lineSeparator() +
                    response.toString()+ System.lineSeparator() +
                    "================================================================================" +
                    System.lineSeparator();


            logger.info(Message);

        }catch(Exception e){
            logger.error("[MCS -> SCS]",e);
        }
    }

    @Async
    public void NsInterFaceSMS(PcAuthInfo pcAuthInfo, String uri)
    {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        logger.info("URL" + uri);
        HttpPost httpPost = new HttpPost(uri);

        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(10000).build();
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-type", "application/json;charset=UTF-8");
        httpPost.addHeader("TN-SOURCE", "ACS"+ String.format("0{}", mcsConfig.getProperty("ems.log.svc.svr_instance")));


        try {

            String data = null;


            String responseBody = Json.toStringJson((Object)new NsSendSmsInfo(pcAuthInfo));

            httpPost.setEntity(new StringEntity(responseBody, HTTP.UTF_8));



            CloseableHttpResponse response = httpClient.execute(httpPost);
            InputStreamReader is = new InputStreamReader(response.getEntity().getContent());
            BufferedReader br = new BufferedReader(is);
            String read = null;
            StringBuffer sb = new StringBuffer();
            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            is.close();
            response.close();
            httpPost.releaseConnection();

            String Message = System.lineSeparator() + "--------------------------------------------------------------------------------" + response.toString()+ System.lineSeparator() +
                    response.toString()+ System.lineSeparator() +
                    "================================================================================" +
                    System.lineSeparator();


            logger.info(Message);

        }catch(Exception e){
            logger.error("[MCS -> SCS]",e);
        }
    }

    @Async
    public void processLoginUserStat(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) {

        try {

            logger.info("로그인 이력 저장 실행");
            if( loginUserDao.selectLoginUser() != null && authInfoBody != null) {

                    loginUserDao.insertLoginUser(new LoginUserinfo(authInfoBody, authReqHeaders, authReqParams));
                    logger.info("======= cert log collect ok =======");
            }
        }
        catch (Exception e)
        {
            logger.error(e.toString());
        }
    }


    @Async
    public void processBssLoginHistStat(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) {

        try {

            Thread.sleep(10000L);
            System.out.println("로그인 이력 저장 실행");
            if( loginUserDao.selectLoginUser() != null && authInfoBody != null) {

                bssLoginHistDao.insertLoginUser(new BssLoginHist(authInfoBody, authReqHeaders, authReqParams));
                logger.info("======= cert log collect ok =======");
            }
        }
        catch (Exception e)
        {
            logger.error("======= cert log collect error!! ========" + e.toString());
        }
    }


    @Async
    public void processLoginUserStat(BssCertBody certBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) {

        try {

            if( loginUserDao.selectLoginUser() != null) {
                loginUserDao.insertLoginUser(new LoginUserinfo(certBody, authReqHeaders, authReqParams));
                logger.info("======= cert log collect ok =======");
            }


        }
        catch (Exception e)
        {
            logger.error(e.toString());
        }
    }


}
