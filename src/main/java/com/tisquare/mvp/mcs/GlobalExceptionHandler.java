package com.tisquare.mvp.mcs;

import com.tisquare.mvp.mcs.urlshort.UrlShortnerNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceView;

/**
 * Created by kimjh on 2015-03-25.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(UrlShortnerNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ModelAndView HandleUrlShortnerNotFoundException(){
        return new ModelAndView(new InternalResourceView("/web_resources/pages/UrlShortner_404.html"));
    }
}
