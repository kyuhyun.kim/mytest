package com.tisquare.mvp.mcs.scheduler;


import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.net.InetAddress;

/**
 * Created by kimkyuhyun on 2015-09-23.
 */
@Controller
public class EmsScheduler {

    private static final Logger logger = LoggerFactory.getLogger(EmsScheduler.class);

    @Autowired
    InitConfig initConfig;

    @Scheduled(fixedDelay = 600000)
    private void EmsFileCheckSch(){


        long time = (System.currentTimeMillis() - (1000 * 60 * 60));

        String hostName = null;
//        System.out.println("time   : " + time + ", time: " + DateUtil.toFormatString(time, "yyyy-MM-dd HH:mm:ss"));

        try {


            /*hostName = InetAddress.getLocalHost().getHostName().toString();

            String OmcEcgFName = initConfig.getRealtime_log_base()  +"/" + hostName +  "_REALTIME_" + initConfig.getEms_log_svc_svrkey() + DateUtil.toFormatString(System.currentTimeMillis(), "_yyyyMMdd") + ".log";

            File file = new File(OmcEcgFName);
            if(!file.exists())
                file.createNewFile();
*/
           /* OmcEcgFName = initConfig.getEms_log_base()  +"/" + hostName +  "_EVENT_" + initConfig.getEms_log_svc_svrkey() + DateUtil.toFormatString(System.currentTimeMillis(), "_yyyyMMdd") + ".log";

            file = new File(OmcEcgFName);
            if(!file.exists())
                file.createNewFile();*/

            /*OmcEcgFName = initConfig.getTrace_log_base() +"/" + hostName +  "_TRACE_" +initConfig.getEms_log_svc_svrkey() + DateUtil.toFormatString(System.currentTimeMillis(), "_yyyyMMdd") + ".log";

            file = new File(OmcEcgFName);
            if(!file.exists())
                file.createNewFile();*/
        }
        catch (Exception e)
        {
            logger.error(e.toString());
        }
    }
}
