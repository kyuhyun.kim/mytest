package com.tisquare.mvp.mcs.filter;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

public class TraceLogFilter extends Filter<ILoggingEvent> {
    private Collection<String> phoneNumbers;

    private TraceLogFilter(Collection<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public static TraceLogFilter create(Collection<String> phoneNumbers) {
        return new TraceLogFilter(phoneNumbers);
    }

    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (!CollectionUtils.isEmpty(phoneNumbers)) {
            return FilterReply.ACCEPT;
        }

        return FilterReply.DENY;
    }
}
