package com.tisquare.mvp.mcs.filter;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by kimjh on 2015-07-15.
 */
public class HttpRequestWrapper extends HttpServletRequestWrapper{
    private final ByteArrayOutputStream bos = new ByteArrayOutputStream();
    private Long id;

    public HttpRequestWrapper(HttpServletRequest request, Long id) {
        super(request);
        this.id = id;
    }

    public ServletInputStream getInputStream() throws IOException {
        return new ServletInputStream() {

            private HttpLogInputStream tee;

            {
                this.tee = new HttpLogInputStream(HttpRequestWrapper.super.getInputStream(), HttpRequestWrapper.this.bos);
            }

            public int read() throws IOException {
                return this.tee.read();
            }
        };
    }

    public byte[] toByteArray() {
        return this.bos.toByteArray();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
