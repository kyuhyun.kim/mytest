package com.tisquare.mvp.mcs.filter.comm;

import com.tisquare.mvp.mcs.filter.HttpRequestWrapper;
import com.tisquare.mvp.mcs.filter.HttpResponseWrapper;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by kimjh on 2016-07-22.
 */
public class HttpLogFilter extends OncePerRequestFilter{

    protected static final Logger logger = LoggerFactory.getLogger(HttpLogFilter.class);
    private AtomicLong id = new AtomicLong(1L);

    public HttpLogFilter() {
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            long requestId = this.id.incrementAndGet();
            Date startTime = DateUtil.getDateNow();

            if(logger.isInfoEnabled()) {
                request = new HttpRequestWrapper(request, requestId);
                response = new HttpResponseWrapper(response, requestId);
                httpRequestLog(request);
            }
            filterChain.doFilter(request, response);

            if(logger.isInfoEnabled()) httpResponseLog(response, DateUtil.getDateNow().getTime() - startTime.getTime());

        } catch (Exception e) {
            logger.error("", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            httpRequestLog(request);
        }
    }

    private void httpRequestLog(HttpServletRequest request){
        HttpRequestWrapper wrequest = (HttpRequestWrapper)request;
        Enumeration<String> enumeration = request.getHeaderNames();
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("@#ID[%d] HTTP LOG REQ M:%s URI:%s",
                wrequest.getId(), request.getMethod(), request.getRequestURI()));

        if(request.getParameterMap().size() > 0) {
            sb.append(" Parameters: ");
            Enumeration<String> e = request.getParameterNames();
            String key;
            while(e.hasMoreElements()){
                key = e.nextElement();
                sb.append(key + "=" + request.getParameter(key));
                if(e.hasMoreElements()) sb.append(",");
            }
        }

        logger.info(sb.toString());
    }

    private void httpResponseLog(HttpServletResponse response, long elapsed){
        HttpResponseWrapper wresponse =  (HttpResponseWrapper) response;

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("@#ID[%d] HTTP LOG RSP:%d elapsed[%dms]",
                wresponse.getId(), response.getStatus(), elapsed));
        logger.info(sb.toString());
    }
}
