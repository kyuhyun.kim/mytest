package com.tisquare.mvp.mcs.encrypt;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLFeatureNotSupportedException;

public class SecureBasicDataSource extends BasicDataSource {

    private static final Logger logger = LoggerFactory.getLogger(SecureBasicDataSource.class);


    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException();
    }

    @Override
    public void setUsername(String username){
        try{
            super.setUsername(SecureEncryptor.decrypt(username));
        }
        catch(Exception e){
            logger.error("DBMS 사용자 계정 복호화 실패!!" + e.toString());
        }
    }

    @Override
    public void setPassword(String password) {
        try{
            super.setPassword(SecureEncryptor.decrypt(password));
        }
        catch(Exception e){
            logger.error("DBMS 사용자 계정 비밀번호 복호화 실패!!" + e.toString());
        }
    }

}
