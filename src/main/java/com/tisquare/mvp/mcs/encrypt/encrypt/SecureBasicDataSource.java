package com.tisquare.mvp.mcs.encrypt.encrypt;

import com.tisquare.mvp.mcs.db.SystemInfoDao;
import com.tisquare.mvp.mcs.encrypt.*;
import com.tisquare.mvp.mcs.encrypt.SecureEncryptor;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLFeatureNotSupportedException;

public class SecureBasicDataSource extends BasicDataSource {

    private static final Logger logger = LoggerFactory.getLogger(com.tisquare.mvp.mcs.encrypt.SecureBasicDataSource.class);


    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException();
    }
/*
    @Override
    public void setUrl(String url){
        try{

            System.out.println("url[" + url);
            logger.error(url);

        }
        catch(Exception e){
            logger.error(e.toString());
        }
    }*/


    @Override
    public void setUsername(String username){
        try{

            System.out.println("username[" + username);
            logger.error(username);
            logger.error(com.tisquare.mvp.mcs.encrypt.SecureEncryptor.decrypt(username));
            super.setUsername(com.tisquare.mvp.mcs.encrypt.SecureEncryptor.decrypt(username));
        }
        catch(Exception e){
            logger.error("DBMS 사용자 계정 복호화 실패!!" + e.toString());
        }
    }

    @Override
    public void setPassword(String password) {
        try{
            System.out.println("password[" + password);
            logger.error(password);
            logger.error(com.tisquare.mvp.mcs.encrypt.SecureEncryptor.decrypt(password));
            super.setPassword(SecureEncryptor.decrypt(password));
        }
        catch(Exception e){
            logger.error("DBMS 사용자 계정 비밀번호 복호화 실패!!" + e.toString());
        }
    }

}
