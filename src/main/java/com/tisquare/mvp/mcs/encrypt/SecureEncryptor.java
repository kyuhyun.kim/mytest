package com.tisquare.mvp.mcs.encrypt;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class SecureEncryptor {
	public static final byte[] DEFAULT_KEY = {
		(byte)0xad, (byte)0xbd, (byte)0xf1, (byte)0x51, 
		(byte)0xcc, (byte)0x52, (byte)0xa8, (byte)0x2a,
		(byte)0xa3, (byte)0x54, (byte)0x60, (byte)0x74, 
		(byte)0x45, (byte)0xab, (byte)0xce, (byte)0x55
	};
	
	
	public static String encrypt(String data) throws UnsupportedEncodingException{
		return Seed128Cipher.encrypt(data, DEFAULT_KEY, null);
	} 

	public static String decrypt(String data) throws UnsupportedEncodingException{
		return Seed128Cipher.decrypt(data, DEFAULT_KEY, null);
	} 

	public static void main(String args[]) {
		
		try {
			
			Scanner sc = new Scanner(System.in);
	        System.out.print("Input and enter : ");
	        String input = sc.nextLine();
	        
	        String encypted = encrypt(input);
	        System.out.println(encypted);
	        
	        System.out.println("Decrypted["+decrypt(encypted)+"]");
			
		} catch(Exception e) {
			System.out.println("E:" + e.getMessage());
		}
	}
}
