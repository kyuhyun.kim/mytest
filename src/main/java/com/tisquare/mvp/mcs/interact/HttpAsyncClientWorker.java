package com.tisquare.mvp.mcs.interact;

import com.tisquare.mvp.mcs.McsConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class HttpAsyncClientWorker implements Runnable {

    @Autowired
    McsConfig mcsConfig;

    private static final Logger logger = LoggerFactory.getLogger(HttpAsyncClientWorker.class);

    private CloseableHttpAsyncClient httpAsyncClient;

    private final static String LINE = "\r\n";

    private HttpPost httpRequest;
    private String requestBody;
    private HttpGet httpGet;

    public HttpAsyncClientWorker() {
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(1000)
                .setConnectTimeout(1000).build();
        this.httpAsyncClient = HttpAsyncClients.custom().setDefaultRequestConfig(requestConfig).build();
        this.httpAsyncClient.start();
    }

    private HttpAsyncClientWorker(String url, Map<String, String> headers, String requestBody, ContentType contentType) {
        this();
        this.requestBody = requestBody;

        ;
        httpRequest = new HttpPost(url);
        httpRequest.setEntity(new StringEntity(requestBody, contentType.withCharset("UTF-8")));
        if (!CollectionUtils.isEmpty(headers)) {
            for (String key : headers.keySet()) {
                httpRequest.setHeader(key, headers.get(key));
            }
        }
    }


    private HttpAsyncClientWorker(String url, Map<String, String> headers, int timeout) {
        this();
        logger.info(url);
        httpGet = new HttpGet(url);
        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(timeout).build();
        httpGet.setConfig(requestConfig);

        if (!CollectionUtils.isEmpty(headers)) {
            for (String key : headers.keySet()) {
                httpGet.setHeader(key, headers.get(key));
            }
        }

        HttpGetRequestLogPrint(httpGet);
    }

    public static HttpAsyncClientWorker create(String url, Map<String, String> headers, String requestBody, ContentType contentType) {
        return new HttpAsyncClientWorker(url, headers, requestBody, contentType);
    }

    public static HttpAsyncClientWorker create(String url, Map<String, String> headers, int timeout) {
        return new HttpAsyncClientWorker(url, headers, timeout);
    }


    public String createHttpGetReqLog() {
        StringBuilder reqSb = new StringBuilder();
        String headerLine = "-----------------------------------------------------------------------";
        String footerLine = "";
        for (int i = 0; i < headerLine.length(); i++) {
            footerLine += "=";
        }

        reqSb.append(" => ").append(httpGet.getURI()).append("\n");
        reqSb.append(headerLine).append("\n");
        reqSb.append(httpGet.getRequestLine()).append("\n");
        for (Header header : httpGet.getAllHeaders()) {
            reqSb.append(header.toString()).append("\n");
        }

        reqSb.append("\n");
        if (StringUtils.isNotBlank(requestBody)) {
            reqSb.append(requestBody).append("\n");
        }
        reqSb.append(footerLine).append("\n");
        return reqSb.toString();
    }

    private String printGetResponseLog(HttpResponse response) {
        StringBuilder logSb = new StringBuilder();
        String headerLine = "-----------------------------------------------------------------------";
        String footerLine = "";
        for (int i = 0; i < headerLine.length(); i++) {
            footerLine += "=";
        }
        logSb.append(" <= ").append(httpGet.getURI()).append("\n");
        logSb.append(headerLine).append("\n");
        logSb.append(response.getStatusLine()).append("\n");
        for (Header header : response.getAllHeaders()) {
            logSb.append(header.toString()).append("\n");
        }
        logSb.append(footerLine).append("\n");
        return logSb.toString();
    }

    public void HttpGetRequestLogPrint( HttpGet httpGet)
    {
        Header[] headers = httpGet.getAllHeaders();

        String Print = LINE + "--------------------------------------------------------------------------------" + LINE +
                httpGet.toString()+LINE;
        for(int idx = 0; idx < headers.length; idx++)
        {
            Print += headers[idx].getName() +":" + headers[idx].getValue() + LINE;
        }

        Print += "================================================================================" +LINE;

        logger.info(Print);

//        if(traceService.isTrace())
//            traceService.printTraceLog(mdn, Print);
    }

    /**
     * 자원 해제
     */
    public void cleanup() {
        try {
            if (httpAsyncClient != null) {
                httpAsyncClient.close();
            }
        } catch (IOException e) {
            logger.error("HTTP async client close failed.", e);
        }
    }

    @Override
    public void run() {
        int retryCount = 1;

        try {
            HttpResponse response = null;

            for (int currentCount = 0; currentCount < retryCount; currentCount++) {
                try {
                    response = httpExecute();
                    break;
                } catch (Exception e) {
                    logger.error("Http request failed. url: {} currentCount: {}", httpRequest.getRequestLine(), currentCount);
                }
            }

            if(httpRequest != null)
                logger.info(createHttpReqLog());
            if(httpGet != null)
                logger.info(createHttpGetReqLog());

            if (response != null) {
                if(httpRequest != null)
                    logger.info(printResponseLog(response));
                if(httpGet != null)
                    logger.info(printGetResponseLog(response));
            } else {
                logger.info("http response none.");
            }
        } catch (Exception e) {
            logger.error("Http request failed. url: {}", httpRequest.getRequestLine(), e);
        } finally {
            cleanup();
        }
    }

    /**
     * HTTP 요청을 비동기로 실행한다.
     *
     * @return HTTP 응답 객체.
     */
    public HttpResponse httpExecute() throws ExecutionException, InterruptedException {
        if( httpRequest != null)
            return httpAsyncClient.execute(httpRequest, null).get();
        else {
            return httpAsyncClient.execute(httpGet, null).get();
        }
    }

    /**
     * HTTP 요청 프로토콜 로그를 생성한다.
     *
     * @return HTTP 요청 로그.
     */
    public String createHttpReqLog() {
        StringBuilder reqSb = new StringBuilder();
        String headerLine = "-----------------------------------------------------------------------";
        String footerLine = "";
        for (int i = 0; i < headerLine.length(); i++) {
            footerLine += "=";
        }

        reqSb.append(" => ").append(httpRequest.getURI()).append("\n");
        reqSb.append(headerLine).append("\n");
        reqSb.append(httpRequest.getRequestLine()).append("\n");
        for (Header header : httpRequest.getAllHeaders()) {
            reqSb.append(header.toString()).append("\n");
        }
        if (httpRequest.getEntity() != null) {
            reqSb.append("Content-Length: ").append(httpRequest.getEntity().getContentLength()).append("\n");
            reqSb.append(httpRequest.getEntity().getContentType()).append("\n");
        }
        reqSb.append("\n");
        if (StringUtils.isNotBlank(requestBody)) {
            reqSb.append(requestBody).append("\n");
        }
        reqSb.append(footerLine).append("\n");
        return reqSb.toString();
    }

    /**
     * HTTP 응답 로그를 찍는다.
     */
    private String printResponseLog(HttpResponse response) {
        StringBuilder logSb = new StringBuilder();
        String headerLine = "-----------------------------------------------------------------------";
        String footerLine = "";
        for (int i = 0; i < headerLine.length(); i++) {
            footerLine += "=";
        }
        logSb.append(" <= ").append(httpRequest.getURI()).append("\n");
        logSb.append(headerLine).append("\n");
        logSb.append(response.getStatusLine()).append("\n");
        for (Header header : response.getAllHeaders()) {
            logSb.append(header.toString()).append("\n");
        }
        logSb.append(footerLine).append("\n");
        return logSb.toString();
    }

}
