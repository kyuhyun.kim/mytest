package com.tisquare.mvp.mcs.interact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

//@Service
public class HttpUrlConnectionImpl implements HttpUrlConnection {
    protected final static String LINE = "\r\n";
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private List<String> headerKey = new ArrayList<>();
    private List<String> headerValue = new ArrayList<>();

    // Header
    public void addHeader(String key, String value) {
        this.headerKey.add(key);
        this.headerValue.add(value);
    }

    public List<String> getHeaderKey() {
        return headerKey;
    }

    public List<String> getHeaderValue() {
        return headerValue;
    }

    // UTF-8 Encoding
    public void setURLEncoding(String requestUrl, String encoding_type) {
        try {
            URLEncoder.encode(requestUrl, encoding_type);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public int sendHttpRequest(URLConnection con, String method, String requestBody) {
        int responseCode = 0;

        //setURLEncoding(requestUrl, "UTF-8");

        try {
            // HTTP Method
            ((HttpURLConnection) con).setRequestMethod(method);
        } catch (ProtocolException e) {
            logger.error("HTTP Connection Error : " + e.toString());
            e.printStackTrace();
            return -1;
        }

        // HTTP
        responseCode = this.doHttpUrlConnectionAction(con, requestBody, method);

        dispose();

        return responseCode;
    }

    private void dispose() {
        this.headerKey.clear();
        this.headerValue.clear();
    }

    /**
     * HTTP Header
     *
     * @param con
     * @return
     */
    public boolean setHttpHeader(String phoneNo, URLConnection con, int bodyLen) {
        for (int i = 0; i < this.headerKey.size(); i++) {
            con.setRequestProperty(this.headerKey.get(i), this.headerValue.get(i));
        }

        con.setRequestProperty("Content-Length", Integer.toString(bodyLen));
        con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        con.setRequestProperty("TN-REQ-USER", phoneNo);

        return true;
    }

    /**
     * HTTP Connection
     *
     * @return
     */
    public URLConnection openHttpUrlConnection(String requestUrl) {
        URL url = null;
        try {
            url = new URL(requestUrl);
        } catch (MalformedURLException e) {
            logger.error("HTTP new  error : " + e.toString());
            e.printStackTrace();
            return null;
        }

        URLConnection connect = null;
        try {
            connect = url.openConnection();
        } catch (IOException e) {
            logger.error("HTTP Open connection error : " + e.toString());
            e.printStackTrace();
            return null;
        }

        connect.setDoOutput(true);
        return connect;
    }

    /**
     * HTTP
     *
     * @param connect
     * @param requestBody
     * @return
     */
    public int doHttpUrlConnectionAction(URLConnection connect, String requestBody, String method) {
        int responseCode = 0;
        logger.info("HTTP Request URL : " + connect.getURL());

        try {
            if (connect instanceof HttpURLConnection) {
                connect.connect();
            }

//			OutputStreamWriter out = new OutputStreamWriter(connect.getOutputStream());
//			out.write(requestBody);
            DataOutputStream out = new DataOutputStream(connect.getOutputStream());
            out.write(requestBody.getBytes("UTF-8"));
            out.close();

            logger.info("HTTP Request Body : " + requestBody);
        } catch (Exception e) {
            logger.error("HTTP Send Fail : " + e.toString());
            e.printStackTrace();
        }

        try {
            responseCode = ((HttpURLConnection) connect).getResponseCode();
            logger.info("HTTP Response Code : " + responseCode +
                    "[" + ((HttpURLConnection) connect).getResponseMessage() + "]");

            if ((responseCode >= HttpURLConnection.HTTP_OK) &&
                    (responseCode <= HttpURLConnection.HTTP_ACCEPTED)) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connect.getInputStream(), "UTF-8"));
                StringBuffer response = new StringBuffer();

                for (; ; ) {
                    String line = br.readLine();
                    if (line == null) break;
                    response.append(line);
                    response.append(LINE);
                }

                logger.info("GCM body : " + LINE + response.toString());
                br.close();
            } else {
            }

            ((HttpURLConnection) connect).disconnect();
        } catch (IOException e) {
            logger.error("HTTP Send Fail : " + e.toString());
            e.printStackTrace();
        }

        return responseCode;
    }
}