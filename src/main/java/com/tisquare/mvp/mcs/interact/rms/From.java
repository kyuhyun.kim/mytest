package com.tisquare.mvp.mcs.interact.rms;

import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

/**
 * RMS 요청 시 탈퇴자 정보
 *
 * @author leejs
 * @since 2015-07-27
 */
public class From {
    private String mdn;
    private Long iuid;
    private String name;

    private From(String mdn, Long iuid, String name) {
        this.mdn = mdn;
        this.iuid = iuid;
        this.name = name;
    }

    public static From create(String mdn, Long iuid, String name) {
        return new From(mdn, iuid, name);
    }

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public Long getIuid() {
        return iuid;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }
}
