package com.tisquare.mvp.mcs.interact;

import java.net.URLConnection;
import java.util.List;


public interface HttpUrlConnection {
	URLConnection openHttpUrlConnection(String requestUrl);
	boolean setHttpHeader(String phoneNo, URLConnection con, int bodyLen);
	int sendHttpRequest(URLConnection con, String method, String requestBody);
	void addHeader(String key, String value);
	List<String> getHeaderKey();
	List<String> getHeaderValue();
}
