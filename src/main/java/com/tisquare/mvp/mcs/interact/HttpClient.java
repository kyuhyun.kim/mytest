package com.tisquare.mvp.mcs.interact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;

@Service
public class HttpClient {
	private static final Logger logger = LoggerFactory.getLogger(HttpClient.class);
	
	public int sendHttpRequest(String method, String requestUrl) {
		int responseCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
		URL url = null;
		
		try {
			url = new URL(requestUrl);						
		} catch (MalformedURLException e) {
			logger.error("HTTP new  error : " + e.toString());
			e.printStackTrace();
			return responseCode;
		}
		
		URLConnection connect = null;
		
		try {
			connect = url.openConnection();
		} catch (IOException e) {
			logger.error("HTTP Open connection error : " + e.toString());
			e.printStackTrace();
			return responseCode;
		}
		
		connect.setDoOutput(true);
		
		try {
			((HttpURLConnection) connect).setRequestMethod(method);
		} catch (ProtocolException e) {
			logger.error("HTTP Connection Error : " + e.toString());
			e.printStackTrace();
			return responseCode;
		}
		
		try {						
			if (connect instanceof HttpURLConnection) {
				connect.connect();
				logger.info("HTTP Connection Success: " + connect.getURL());
			}
		}
		catch (Exception e) {
			logger.error("HTTP Send Fail : " + e.toString());
			e.printStackTrace();
			return responseCode;
		}
		
		try {
			InputStream inputStream;
			responseCode = ((HttpURLConnection) connect).getResponseCode();			

			if ((responseCode >= HttpURLConnection.HTTP_OK)	&& 
				(responseCode <= HttpURLConnection.HTTP_CREATED)) {
				// Get Response
				inputStream = connect.getInputStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
				
				String line;
				StringBuffer response = new StringBuffer();
				
				while ((line = rd.readLine()) != null) {
					response.append(line);
					response.append('\r');
				}
				rd.close();
				logger.info("HTTP Response Code : " + responseCode);
				logger.info("HTTP Response Body : " + response.toString());
			} else {
				inputStream = ((HttpURLConnection) connect).getErrorStream();
				logger.info("HTTP Response Code : " + responseCode +
									"[" + ((HttpURLConnection) connect).getResponseMessage()  + "]");
			}						
			((HttpURLConnection) connect).disconnect();
			logger.info("HTTP Disconnection Success : " + connect.getURL());
		} 
		catch (IOException e) {
			logger.error("HTTP Send Fail : " + e.toString());
			e.printStackTrace();
		}
		
		return responseCode;
	}
	
}