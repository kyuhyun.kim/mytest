package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import com.tisquare.mvp.mcs.service.BssNoticeService;
import com.tisquare.mvp.mcs.service.NoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */


/*
@startuml
hide footbox
        title 서비스 공지사항 조회
        actor UE_B2C #red
        actor UE_B2B #blue

        participant NGINX
        participant ACS

        database SVC_DB

        note over UE_B2C : 서비스 공지사항 \n리스트 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/notice/list


        note over UE_B2B : 서비스 공지사항\n리스트 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/notice/list

        NGINX -> ACS : HTTP(POST) / notice/list

        alt successful case
        ACS-> SVC_DB : 서비스 공지사항 리스트 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200 OK (공지사항 목록 리스트)


        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


        autonumber stop
        note over UE_B2C : 공지사항 내역 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/notice/sid


        note over UE_B2B : 공지 사항 내역 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/notice/sid


        NGINX -> ACS : HTTP(POST) / notice/sid

        alt successful case
        ACS-> SVC_DB : 공지 사항 내역 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200OK (공지사항 내역 정보)

        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


@enduml
*/

@Controller
@RequestMapping(value="acs/")
public class BssNoticeController {

    private static final Logger logger = LoggerFactory.getLogger(BssNoticeController.class);



    @Autowired
    BssNoticeService bssNoticeService;

    @RequestMapping(value="/notice/bsslist", method = RequestMethod.GET)
    @ResponseBody
    public Object NoticeList(HttpServletRequest request,
                           @RequestHeader("TN-START-SID") int start_sid,
                           @RequestHeader("TN-LAST-READ-SID") int last_read_sid,
                           @RequestHeader("TN-NOTICE-CNT") int notice_cnt,
                           @RequestHeader(value = "TN-TELECOM-CODE") String telecomCode,
                           @RequestHeader(value = "TN-IUID") long iuid,
                           @RequestHeader(value = "TN-AUTH-KEY") String cAuthKey)
    {

       return bssNoticeService.processBssNoticeListRequest(request, new NoticeReqHeaders(start_sid, last_read_sid, notice_cnt, telecomCode, iuid, cAuthKey));

    }

    @RequestMapping(value="/notice/bsssid", method = RequestMethod.GET)
    @ResponseBody
    public Object Notice(HttpServletRequest request,
                       @RequestHeader("tn-sid") int sid,
                       @RequestHeader(value = "tn-telecom-code") String telecomCode)
    {

        return bssNoticeService.processBssNoticeRequest(request, sid, telecomCode);

    }

}
