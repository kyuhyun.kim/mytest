package com.tisquare.mvp.mcs.controller;

import NiceID.Check.CPClient;
import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.service.IdentifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jhkim on 14. 3. 4.
 */
@Controller
@RequestMapping(value="/acs")
public class IndexMgrController {

    private static final Logger logger = LoggerFactory.getLogger(IndexMgrController.class);


    @Autowired
    IdentifyService identifyService;

    @Autowired
    private McsConfig mcsConfig;

    @RequestMapping(value="/checkplus_main.do")
    public String checkplus_main(Model model,
                                 @RequestParam(value = "iuid") Long iuid,
                                 @RequestParam(value = "os-type", required = false) String osType,
                                 @RequestParam(value = "os-version", required = false) String osVersion,
                                 @RequestParam(value = "type", required = false) String type
    )throws Exception {


        if( osType == null) osType ="";
        if( osVersion == null) osVersion ="";
        if( type == null) type = "";

        model.addAttribute("iuid", iuid);
        model.addAttribute("os-type", osType);
        model.addAttribute("os-version", osVersion);
        model.addAttribute("type", type);
        model.addAttribute("nice_site_code", mcsConfig.getProperty("NICE.SITE.CODE"));
        model.addAttribute("nice_site_pwd", mcsConfig.getProperty("NICE.SITE.PWD"));
        logger.info("OS TYPE[{}]", osType);
        if( osType.equals(""))
        {
            model.addAttribute("success_url", mcsConfig.getProperty("URL_DOMAIN") + mcsConfig.getProperty("URL_NICE_SUCC_INFO"));
            model.addAttribute("fail_url", mcsConfig.getProperty("URL_DOMAIN") + mcsConfig.getProperty("URL_NICE_FAIL_INFO"));
        }
        else
        {
            model.addAttribute("success_url", "http://221.140.57.231:6700/pcws/member/join/viewNiceSuccess.do");
            model.addAttribute("fail_url", "http://221.140.57.231:6700/pcws/member/join/viewNiceFail.do");
        }
        return "checkplus_main";
    }

    /*@RequestMapping(value="/checkplus_main.do")
    public String checkplus_main(Model model, @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                                 @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey) throws Exception {

        model.addAttribute("iuid", iuid);
        return "checkplus_main";
    }
*/

    @RequestMapping(value="checkplus_fail.do")
    public String checkplus_fail(HttpServletRequest request, Model model, @RequestParam(value = "iuid") Long iuid){

      /*  identifyService.processIdentifyResultFail(request, null, null,  iuid);*/
        model.addAttribute("nice_site_code", mcsConfig.getProperty("NICE.SITE.CODE"));
        model.addAttribute("nice_site_pwd", mcsConfig.getProperty("NICE.SITE.PWD"));
        identifyService.processIdentifyFail(request, null, null);
        return "checkplus_fail";
    }

    // 공지사항.
    @RequestMapping(value="checkplus_success.do")
    public String checkplus_success(HttpServletRequest request, Model model, @RequestParam(value = "iuid") Long iuid,
                                    @RequestParam(value = "EncodeData") String EncodeData){


        String sCipherTime = "";			// 복호화한 시간
        String sRequestNumber = "ti2";			// 요청 번호
        String sResponseNumber = "";		// 인증 고유번호
        String sAuthType = "";				// 인증 수단
        String sName = "";					// 성명
        String sDupInfo = "";				// 중복가입 확인값 (DI_64 byte)
        String sConnInfo = "";				// 연계정보 확인값 (CI_88 byte)
        String sBirthDate = "";				// 생년월일(YYYYMMDD)
        String sGender = "";				// 성별
        String sNationalInfo = "";			// 내/외국인정보 (개발가이드 참조)
        String sMobileNo = "";				// 휴대폰번호
        String sMobileCo = "";				// 통신사
        String sMessage = "";
        String sPlainData = "";

        CPClient niceCheck = new CPClient();
        /*CPClient niceCheck = new CPClient();*/

        /*NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();*/

        /*NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();*/

        int iReturn = niceCheck.fnDecode(mcsConfig.getProperty("NICE.SITE.CODE"), mcsConfig.getProperty("NICE.SITE.PWD"), EncodeData);
        if( iReturn == 0 )
        {
            sPlainData = niceCheck.getPlainData();
            sCipherTime = niceCheck.getCipherDateTime();

            // 데이타를 추출합니다.
            java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);

            sRequestNumber  = (String)mapresult.get("REQ_SEQ");
            logger.info(sRequestNumber);
            sResponseNumber = (String)mapresult.get("RES_SEQ");
            logger.info(sResponseNumber);
            sAuthType		= (String)mapresult.get("AUTH_TYPE");
            logger.info(sAuthType);
            sName			= (String)mapresult.get("NAME");
            logger.info(sName);
            //sName			= (String)mapresult.get("UTF8_NAME"); //charset utf8 사용시 주석 해제 후 사용
            sBirthDate		= (String)mapresult.get("BIRTHDATE");
            logger.info(sBirthDate);
            sGender			= (String)mapresult.get("GENDER");
            logger.info(sGender);
            sNationalInfo  	= (String)mapresult.get("NATIONALINFO");
            logger.info(sNationalInfo);
            sDupInfo		= (String)mapresult.get("DI");
            logger.info(sDupInfo);
            sConnInfo		= (String)mapresult.get("CI");
            logger.info(sConnInfo);
            sMobileNo		= (String)mapresult.get("MOBILE_NO");
            logger.info(sMobileNo);
            sMobileCo		= (String)mapresult.get("MOBILE_CO");
            logger.info(sMobileCo);

            String session_sRequestNumber = (String)request.getAttribute("REQ_SEQ");
            if(!sRequestNumber.equals(session_sRequestNumber))
            {
                sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
                sResponseNumber = "";
                sAuthType = "";
            }



            identifyService.processIdentifyresult(request, null, null, EncodeData, iuid);
        }
        else if( iReturn == -1)
        {
            sMessage = "복호화 시스템 에러입니다.";
        }
        else if( iReturn == -4)
        {
            sMessage = "복호화 처리오류입니다.";
        }
        else if( iReturn == -5)
        {
            sMessage = "복호화 해쉬 오류입니다.";
        }
        else if( iReturn == -6)
        {
            sMessage = "복호화 데이터 오류입니다.";
        }
        else if( iReturn == -9)
        {
            sMessage = "입력 데이터 오류입니다.";
        }
        else if( iReturn == -12)
        {
            sMessage = "사이트 패스워드 오류입니다.";
        }
        else
        {
            sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
        }

        model.addAttribute("iuid", iuid);
        model.addAttribute("nice_site_code", mcsConfig.getProperty("NICE.SITE.CODE"));
        model.addAttribute("nice_site_pwd", mcsConfig.getProperty("NICE.SITE.PWD"));

        return "checkplus_success";
    }

    // 개인정보 취급동의
    @RequestMapping(value="/privacy.do")
    public String privacyFrame() throws Exception {
        return "H-private";
    }

    @RequestMapping(value="/location.do")
    public String locationFrame() throws Exception {
        return "H-location";
    }



}
