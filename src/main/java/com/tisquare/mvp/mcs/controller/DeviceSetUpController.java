package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.IdentifyService;
import com.tisquare.mvp.mcs.service.SetupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Controller
@RequestMapping(value="acs/")
public class DeviceSetUpController {

    private static final Logger logger = LoggerFactory.getLogger(DeviceSetUpController.class);


    @Autowired
    SetupService setupService;


    @RequestMapping(value="/setup/alarm", method = RequestMethod.POST)
    @ResponseBody
    public Object setup_alarm(HttpServletRequest request,
                          @RequestHeader("TN-REQ-USER") String mdn,
                          @RequestHeader("TN-VERSION") String version,
                           @RequestHeader("TN-IUID") Long iuid,
                          @RequestHeader(value = "TN-APP-MARKET", required = false) Integer market ,
                           @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                          @RequestBody Alarm param)

    {

        return setupService.processSetupAlram(request,  new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey,  market, null),
               new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), param);

    }

    @RequestMapping(value="/home/sched/reg", method = RequestMethod.POST)
    @ResponseBody
    public Object home_sched_reg(HttpServletRequest request,
                           @RequestHeader("TN-REQ-USER") String mdn,
                           @RequestHeader("TN-VERSION") String version,
                           @RequestHeader("TN-IUID") Long iuid,
                           @RequestHeader(value = "TN-APP-MARKET", required = true) Integer market ,
                           @RequestHeader(value = "TN-AUTH-KEY", required = true) String cAuthKey,
                           @RequestBody HomeSchedIuid homeSchedIuid)

    {

        return setupService.processSetupHomeIuid(request, new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey, market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), homeSchedIuid);

    }

    @RequestMapping(value="/0701/home/sched/info", method = RequestMethod.GET)
    @ResponseBody
    public Object home_sched_info(HttpServletRequest request,
                                 @RequestHeader("TN-REQ-USER") String mdn,
                                 @RequestHeader("TN-VERSION") String version,
                                 @RequestHeader("TN-IUID") Long iuid,
                                 @RequestHeader(value = "TN-APP-MARKET", required = true) Integer market ,
                                 @RequestHeader(value = "TN-AUTH-KEY", required = true) String cAuthKey)

    {


        return setupService.processHomeSchedInfo(request, new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey, market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null));

    }

    @RequestMapping(value="/0702/home/sched/reg", method = RequestMethod.POST)
    @ResponseBody
    public Object home_sched_info_reg(HttpServletRequest request,
                                  @RequestHeader("TN-REQ-USER") String mdn,
                                  @RequestHeader("TN-VERSION") String version,
                                  @RequestHeader("TN-IUID") Long iuid,
                                  @RequestHeader(value = "TN-APP-MARKET", required = true) Integer market ,
                                  @RequestHeader(value = "TN-AUTH-KEY", required = true) String cAuthKey,
                                  @RequestBody HomeSched homeSched)

    {


        return setupService.processHomeSchedReg(request, new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey, market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), homeSched);

    }


    @RequestMapping(value="/0703/home/sched/edit", method = RequestMethod.POST)
    @ResponseBody
    public Object home_sched_info_edit(HttpServletRequest request,
                                      @RequestHeader("TN-REQ-USER") String mdn,
                                      @RequestHeader("TN-VERSION") String version,
                                      @RequestHeader("TN-IUID") Long iuid,
                                      @RequestHeader(value = "TN-APP-MARKET", required = true) Integer market ,
                                      @RequestHeader(value = "TN-AUTH-KEY", required = true) String cAuthKey,
                                      @RequestBody HomeSched homeSched)

    {


        return setupService.processHomeSchedEdit(request, new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey, market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), homeSched);

    }

    @RequestMapping(value="/0704/home/sched/del", method = RequestMethod.POST)
    @ResponseBody
    public Object home_sched_info_del(HttpServletRequest request,
                                       @RequestHeader("TN-REQ-USER") String mdn,
                                       @RequestHeader("TN-VERSION") String version,
                                       @RequestHeader("TN-IUID") Long iuid,
                                       @RequestHeader(value = "TN-APP-MARKET", required = true) Integer market ,
                                       @RequestHeader(value = "TN-AUTH-KEY", required = true) String cAuthKey,
                                       @RequestBody HomeSched homeSched)
    {

        return setupService.processHomeSchedDel(request, new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey, market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), homeSched);

    }

    @RequestMapping(value="/0705/home/sched/seq", method = RequestMethod.POST)
    @ResponseBody
    public Object home_sched_info_seq(HttpServletRequest request,
                                      @RequestHeader("TN-REQ-USER") String mdn,
                                      @RequestHeader("TN-VERSION") String version,
                                      @RequestHeader("TN-IUID") Long iuid,
                                      @RequestHeader(value = "TN-APP-MARKET", required = true) Integer market ,
                                      @RequestHeader(value = "TN-AUTH-KEY", required = true) String cAuthKey,
                                      @RequestBody HomeSched homeSched)
    {

        return setupService.processHomeSchedSeq(request, new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey, market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), homeSched);

    }
}
