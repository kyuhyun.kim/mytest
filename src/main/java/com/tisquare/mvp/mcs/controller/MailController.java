package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.service.AlarmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */


/*
@startuml
hide footbox
        title 서비스 공지사항 조회
        actor UE_B2C #red
        actor UE_B2B #blue

        participant NGINX
        participant ACS

        database SVC_DB

        note over UE_B2C : 서비스 공지사항 \n리스트 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/notice/list


        note over UE_B2B : 서비스 공지사항\n리스트 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/notice/list

        NGINX -> ACS : HTTP(POST) / notice/list

        alt successful case
        ACS-> SVC_DB : 서비스 공지사항 리스트 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200 OK (공지사항 목록 리스트)


        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


        autonumber stop
        note over UE_B2C : 공지사항 내역 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/notice/sid


        note over UE_B2B : 공지 사항 내역 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/notice/sid


        NGINX -> ACS : HTTP(POST) / notice/sid

        alt successful case
        ACS-> SVC_DB : 공지 사항 내역 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200OK (공지사항 내역 정보)

        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


@enduml
*/

@Controller
@RequestMapping(value="acs/")
public class MailController {

    private static final Logger logger = LoggerFactory.getLogger(MailController.class);


    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private McsConfig mcsConfig;

    @RequestMapping(value="/mail", method = RequestMethod.GET)
    @ResponseBody
    public Object AlarmJoin(HttpServletRequest request)
    {






/*        String setfrom = "bite2058.kk@gmail.com";
        String tomail  = "kyuhyun.kim@tisquare.com";
        String title   = "제목";
        String content = "내용";*/

        try {

            Resource resource = new ClassPathResource("samsung.html");
            System.out.println("파일사이즈::"+resource.getFile().length());
            System.out.println("파일절대경로+파일명:"+resource.getURI().getPath().substring(1));


            MimeMessage message = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper
                    = new MimeMessageHelper(message, true, "UTF-8");

       /*     messageHelper.setFrom(setfrom);  // 보내는사람 생략하거나 하면 정상작동을 안함

            messageHelper.setTo(tomail);     // 받는사람 이메일

            messageHelper.setSubject(title); // 메일제목은 생략이 가능하다

            messageHelper.setText(content);  // 메일 내용
*/
            mailSender.send(message);

        } catch(Exception e){

            System.out.println(e);

        }
        return null;
    }
}
