package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.db.WebViewDao;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.domain.HelpNoticeCnt;
import com.tisquare.mvp.mcs.model.HelpJh;
import com.tisquare.mvp.mcs.model.NoticeJh;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jhkim on 14. 3. 3.
 */

@Controller
@RequestMapping(value = "/mcs/list")
public class HlistController {
    private static Logger logger = LoggerFactory.getLogger(HlistController.class);

    @Autowired
    private InitConfig config;

//    @Autowired
//    private NoticeFacade noticeFacade;

    @Autowired
    private WebViewDao viewDao;


//    @Autowired
//    private HelpFacade helpFacade;

//    @Autowired
//    private DemandFacade demandFacade;

    @RequestMapping(value = "/more.do", method= RequestMethod.GET)
    public String getList(HttpSession session, @RequestParam Map<String, Object> paramMap, Model model) {
        //category_id 0 은 공지사항 1은 개선사항

//        String id = paramMap.get("id").toString();
//        String pageindex = paramMap.get("page_index").toString();
//        String category_id = paramMap.get("category_id").toString();
//
//        int page_index =Integer.parseInt(pageindex);
//        int page = Integer.parseInt(this.config.getListNum());
//
//        if(page_index == 0){
//            paramMap.put("startRow", page_index);
//        }else if (page_index == 1){
//            paramMap.put("startRow", (page_index-1));
//        }else{
//            paramMap.put("startRow", ((page_index-1)* page));
//        }
//
//        if (config.getDb().equals("mysql")) {
//            paramMap.put("endRow", page );
//        }
//        else {
//            paramMap.put("endRow", (Integer)(paramMap.get("startRow")) + page );
//        }
//
//
//        // 0:공지사항
//        if(category_id.equals("0")){
//            List<Notice> noticeList= noticeFacade.findList(paramMap );
//
//            // 날짜 추가
//            List<String> day = new LinkedList<String>();
//            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd");
//
//            for(int i=0; i<noticeList.size(); i++) {
//                Date d = noticeList.get(i).getCreate_date();
//                String input = format.format(d);
//                day.add(input);
//            }
//
//            model.addAttribute("date", day);
//            model.addAttribute("list", noticeList);
//
//            int totalCount = noticeFacade.getCount();
//            int totalPage = (totalCount / page) + (((totalCount % page) > 0)? 1:0);
//            model.addAttribute("totalCount", totalCount);
//            model.addAttribute("currentPage", page_index);
//            model.addAttribute("totalPage", totalPage);
//
//            if (config.getService().equals("ringding")) {
//                return "ringding/listData";
//            }
//            return "listData";
//        }
//        // 1:도움말
//        else if(category_id.equals("1")){
//            List<Help> helpList= helpFacade.findList(paramMap );
//
//            // 날짜 추가
//            List<String> day = new LinkedList<String>();
//            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd");
//
//            for(int i=0; i<helpList.size(); i++) {
//                Date d = helpList.get(i).getCreate_date();
//                String input = format.format(d);
//                day.add(input);
//            }
//
//            model.addAttribute("date", day);
//            model.addAttribute("list", helpList);
//
//            int totalCount = helpFacade.getCount();
//            int totalPage = (totalCount / page) + (((totalCount % page) > 0)? 1:0);
//            model.addAttribute("totalCount", totalCount);
//            model.addAttribute("currentPage", page_index);
//            model.addAttribute("totalPage", totalPage);
//
//            if (config.getService().equals("ringding")) {
//                return "ringding/listData";
//            }
//            return "listData";
//        }
//        // 2: 건의사항
//        else if(category_id.equals("2")){
//            List<Demand> demandList= demandFacade.findList(paramMap);
//            model.addAttribute("list", demandList);
//
//            if (config.getService().equals("ringding")) {
//                return "ringding/listData";
//            }
//            return "listData";
//        }else{
//            model.addAttribute("list", new ArrayList<Notice>());
//            return "intro";
//        }
        return "intro";
    }

    @RequestMapping(value="/noticeJson.do", method = RequestMethod.GET)
    @ResponseBody
    public Object noticeMore(@RequestParam Map<String, Object> paramMap){

        String start_sid;
        String LRead_id;

        if(paramMap.get("start_sid") != null){
            start_sid = paramMap.get("start_sid").toString();
        } else
            start_sid = "0";

        try
        {
            if(paramMap.get("last_read_id") != null){
                LRead_id = paramMap.get("last_read_id").toString();
            } else
                LRead_id = "0";
        } catch(Exception e){
            logger.info("", e);
            LRead_id = "0";
        }


        int sid =Integer.parseInt(start_sid);
        int pageCnt = Integer.parseInt(this.config.getListNum());
        int eid = sid + pageCnt;
        int isEnd = 0;
        int last_read_id=0;

        if(sid == 1) sid = 0;
        paramMap.put("sid", (sid));

        HelpNoticeCnt helpNoticeCnt = viewDao.selectNoticeTotCnt();
        if( (eid) >= helpNoticeCnt.getCnt() ){
            eid = helpNoticeCnt.getCnt();
            isEnd = 1;
        }

//        if (config.getDb().equals("mysql")) {
//            paramMap.put("eid", eid );
//        }
//        else {
//            paramMap.put("eid", (Integer)(paramMap.get("startRow")) + eid );
//        }

        paramMap.put("eid", eid );
        try{
            if(LRead_id != null)
                last_read_id = Integer.parseInt(LRead_id);
        }catch(Exception e){
            logger.info("", e);
            last_read_id = 0;
        }
        paramMap.put("l_read_id", last_read_id);

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("sid", sid);
        param.put("eid", eid);
        param.put("l_read_id", last_read_id);



        //List<NoticeJh> moreList = noticeFacade.moreList(paramMap);
        List<NoticeJh> moreList = viewDao.selectNoticeMoreList(param);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("result", 1);
        result.put("isEnd", isEnd);
        result.put("EndNum", eid);
        result.put("TotCnt", helpNoticeCnt.getCnt());
        result.put("items", moreList);

        //return new ModelAndView("jsonReport", result);
        return result;
    }

    @RequestMapping(value="/helpJson.do", method = RequestMethod.GET)
    @ResponseBody
    public Object helpMore(@RequestParam Map<String, Object> paramMap){

        String start_sid;
        String cate;

        if(paramMap.get("start_sid") != null){
            start_sid = paramMap.get("start_sid").toString();
        } else
            start_sid = "0";


        if(paramMap.get("cate") != null){
            cate = paramMap.get("cate").toString();
        } else
            cate = "0";

        int sid =Integer.parseInt(start_sid);
        int pageCnt = Integer.parseInt(this.config.getListNum());
        int eid = sid + pageCnt;
        int isEnd = 0;


        if(sid == 1) sid = 0;
        paramMap.put("sid", (sid));

        HelpNoticeCnt helpNoticeCnt = viewDao.selectHelpTotCnt();
        if( (eid) >= helpNoticeCnt.getCnt() ){
            eid = helpNoticeCnt.getCnt();
            isEnd = 1;
        }

//        if (config.getDb().equals("mysql")) {
//            paramMap.put("eid", eid );
//        }
//        else {
//            paramMap.put("eid", (Integer)(paramMap.get("startRow")) + eid );
//        }
        paramMap.put("eid", eid );

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("sid", sid);
        param.put("eid", eid);

        List<HelpJh> moreList = viewDao.selectHelpeMoreList(param);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("result", 1);
        result.put("isEnd", isEnd);
        result.put("EndNum", eid);
        result.put("TotCnt", helpNoticeCnt.getCnt());
        result.put("items", moreList);

        return result;
    }


    @RequestMapping(value = "/moreshow.do", method=RequestMethod.GET)
    public String moreshowList(@RequestParam Map<String, String> paramMap, Model model) {
//        String selected_id = paramMap.get("selected_id").toString();
//        String category_id = paramMap.get("category_id").toString();
//
//        long id =Integer.parseInt(selected_id);
//
//        // 0:공지사항
//        if(category_id.equals("0")){
//            Notice notice= noticeFacade.findById(id);
//
//            // 날짜 추가
//            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd");
//
//            Date d = notice.getCreate_date();
//            String day = format.format(d);
//
//            model.addAttribute("date", day);
//            // end
//
//            model.addAttribute("obj", notice);
//        }
//        // 1:도움말
//        else if(category_id.equals("1")){
//            Help help= helpFacade.findById(id);
//
//            // 날짜 추가
//            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd");
//
//            Date d = help.getCreate_date();
//            String day = format.format(d);
//
//            model.addAttribute("date", day);
//            // end
//
//            model.addAttribute("obj", help);
//        }
//        // 2: 건의사항
//        else if(category_id.equals("2")){
//            Demand demand= demandFacade.findById(id);
//            model.addAttribute("obj", demand);
//        }else{
//
//        }
//
//        if (config.getService().equals("ringding")) {
//            return "ringding/objData";
//        }
        return "objData";
    }
}

