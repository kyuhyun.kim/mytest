package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.model.TraceReq;
import com.tisquare.mvp.mcs.service.TraceService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Created by kimkyuhyun on 2015-07-28.
 */
@Controller
@RequestMapping(value = "/acs")
public class TraceController {

    private static Logger logger = LoggerFactory.getLogger(TraceController.class);

    @Autowired
    private TraceService traceService;


    @RequestMapping(value = "/trace", method = RequestMethod.POST)
    public ResponseEntity<String> configTrace(@RequestHeader("REQ-TRACE") String trace,
                                              @RequestBody(required = false) TraceReq traceReq) {

        if (StringUtils.equals(trace, "on")) {
            return traceService.startTrace(traceReq);
        } else {
            traceService.stopTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
