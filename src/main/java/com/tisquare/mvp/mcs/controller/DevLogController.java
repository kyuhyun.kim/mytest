package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.db.CrashLogDao;
import com.tisquare.mvp.mcs.db.domain.DevCrashLog;
import com.tisquare.mvp.mcs.model.CrashLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */


/*
@startuml
hide footbox
        title 단말 로그 수집
        actor UE_B2C #red
        actor UE_B2B #blue

        participant NGINX
        participant ACS

        database SVC_DB

        note over UE_B2C : 단말 fault 발생


        UE_B2C -> NGINX : HTTP(POST) /acs/log/crash


        autonumber stop
        note over UE_B2B : 단말 fault 발생
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/log/crash

        NGINX -> ACS : HTTP(POST) / log/crash

        alt successful case
        ACS-> SVC_DB : 단말 수집 로그 정보 저장



        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200 OK (성공)


        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end








@enduml
*/
@Controller
@RequestMapping(value="acs/")
public class DevLogController {
    private static final Logger logger = LoggerFactory.getLogger(DevLogController.class);

    @Autowired
    CrashLogDao crashLogDao;

@RequestMapping(value="/log/crash", method = RequestMethod.POST)
@ResponseBody
public Object CrashLog(@RequestHeader("TN-IUID") String iuid,
                       @RequestHeader("TN-REQ-USER") String mdn,
                       @RequestHeader("TN-VERSION") String version,
                       @RequestBody DevCrashLog param) {



        if((iuid == null) || (mdn == null) || (version == null) ){
            logger.info("iuid[{}] mdn[{}] version[{}] header  is null", iuid, mdn, version);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }



        param.setIuid(iuid);
        param.setMdn(mdn);
        param.setApp_version(version);

        CrashLog crashLog = new CrashLog(param);
        try {
            crashLogDao.insertCrashLog(crashLog);
        }catch (Exception e){
            logger.error("DB INSERT FAILED", e);
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(HttpStatus.OK);
    }




}

