package com.tisquare.mvp.mcs.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tisquare.mvp.mcs.HttpMsgService;
import com.tisquare.mvp.mcs.db.TDao;
import com.tisquare.mvp.mcs.db.UrlMapDao;
import com.tisquare.mvp.mcs.db.domain.SnsStat;
import com.tisquare.mvp.mcs.db.domain.UrlMap;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.model.ShortUrlMap;
import com.tisquare.mvp.mcs.model.TraceReq;
import com.tisquare.mvp.mcs.model.UrlShortData;
import com.tisquare.mvp.mcs.urlshort.UrlShortService;
import com.tisquare.mvp.mcs.urlshort.UrlShortnerNotFoundException;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Target;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.HashMap;

/**
 * Created by asus on 2015-03-01.
 */

/*
@startuml
hide footbox
        title 단축 URL 생성 요청
        actor UE_B2C #red
        actor UE_B2B #blue

        participant NGINX
        participant ACS

        database SVC_DB

        note over UE_B2C : 채널 초대

        UE_B2C -> NGINX : HTTP(POST) /acs/{sid}/urlshortener


        note over UE_B2B : 채널 초대
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/{sid}/urlshortener

        NGINX -> ACS : HTTP(POST) /{sid}/urlshortener

        alt successful case
        ACS-> ACS : 10자리 RAMDOM 문자 생성.

        ACS-> SVC_DB : 단축 URL 중복 검사

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200 OK \n<font color=blue><B>(단축 URL 정보)


        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end




@enduml
*/

@Controller
@RequestMapping(value="acs/")
public class UrlShortnerController {
    private static Logger logger = LoggerFactory.getLogger(UrlShortnerController.class);

    @Autowired
    TDao tdao;

    @Autowired
    UrlMapDao urlMapDao;

    @Autowired
    InitConfig initConfig;

    @Autowired
    UrlShortService urlService;

    @Autowired
    HttpMsgService httpMsgService;

    ObjectMapper mapper = new ObjectMapper();

    @RequestMapping(value="/sns", method = RequestMethod.GET)
    public Object UrlSnsStat(
            @RequestHeader(value = "TN-SID") long sid,
            @RequestHeader(value = "TN-SNS-TYPE") Integer sns_type)
    {
        if(sid == 0 || sns_type == 0)
            return new ResponseEntity<String>("잘못 된 sid 또는 sns_type 입니다.", HttpStatus.BAD_REQUEST);


        String CurrentDate = DateUtil.toFormatString();

        SnsStat snsStat = new SnsStat();

        try
        {
            snsStat.setReg_dt(CurrentDate.substring(0, 8));
            snsStat.setReg_tm(CurrentDate.substring(8, 10));
            snsStat.setSid(sid);
            snsStat.setSns(sns_type);
            snsStat.setCnt(1);
            urlMapDao.updateSnsStat(snsStat);
        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return new ResponseEntity<String>("server_Err", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>( HttpStatus.OK);
    }


    @RequestMapping(value="/{sid}/urlshortener", method = RequestMethod.POST)
    public Object UrlShortener(
            HttpServletRequest req,
//        @RequestHeader(value)
            @PathVariable("sid") Long sid,
            @RequestHeader(value = "TN-PASSWD-IS") Integer passwd_is,
            @RequestHeader(value = "TN-PASSWD",  required = false) String passwd,
            @RequestHeader(value = "TN-SNS-TYPE",  required = false) String sns_type,
            @RequestBody String jsonBody){


        if(sid == null || jsonBody == null){
            return new ResponseEntity<String>("Can't find info", HttpStatus.BAD_REQUEST);
        }


        String redirectUrl = null;

        UrlShortData jsonData = null;
        UrlMap urlMap = null;

        try{
            jsonData = mapper.readValue(jsonBody, UrlShortData.class);
        } catch (Exception e){
            logger.error("JSon parse error. jsonData[" + jsonBody + "] :" , e);
            return new ResponseEntity<String>("Invalid json format", HttpStatus.BAD_REQUEST);
        }


        try {
            redirectUrl =tdao.existSid(sid);

        }catch (Exception e){
            logger.error("sid error. [" + sid + "] : ", e);
            return new ResponseEntity<String>("INTERNAL SERVER ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        StringBuilder TargetUrl = new StringBuilder();

        String expiredDate = urlService.getExpireDate(jsonData.getSvcType());

        try {
            if (redirectUrl == null) {

                for (int i = 0; i < 100; i++) {
                    redirectUrl = urlService.makeRandom10Cahr();
                    logger.info("Temp redirectUrl[{}] loop[{}]", redirectUrl, i);
                    if (urlMapDao.existUrlCheck(redirectUrl)) {
                        logger.info("Already use url[{}]", redirectUrl);
                        redirectUrl = null;
                        continue;
                    } else
                        break;
                }


                if (redirectUrl == null) {
                    logger.error("redirectUrl is null. check make url logic");
                    return new ResponseEntity<String>("make redirectUrl failed", HttpStatus.INTERNAL_SERVER_ERROR);
                }

                if (passwd == null)
                    passwd = "";
                String Urlencoding = URLEncoder.encode("sid=" + sid + "&passwd=" + passwd , "UTF-8");
                TargetUrl.append(initConfig.getUrl_domain() + initConfig.getUrlshortPfxDomain() + "?" + "referrer=" + Urlencoding);


                urlMap = new UrlMap();
                urlMap.setShortUrl(redirectUrl);
                urlMap.setTargetUrl(TargetUrl.toString());
                urlMap.setSvcType(jsonData.getSvcType());
                urlMap.setSid(sid);
                urlMap.setExpired_Date(expiredDate);
                urlMapDao.insertUrlMap(urlMap);

            } else {
                if (passwd == null || passwd_is == 0)
                    passwd = "";


                String Urlencoding = URLEncoder.encode("sid=" + sid + "&passwd=" + passwd , "UTF-8");
                TargetUrl.append(initConfig.getUrl_domain() + initConfig.getUrlshortPfxDomain() + "?" + "referrer=" + Urlencoding);

                urlMap = new UrlMap();
                urlMap.setShortUrl(redirectUrl);
                urlMap.setTargetUrl(TargetUrl.toString());
                urlMap.setSid(sid);
                urlMapDao.updateUrlMap(urlMap);
            }

            logger.info("ORIGIN URL [" + TargetUrl.toString() + "]");
            logger.info("SHORT URL [" + redirectUrl + "]");


        }catch (Exception e)
        {
            logger.error(e.toString());
        }

        HashMap<String, String> rsp = new HashMap<String, String>();
        rsp.put("redirect-url", String.format("%s/%s", initConfig.getUrl_domain() + initConfig.getUrlPfxDomain(), redirectUrl));
        rsp.put("expire-date", expiredDate);

        logger.debug("Response targetUrl[{}] expireDate[{}]",
                rsp.get("redirect-url"), rsp.get("expire-date"));

        return new ResponseEntity(rsp, HttpStatus.OK);
    }



    @RequestMapping(value="/{shorturl}/url", method = RequestMethod.GET)
    public Object UrlSeach(
            HttpServletRequest req,
            @PathVariable("shorturl") String short_url){



        if(short_url == null ){
            return new ResponseEntity<String>("Can't find info", HttpStatus.BAD_REQUEST);
        }

        try {
            ShortUrlMap shortUrlMap = tdao.existSidTargetUrl(short_url);


            if(shortUrlMap == null){
                return new ResponseEntity<String>("find target_url", HttpStatus.NOT_ACCEPTABLE);
            }

            return new ResponseEntity(shortUrlMap, HttpStatus.OK);


        }catch (Exception e){
            logger.error("short_url error. [" + short_url + "] : ", e);
            return new ResponseEntity<String>("INTERNAL SERVER ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @RequestMapping(value="s/{redirectUrl}", method = RequestMethod.GET)
    public Object redirectUrl(
            HttpServletRequest req,
            @PathVariable("redirectUrl") String redirectUrl
    ){

        UrlMap targetUrl;
        try{
            targetUrl = urlMapDao.selectTargetUrl(redirectUrl);
        } catch (Exception e){
            logger.error("Select failed redirectUrl[" + redirectUrl + "] : ", e);
            return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if(targetUrl == null){
            logger.error("targetUrl not found redirectUrl[{}]", redirectUrl);
            throw new UrlShortnerNotFoundException();
//            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        logger.info("Redirect redirectUrl[{}] targetUrl[{}]", redirectUrl, targetUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", targetUrl.getTargetUrl());

        try {
            long expireDate = urlService.strExpireDateToLong(targetUrl.getExpired_Date());
            headers.setExpires(expireDate);
            logger.info("RedirectUrl[{}]Set ExpiredDate[{}]", redirectUrl, expireDate);
        } catch (ParseException pe) {
            logger.error("dateFormat parse Exception. ExpiredDate[" + targetUrl.getExpired_Date() + "] :", pe);
        }

        return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
    }

    @RequestMapping(value="/i", method = RequestMethod.GET)
    public Object redirectUrl(
            HttpServletRequest req

    ){

        HttpHeaders headers = new HttpHeaders();


        try {
            headers.add("Location", initConfig.getUrl_domain() +initConfig.getURL_DL_DOMAIN() );
        } catch (Exception pe) {
            logger.error( pe.toString() );
        }

        return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
    }


    @RequestMapping(value="test/{redirectUrl}", method = RequestMethod.GET)
    public Object test(
            HttpServletRequest req,
            @PathVariable("redirectUrl") String redirectUrl
    ){


        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", redirectUrl);



        return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
    }
}
