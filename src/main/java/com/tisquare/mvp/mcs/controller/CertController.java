package com.tisquare.mvp.mcs.controller;


import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.CertifiBssService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */

/*
@startuml
hide footbox
        title B2B 로그인 인증
        actor UE_A
        participant BSS
        participant ACS

        database SVC_DB

        note over UE_A : 앱 기동
        UE_A -> BSS : HTTP / authbss

        BSS-> ACS : HTTP / authbss  Client 인증 요청 (기업코드, 기업 사용자 ID)

        alt successful case
        ACS-> SVC_DB : 고객 정보 조회 (기업코드, 기업 사용자 ID)

        ACS<- SVC_DB : (고객 정보, 프로필 정보 서버연동정보, 앱 업데이트, 작업 공지)

        ACS->ACS: config확인\n

        note right: BT 기기 정보, 약관 정보
        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_A : <font color=blue><b>HTTP / 200 OK (인증 정보)

        else  미 가입자 case
        ACS -[#FF0000]-> UE_A : <font color=red><b>HTTP / 404 NOT FOUND

        else 인증 키 불일 치 case
        ACS -[#FF0000]-> UE_A : <font color=red><b>HTTP / 403 FORBIDDEN

        else 고객 정보 불일치 case
        ACS -[#FF0000]-> UE_A : <font color=red><b>HTTP / 406 METHOD_NOT_ALLOWED

        else 앱 강제 업데이트 case
        ACS -[#FF0000]-> UE_A : <font color=red><b>HTTP / 406 NOT_ACCEPTABLE

        else 작업 공지 case
        ACS -[#FF0000]-> UE_A : <font color=red><b>HTTP / 503 SERVICE_UNAVAILABLE

        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_A : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end
        == 조직도 업데이트 ==

@enduml
*/
@Controller
@RequestMapping(value="acs/")
public class CertController {

    private static final Logger logger = LoggerFactory.getLogger(CertController.class);



    @Autowired
    CertifiBssService certifiBssService;

    @RequestMapping(value="/mvpinfo", method = RequestMethod.POST)
    @ResponseBody
    public Object mvpinfo(HttpServletRequest request,
                           @RequestHeader("TN-VERSION") String version,
                           @RequestHeader(value = "TN-SUBS-TYPE") int subs_type,
                           @RequestHeader(value = "TN-APP-MARKET") Integer market ,
                           @RequestParam(value = "os-type") String osType,
                           @RequestParam(value = "os-version") String osVersion,
                           @RequestBody(required = false) UsInfo param)

    {
        return certifiBssService.processClientMvpInfoRequest(request, new ClientAuthReqHeaders(null, null, version, null, market, subs_type),
                new ClientAuthReqParams(osType, osVersion, null, null, null, null, null, null, null, null), param);
    }

    @RequestMapping(value="/authinfo", method = RequestMethod.POST)
    @ResponseBody
    public Object authinfo(HttpServletRequest request,
                           @RequestHeader("TN-REQ-USER") String mdn,
                           @RequestHeader("TN-VERSION") String version,
                           @RequestHeader(value = "TN-SUBS-TYPE") int subs_type,
                           @RequestHeader(value = "TN-APP-MARKET") Integer market ,
                           @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                           @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                           @RequestParam(value = "os-type") String osType,
                           @RequestParam(value = "os-version") String osVersion,
                           @RequestParam(value = "device-id") String deviceID,
                           @RequestParam(value = "telecom-code") String telecomCode,
                           @RequestParam(value = "telecom-str", required = false) String telecomStr,
                           @RequestParam(value = "net-type", required = false) String netType,
                           @RequestParam(value = "nid", required = false) Long nid,
                           @RequestParam(value = "hid", required = false) Long hid,
                           @RequestParam(value = "sid", required = false) Long sid,
                           @RequestParam(value = "device-model", required = false) String deviceModel,
                           @RequestBody(required = false) UsInfo param)

    {
        return certifiBssService.processClientAuthInfoRequest(request, new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey,  market, subs_type),
                new ClientAuthReqParams(osType, osVersion, deviceID, telecomCode, telecomStr, netType, nid, hid, sid, deviceModel), param);
    }


    @RequestMapping(value="/logout", method = RequestMethod.GET)
    @ResponseBody
    public Object logout(HttpServletRequest request,
                       @RequestHeader("TN-IUID") Long iuid,
                       @RequestHeader(value = "TN-AUTH-KEY") String cAuthKey,
                       @RequestHeader(value = "TN-APP-MARKET", required = false) Integer market)
    {
        return certifiBssService.processClientlogoutRequest(request, new ClientAuthReqHeaders(iuid, null, null, cAuthKey, market), null);

    }

    @RequestMapping(value="/passwd", method = RequestMethod.POST)
    @ResponseBody
    public Object passwd_init(HttpServletRequest request,
                              @RequestHeader("TN-IUID") long iuid,
                              @RequestHeader("TN-AUTH-KEY") String cAuthKey,
                              @RequestBody UsInfo param)
    {
        return certifiBssService.processClientPwdChgRequest(request, new ClientAuthReqHeaders(iuid, null, null, cAuthKey, null), null, param);

    }


    @RequestMapping(value="/passwd/auth", method = RequestMethod.POST)
    @ResponseBody
    public Object authinfo(HttpServletRequest request,
                           @RequestHeader("TN-REQ-USER") String mdn,
                           @RequestBody UsInfo param)

    {

        return certifiBssService.processClienPwdAuthRequest(request, new ClientAuthReqHeaders(0L, mdn, null, null, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), param);
    }


    @RequestMapping(value="/chg_mdn", method = RequestMethod.GET)
    @ResponseBody
    public Object chg_mdn(HttpServletRequest request,
                              @RequestHeader("TN-IUID") long iuid,
                              @RequestHeader("TN-AUTH-KEY") String cAuthKey,
                              @RequestHeader("TN-REQ-USER") String phoneNo)
    {
        return certifiBssService.processClientMdnChgRequest(request, new ClientAuthReqHeaders(iuid, phoneNo, null, cAuthKey, null), null);

    }


    @RequestMapping(value="/0301/id", method = RequestMethod.POST)
    @ResponseBody
    public Object id_0301(HttpServletRequest request,
                          @RequestHeader("TN-APP-MARKET") Integer market ,
                          @RequestHeader("TN-VERSION") String version,
                          @RequestHeader("TN-REQ-USER") String phoneNo,
                          @RequestParam(value = "os-type") String osType,
                          @RequestParam(value = "os-version") String osVersion,
                          @RequestBody ProfileDetail profileDetail)
    {
        return certifiBssService.processClientSearchIDRequest(request, new ClientAuthReqHeaders(null, phoneNo, null, null, null),
                new ClientAuthReqParams(osType, osVersion, null, null, null, null, null, null, null, null), profileDetail);

    }

    @RequestMapping(value="/active", method = RequestMethod.GET)
    @ResponseBody
    public Object authinfo(HttpServletRequest request,
                           @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                           @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                           @RequestParam(value = "os-type") String osType,
                           @RequestParam(value = "os-version") String osVersion,
                           @RequestParam(value = "device-id") String deviceID
                           )
    {
        return certifiBssService.processClientActiveRequest(request, new ClientAuthReqHeaders(iuid, null, null, cAuthKey, null, null),
                new ClientAuthReqParams(osType, osVersion, deviceID, null, null, null, null, null, null, null));
    }

    @RequestMapping(value="/user/search", method = RequestMethod.POST)
    @ResponseBody
    public Object user_search(HttpServletRequest request,
                              @RequestHeader("TN-IUID") Long iuid,
                              @RequestHeader(value = "TN-AUTH-KEY") String cAuthKey,
                              @RequestBody UserSearchInfo param)

    {
        return certifiBssService.processClientUserSearchRequest(request, new ClientAuthReqHeaders(iuid, null, null, cAuthKey, null), null, param);

    }
}
