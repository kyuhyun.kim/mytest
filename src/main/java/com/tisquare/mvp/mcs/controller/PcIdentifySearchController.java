package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.UsInfo;
import com.tisquare.mvp.mcs.model.pc.IdentifyPasswd;
import com.tisquare.mvp.mcs.model.pc.PcAuthInfo;
import com.tisquare.mvp.mcs.service.CertifiBssService;
import com.tisquare.mvp.mcs.service.PcIdentifyPasswdService;
import com.tisquare.mvp.mcs.service.PromotionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */


/*
@startuml
hide footbox
        title 서비스 공지사항 조회
        actor UE_B2C #red
        actor UE_B2B #blue

        participant NGINX
        participant ACS

        database SVC_DB

        note over UE_B2C : 서비스 공지사항 \n리스트 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/notice/list


        note over UE_B2B : 서비스 공지사항\n리스트 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/notice/list

        NGINX -> ACS : HTTP(POST) / notice/list

        alt successful case
        ACS-> SVC_DB : 서비스 공지사항 리스트 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200 OK (공지사항 목록 리스트)


        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


        autonumber stop
        note over UE_B2C : 공지사항 내역 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/notice/sid


        note over UE_B2B : 공지 사항 내역 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/notice/sid


        NGINX -> ACS : HTTP(POST) / notice/sid

        alt successful case
        ACS-> SVC_DB : 공지 사항 내역 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200OK (공지사항 내역 정보)

        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


@enduml
*/

@Controller
@RequestMapping(value="acs/")
public class PcIdentifySearchController {

    private static final Logger logger = LoggerFactory.getLogger(PcIdentifySearchController.class);


    @Autowired
    private PcIdentifyPasswdService pcIdentifyPasswdService;

    @Autowired
    private CertifiBssService certifiBssService;

    @Autowired
    PromotionService promotionService;

    @RequestMapping(value="/pc/id", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_id(HttpServletRequest request,
                            @RequestHeader(value = "TN-VERSION", required = false) String version,
                            @RequestParam(value = "os-type") String osType,
                            @RequestParam(value = "os-version") String osVersion,
                            @RequestBody(required = true) IdentifyPasswd identifyPasswd)
    {

        return pcIdentifyPasswdService.processPcIdRequest(request,  new ClientAuthReqHeaders(null, null, version, null,  null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), identifyPasswd);
    }

    @RequestMapping(value="/pc/passwd/sms", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_passwd_sms(HttpServletRequest request,
                                @RequestHeader(value = "TN-VERSION", required = false) String version,
                                @RequestParam(value = "os-type") String osType,
                                @RequestParam(value = "os-version") String osVersion,
                                @RequestBody(required = true) IdentifyPasswd identifyPasswd)
    {

        return pcIdentifyPasswdService.processPcPasswdSmsRequest(request, new ClientAuthReqHeaders(null, null, version, null, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), identifyPasswd);
    }

    @RequestMapping(value="/pc/passwd/cert", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_passwd_cert(HttpServletRequest request,
                                 @RequestHeader(value = "TN-VERSION", required = false) String version,
                                 @RequestParam(value = "os-type") String osType,
                                 @RequestParam(value = "os-version") String osVersion,
                                 @RequestBody(required = true) IdentifyPasswd identifyPasswd)
    {

        return pcIdentifyPasswdService.processPcPasswdCertRequest(request, new ClientAuthReqHeaders(null, null, version, null, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), identifyPasswd);
    }

    @RequestMapping(value="/pc/passwd/reset", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_passwd_reset(HttpServletRequest request,
                                  @RequestHeader(value = "TN-VERSION", required = false) String version,
                                  @RequestParam(value = "os-type") String osType,
                                  @RequestParam(value = "os-version") String osVersion,
                                  @RequestBody(required = true) IdentifyPasswd identifyPasswd)
    {

        return pcIdentifyPasswdService.processPcPasswdResetRequest(request,  new ClientAuthReqHeaders(null, null, version, null,  null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), identifyPasswd);
    }

    @RequestMapping(value="/pc/authinfo", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_authinfo(HttpServletRequest request,
                           @RequestHeader(value = "TN-REQ-USER", required = false) String mdn,
                           @RequestHeader(value = "TN-SUBS-TYPE") Integer subs_type,
                           @RequestHeader(value = "TN-APP-MARKET") Integer market ,
                           @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                           @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                           @RequestParam(value = "os-type") String osType,
                           @RequestParam(value = "os-version") String osVersion,
                           @RequestParam(value = "device-id") String deviceID,
                           @RequestParam(value = "telecom-code", required = false) String telecomCode,
                           @RequestParam(value = "telecom-str", required = false) String telecomStr,
                           @RequestParam(value = "net-type", required = false) String netType,
                           @RequestParam(value = "nid", required = false) Long nid,
                           @RequestParam(value = "hid", required = false) Long hid,
                           @RequestParam(value = "sid", required = false) Long sid,
                           @RequestParam(value = "device-model", required = false) String deviceModel,
                           @RequestBody(required = false) UsInfo param)

    {
        return certifiBssService.processClientPcAuthInfoRequest(request, new ClientAuthReqHeaders(iuid, mdn, null, cAuthKey, market, subs_type),
                new ClientAuthReqParams(osType, osVersion, deviceID, telecomCode, telecomStr, netType, nid, hid, sid, deviceModel), param);
    }


    @RequestMapping(value="/pc/id_send", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_id_send(HttpServletRequest request,
                                  @RequestHeader(value = "TN-VERSION", required = false) String version,
                                  @RequestParam(value = "os-type") String osType,
                                  @RequestParam(value = "os-version") String osVersion,
                                  @RequestBody(required = true) PcAuthInfo pcAuthInfo)
    {

        return pcIdentifyPasswdService.processPcId_sendResetRequest(request, new ClientAuthReqHeaders(null, null, version, null, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), pcAuthInfo);
    }

    @RequestMapping(value="/pc/join_mail", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_join_mail(HttpServletRequest request,
                               @RequestHeader(value = "TN-VERSION", required = false) String version,
                               @RequestParam(value = "os-type") String osType,
                               @RequestParam(value = "os-version") String osVersion,
                               @RequestBody(required = true) PcAuthInfo pcAuthInfo)
    {

        return pcIdentifyPasswdService.processPcJoinMailResetRequest(request, new ClientAuthReqHeaders(null, null, version, null, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), pcAuthInfo);
    }

    @RequestMapping(value="/pc/join_mail_cert", method = RequestMethod.POST)
    @ResponseBody
    public Object pc_join_mail_cert(HttpServletRequest request,
                               @RequestHeader(value = "TN-VERSION", required = false) String version,
                               @RequestParam(value = "os-type") String osType,
                               @RequestParam(value = "os-version") String osVersion,
                               @RequestBody(required = true) PcAuthInfo pcAuthInfo)
    {

        return pcIdentifyPasswdService.processPcJoinMailCertResetRequest(request, new ClientAuthReqHeaders(null, null, version, null, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), pcAuthInfo);
    }

    /*@RequestMapping(value="/pc/logout", method = RequestMethod.GET)
    @ResponseBody
    public Object logout(HttpServletRequest request,
                         @RequestHeader("TN-IUID") Long iuid, @RequestHeader(value = "TN-AUTH-KEY") String cAuthKey)
    {
        return certifiBssService.processClientPclogoutRequest(request, new ClientAuthReqHeaders(iuid, null, null, cAuthKey, null), null);

    }*/


    @RequestMapping(value="/pc/promotion", method = RequestMethod.GET)
    @ResponseBody
    public Object promotion_list(HttpServletRequest request)
    {
        return promotionService.processPromotionPcListRequest(request, new ClientAuthReqHeaders(null, null, null, null, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, null, null, null, null));
    }
}
