package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.db.domain.PromotionSamsung;
import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.PromotionCampPackInfo;
import com.tisquare.mvp.mcs.model.UsInfo;
import com.tisquare.mvp.mcs.service.PromotionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Controller
@RequestMapping(value="acs/")
public class PromotionController {

    private static final Logger logger = LoggerFactory.getLogger(PromotionController.class);


    @Autowired
    PromotionService promotionService;

/*    @RequestMapping(value="/promotion", method = RequestMethod.GET)
    @ResponseBody
    public Object promotion(HttpServletRequest request,
                        @RequestHeader("TN-REQ-USER") String mdn,
                        @RequestParam("os-type") String os_type,
                        @RequestParam(value = "os-version") String osVersion,
                        @RequestParam(value = "telecom-code") String telecomCode,
                        @RequestParam(value = "telecom-str", required = false) String telecomStr)
    {


        return promotionService.processPromotionListRequest(request, mdn, os_type, osVersion, telecomCode);

    }*/


    @RequestMapping(value="/promotion/samsung/info", method = RequestMethod.GET)
    @ResponseBody
    public Object promotion(HttpServletRequest request,
                            @RequestHeader("TN-REQ-USER") String mdn,
                            @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                            @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey
                            )
    {


        return promotionService.processPromotionSamSungInfoRequest(request, new ClientAuthReqHeaders(iuid, mdn, null, cAuthKey, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, null, null, null, null));

    }


    @RequestMapping(value="/promotion/samsung/reg", method = RequestMethod.POST)
    @ResponseBody
    public Object promotion_reg(HttpServletRequest request,
                            @RequestHeader("TN-REQ-USER") String mdn,
                            @RequestHeader(value = "TN-IUID")  Long iuid,
                            @RequestHeader(value = "TN-AUTH-KEY") String cAuthKey,
                            @RequestBody PromotionSamsung promotionSamsung)
    {


        return promotionService.processPromotionSamSungRegRequest(request, new ClientAuthReqHeaders(iuid, mdn, null, cAuthKey, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, null, null, null, null), promotionSamsung);

    }

    @RequestMapping(value="/promotion/samsung/oem", method = RequestMethod.POST)
    @ResponseBody
    public Object promotion_oem(HttpServletRequest request,
                            @RequestHeader("TN-REQ-USER") String mdn,
                            @RequestHeader(value = "TN-IUID")  Long iuid,
                            @RequestHeader(value = "TN-AUTH-KEY") String cAuthKey,
                            @RequestBody PromotionSamsung promotionSamsung)
    {


        return promotionService.processPromotionSamSungOemRequest(request, new ClientAuthReqHeaders(iuid, mdn, null, cAuthKey, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, null, null, null, null), promotionSamsung);

    }
    @RequestMapping(value="/promotion", method = RequestMethod.GET)
    @ResponseBody
    public Object promotion_list(HttpServletRequest request,
                                    @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                                    @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey)
    {
        return promotionService.processPromotionListRequest(request, new ClientAuthReqHeaders(iuid, null, null, cAuthKey, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, null, null, null, null));
    }


    @RequestMapping(value="/promotion/campack/info", method = RequestMethod.POST)
    @ResponseBody
    public Object general_promotion(HttpServletRequest request,
                            @RequestHeader("TN-REQ-USER") String mdn,
                            @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                            @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                            @RequestBody PromotionCampPackInfo promotionCampPackInfo)

    {

        return promotionService.processPromotionCampPackInfoRequest(request, new ClientAuthReqHeaders(iuid, mdn, null, cAuthKey, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, null, null, null, null), promotionCampPackInfo);

    }

    @RequestMapping(value="/promotion/campack/reg", method = RequestMethod.POST)
    @ResponseBody
    public Object general_promotion_reg(HttpServletRequest request,
                                    @RequestHeader("TN-REQ-USER") String mdn,
                                    @RequestHeader(value = "TN-IUID", required = false)  Long iuid,
                                    @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                                    @RequestBody PromotionCampPackInfo promotionCampPackInfo)
    {

        return promotionService.processPromotionCampPackInfoReg(request, new ClientAuthReqHeaders(iuid, mdn, null, cAuthKey, null, null),
                new ClientAuthReqParams(null, null, null, null, null, null, null, null, null, null), promotionCampPackInfo);

    }
}
