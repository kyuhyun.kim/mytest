package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.Identify;
import com.tisquare.mvp.mcs.model.Identify_check;
import com.tisquare.mvp.mcs.service.IdentifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Controller
@RequestMapping(value="acs/")
public class IdentifyController {

    private static final Logger logger = LoggerFactory.getLogger(IdentifyController.class);


    @Autowired
    IdentifyService identifyService;




    @RequestMapping(value="/Identify", method = RequestMethod.POST)
    @ResponseBody
    public Object Identify(HttpServletRequest request,
                          @RequestHeader("TN-REQ-USER") String mdn,
                          @RequestHeader("TN-VERSION") String version,
                           @RequestHeader("TN-IUID") Long iuid,
                          @RequestHeader(value = "TN-APP-MARKET", required = false) Integer market ,
                           @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                          @RequestBody Identify param)

    {

        return identifyService.processIdentifyRequest(request,  new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey,  market, null),
               new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), param);

    }


    @RequestMapping(value="/Identify_check", method = RequestMethod.POST)
    @ResponseBody
    public Object Identify_check(HttpServletRequest request,
                          @RequestHeader("TN-REQ-USER") String mdn,
                          @RequestHeader("TN-VERSION") String version,
                          @RequestHeader(value = "TN-APP-MARKET", required = false) Integer market ,
                          @RequestHeader("TN-IUID") Long iuid,
                          @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey,
                          @RequestBody Identify_check param)

    {

        return identifyService.processIdentifyCheckRequest(request,  new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey,  market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null), param);

    }

    @RequestMapping(value="/Identify_state", method = RequestMethod.POST)
    @ResponseBody
    public Object Identify_state(HttpServletRequest request,
                                 @RequestHeader("TN-REQ-USER") String mdn,
                                 @RequestHeader("TN-VERSION") String version,
                                 @RequestHeader(value = "TN-APP-MARKET", required = false) Integer market ,
                                 @RequestHeader("TN-IUID") Long iuid,
                                 @RequestHeader(value = "TN-AUTH-KEY", required = false) String cAuthKey)

    {
        return identifyService.processIdentifyStateRequest(request,  new ClientAuthReqHeaders(iuid, mdn, version, cAuthKey,  market, null),
                new ClientAuthReqParams(null, null, null, null, null, null, 0L, 0L, 0L, null));

    }

}
