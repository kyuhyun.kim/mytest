package com.tisquare.mvp.mcs.controller;

import com.tisquare.mvp.mcs.model.HelpReqHeaders;
import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import com.tisquare.mvp.mcs.service.HelpService;
import com.tisquare.mvp.mcs.service.NoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */



/*
@startuml
hide footbox
        title FAQ/사용안내 조회
        actor UE_B2C #red
        actor UE_B2B #blue

        participant NGINX
        participant ACS

        database SVC_DB

        note over UE_B2C : FAQ/사용안내\n리스트 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/help/list


        note over UE_B2B : FAQ/사용안내\n리스트 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/help/list

        NGINX -> ACS : HTTP(POST) / help/list

        alt successful case
        ACS-> SVC_DB : FAQ/사용안내 리스트 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200 OK (FAQ/사용안내 목록 리스트)


        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


        autonumber stop
        note over UE_B2C : FAQ/사용안내\n내역 조회 조회

        UE_B2C -> NGINX : HTTP(POST) /acs/help/hid


        note over UE_B2B : FAQ/사용안내\n내역 조회
        UE_B2B -> NGINX : HTTP(POST)/ bss/acs/help/hid


        NGINX -> ACS : HTTP(POST) / help/hid

        alt successful case
        ACS-> SVC_DB : FAQ/사용안내 내역 조회.

        autonumber  "<b>Message 0 "
        ACS -[#0000FF]-> UE_B2C : <font color=blue><b>HTTP / 200 OK (FAQ/사용안내 정보)

        else 서버 내부 에러 case
        ACS -[#FF0000]-> UE_B2C : <font color=red><b>HTTP / 500 INTERNAL_SERVER_ERROR
        end


@enduml
*/

@Controller
@RequestMapping(value="acs/")
public class HelpController {

    private static final Logger logger = LoggerFactory.getLogger(HelpController.class);



    @Autowired
    HelpService helpService;

    @RequestMapping(value="/help/list", method = RequestMethod.GET)
    @ResponseBody
    public Object NoticeList(HttpServletRequest request,
                           @RequestHeader("tn-start-hid") int start_hid,
                           @RequestHeader("tn-last-read-hid") int last_read_hid,
                           @RequestHeader("tn-help-cnt") int help_cnt)

    {

       return helpService.processHelpListRequest(request, new HelpReqHeaders(start_hid, last_read_hid, help_cnt));

    }

    @RequestMapping(value="/help/hid", method = RequestMethod.GET)
    @ResponseBody
    public Object Notice(HttpServletRequest request,
                       @RequestHeader("tn-hid") int hid)

    {

        return helpService.processHelpRequest(request, hid);

    }

}
