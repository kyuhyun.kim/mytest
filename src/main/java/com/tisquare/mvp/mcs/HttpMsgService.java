package com.tisquare.mvp.mcs;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.Enumeration;

/**
 * Created by asus on 2015-03-01.
 */
@Service
public class HttpMsgService {

    private static Logger logger = LoggerFactory.getLogger(HttpMsgService.class);

    public String BufferedReaderToString(BufferedReader reader) {
        if (reader == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();

        char[] c = new char[512];
        try {
            int byteread;
            while ((byteread = reader.read(c)) != -1) {
                sb.append(c, 0, byteread);
            }
            reader.close();
        } catch (Exception e) {
            Error("BufferedReaderToString Exception : ", e);
        }
        return sb.toString();
    }



    public void HttpMessageToString(HttpServletRequest req, String body){

        if(req == null){
            Debug("Req is null");
            return;
        }

        Enumeration enumeration = req.getHeaderNames();
        StringBuilder sb = new StringBuilder();
        sb.append("\r\n============= HTTP MESSAGE START ======================\r\n");
        sb.append("Request URI : " + req.getRequestURI() + "\r\n");
        sb.append("Request URL : " + req.getRequestURL() + "\r\n");
        while (enumeration != null && enumeration.hasMoreElements()){
            String key = (String)enumeration.nextElement();
            String value = req.getHeader(key);
            sb.append(String.format("%s : %s\r\n", StringUtils.rightPad(key, 24),value));
        }
        sb.append("=================  BODY  =============================\r\n"+  body + "\r\n");
        sb.append("============= HTTP MESSAGE END ========================\r\n");
        Debug(sb.toString());
    }



    private void Debug(String s) {
        if (logger.isDebugEnabled()) {
            logger.debug(s);
        }
    }

    private void Debug(String s, Object... arg) {
        if (logger.isDebugEnabled()) {
            logger.debug(s, arg);
        }
    }

    private void Error(String s) {
        if (logger.isErrorEnabled()) {
            logger.error(s);
        }
    }

    private void Error(String s, Object... arg) {
        if (logger.isErrorEnabled()) {
            logger.error(s, arg);
        }
    }

    private void Info(String s) {
        if (logger.isInfoEnabled()) {
            logger.info(s);
        }
    }

    private void Info(String s, Object... arg) {
        if (logger.isInfoEnabled()) {
            logger.info(s, arg);
        }

    }
}
