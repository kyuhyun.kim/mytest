package com.tisquare.mvp.mcs.interceptor;

import com.tisquare.mvp.mcs.db.ClientDao;
import com.tisquare.mvp.mcs.db.domain.AuthTocken;
import com.tisquare.mvp.mcs.exception.AuthException;
import com.tisquare.mvp.mcs.exception.ParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by 상기 on 2015-04-06.
 */
@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);

    @Autowired
    ClientDao clientDao;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        String mdn = request.getHeader("TN-REQ-USER");
        String iuid = request.getHeader("TN-IUID");
        String version = request.getHeader("TN-VERSION");
        String cauth_key = request.getHeader("TN-AUTH-KEY");

        if ((mdn == null) || (iuid == null) || (version == null)) {
            throw new ParameterException();
        }

        AuthTocken tocken = new AuthTocken();
        tocken.setPhone_no(mdn);
        tocken.setCauth_key(version);
        tocken.setIuid(iuid);


        logger.info(request.getRequestURI());
        if (request.getRequestURI().startsWith("/mcs/authuac")) {
            tocken.setCauth_key(cauth_key);
            if(clientDao.isAuthUserKey(tocken) == false)
                throw  new AuthException();
        }
        else {
            if (clientDao.isAuthUser(tocken) == false)
                throw new AuthException();
        }
        return true;
    }




}
