package com.tisquare.mvp.mcs.interceptor;

import com.tisquare.mvp.mcs.db.ClientDao;
import com.tisquare.mvp.mcs.db.domain.AuthTocken;
import com.tisquare.mvp.mcs.exception.AuthException;
import com.tisquare.mvp.mcs.exception.ParameterException;
import com.tisquare.mvp.mcs.service.TraceService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import tn.oma.OmAgent;
import tn.oma.TraceFacility;
import tn.oma.type.ServerType;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;


@Component
public class LogInterceptor extends HandlerInterceptorAdapter {

    private static Logger logger = LoggerFactory.getLogger(LogInterceptor.class);

    private final static String LINE = "\r\n";
    private final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private TraceFacility trace = null;

    private static String ID;
    @Autowired
    ClientDao clientDao;

    @Autowired
    TraceService traceService;

    @PostConstruct
    public void initialize() {
        int pid = Integer.parseInt(System.getProperty("ID", "0"));
        ID = String.format("%08x", (((0xFF & 20) << 24) + ((0xFFFF & 1) << 8) + (0xFF & pid)));
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        String[] token = getTraceInfo(request);

//        logger.info("[Interceptored....] token[1]:" + token[1] + ",token[2]:" + token[2] + ",handler:" + handler.getClass());

/*        String message = df.format(new Date()) + " [" + token[1] + "] --> MCS(" + ID + ") " + token[0] + "." + request.getMethod();

        if (token[2] != null) {
            message += (" (" + token[1] + ":" + token[2] + ")");
        }
        message += (LINE +
                        "--------------------------------------------------------------------------------" +
                        LINE +
                        toStringRequest(request) +
                        "================================================================================" +
                        LINE);

        logger.info(LINE + message);


        if (trace != null && trace.isTrace(token[1])) {
            trace.log(token[1], token[2], message);
        }*/

        return super.preHandle(request, response, handler);

//        BufferedReader reader = request.getReader();
//        StringBuilder sb = new StringBuilder();
//
//        try {
//
//
//
//            char[] c = new char[4086]; int byteread;
//            while ((byteread = reader.read(c)) != -1)
//                sb.append(c, 0, byteread);
//
//            reader.close();
//
//            logger.info("==================================================================================================");
//            logger.info(sb.toString());
//            logger.info("==================================================================================================");
//
//
//        }
//        catch (Exception e)
//        {p
//            e.printStackTrace();
//        }
//        return true;
    }


    private void printTraceLog(HttpServletRequest request, String message) {
        String[] token = getTraceInfo(request);
        if (traceService.isTrace()) {
            boolean isTarget = false;
            for (String target : traceService.getTraceTarget()) {
                if (StringUtils.equals(target, token[1])) {
                    isTarget = true;
                    break;
                }
            }
            if (isTarget) {
                try {
                    MDC.put("phoneNo", token[1]);
                    String traceMessage = message;
                    traceService.getTraceLogger().debug(traceMessage);
                    MDC.clear();
                } catch (Exception e) {
                    logger.error("trace log failed", e);
                }
            }
        }
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
/*
        logger.info("[AfterCompletion....]");
        String[] token = getTraceInfo(request);


        String message = df.format(new Date()) + " [" + token[1] + "] <-- MCS(" + ID + ") " + token[0] + "." + request.getMethod();
        if (token[2] != null) {
            message += (" (" + token[1] + ":" + token[2] + ")");
        }

        message +=
                (
                        " RES(" + response.getStatus() + ")" +
                                LINE +
                                "--------------------------------------------------------------------------------" +
                                LINE +
                                toStringResponse(request, response) +
                                "================================================================================" +
                                LINE);
*/




//        if (trace != null && trace.isTrace(token[1])) {
//            trace.log(token[1], token[2], message);
//        }
//
//
//
//			message +=
//					(
//
//					LINE+
//					"--------------------------------------------------------------------------------"+
//					LINE+
//					toStringResponse(request, response)+
//					"================================================================================"+
//					LINE);
//
//
//
//
//
//			// 트레이스가 설정되어 있지 않을 경우에는 약식으로 요청/응답 로그를 출력한다.
//
//
//			String content = (String)request.getAttribute("content.response");
//			if (content != null) {
//				message += (
//						LINE+
//						"--------------------------------------------------------------------------------"+
//						LINE+
//						content+
//						LINE+
//						"================================================================================"+
//						LINE);
//			}
/*
		logger.info(message);
*/

        super.afterCompletion(request, response, handler, ex);
    }

    private String[] getTraceInfo(HttpServletRequest request) {
        String[] result = new String[3];

        result[1] = request.getHeader("TN-REQ-USER");

        if (request.getRequestURI().startsWith("/mcs/mvpinfo")) {
            result[0] = "MVPINFO";
        } else if (request.getRequestURI().startsWith("/mcs/authuac")) {
            result[0] = "AUTHUAC";
        } else if (request.getRequestURI().startsWith("/mcs/{iuid}/urlshortener")) {
            result[0] = "URLSHORTENER";
        } else if (request.getRequestURI().startsWith("/mcs/notice/list")) {
            result[0] = "NOTICE_LIST";
        } else if (request.getRequestURI().startsWith("/mcs/notice/sid")) {
            result[0] = "NOTICE_SID";
        } else if (request.getRequestURI().endsWith("/mcs/help/list")) {
            result[0] = "HELP_LIST";
        } else if (request.getRequestURI().endsWith("/mcs/help/hid")) {
            result[0] = "HELP_HID";
        } else if (request.getRequestURI().endsWith("/mcs/log/crash")) {
            result[0] = "CRASH";
        } else if (request.getRequestURI().endsWith("/mcs/help/list")) {
            result[0] = "HELP_LIST";
        }
        else if (request.getRequestURI().endsWith("/mcs/pages/agreement.html")) {
            result[0] = "AGREEMENT";
        }
        else if (request.getRequestURI().endsWith("/mcs/pages/privacy.html")) {
            result[0] = "PRIVACY";
        }
        else if (request.getRequestURI().endsWith("/mcs/pages/th.html")) {
            result[0] = "TH";
        }
        else if (request.getRequestURI().endsWith("/mcs/pages/ad.html")) {
            result[0] = "AD";
        }



        /**
         for (int i = 0; i < token.length; i++) {
         log.info("[" + i + "] :" + token[i]);
         if (token[i].equals("user") && (i+2) < token.length) {
         result[0] = token[i+2];
         result[1] = token[i+1];
         } else if (token[i].equals("destination") && (i+1) < token.length) {
         result[2] = token[i+1];
         }
         }

         // 파일 다운로드의 경우 발신번호가 헤더에 있음.
         if (result[0] == null) {
         result[1] = request.getHeader("tn-user-id");
         if (token.length >= 3) { result[0] = token[2]; }
         else { result[0] = "not-implementation"; }
         }
         */

        return result;
    }

    public String getBody(HttpServletRequest request) {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (IOException ex) {
            logger.error(ex.toString());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    logger.error(ex.toString());
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }

    private String toStringRequest(HttpServletRequest request) {

        StringBuffer buffer = new StringBuffer();
        String content = null;
        // method
        buffer.append(request.getMethod() + " ");

//        if(request.getMethod().equals("POST"))
//            content = getBody(request);

        // request-uri
        buffer.append(request.getRequestURI());

        // query string
        String queryString = request.getQueryString();
        if (queryString != null) {
            buffer.append("?" + queryString);
        }
        buffer.append(" " + request.getProtocol() + LINE);


        // headers
        Enumeration<String> headers = request.getHeaderNames();
        String name;
        Enumeration<String> values;
        while (headers != null && headers.hasMoreElements()) {
            name = headers.nextElement();
            buffer.append(name + ": ");
            values = request.getHeaders(name);
            while (values != null && values.hasMoreElements()) {
                buffer.append(values.nextElement());
                if (values.hasMoreElements()) {
                    buffer.append(",");
                }
            }
            buffer.append(LINE);
        }

        if (request.getContentLength() > 0) {
            buffer.append(LINE + content+ LINE);
        }

        return buffer.toString();
    }


    private String toStringResponse(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer buffer = new StringBuffer();


        // status line
        buffer.append(request.getProtocol() + " " + response.getStatus() + " " + HttpStatus.valueOf(response.getStatus()).name() + LINE);

        // headers
        Collection<String> tmp = response.getHeaderNames();
        if (tmp != null) {
            Iterator<String> headers = tmp.iterator();
            String name;
            Iterator<String> values;
            while (headers != null && headers.hasNext()) {
                name = headers.next();
                buffer.append(name + ": ");
                if ((tmp = response.getHeaders(name)) != null) {
                    values = tmp.iterator();
                    while (values != null && values.hasNext()) {
                        buffer.append(values.next());
                        if (values.hasNext()) {
                            buffer.append(",");
                        }
                    }
                }
                buffer.append(LINE);
            }
        }

        // content
        String content = (String) request.getAttribute("content.response");
        if (content != null) {
            try {
                buffer.append(LINE);
                buffer.append(new String(content));
                buffer.append(LINE);
            } catch (Exception e) {
                logger.error("", e);
            }
        } else {


            String clen = response.getHeader("content-length");
            if (clen != null) {
                try {
                    int contentLength = Integer.parseInt(clen);
                    if (contentLength > 0) {
                        buffer.append(LINE + "(...)" + LINE);
                    }
                } catch (Exception e) {
                }
            }
        }

        return buffer.toString();
    }




}
