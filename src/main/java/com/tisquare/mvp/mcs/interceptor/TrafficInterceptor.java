package com.tisquare.mvp.mcs.interceptor;

import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.db.ClientDao;
import com.tisquare.mvp.mcs.db.domain.AuthTocken;
import com.tisquare.mvp.mcs.exception.AuthException;
import com.tisquare.mvp.mcs.exception.ParameterException;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.LinkedBlockingDeque;

@Component
public class TrafficInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(TrafficInterceptor.class);

    public static LinkedBlockingDeque<HttpServletRequest> reqQueue = new LinkedBlockingDeque<HttpServletRequest>();

    public static Object mutex = new Object();

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    public  static McsConfig mcsConfig1;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        if(!isMaxTraffic()) {

            response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
            return false;
        }

        else {
            addQueue(request);
            return true;
        }

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        DelQueue(request);
    }


    public static void addQueue(HttpServletRequest request)
    {
        synchronized (mutex) {

            logger.info("Traffic count curr[{}]" , reqQueue.size() );
            if(reqQueue.remainingCapacity() > 1)
                reqQueue.add(request);
        }
    }

    public int DelQueue(HttpServletRequest request) {
        synchronized (mutex) {

            try {
                reqQueue.remove(request);
                return reqQueue.size();
            } catch (Exception e) {
                logger.error(e.toString());
                return -1;
            }

        }
    }

    public boolean isMaxTraffic()
    {
        synchronized (mutex) {
            if(StringUtils.equals(mcsConfig.getProperty("TRAFFIC.CONTROL"), "ON")) {
                if (reqQueue.size() < Integer.parseInt(mcsConfig.getProperty("TRAFFIC.MAX"))) {
                    logger.info("Traffic count curr [{}] max [{}] limit !!", reqQueue.size(), mcsConfig.getProperty("TRAFFIC.MAX"));
                    return true;
                } else {
                    return false;
                }
            }
            else
                return true;
        }
    }

    public static void QueueStatusLog() {

        try {

            logger.info("==============================================");
            logger.info("############### QUEUE SIZE [{}]###############", reqQueue.size());
            logger.info("==============================================");
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }


    public static Integer getCurrTraffic()
    {
        synchronized (mutex) {
            try {

                QueueStatusLog();
                return reqQueue.size();
            }
            catch (Exception e)
            {
                logger.error(e.toString());
                return null;
            }

        }
    }


    public static   String getTest()
    {

        return mcsConfig1.getProperty("TRAFFIC.MAX");
    }





}
