package com.tisquare.mvp.mcs.core.task;

import com.tisquare.mvp.mcs.db.StatDao;
import com.tisquare.mvp.mcs.db.domain.*;
import com.tisquare.mvp.mcs.model.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jhkim on 14. 2. 28.
 */

@Service
public class StatLogToRepo implements Runnable{

    private static Logger logger = LoggerFactory.getLogger(StatLogToRepo.class);

    HashMap<SvcCode, Integer> svcFeature;

    @Value("#{statconf.statlog_basedir}")  String baseDir;

    Boolean isRunning = Boolean.FALSE;
    Thread thr;
    String workRootPath;
    String workDonePath;

    String currDate;

    @Autowired
    StatDao statDao;


    @PostConstruct
    private void init(){
        svcFeature = new HashMap<SvcCode, Integer>();
//        LoadSvcFeature();
    }
    //@Scheduled (cron = "#{statconf.stat_to_db_cron}")
//    @Scheduled (cron = "${stat_to_db_cron}")
    private void thr_init(){
        Info("init() !!!!!!!!!!!!!!!!!!!!!!!!!!!!! start stat to repository" );
        isRunning = Boolean.TRUE;

        currDate = currentDate();

        //set path
        workRootPath = setWorkPath();
        workDonePath = setDonePath();

        thr = new Thread(this);
        thr.start();
    }

    @Async
    private void initByWeb(){
        this.thr_init();
    }

    public Boolean runByWeb(){
        if(isRunning){
            Info("Already running. try later");
            return Boolean.FALSE;
        }
        this.initByWeb();
        return Boolean.TRUE;
    }

    @PreDestroy
    private void close(){

        isRunning = Boolean.FALSE;

        try {
            if(thr != null) {
                thr.interrupt();
                thr.join();
            }
        } catch (Exception e) {
            Error("close Exceptions", e);
        }
        finally {
            Info("close() PreDestroy");
        }
    }

    private String setWorkPath(){
        return baseDir + File.separator + "ready" + File.separator;
    }
    private String setDonePath(){
        return baseDir + File.separator + "done" + File.separator;
    }

    private String currentDate() {
        Calendar cal = Calendar.getInstance();
        cal.get(Calendar.YEAR);
        String curDate = String.format("%04d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));

        //TODO StatLogToRepo와 합치면 좋을 듯.

        return curDate;
    }

//    private void readIntiFiles(){
//        File fileRoot = new File(workRootPath);
//        File[] files = fileRoot.listFiles();
//        // TODO
//    }

    @Override
    public void run() {
//        File fileRoot = new File(workRootPath);
//        File[] files = fileRoot.listFiles();
//
//        Boolean dbFail = Boolean.FALSE;
//
//        for(File f : files){
//            //Directory인지 체크
//            if(f.isDirectory()){
//                Info("Current Directory [{}]", f.getPath());
//                if(currDate.equalsIgnoreCase(f.getName())){
//                    Info("오늘 날짜 파일 명은 무시합니다. currDate[{}]", currDate);
//                    continue;
//                }
//                //완료되면 파일을 옮겨갈 경로.
//                File dst = new File(workDonePath + File.separator + f.getName());
//
//                File[] subfiles = f.listFiles();
//                for(File subf : subfiles){
//                    // TODO 주석처리 잘 확인 할 것..
//                    try {
//                        int pos=0;
//                        RandomAccessFile raf = null;
//                        try{
//
//                            raf = new RandomAccessFile(subf, "rws");
//                            String line = null;
//                            if(raf.length() == 0){
//                                Debug("File size is 0.");
//                                raf.close(); raf = null;
//                            }
//                            //readBoolean 함수하면.. 1바이트 이동하는 기분임..
//                            while(raf != null && (pos < raf.length()) && raf.readBoolean()){
//                                raf.seek(pos); //readBoolean 함수하면 1바이트 이동하는 것 같아.. 다시 바로잡음
//                                line = raf.readLine();
//
//                                if(line.startsWith("#")){
//                                    pos = pos + line.length() + 1; //1은 줄바꿈 크기
//                                    continue;
//                                } else {
//                                    Info("PARSE : [{}]", line.toString());
//                                    // FIX ME
//                                    StatLog node = StatLog.fileToObject(line);
//                                    Info(node.toString());
//                                    // 테스트 코드.
////                                    node.setSvr_id(0);
////                                    node.setSvc_id(10001);
////                                    node.setSub_svc_id(100011);
////                                    node.setPart_id(0);
////                                    node.setReceiver_type(0);
////                                    statDao.insertStat(node);
//
////                                    StatToDbBySvcType(node);
//
//
//                                    raf.seek(pos);
//                                    raf.writeByte('#');
//                                    pos = pos + line.length() + 1; // 1은 줄바꿈 크기.
//                                    raf.seek(pos);
//                                }
//                            }
//                        } catch (Exception e){
//                            Error("", e);
//                            dbFail = Boolean.TRUE;
//
//                        } finally {
//                            if(raf != null ) raf.close();
//                        }
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        Error("",e);
//                    }
//
//
//                    //완료된 파일 이동하는 루틴 위치 이동.
//                    //실패한 파일들 재시도 보장이 되지 않아서 바꿈..
//                    try{
//                        if(!dbFail) {
//                            FileUtils.moveFileToDirectory(subf, dst, Boolean.TRUE);
//                            if (!subf.delete()) Error("[{}]", subf.getPath());
//                        }
//                    } catch ( Exception e) {
//                        Error("File move fail. source={}, dest={}\r\n",
//                                String.format("%s/%s", subf.getPath(), subf.getName()),
//                                String.format("%s/%s", dst.getPath(), dst.getName()), e );
//                    }
//                }
//            }
//            else{
//                Info("Ignore Files [{}]", f.getPath());
//            }
//
//            try{
//                if(!(f.list().length > 0))
//                    FileUtils.forceDelete(f);
//            } catch ( Exception e){
//                logger.error("directory delete failed. value={}", f.getPath());
//            }
//
//
////            try {
////                //완료된 파일들 이동.
////                File dst = new File(workDonePath + File.separator + f.getName());
////                if(f.isDirectory()){
////                    File[] fList = f.listFiles();
////                    if(fList != null){
////                        for(File m : fList) {
////                            FileUtils.moveFileToDirectory(m, dst, Boolean.TRUE);
////                        }
////                        if( !f.delete() ) Error("[{}] delete failed", f.getPath());
////                    }
////                } else FileUtils.moveFileToDirectory(f, dst, Boolean.TRUE);
////            } catch (IOException e) {
////                Error("", e);
////            }
//        }
//        isRunning = Boolean.FALSE;
//        Debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! end stat to repository");
    }

//    public void LoadSvcFeature(){
//        List<SvcFeature> list = (List<SvcFeature>)statDao.selectSvcFeature();
//
//        svcFeature.clear();
//        if(list == null || list.size() <= 0)
//        {
//            logger.error("Can't load svcfeature. using default value");
//            svcFeature.put(SvcCode.HDR_SMALLCALL, 10000);
//            svcFeature.put(SvcCode.HDR_MUSICAL, 10001);
//            svcFeature.put(SvcCode.HDR_THANKQ_MSG,      10002);
//            svcFeature.put(SvcCode.HDR_CONTACT_SHR,     10006);
//        } else {
//            for(SvcFeature sf : list){
//                if(sf.getFname().startsWith("Small")){
//                    svcFeature.put(SvcCode.HDR_SMALLCALL, sf.getFid());
//                } else if (sf.getFname().startsWith("Music")){
//                    svcFeature.put(SvcCode.HDR_MUSICAL, sf.getFid());
//                } else if (sf.getFname().startsWith("Thank")){
//                    svcFeature.put(SvcCode.HDR_THANKQ_MSG, sf.getFid());
//                } else if (sf.getFname().startsWith("Contact")){
//                    svcFeature.put(SvcCode.HDR_CONTACT_SHR, sf.getFid());
//                } else {
//                    logger.debug("Unknown Service FID={}, FNAME={}", sf.getFid(), sf.getFname());
//                }
//            }
//        }
//
//        logger.debug("====[ Load Service Feature Summary from T_SERVICE_FEATURE_SUMMARY ]====");
//        for(SvcCode value : svcFeature.keySet()){
//            logger.debug("{} : {}", StringUtils.rightPad(String.format("%s", svcFeature.get(value)), 15),
//                    StringUtils.rightPad(String.format("%s", value), 15));
//        }
//        logger.debug("========================================================================");
//    }

    public void
    StatServiceInsert(Statistic stat)
    {
        StatSvcInfo statSvcInfo = new StatSvcInfo(stat);


        statDao.updateServiceStat(statSvcInfo);
        statDao.updateServiceStat_day(statSvcInfo);
        statDao.updateServiceStat_month(statSvcInfo);

    }

    public void StatToSessionInfo(AgentStat stat)
    {
        int nIdx = 0;
        SessionInfo sessionInfo = null;

        try {
            for (Call call : stat.getCall()) {
                nIdx = 0;
                for (Statistic statistic : call.getStat()) {

                    int nSessionCnt = 0;


                    if (nIdx > 0) {
                        nSessionCnt = statDao.selectMaxSessionCnt(sessionInfo);
                    }
                    sessionInfo = new SessionInfo(statistic);

                    SimpleDateFormat Regdt_format = new SimpleDateFormat("yyyyMMddHH", Locale.KOREA); //
                    Timestamp currentTime = new Timestamp( call.getStart_time());
                    String sDateTime = Regdt_format.format(currentTime);


                    sessionInfo.setReg_dt(sDateTime.substring(0, 8));
                    sessionInfo.setReg_tm(sDateTime.substring(8, 10));


                    if( nIdx > 0) {
                        sessionInfo.setSession_cnt(nSessionCnt);
                        statDao.updateServiceStatSession_ext(sessionInfo);

                    }
                    else
                        statDao.updateServiceStatSession(sessionInfo);
                    nIdx++;
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            Error("",e);
        }


        //int nMaxSession = statDao.selectMaxSessionCnt();
    }

//    public void StatToDbBySvcType(StatLog node){
//
//        ServiceTotal totCnt = new ServiceTotal(node);
//        totCnt.setSub_svc_id(node.getSvcType());
//        totCnt.setSvr_id(1000013);
//        switch (SvcCode.fromInt(node.getSvcType())){
//            case CONTACT_SHARE:
//                ShareContact svc = new ShareContact(node);
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_CONTACT_SHR));
//
//                statDao.updateServiceContact(svc);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case MUSICAL_SOUNDCON:
//            case MUSICAL_STICKERCON:
//                ServiceMusical soundcon = new ServiceMusical(node);
//                soundcon.setSub_svc_id(node.getSvcType());
//                soundcon.setUsed_cnt(node.getUsed_cnt());
//
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_MUSICAL));
//
//                statDao.updateMusicalCnt(soundcon);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case SMALLCALL_ACCEPT:
//                ServiceSmallCall sCallAccept = new ServiceSmallCall(node);
//                sCallAccept.setAccept_call_cnt(node.getUsed_cnt());
//
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_SMALLCALL));
//
//                statDao.updateSmallCall(sCallAccept);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case SMALLCALL_CALLRECV:
//                ServiceSmallCall sCallRecv = new ServiceSmallCall(node);
//                sCallRecv.setReceive_call_cnt(node.getUsed_cnt());
//
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_SMALLCALL));
//
//                statDao.updateSmallCall(sCallRecv);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case SMALLCALL_DENIED:
//                ServiceSmallCall sCallDenied = new ServiceSmallCall(node);
//                sCallDenied.setReject_call_cnt(node.getUsed_cnt());
//
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_SMALLCALL));
//
//                statDao.updateSmallCall(sCallDenied);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case SMALLCALL_DENIED_MSG:
//                ServiceSmallCall sCallDeniedMsg = new ServiceSmallCall(node);
//                sCallDeniedMsg.setReject_msg_cnt(node.getUsed_cnt());
//
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_SMALLCALL));
//
//                statDao.updateSmallCall(sCallDeniedMsg);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case SMALLCALL_KEEP_RUN:
//                ServiceSmallCall sCallKeepRun = new ServiceSmallCall(node);
//                sCallKeepRun.setDisplay_cnt(node.getUsed_cnt());
//
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_SMALLCALL));
//
//                statDao.updateSmallCall(sCallKeepRun);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case THANKQ_MSG:
//                ServiceThank sThankQ = new ServiceThank(node);
//
//                totCnt.setSvc_id(svcFeature.get(SvcCode.HDR_THANKQ_MSG));
//
//                statDao.updateServiceThankQ(sThankQ);
//                statDao.updateTotCnt(totCnt);
//                break;
//            case UNKNOWN:
//                logger.error("Unknown SvcID=[{}]", node.toString());
//
//        }
//    }

    private void Debug(String s){
        if(logger.isDebugEnabled()){
            logger.debug(s);
        }
    }

    private void Debug(String s, Object... arg){
        if(logger.isDebugEnabled()){
            logger.debug(s, arg);
        }
    }

    private void Error(String s){
        if(logger.isErrorEnabled()){
            logger.error(s);
        }
    }

    private void Error(String s, Object... arg){
        if(logger.isErrorEnabled()){
            logger.error(s, arg);
        }
    }

    private void Info(String s){
        if(logger.isInfoEnabled()){
            logger.info(s);
        }
    }

    private void Info(String s, Object... arg){
        if(logger.isInfoEnabled()){
            logger.info(s, arg);
        }
    }


}
