package com.tisquare.mvp.mcs.core.task;

import com.tisquare.mvp.mcs.model.AgentStat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by jhkim on 14. 2. 25.
 */
@Component
public class FileStatLog implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(FileStatLog.class);

    private final int MAX_FD_SIZE = 11;

    LinkedBlockingDeque<AgentStat> queue = new LinkedBlockingDeque<AgentStat>();
    Object mutex = new Object();
    Thread thr;
    Boolean isRunning = Boolean.FALSE;

    String currentPath;
    File[] fds;
    FileWriter[] fWriter;
    Timer dayChecker;
    int today;


    //sample
    //@Value("#{statconf['statlog_basedir']}") String defValue;
    @Value("#{statconf.statlog_basedir}")
    String defValue;

//    @Value("#{statconf.stat_to_db_cron}")
//    String cronValue;


    public void addQueue(AgentStat stat) {
        synchronized (mutex) {
            queue.add(stat);
        }
    }

    private AgentStat deQueue() {
        synchronized (mutex) {
            if (queue.isEmpty())
                return null;
            return queue.removeFirst();
        }
    }

    @PostConstruct
    private void init() {

        Info("init() PostConstruct ACS");
        logger.info( "String Framework version[" + org.springframework.core.SpringVersion.getVersion() +"]");
        thr = new Thread(this, "FileLogThread");
      //  isRunning = Boolean.TRUE;
     //   setPath(); //현재 시간 설정
     //   initFds(); // 파일 지시사 셋팅
       // createTimerTask(); // 날짜 바뀜 체크
        thr.start(); // 파일 쌓아주는 쓰레드

        //TODO  여기에 config 로그 찍어주면 좋으려나...
    }

    @PreDestroy
    private void close() {
        Info("close() PreDestroy");
        isRunning = Boolean.FALSE;
        dayChecker.cancel();
        dayChecker.purge();
        deInitFds();

        try {
            thr.interrupt();
            thr.join();
        } catch (Exception e) {
            Error("close Exceptions", e);
        }
    }

    private void initFds() {
        deInitFds();
        fds = new File[MAX_FD_SIZE];
        fWriter = new FileWriter[MAX_FD_SIZE];
        for (int i = 0; i < MAX_FD_SIZE; i++) {
            String fName;
            if(i==MAX_FD_SIZE - 1)
                fName = String.format("%s%s010X", currentPath, File.separator);
            else
                fName = String.format("%s%s010%d", currentPath, File.separator, i);

            try {
                fds[i] = new File(fName);
            } catch (Exception e) {
                Error("", e);
            }

            try {
                if (fds[i] != null && !fds[i].exists()) {
                    if (!fds[i].createNewFile()) Error("File Create Failed [{}]", i);
                    else Debug("File Create Success [{}]", i);
                }
                if (fds[i] != null) {
                    fWriter[i] = new FileWriter(fds[i], Boolean.TRUE);
                    Debug("Create File Writer description Success [{}]", i);
                } else {
                    Debug("Create File Writer description Failed fd is null [{}]", i);
                }

            } catch (IOException e) {
                Error("", e);
            }
        }
    }

    private void deInitFds() {
        if (fds != null) {
            for (int i = 0; i < MAX_FD_SIZE; i++) {
                if (fds[i] != null)
                    fds[i] = null;
            }
        }
        if (fWriter != null) {
            for (int i = 0; i < MAX_FD_SIZE; i++) {
                if (fWriter[i] != null) {
                    try {
                        fWriter[i].flush();
                        ;
                        fWriter[i].close();
                    } catch (IOException e) {
                        // TODO
                        Error("deInitFds Exceptions", e);
                    }
                }
            }
        }
    }

    private void setPath() {
        this.currentPath = makeDir();
    }

    private String makeDir() {
        String fPath = defValue + File.separator + "ready" + File.separator + currentDate();
        File fd = new File(fPath);

        if (!fd.exists()) {
            fd.mkdirs();
        }
        return fPath;
    }


    private String currentDate() {
        Calendar cal = Calendar.getInstance();
        cal.get(Calendar.YEAR);
        String curDate = String.format("%04d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
        today = cal.get(Calendar.DAY_OF_MONTH);
        Info("현재날짜[{}], TODAY[{}]", curDate, today);

        //TODO StatLogToRepo와 합치면 좋을 듯.

        return curDate;
    }

    private FileWriter getFd(String mdn) {
        if (mdn.length() > 4) {
            int pos = 10;
            try {
                pos = Integer.parseInt("" + mdn.charAt(3));
            } catch (Exception e) {
                pos = 10;
            }

            return fWriter[pos];
        }
        return fWriter[MAX_FD_SIZE - 1];
    }

    private void createTimerTask() {
        dayChecker = new Timer();
        dayChecker.schedule(new TimerTask() {
            @Override
            public void run() {
                Calendar cal = Calendar.getInstance();
                int checkDay = cal.get(Calendar.DAY_OF_MONTH);
                Debug("Wake up timer");
                synchronized (dayChecker) {
                    if (today != checkDay) {
                        Info("날짜가 변경 되어 새로운 폴더 및 새로운 des를 생성합니다. today[{}], checkDay[{]]", today, checkDay);
                        setPath();
                        initFds();
                    }
                }
            }
        }, 60000, 60000);
    }

    @Override
    public void run() {
//        AgentStat node;
//        FileWriter fw;
//        while (isRunning) {
//            node = deQueue();
//            if (node == null) {
//                try {
//                    Thread.sleep(2000);
//                } catch (Exception e) {
//                    Error("Thread Sleep Error", e);
//                }
//                continue;
//            }
//
//            Info(node.toString());

//            synchronized (dayChecker) {
//                fw = getFd(node.getMdnNonE164());
//                if (fw != null) {
//                    try {
//                        fw.write(node.toFileLogString());
//                        fw.flush();
//                    } catch (IOException e) {
//                        Error("", e);
//                    }
//                } else {
//                    Error("FD를 찾을 수 없습니다. {} " + node.toString());
//                }
//            }
//        }
    }


    private void Debug(String s){
        if(logger.isDebugEnabled()){
            logger.debug(s);
        }
    }

    private void Debug(String s, Object... arg){
        if(logger.isDebugEnabled()){
            logger.debug(s, arg);
        }
    }

    private void Error(String s){
        if(logger.isErrorEnabled()){
            logger.error(s);
        }
    }

    private void Error(String s, Object... arg){
        if(logger.isErrorEnabled()){
            logger.error(s, arg);
        }
    }

    private void Info(String s){
        if(logger.isInfoEnabled()){
            logger.info(s);
        }
    }

    private void Info(String s, Object... arg){
        if(logger.isInfoEnabled()){
            logger.info(s, arg);
        }
    }
}
