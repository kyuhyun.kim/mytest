package com.tisquare.mvp.mcs.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class EventPopup {

    public static final int FORCE_UPDATE = 1; //강제 업데이트

	@JsonProperty("epid")
	private Long epid;
	
	@JsonProperty("noti-stime")
	private String notiStime;
	
	@JsonProperty("noti-etime")
	private String notiEtime;
	
	@JsonProperty("event-stime")
	private String eventStime;
	
	@JsonProperty("event-etime")
	private String eventEtime;
	
	@JsonProperty("event-type")
	private Integer eventType;
	
	@JsonProperty("title")
	private String title;
	
	@JsonProperty("text")
	private String text;
	
	@JsonProperty("force-stop")
	private Integer forceStop;
	
	@JsonIgnore
	private String regDate;
	
	@JsonIgnore
	private String uptDate;

	public Long getEpid() {
		return epid;
	}

	public void setEpid(Long epid) {
		this.epid = epid;
	}

	public String getNotiStime() {
		return notiStime;
	}

	public void setNotiStime(String notiStime) {
		this.notiStime = notiStime;
	}

	public String getNotiEtime() {
		return notiEtime;
	}

	public void setNotiEtime(String notiEtime) {
		this.notiEtime = notiEtime;
	}

	public String getEventStime() {
		return eventStime;
	}

	public void setEventStime(String eventStime) {
		this.eventStime = eventStime;
	}

	public String getEventEtime() {
		return eventEtime;
	}

	public void setEventEtime(String eventEtime) {
		this.eventEtime = eventEtime;
	}

	public Integer getEventType() {
		return eventType;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getForceStop() {
		return forceStop;
	}

	public void setForceStop(Integer forceStop) {
		this.forceStop = forceStop;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public String getUptDate() {
		return uptDate;
	}

	public void setUptDate(String uptDate) {
		this.uptDate = uptDate;
	}

	@Override
	public String toString() {
		return "EventPopup [epid=" + epid + ", notiStime=" + notiStime
				+ ", notiEtime=" + notiEtime + ", eventStime=" + eventStime
				+ ", eventEtime=" + eventEtime + ", eventType=" + eventType
				+ ", title=" + title + ", text=" + text + ", forceStop="
				+ forceStop + ", regDate=" + regDate + ", uptDate=" + uptDate
				+ "]";
	}
}
