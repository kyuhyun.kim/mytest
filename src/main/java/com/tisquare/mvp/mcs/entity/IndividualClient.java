package com.tisquare.mvp.mcs.entity;


import com.tisquare.mvp.mcs.utils.Json;
import com.tisquare.mvp.mcs.model.UserInfo;
import com.tisquare.mvp.mcs.model.Profile;

import java.io.Serializable;

public class IndividualClient implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1125150821239904219L;
    private static final int FLAG_PAID_REGULAR_MEMBER = 1; // 유료 정회원
    private static final int FLAG_FREE_REGULAR_MEMBER = 2; // 무료 정회원
    private static final int FLAG_ASSOCIATE_MEMBER = 3;  // 준회원

    private static final String TELECOM_CODE_SKT = "45005";
    //private static final String TELECOM_CODE_KT = "45008";

    private Long iuid;
    private String phoneNo;
    private int subsType;
    private String appVersion;
    private String cAuthKey;
    private String pnotiToken;
    private String osType;
    private String osVersion;
    private String telecomStr;
    private String telecomCode;
    private String deviceID;
    private String profileTag;
    private String profile;
    private String addressBook;
    private String groupList;
    private Integer storeType;
    //region 프로필에 포함되는 필드
    private String nickname;
    private String statusMessage;
    private Integer presence;
    //endregion
    //region 단말 정보
    private String deviceModel;
    private Integer deviceWidth;
    private Integer deviceHeight;
    private Integer login_status;
    //endregion

    private Integer device_chg_f;

    private Integer svc_net_type;

    private Integer ch_auth;

    private long home_sched_iuid;

    private Integer alarm_setup;
    private Integer alarm_setup_gr;
    private Integer alarm_setup_sched;
    private Integer alarm_setup_msg;
    private Integer alarm_setup_camp;

    public void setAlarm_setup_camp(Integer alarm_setup_camp) {
        this.alarm_setup_camp = alarm_setup_camp;
    }

    public Integer getAlarm_setup_camp() {
        return alarm_setup_camp;
    }

    public void setAlarm_setup(Integer alarm_setup) {
        this.alarm_setup = alarm_setup;
    }

    public void setAlarm_setup_gr(Integer alarm_setup_gr) {
        this.alarm_setup_gr = alarm_setup_gr;
    }

    public void setAlarm_setup_sched(Integer alarm_setup_sched) {
        this.alarm_setup_sched = alarm_setup_sched;
    }

    public void setAlarm_setup_msg(Integer alarm_setup_msg) {
        this.alarm_setup_msg = alarm_setup_msg;
    }

    public Integer getAlarm_setup() {
        return alarm_setup;
    }

    public Integer getAlarm_setup_gr() {
        return alarm_setup_gr;
    }

    public Integer getAlarm_setup_sched() {
        return alarm_setup_sched;
    }

    public Integer getAlarm_setup_msg() {
        return alarm_setup_msg;
    }

    public void setHome_sched_iuid(long home_sched_iuid) {
        this.home_sched_iuid = home_sched_iuid;
    }

    public long getHome_sched_iuid() {
        return home_sched_iuid;
    }

    public void setCh_auth(Integer ch_auth) {
        this.ch_auth = ch_auth;
    }

    public Integer getCh_auth() {
        return ch_auth;
    }

    public void setLogin_status(Integer login_status) {
        this.login_status = login_status;
    }

    public Integer getLogin_status() {
        return login_status;
    }

    public void setDevice_chg_f(Integer device_chg_f) {
        this.device_chg_f = device_chg_f;
    }

    public void setSvc_net_type(Integer svc_net_type) {
        this.svc_net_type = svc_net_type;
    }

    public Integer getDevice_chg_f() {
        return device_chg_f;
    }

    public Integer getSvc_net_type() {
        return svc_net_type;
    }

    public Integer getDevice_chg_flag() {
        return device_chg_f;
    }

    public void setDevice_chg_flag(Integer device_chg_f) {
        this.device_chg_f = device_chg_f;
    }

    public IndividualClient() {
        super();
        this.storeType = 0;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static int getFlagPaidRegularMember() {
        return FLAG_PAID_REGULAR_MEMBER;
    }

    public static int getFlagFreeRegularMember() {
        return FLAG_FREE_REGULAR_MEMBER;
    }

    public static int getFlagAssociateMember() {
        return FLAG_ASSOCIATE_MEMBER;
    }

    public static String getTelecomCodeSkt() {
        return TELECOM_CODE_SKT;
    }

    public IndividualClient(UserInfo userInfo) throws Exception {
        iuid = userInfo.getIuid();
        phoneNo = userInfo.getPhoneNo();
        subsType = userInfo.getSubsType().value();
        appVersion = userInfo.getAppVersion();
        cAuthKey = userInfo.getCauthKey();
        pnotiToken = userInfo.getPnotiToken();
        osType = userInfo.getOsType();
        osVersion = userInfo.getOsVersion();
        telecomStr = userInfo.getTelecomStr();
        telecomCode = userInfo.getTelecomCode();
        deviceID = userInfo.getDeviceId();
        profileTag = userInfo.getProfileTag();
        //region 프로필에 포함되는 필드
        nickname = userInfo.getNickname();
        statusMessage = userInfo.getStatusMessage();
        presence = userInfo.getPresence();
        //endregion
        //region 단말 정보
        this.deviceModel = userInfo.getDeviceModel();
        this.deviceWidth = userInfo.getDeviceWidth();
        this.deviceHeight = userInfo.getDeviceHeight();
        //endregion

        profile = userInfo.getProfile().toJsonString();


        this.storeType = userInfo.getStoreType();
        this.ch_auth = userInfo.getCh_auth();
    }

    public IndividualClient(Long iuid, String phoneNo, String appVersion, String cauthKey, String osType, String osVersion) {
        this.iuid = iuid;
        this.phoneNo = phoneNo;
        this.appVersion = appVersion;
        this.cAuthKey = cauthKey;
        this.osType = osType;
        this.osVersion = osVersion;
    }

    public Long getIuid() {
        return this.iuid;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public int getSubsType() {
        return subsType;
    }

    public void setSubsType(int subsType) {
        this.subsType = subsType;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getCAuthKey() {
        return cAuthKey;
    }

    public void setCAuthKey(String cauthKey) {
        this.cAuthKey = cauthKey;
    }

    public String getPNotiToken() {
        return pnotiToken;
    }

    public void setPNotiKey(String pnotiToken) {
        this.pnotiToken = pnotiToken;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getTelecomStr() {
        return telecomStr;
    }

    public void setTelecomStr(String telecomStr) {
        this.telecomStr = telecomStr;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getSubsTypeStr() {
        if (this.subsType == FLAG_PAID_REGULAR_MEMBER)
            return "PaidRegularMember";
        else if (this.subsType == FLAG_FREE_REGULAR_MEMBER)
            return "FreeRegularMember";
        else if (this.subsType == FLAG_ASSOCIATE_MEMBER)
            return "AssociateMember";

        return "Unknown";
    }

    public void setFreeRegularMember() {
        this.setSubsType(FLAG_FREE_REGULAR_MEMBER);
    }

    public void setAssociateMember() {
        this.setSubsType(FLAG_ASSOCIATE_MEMBER);
    }

    public static String getLocalTelcomCode() {
        return TELECOM_CODE_SKT;
    }

    public String getProfileTag() {
        return profileTag;
    }

    public void setProfileTag(String profileTag) {
        this.profileTag = profileTag;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(String addressBook) {
        this.addressBook = addressBook;
    }

    public String getGroupList() {
        return groupList;
    }

    public void setGroupList(String groupList) {
        this.groupList = groupList;
    }

    public String getcAuthKey() {
        return cAuthKey;
    }

    public void setcAuthKey(String cAuthKey) {
        this.cAuthKey = cAuthKey;
    }

    public String getPnotiToken() {
        return pnotiToken;
    }

    public void setPnotiToken(String pnotiToken) {
        this.pnotiToken = pnotiToken;
    }

    public Integer getStoreType() {
        return this.storeType;
    }

    public void setStoreType(Integer storeType) {
        this.storeType = storeType;
    }

    //region 프로필에 포함되는 필드

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Integer getPresence() {
        return presence;
    }

    public void setPresence(Integer presence) {
        this.presence = presence;
    }

    //endregion

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public Integer getDeviceWidth() {
        return deviceWidth;
    }

    public void setDeviceWidth(Integer deviceWidth) {
        this.deviceWidth = deviceWidth;
    }

    public Integer getDeviceHeight() {
        return deviceHeight;
    }

    public void setDeviceHeight(Integer deviceHeight) {
        this.deviceHeight = deviceHeight;
    }

    @Override
    public String toString() {
        return "[iuid=" + iuid
                + ", phoneNo=" + phoneNo
                + ", subsType=" + subsType
                + ", appVersion=" + appVersion
                + ", cAuthKey=" + cAuthKey
                + ", pnotiToken=" + pnotiToken
                + ", osType=" + osType
                + ", osVersion=" + osVersion
                + ", telecomStr=" + telecomStr
                + ", telecomCode=" + telecomCode
                + ", deviceID=" + deviceID
                + ", profileTag=" + profileTag
                + ", profile=" + profile
                + ", addressBook=" + addressBook
                + ", groupList=" + groupList
                + ", storeType=" + storeType
                + ", deviceModel=" + deviceModel
                + ", deviceWidth=" + deviceWidth
                + ", deviceHeight=" + deviceHeight
                + "]";
    }

    public Profile getProfileFromString() throws Exception {
        return Json.toObjectJson(profile, Profile.class);
    }
}
