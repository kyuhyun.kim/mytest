package com.tisquare.mvp.mcs.entity;

public enum SubscriberType {
	DEFAULT(0),
    NORMAL(1),
	NAVER(2),
	GOOGLE(3),
	KAKAO(4),
    FACEBOOK(5);
	
	private final int value;
	private SubscriberType(int value) { this.value = value; }

	public int value() {
		return this.value;
	}
	
	public String toString() {
		return Integer.toString(value);
	}
	
	public static SubscriberType fromInt(int x) {
        switch(x)
        {
        case 1:
            return NORMAL;
        case 2:
            return NAVER;
        case 3:
        	return GOOGLE;
        case 4:
        	return KAKAO;
        case 5:
            return FACEBOOK;
        }
        return null;
    }
}