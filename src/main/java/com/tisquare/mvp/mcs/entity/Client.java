package com.tisquare.mvp.mcs.entity;

import com.tisquare.mvp.mcs.model.UserInfo;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Client {
    private static final int FLAG_INDIVIDUAL_CLIENT = 0;
    private static final int FLAG_COOPERATE_CLIENT = 1;

    private boolean fromDB;
    private Long iuid;
    private String phoneNo;
    private String countryCode;
    private String regionCode;
    private String nationalFmt;
    private int subsType;
    private String baseSvcDir;
    //private Date regDate;
    private String regDate;
    private int fTest;        // 0 : no, 1 : for test
    private int fActive;    // 0 : no, 1 : active
    private String suid;
    private String tuid;
    private String suidPw;


    public Client() {
        super();
    }

    public Client(UserInfo userInfo) {
        long time = System.currentTimeMillis();
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        this.regDate = dateTimeFormat.format(new Date(time));

        fromDB = false;
        iuid = userInfo.getIuid();
        phoneNo = userInfo.getPhoneNo();
        countryCode = userInfo.getCountryCode();
        regionCode = userInfo.getRegionCode();
        nationalFmt = userInfo.getNationalFmt();
        subsType = userInfo.getClientType().value();
        baseSvcDir = userInfo.getBaseSvcDir();
        fTest = userInfo.getfTest();
    }

    public Client(Long iuid, String phoneNo, String countryCode, String nationalFmt) {
        long time = System.currentTimeMillis();
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        this.regDate = dateTimeFormat.format(new Date(time));
        this.setIuid(iuid);
        this.setPhoneNo(phoneNo);
        this.setCountryCode(countryCode);
        this.setNationalFmt(nationalFmt);
        this.setIndividualClient(); // default
    }

    public void setTuid(String tuid) {
        this.tuid = tuid;
    }

    public String getTuid() {
        return tuid;
    }

    public Long getIuid() {
        return this.iuid;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNationalFmt() {
        return nationalFmt;
    }

    public void setNationalFmt(String nationalFmt) {
        this.nationalFmt = nationalFmt;
    }

    public int getSubsType() {
        return subsType;
    }

    public void setSubsType(int subsType) {
        this.subsType = subsType;
    }

    public String getBaseSvcDir() {
        return baseSvcDir;
    }

    public void setBaseSvcDir(String baseSvcDir) {
        this.baseSvcDir = baseSvcDir;
    }

    //public Date getRegDate() {
    //	return this.regDate;
    //}

    //public void setRegDate(Date regDate) {
    //	this.regDate = regDate;
    //}

    public int getFTest() {
        return fTest;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public void setFTest(int fTest) {
        this.fTest = fTest;
    }

    public int getFActive() {
        return this.fActive;
    }

    public void setFActive(int fActive) {
        this.fActive = fActive;
    }

    public boolean isIndividualClient() {
        if (this.subsType == FLAG_INDIVIDUAL_CLIENT)
            return true;

        return false;
    }

    public boolean isCooperateClient() {
        if (this.subsType == FLAG_COOPERATE_CLIENT)
            return true;

        return false;
    }

    public void setIndividualClient() {
        this.subsType = FLAG_INDIVIDUAL_CLIENT;
    }

    public void setCooperateClient() {
        this.subsType = FLAG_COOPERATE_CLIENT;
    }

    public String getClientTypeStr() {
        if (this.subsType == FLAG_INDIVIDUAL_CLIENT)
            return "individual";
        else if (this.subsType == FLAG_COOPERATE_CLIENT)
            return "cooperate";

        return "unknown";
    }

    public String getRegDateStr() {
        if (this.regDate != null)
            return this.regDate.toString();

        return "";
    }

    public boolean isFromDB() {
        return fromDB;
    }

    public void setFromDB(boolean fromDB) {
        this.fromDB = fromDB;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getSuid() {
        return suid;
    }

    public void setSuid(String suid) {
        this.suid = suid;
    }

    public String getSuidPw() {
        return suidPw;
    }

    public void setSuidPw(String suidPw) {
        this.suidPw = suidPw;
    }

    @Override
    public String toString() {
        return "[" +
                "fromDB=" + fromDB
                + ", iuid=" + iuid
                + ", phoneNo=" + phoneNo
                + ", countryCode=" + countryCode
                + ", regionCode=" + regionCode
                + ", nationalFmt=" + nationalFmt
                + ", subsType=" + subsType
                + ", baseSvcDir=" + baseSvcDir
                + ", regDate=" + regDate
                + ", fTest=" + fTest
                + ", fActive=" + fActive
                + ", suid=" + suid
                + ", suidPw=" + suidPw
                + "]";
    }
}
