package com.tisquare.mvp.mcs.entity;

public class Server {
	private String id;
	private String name;
	private String httpHost;
	private String httpsHost;
	
	public String getID() {
		return id;
	}
	
	public void setServerID(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHttpHost() {
		return httpHost;
	}
	
	public void setHttpHost(String httpHost) {
		this.httpHost = httpHost;
	}
	
	public String getHttpsHost() {
		return httpsHost;
	}
	
	public void setHttpsHost(String httpsHost) {
		this.httpsHost = httpsHost;
	}
	
	public String toString() {
		return String.format("ID(%s) Name(%s) httpHost(%s) httpsHost(%s)",
			getID(), getName(), getHttpHost(), getHttpsHost());
	}
}
	