package com.tisquare.mvp.mcs.entity;

import com.tisquare.mvp.mcs.type.ActiveFlag;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
public class ActiveUser {
    private Long iuid;
    private String phone_no;
    private Integer active_flag;
    private Date reg_date;
    private Date trace_stime;
    private Date trace_etime;

    public ActiveUser() {
    }

    private ActiveUser(Long iuid, String phone_no) {
        this.iuid = iuid;
        this.phone_no = phone_no;
    }

    private ActiveUser(Long iuid, String phone_no, ActiveFlag activeFlag, Date reg_date) {
        this.iuid = iuid;
        this.phone_no = phone_no;
        this.active_flag = activeFlag.getCode();
        this.reg_date = reg_date;
    }

    /**
     * 객체를 생성한다.
     *
     * @param iuid     사용자 IUID
     * @param phone_no 사용자 전화번호
     * @return 사용자 정보
     */
    public static ActiveUser create(Long iuid, String phone_no) {
        return new ActiveUser(iuid, phone_no);
    }

    /**
     * 신규 생성 시 사용할 객체를 생성한다.
     *
     * @param iuid       사용자 IUID
     * @param phone_no   사용자 전화번호
     * @param activeFlag 활성화 여부
     * @param reg_date   인증 시간
     * @return 사용자 정보
     */
    public static ActiveUser create(Long iuid, String phone_no, ActiveFlag activeFlag, Date reg_date) {
        return new ActiveUser(iuid, phone_no, activeFlag, reg_date);
    }

    public Long getIuid() {
        return iuid;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public Integer getActive_flag() {
        return active_flag;
    }

    public void setActive_flag(Integer active_flag) {
        this.active_flag = active_flag;
    }

    public Date getReg_date() {
        return reg_date;
    }

    public void setReg_date(Date reg_date) {
        this.reg_date = reg_date;
    }

    public Date getTrace_stime() {
        return trace_stime;
    }

    public void setTrace_stime(Date trace_stime) {
        this.trace_stime = trace_stime;
    }

    public Date getTrace_etime() {
        return trace_etime;
    }

    public void setTrace_etime(Date trace_etime) {
        this.trace_etime = trace_etime;
    }

    /**
     * 현재 시간을 기준으로 추적 시작 시간과 종료 시간을 설정한다.
     *
     * @param traceStartTime 추적 시작 시간
     * @param traceEndTime   추적 종료 시간
     */
    private void setTraceDate(Date traceStartTime, Date traceEndTime) {
        this.setTrace_stime(traceStartTime);
        this.setTrace_etime(traceEndTime);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }
}
