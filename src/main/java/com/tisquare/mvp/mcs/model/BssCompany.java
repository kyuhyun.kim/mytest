package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BssCompany {

    private String cp_code;
    private String cp_name;
    private String cp_eng_name;
    private int name_type;
    private int cp_status;
    private int cp_active;
    private String cp_tel;
    private String dp_tag;
    private String agency_code;
    private int auth_type;
    private int active_admin;
    private int active_comm;
    private int cp_org_manage_flag;
    private long lc_use_yn;
    private long lc_collect_cycle;
    private long lc_report_cycle;

    public void setLc_use_yn(long lc_use_yn) {
        this.lc_use_yn = lc_use_yn;
    }

    public void setLc_collect_cycle(long lc_collect_cycle) {
        this.lc_collect_cycle = lc_collect_cycle;
    }

    public void setLc_report_cycle(long lc_report_cycle) {
        this.lc_report_cycle = lc_report_cycle;
    }

    public long getLc_use_yn() {
        return lc_use_yn;
    }

    public long getLc_collect_cycle() {
        return lc_collect_cycle;
    }

    public long getLc_report_cycle() {
        return lc_report_cycle;
    }

    public void setActive_comm(int active_comm) {
        this.active_comm = active_comm;
    }

    public int getActive_comm() {
        return active_comm;
    }

    public void setActive_admin(int active_admin) {
        this.active_admin = active_admin;
    }

    public int getActive_admin() {
        return active_admin;
    }

    public void setCp_org_manage_flag(int cp_org_manage_flag) {
        this.cp_org_manage_flag = cp_org_manage_flag;
    }

    public int getCp_org_manage_flag() {
        return cp_org_manage_flag;
    }

    public void setCp_eng_name(String cp_eng_name) {
        this.cp_eng_name = cp_eng_name;
    }

    public void setName_type(int name_type) {
        this.name_type = name_type;
    }

    public String getCp_eng_name() {
        return cp_eng_name;
    }

    public int getName_type() {
        return name_type;
    }

    public void setAuth_type(int auth_type) {
        this.auth_type = auth_type;
    }

    public int getAuth_type() {
        return auth_type;
    }

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public void setCp_name(String cp_name) {
        this.cp_name = cp_name;
    }

    public void setCp_status(int cp_status) {
        this.cp_status = cp_status;
    }

    public void setCp_active(int cp_active) {
        this.cp_active = cp_active;
    }

    public void setCp_tel(String cp_tel) {
        this.cp_tel = cp_tel;
    }

    public void setDp_tag(String dp_tag) {
        this.dp_tag = dp_tag;
    }

    public void setAgency_code(String agency_code) {
        this.agency_code = agency_code;
    }

    public String getCp_code() {
        return cp_code;
    }

    public String getCp_name() {
        return cp_name;
    }

    public int getCp_status() {
        return cp_status;
    }

    public int getCp_active() {
        return cp_active;
    }

    public String getCp_tel() {
        return cp_tel;
    }

    public String getDp_tag() {
        return dp_tag;
    }

    public String getAgency_code() {
        return agency_code;
    }
}