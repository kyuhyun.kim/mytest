package com.tisquare.mvp.mcs.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by jhkim on 14. 2. 25.
 */
public class Items {

    @JsonProperty("sid")
    int sid;

    @JsonProperty("title")
    String title;


    @JsonProperty("date")
    String date;

    @JsonProperty("is-new")
    long is_new;

    public int getSid() {
        return sid;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public long getIs_new() {
        return is_new;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setIs_new(long is_new) {
        this.is_new = is_new;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\"Items\":[{\n");
        sb.append("\"sid\":\""+         getSid() + "\"\n");
        sb.append("\"title\":\""+       getTitle() + "\"\n");
        sb.append("\"date\":\""+        getIs_new() + "\"\n");
        sb.append("\"is_new\":\""+      getIs_new() + "\"\n");
        return sb.toString();
    }


}
