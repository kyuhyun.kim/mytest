package com.tisquare.mvp.mcs.model;


/**
 * Created by jhkim on 14. 3. 20.
 */
public enum SvcCode {
    HDR_MUSICAL(1),
    HDR_SMALLCALL(2),
    HDR_CONTACT_SHR(3),
    HDR_THANKQ_MSG(4),

    //뮤지컬콜
    MUSICAL_SOUNDCON(101),
    MUSICAL_STICKERCON(102),

    //화면유지콜
    SMALLCALL_CALLRECV(201),
    SMALLCALL_KEEP_RUN(202),
    SMALLCALL_ACCEPT(203),
    SMALLCALL_DENIED(204),
    SMALLCALL_DENIED_MSG(205),

    //연락처공유
    CONTACT_SHARE(300),

    //감사메시지
    THANKQ_MSG(400),
    UNKNOWN(999);

    private static SvcCode[] svcArray;

    SvcCode (int value){ this.value = value; }
    private int value;
    public int value(){ return value; }

    public static Boolean validSvcCodeCheck(int code){
        for(SvcCode s : SvcCode.values()){
            if(s.value() == code){
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static SvcCode fromInt(int code){
        if(svcArray == null)
            svcArray = SvcCode.values();
        for(int i=0; i < svcArray.length; i++){
            if(svcArray[i].value() == code)
                return  svcArray[i];
        }
        return UNKNOWN;
    }
}
