package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Login_info {

    @JsonProperty("device-chg-f")
    private int device_chg_f;

    @JsonProperty("login-status")
    private int login_status;

    @JsonProperty("phone-chg-f")
    private int phone_chg_f;

    public void setDevice_chg_f(int device_chg_f) {
        this.device_chg_f = device_chg_f;
    }

    public void setLogin_status(int login_status) {
        this.login_status = login_status;
    }

    public int getDevice_chg_f() {
        return device_chg_f;
    }

    public int getLogin_status() {
        return login_status;
    }

    public void setPhone_chg_f(int phone_chg_f) {
        this.phone_chg_f = phone_chg_f;
    }

    public int getPhone_chg_f() {
        return phone_chg_f;
    }
}