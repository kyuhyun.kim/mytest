package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class IPCNsFcm {

    @JsonProperty("registration_ids")
    private String[] registration_ids;

    @JsonProperty("data")
    private String data;

    public IPCNsFcm(String[] registration_ids, String data )
    {
        this.registration_ids = registration_ids;
        this.data = data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setRegistration_ids(String[] registration_ids) {
        this.registration_ids = registration_ids;
    }



    public String[] getRegistration_ids() {
        return registration_ids;
    }


}