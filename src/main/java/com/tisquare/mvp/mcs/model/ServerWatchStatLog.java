package com.tisquare.mvp.mcs.model;

import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.domain.DevCrashLog;
import com.tisquare.mvp.mcs.filter.HttpRequestWrapper;
import com.tisquare.mvp.mcs.interceptor.TrafficInterceptor;
import com.tisquare.mvp.mcs.type.ApiCodeType;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.util.*;

/**
 * Created by jhkim on 14. 11. 10.
 */

public class ServerWatchStatLog {

    String 발생시간;
    String 호스트명;
    String 서비스명;
    String 인스턴스_아이디;
    String API_명;
    String API_코드;
    String 성공_유무;
    String 결과_코드;
    String 결과_메시지;
    String 명령_처리_시간;
    String 트래픽_정보;
    String 사용자_ID;
    String 단말_운영체제_타입;
    String 단말_운영체제_버전;
    String 앱_버전;
    String 요청자;
    String 통신사_코드;
    String 가입자_IUID;
    String 인증키;
    String 마켓_유형;
    String 단말_ID;
    String 단말_모델명;
    String 공지사항_FAQ의_1번째_ID;
    String 마지막_공지사항_FAQ_ID;
    String 공지사항_FAQ_ID;
    String 가족_구성원_MDN;
    String 가족_구성원_IUID;
    String 가족_관계;
    String 일정_등록_대상_IUID;

    public ServerWatchStatLog(HttpServletRequest request, HttpServletResponse response,  long elapse) {

        try {


            /*this.발생시간 = String.valueOf(System.currentTimeMillis());
            this.호스트명 = InetAddress.getLocalHost().getHostName();
            this.트래픽_정보 = String.valueOf(TrafficInterceptor.getCurrTraffic());
            this.트래픽_정보 = TrafficInterceptor.getTest();
            *//*this.서비스명 =test;*/
          /*  this.서비스명 = initConfig.getEms_log_svc_key();
            this.인스턴스_아이디 = initConfig.getEms_log_svc_svr_instance();*/
            this.API_명 = request.getRequestURI();
            this.트래픽_정보 = String.valueOf(TrafficInterceptor.getCurrTraffic());
            this.명령_처리_시간 = String.valueOf( elapse );

            this.API_코드 = ApiCodeType.getApiCode(this.API_명);

            if(response.getStatus() == HttpStatus.SC_OK)
                this.성공_유무 = "Y";
            else
                this.성공_유무 = "N";
            this.결과_코드 = String.valueOf(response.getStatus());
            this.결과_메시지 =ApiCodeType.getResultMsg(this.API_명, response.getStatus());

            if (request.getParameterMap().size() > 0) {
                Enumeration<String> e = request.getParameterNames();
                String key;
                while (e.hasMoreElements()) {
                    key = e.nextElement();
                    if (key != null) {
                        key.toLowerCase();
                        if (StringUtils.equals(key, "os-type"))
                            this.단말_운영체제_타입 = request.getParameter(key);
                        else if (StringUtils.equals(key, "os-version"))
                            this.단말_운영체제_버전 = request.getParameter(key);
                        else if (StringUtils.equals(key, "telecom-code"))
                            this.통신사_코드 = request.getParameter(key);
                        else if (StringUtils.equals(key, "device-id"))
                            this.단말_ID = request.getParameter(key);
                        else if (StringUtils.equals(key, "device-model"))
                            this.단말_모델명 = request.getParameter(key);
                    }
                }
            }

            Enumeration<String> enumeration = request.getHeaderNames();
            while (enumeration != null && enumeration.hasMoreElements()) {
                String key = enumeration.nextElement();
                if (key != null) {
                    if (StringUtils.equals(key.toLowerCase(), "tn-app-market"))
                        this.마켓_유형 = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-req-user"))
                        this.요청자 = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-app-version"))
                        this.앱_버전 = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-iuid"))
                        this.가입자_IUID = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-auth-key"))
                        this.인증키 = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-start-hid"))
                        this.공지사항_FAQ의_1번째_ID = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-start-sid"))
                        this.공지사항_FAQ의_1번째_ID = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-last-read-hid"))
                        this.마지막_공지사항_FAQ_ID = request.getHeader(key);
                    else if (StringUtils.equals(key.toLowerCase(), "tn-last-read-sid"))
                        this.마지막_공지사항_FAQ_ID = request.getHeader(key);
                }
            }

            HttpRequestWrapper wrequest = (HttpRequestWrapper) request;
            String contentType = request.getContentType();

            if (!StringUtils.isEmpty(contentType) && contentType.startsWith("multipart")) {

                String boundary = getAcceptParamValue("boundary", getAcceptParam("boundary", contentType));
                List<byte[]> bytes = getMultiPartAsByteList(wrequest.toByteArray(), boundary);
                for (byte[] b : bytes) {
                    HttpLogMultiPart part = getMultiParts(b);

                    if (part != null) {
                        Set<String> keys = part.getHeaders().keySet();
                        for (String key : keys) {
                        }

                    } else {

                    }
                }
            } else if (request.getContentLength() > 0) {

                if (contentType.startsWith("application")) {
                    String body = new String(wrequest.toByteArray(), request.getCharacterEncoding() != null ? request.getCharacterEncoding() : "UTF-8");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private HttpHeader getHeaderValue(String hdr) {

        String[] str = hdr.split(":", 2);
        return new HttpHeader(str[0], str[1]);
    }

    private String getAcceptParam(String name, String param) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(param)) return null;
        try {
            String[] split = StringUtils.split(param, ';');
            if (split != null) {
                for (String s : split) {
                    if (StringUtils.isEmpty(s)) continue;
                    if (StringUtils.startsWithIgnoreCase(StringUtils.trim(s), name))
                        return s;
                }
            }
            return null;
//            return StringUtils.split(value, ';')[1];
        } catch (Exception e) {
            return null;
        }
    }

    private String getAcceptParamValue(String name, String param) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(param)) return null;
        try {
            String[] split = StringUtils.split(param, '=');
            if (split != null && split.length > 0) {
                if (StringUtils.startsWithIgnoreCase(StringUtils.trim(split[0]), name)) {
                    return split[1];
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private HttpLogMultiPart getMultiParts(byte[] in) {
        HttpLogMultiPart ret = new HttpLogMultiPart();
        try {
            int i;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            //get Headers
            for (i = 0; i < in.length; i++) {
                if (in[i] == '\r' && in[i + 1] == '\n') {
                    if (bos.size() == 0) {
                        i += 2;
                        break; // Header Part end
                    } else {
                        HttpHeader hdr = getHeaderValue(new String(bos.toByteArray()));
                        ret.setHeaders(hdr.getHdr(), hdr.getValue());
                        bos.reset();
                        i = i + 1;
                    }
                } else {
                    bos.write(in[i]);
                }
            }

            // get Body
            if (StringUtils.startsWithIgnoreCase(ret.getContent_Type(), MediaType.APPLICATION_JSON_VALUE)) {
                String charset = getAcceptParamValue("charset", getAcceptParam("charset", ret.getContent_Type()));
                ret.setBody(new String(Arrays.copyOfRange(in, i, in.length),
                        charset != null ? charset : "UTF-8"));
            } else {
                ret.setBody("(non application/json data)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    private List<byte[]> getMultiPartAsByteList(byte[] in, String boundary) {
        List<byte[]> bytes = new ArrayList<byte[]>();

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int i;
            byte[] byte_boundary = ("--" + boundary).getBytes();
            for (i = 0; i < in.length; i++) {
                int j;
                for (j = 0; j < byte_boundary.length; j++) {
                    if (j + i >= in.length) break;
                    if (in[i + j] != byte_boundary[j]) break;
                }
                if (j == byte_boundary.length) {
                    if (in[i + j] == '\r' && in[i + j + 1] == '\n') j = j + 1;
                    else if (in[i + j] == '-' && in[i + j + 1] == '-') {
                        bytes.add(Arrays.copyOfRange(bos.toByteArray(), 0, bos.size() - 2));
                        bos.reset();
                        break;
                    }
                    if (bos.size() > 0) bytes.add(Arrays.copyOfRange(bos.toByteArray(), 0, bos.size() - 2));
                    bos.reset();
                    i = i + j;
                } else bos.write(in[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }


    public void set발생시간(String 발생시간) {
        this.발생시간 = 발생시간;
    }

    public void set호스트명(String 호스트명) {
        this.호스트명 = 호스트명;
    }

    public void set서비스명(String 서비스명) {
        this.서비스명 = 서비스명;
    }

    public void set인스턴스_아이디(String 인스턴스_아이디) {
        this.인스턴스_아이디 = 인스턴스_아이디;
    }

    public void setAPI_명(String API_명) {
        this.API_명 = API_명;
    }

    public void setAPI_코드(String API_코드) {
        this.API_코드 = API_코드;
    }

    public void set성공_유무(String 성공_유무) {
        this.성공_유무 = 성공_유무;
    }

    public void set결과_코드(String 결과_코드) {
        this.결과_코드 = 결과_코드;
    }

    public void set결과_메시지(String 결과_메시지) {
        this.결과_메시지 = 결과_메시지;
    }

    public void set명령_처리_시간(String 명령_처리_시간) {
        this.명령_처리_시간 = 명령_처리_시간;
    }

    public void set트래픽_정보(String 트래픽_정보) {
        this.트래픽_정보 = 트래픽_정보;
    }

    public void set사용자_ID(String 사용자_ID) {
        this.사용자_ID = 사용자_ID;
    }

    public void set단말_운영체제_타입(String 단말_운영체제_타입) {
        this.단말_운영체제_타입 = 단말_운영체제_타입;
    }

    public void set단말_운영체제_버전(String 단말_운영체제_버전) {
        this.단말_운영체제_버전 = 단말_운영체제_버전;
    }

    public void set앱_버전(String 앱_버전) {
        this.앱_버전 = 앱_버전;
    }

    public void set요청자(String 요청자) {
        this.요청자 = 요청자;
    }

    public void set통신사_코드(String 통신사_코드) {
        this.통신사_코드 = 통신사_코드;
    }

    public void set가입자_IUID(String 가입자_IUID) {
        this.가입자_IUID = 가입자_IUID;
    }

    public void set마켓_유형(String 마켓_유형) {
        this.마켓_유형 = 마켓_유형;
    }

    public void set단말_ID(String 단말_ID) {
        this.단말_ID = 단말_ID;
    }

    public void set단말_모델명(String 단말_모델명) {
        this.단말_모델명 = 단말_모델명;
    }

    public void set공지사항_FAQ의_1번째_ID(String 공지사항_FAQ의_1번째_ID) {
        this.공지사항_FAQ의_1번째_ID = 공지사항_FAQ의_1번째_ID;
    }

    public void set마지막_공지사항_FAQ_ID(String 마지막_공지사항_FAQ_ID) {
        this.마지막_공지사항_FAQ_ID = 마지막_공지사항_FAQ_ID;
    }

    public void set공지사항_FAQ_ID(String 공지사항_FAQ_ID) {
        this.공지사항_FAQ_ID = 공지사항_FAQ_ID;
    }

    public void set가족_구성원_MDN(String 가족_구성원_MDN) {
        this.가족_구성원_MDN = 가족_구성원_MDN;
    }

    public void set가족_구성원_IUID(String 가족_구성원_IUID) {
        this.가족_구성원_IUID = 가족_구성원_IUID;
    }

    public void set가족_관계(String 가족_관계) {
        this.가족_관계 = 가족_관계;
    }

    public void set일정_등록_대상_IUID(String 일정_등록_대상_IUID) {
        this.일정_등록_대상_IUID = 일정_등록_대상_IUID;
    }

    public String get발생시간() {
        return 발생시간;
    }

    public String get호스트명() {
        return 호스트명;
    }

    public String get서비스명() {
        return 서비스명;
    }

    public String get인스턴스_아이디() {
        return 인스턴스_아이디;
    }

    public String getAPI_명() {
        return API_명;
    }

    public String getAPI_코드() {
        return API_코드;
    }

    public String get성공_유무() {
        return 성공_유무;
    }

    public String get결과_코드() {
        return 결과_코드;
    }

    public String get결과_메시지() {
        return 결과_메시지;
    }

    public String get명령_처리_시간() {
        return 명령_처리_시간;
    }

    public String get트래픽_정보() {
        return 트래픽_정보;
    }

    public String get사용자_ID() {
        return 사용자_ID;
    }

    public String get단말_운영체제_타입() {
        return 단말_운영체제_타입;
    }

    public String get단말_운영체제_버전() {
        return 단말_운영체제_버전;
    }

    public String get앱_버전() {
        return 앱_버전;
    }

    public String get요청자() {
        return 요청자;
    }

    public String get통신사_코드() {
        return 통신사_코드;
    }

    public String get가입자_IUID() {
        return 가입자_IUID;
    }

    public String get마켓_유형() {
        return 마켓_유형;
    }

    public String get단말_ID() {
        return 단말_ID;
    }

    public String get단말_모델명() {
        return 단말_모델명;
    }

    public String get공지사항_FAQ의_1번째_ID() {
        return 공지사항_FAQ의_1번째_ID;
    }

    public String get마지막_공지사항_FAQ_ID() {
        return 마지막_공지사항_FAQ_ID;
    }

    public String get공지사항_FAQ_ID() {
        return 공지사항_FAQ_ID;
    }

    public String get가족_구성원_MDN() {
        return 가족_구성원_MDN;
    }

    public String get가족_구성원_IUID() {
        return 가족_구성원_IUID;
    }

    public String get가족_관계() {
        return 가족_관계;
    }

    public String get일정_등록_대상_IUID() {
        return 일정_등록_대상_IUID;
    }

    @Override
    public String toString() {
        String log = null;
        if(!StringUtils.equals( this.API_명,"/"))
        {
            log = String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                    this.API_명, this.API_코드, this.성공_유무, this.결과_코드, this.결과_메시지, this.명령_처리_시간, this.트래픽_정보,
                    this.사용자_ID, this.단말_운영체제_타입, this.단말_운영체제_버전, this.앱_버전, this.요청자, this.통신사_코드,
                    this.가입자_IUID, this.인증키, this.마켓_유형, this.단말_ID, this.단말_모델명, this.공지사항_FAQ의_1번째_ID, this.마지막_공지사항_FAQ_ID, this.공지사항_FAQ_ID);

        }
        return log;
    }

    class HttpLogMultiPart {
        private static final String CONTENT_TYPE = "content-type";

        Map<String, String> headers = new HashMap<String, String>();
        String body;

        public String getContent_Type() {
            return headers.get(CONTENT_TYPE);
        }

        public Map<String, String> getHeaders() {
            return headers;
        }

        public String getHeaderValue(String key) {
            return headers.get(key);
        }

        public void setHeaders(String hdr, String value) {
            headers.put(hdr, value);
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }

    class HttpHeader {
        String hdr;
        String value;

        public HttpHeader(String hdr, String value) {
            this.hdr = StringUtils.lowerCase(hdr);
            this.value = value.trim();
        }

        public String getHdr() {
            return hdr;
        }

        public void setHdr(String hdr) {
            this.hdr = hdr;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
