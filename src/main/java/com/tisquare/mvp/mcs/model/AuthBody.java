package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;

import com.tisquare.mvp.mcs.entity.EventPopup;
import org.apache.http.HttpStatus;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import java.net.InetAddress;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AuthBody {
    private static final int FLAG_PAID_REGULAR_MEMBER = 1; // 유료 정회원
    private static final int FLAG_FREE_REGULAR_MEMBER = 2; // 무료 정회원
    private static final int FLAG_ASSOCIATE_MEMBER = 3;  // 준회원


    @JsonProperty("is-subs")
    private int is_subs;

    @JsonProperty("iuid")
    private Long iuid;

    @JsonProperty("auth-key")
    private String authKey;

    @JsonProperty("servers")
    private List<ServerConnInfo> serverList;

    @JsonProperty("notice")
    private NoticeInfo noticeInfo;

    @JsonProperty("help")
    private HelpInfo helpInfo;

    @JsonProperty("app-update")
    private AppUpdatetInfo app_update;

    @JsonProperty("new-notice-cnt")
    private Integer newNoticeCnt;

    @JsonProperty("event-notice")
    private EventPopup eventPopup;

    @JsonProperty("last-notice-time")
    private String lastNoticeTime;

    @JsonProperty("last-nid")
    private Long lastNid;

    @JsonProperty("bt-info")
    private List<BtInfo> bt_info;

    @JsonProperty("url-info")
    private UrlInfo url_info;

    @JsonProperty("profile")

    private Profile profile;

    @JsonProperty("profile-tag")
    private String profileTag;



    public void setUrl_info(UrlInfo url_info) {
        this.url_info = url_info;
    }

    public UrlInfo getUrl_info() {
        return url_info;
    }

    private String Trace ;

    public void setBt_info(List<BtInfo> bt_info) {
        this.bt_info = bt_info;
    }

    public List<BtInfo> getBt_info() {
        return bt_info;
    }

    public int getIs_subs() {
        return is_subs;
    }

    public void setIs_subs(int is_subs) {
        this.is_subs = is_subs;
    }

    public String getTrace() {
        return Trace;
    }

    public void setTrace(String trace) {

        Trace = trace;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setProfileTag(String profileTag) {
        this.profileTag = profileTag;
    }

    public Profile getProfile() {

        return profile;
    }

    public String getProfileTag() {
        return profileTag;
    }




    public void setLastNid(Long lastNid) {
        this.lastNid = lastNid;
    }

    public Long getLastNid() {

        return lastNid;
    }

    public void setNewNoticeCnt(Integer newNoticeCnt) {
        this.newNoticeCnt = newNoticeCnt;
    }

    public Integer getNewNoticeCnt() {

        return newNoticeCnt;
    }

    public void setLastNoticeTime(String lastNoticeTime) {
        this.lastNoticeTime = lastNoticeTime;
    }

    public String getLastNoticeTime() {

        return lastNoticeTime;
    }

    public void setEventPopup(EventPopup eventPopup) {
        this.eventPopup = eventPopup;
    }

    public EventPopup getEventPopup() {

        return eventPopup;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setServerList(List<ServerConnInfo> serverList) {
        this.serverList = serverList;
    }

    public void setNoticeInfo(NoticeInfo noticeInfo) {
        this.noticeInfo = noticeInfo;
    }

    public void setHelpInfo(HelpInfo helpInfo) {
        this.helpInfo = helpInfo;
    }

    public void setApp_update(AppUpdatetInfo app_update) {
        this.app_update = app_update;
    }

    public static int getFlagPaidRegularMember() {

        return FLAG_PAID_REGULAR_MEMBER;
    }

    public static int getFlagFreeRegularMember() {
        return FLAG_FREE_REGULAR_MEMBER;
    }

    public static int getFlagAssociateMember() {
        return FLAG_ASSOCIATE_MEMBER;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getAuthKey() {
        return authKey;
    }

    public List<ServerConnInfo> getServerList() {
        return serverList;
    }

    public NoticeInfo getNoticeInfo() {
        return noticeInfo;
    }

    public HelpInfo getHelpInfo() {
        return helpInfo;
    }

    public AppUpdatetInfo getApp_update() {
        return app_update;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("iuid=").append(iuid);
        sb.append(", authKey=").append(authKey);
        sb.append(", serverList=").append(serverList);
        sb.append(", notice=").append(noticeInfo);
        sb.append(", help=").append(helpInfo);
        sb.append(", app-update=").append(app_update);
        sb.append("]");
        return sb.toString();
    }

    public void RequestToString( String proto, String url, String telecomCode, int httpStatus) {


            final StringBuilder sb = new StringBuilder();

            sb.append(proto + "|");
            sb.append("ACS" + "|");
            sb.append(url + "|");
            if (telecomCode.equals("45005"))
                sb.append("자사" + "|");
            else
                sb.append("타사" + "|");
            sb.append(iuid + "|");
            sb.append(is_subs + "|");
            sb.append(httpStatus);
            if (httpStatus == HttpStatus.SC_OK)
                sb.append("(OK)" + "|");
            else
                sb.append("(FAIL)" + "|");

            Trace = sb.toString();


    }


    public void CspToString( String proto, String url, MCSResponse mcsResponse ) {
        final StringBuilder sb = new StringBuilder("");

        sb.append(proto + "|");
        sb.append("CSP" + "|");
        sb.append(url + "|");
        if(mcsResponse != null) {
            sb.append(mcsResponse.getReturnCode() + "|");
            sb.append(mcsResponse.getCustomerMgntNum() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerVolte() + "|");
            sb.append(mcsResponse.getCustomerMainProdId() + "|");
            sb.append("200(OK)" + "|");
        }
        else if(mcsResponse == null) {
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("FAIL" + "|");
        }

        Trace += sb.toString();
    }

    public void append( String append) {
        Trace += append;
    }

    public void SetInfo( ) {
        final StringBuilder sb = new StringBuilder("");

        Trace += sb.toString();
    }



//    public String toJsonString() throws Exception {
//        return Json.toStringJson(this);
//    }
}