package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.PromotionSamsung;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PromotionSamsungInfo {

    @JsonProperty("rel-phone-no")
    private String rel_phone_no;

    @JsonProperty("name")
    private String name;

    @JsonProperty("gender")
    private Integer gender;

    @JsonProperty("birth")
    private Long birth;

    @JsonProperty("relationship")
    private String relationship;

    @JsonProperty("reg-date")
    private String reg_date;

    @JsonProperty("rel-iuid")
    private Long rel_iuid;

    @JsonProperty("samsung-url")
    private String samsung_url;


    @JsonProperty("rel-addr")
    private String rel_addr;


    @JsonProperty("rel-addr-no")
    private String rel_addr_no;

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public void setBirth(Long birth) {
        this.birth = birth;
    }

    public Integer getGender() {
        return gender;
    }

    public Long getBirth() {
        return birth;
    }

    public PromotionSamsungInfo()
    {

    }

    public void setRel_addr_no(String rel_addr_no) {
        this.rel_addr_no = rel_addr_no;
    }

    public String getRel_addr_no() {
        return rel_addr_no;
    }

    public void PromotionSamsungInfo(PromotionSamsung promotionSamsung)
    {
        this.rel_phone_no = promotionSamsung.getRel_phone_no();
        this.relationship = promotionSamsung.getRelationship();
        this.reg_date = promotionSamsung.getReg_date();
        this.rel_iuid = promotionSamsung.getRel_iuid();
        this.name = promotionSamsung.getName();
        this.rel_addr = promotionSamsung.getRel_addr();
        this.rel_addr_no = promotionSamsung.getRel_addr_no();
        this.gender = promotionSamsung.getGender();
        this.birth = promotionSamsung.getBirth();
    }

    public void setRel_addr(String rel_addr) {
        this.rel_addr = rel_addr;
    }

    public String getRel_addr() {
        return rel_addr;
    }

    public void setRel_iuid(Long rel_iuid) {
        this.rel_iuid = rel_iuid;
    }

    public void setSamsung_url(String samsung_url) {
        this.samsung_url = samsung_url;
    }

    public Long getRel_iuid() {
        return rel_iuid;
    }

    public String getSamsung_url() {
        return samsung_url;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setRel_phone_no(String rel_phone_no) {
        this.rel_phone_no = rel_phone_no;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }


    public String getRel_phone_no() {
        return rel_phone_no;
    }

    public String getName() {
        return name;
    }

    public String getRelationship() {
        return relationship;
    }


}