package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class HelpIndex {

    @JsonProperty("CNT")
    private Long hid_cnt;

    @JsonProperty("MAX_HID")
    private String last_hid;

    public HelpIndex() {
    }

    public Long getHid_cnt() {
        return hid_cnt;
    }

    public String getLast_hid() {
        return last_hid;
    }

    public void setHid_cnt(Long hid_cnt) {

        this.hid_cnt = hid_cnt;
    }

    public void setLast_hid(String last_hid) {
        this.last_hid = last_hid;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HelpIndex:{");
        sb.append("MAX_HID='").append(last_hid).append('\'');
        sb.append(", CNT='").append(hid_cnt).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
