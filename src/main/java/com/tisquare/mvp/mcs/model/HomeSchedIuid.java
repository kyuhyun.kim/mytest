package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class HomeSchedIuid {

    @JsonProperty("target-iuid")
    private Long target_iuid;

    @JsonIgnore
    private Long iuid;

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public Long getIuid() {
        return iuid;
    }

    public void setTarget_iuid(Long target_iuid) {
        this.target_iuid = target_iuid;
    }

    public Long getTarget_iuid() {
        return target_iuid;
    }
}