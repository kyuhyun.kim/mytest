package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;




@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BtInfo {

    @JsonProperty("model")
    private String model;

    @JsonProperty("prod-cd")
    private String prod_cd;

    @JsonProperty("url")
    private String url;

    @JsonProperty("img-url")
    private String img_url;

    @JsonProperty("description")
    private String description;

    public BtInfo()
    {

    }

    public BtInfo(String model, String prod_cd, String seq, String url) {

        this.model = model;
        this.prod_cd = prod_cd;
        this.url = url + "=" + seq;

    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getDescription() {
        return description;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setProd_cd(String prod_cd) {
        this.prod_cd = prod_cd;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getModel() {
        return model;
    }

    public String getProd_cd() {
        return prod_cd;
    }

    public String getUrl() {
        return url;
    }
}