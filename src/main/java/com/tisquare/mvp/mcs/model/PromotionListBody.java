package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PromotionListBody {

    @JsonProperty("promotion-cnt")
    private int pro_cnt;

    @JsonProperty("promotion-list")
    private List<Promotion> promotionList;

    public void setPro_cnt(int pro_cnt) {
        this.pro_cnt = pro_cnt;
    }

    public void setPromotionList(List<Promotion> promotionList) {
        this.promotionList = promotionList;
    }

    public int getPro_cnt() {
        return pro_cnt;
    }

    public List<Promotion> getPromotionList() {
        return promotionList;
    }
}