package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.entity.EventPopup;
import org.apache.http.HttpStatus;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class MsgSmsBody {

    @JsonProperty("cause")
    private String cause;

    @JsonProperty("value")
    private String value;

    public void setCause(String cause) {
        this.cause = cause;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCause() {
        return cause;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("cause=").append(cause);
        sb.append(", value=").append(cause);
        sb.append("}");
        return sb.toString();
    }

}