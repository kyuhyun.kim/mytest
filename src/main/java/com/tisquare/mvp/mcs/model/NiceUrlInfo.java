package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by kimkyuhyun on 2016-03-30.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class NiceUrlInfo {

    @JsonIgnore
    private final String HTML = ".html";

    @JsonIgnore
    private String privacy_kt_info;

    @JsonIgnore
    private String privacy_lgt_info;

    @JsonIgnore
    private String privacy_skt_info;

    @JsonIgnore
    private String unique_kt_info;

    @JsonIgnore
    private String unique_lgt_info;

    @JsonIgnore
    private String unique_skt_info;

    @JsonIgnore
    private String telecom_kt_info;

    @JsonIgnore
    private String telecom_lgt_info;

    @JsonIgnore
    private String telecom_skt_info;

    @JsonIgnore
    private String common_agreement_info;

    @JsonIgnore
    private String safe_kt_info;

    @JsonIgnore
    private String safe_lgt_info;

    @JsonIgnore
    private String safe_skt_info;

    @JsonIgnore
    private String smart_otp_info;

    @JsonProperty("nice-info")
    private String nice_info;

    public NiceUrlInfo(String TelecomList, String privacy_info, String unique_info, String telecom_info, String common_agreement_info, String safe_info, String smart_otp_info, String nice_info)
    {
        String[] TelList = TelecomList.split(",");
        for(int nIdx = 0; nIdx < TelList.length; nIdx++)
        {
            if( TelList[nIdx].toLowerCase().equals("kt")) {
                this.privacy_kt_info = privacy_info + "_" + TelList[nIdx] + HTML;
                this.unique_kt_info = unique_info  + "_" + TelList[nIdx] + HTML;
                this.telecom_kt_info = telecom_info + "_" + TelList[nIdx] + HTML;
                this.safe_kt_info = safe_info    + "_" + TelList[nIdx] + HTML;
            }
            else if( TelList[nIdx].toLowerCase().equals("lgu")) {
                this.privacy_lgt_info = privacy_info + "_" + TelList[nIdx] + HTML;
                this.unique_lgt_info = unique_info  + "_" + TelList[nIdx] + HTML;
                this.telecom_lgt_info = telecom_info + "_" + TelList[nIdx] + HTML;
                this.safe_lgt_info = safe_info    + "_" + TelList[nIdx] + HTML;
            }
            else if( TelList[nIdx].toLowerCase().equals("skt")) {
                this.privacy_skt_info = privacy_info + "_" + TelList[nIdx] + HTML;
                this.unique_skt_info = unique_info  + "_" + TelList[nIdx] + HTML;
                this.telecom_skt_info = telecom_info + "_" + TelList[nIdx] + HTML;
                this.safe_skt_info = safe_info    + "_" + TelList[nIdx] + HTML;
            }
        }

        this.common_agreement_info = common_agreement_info + HTML;
        this.smart_otp_info =  smart_otp_info + HTML;
        this.nice_info = nice_info;
    }

    public NiceUrlInfo(String URL_DOMAIN, String TelecomList, String privacy_info, String unique_info, String telecom_info, String common_agreement_info, String safe_info, String smart_otp_info, String nice_info)
    {


         this.privacy_kt_info = URL_DOMAIN + privacy_info + "_" + "kt" + HTML;
        this.unique_kt_info = URL_DOMAIN + unique_info + "_" + "kt" + HTML;
        this.telecom_kt_info = URL_DOMAIN + telecom_info + "_" + "kt" + HTML;
        this.safe_kt_info = URL_DOMAIN + safe_info + "_" + "kt" + HTML;

        this.privacy_lgt_info = URL_DOMAIN + privacy_info + "_" + "lgu" + HTML;
        this.unique_lgt_info = URL_DOMAIN + unique_info + "_" + "lgu" + HTML;
        this.telecom_lgt_info = URL_DOMAIN + telecom_info + "_" + "lgu" + HTML;
        this.safe_lgt_info = URL_DOMAIN + safe_info + "_" + "lgu" + HTML;

        this.privacy_skt_info = URL_DOMAIN + privacy_info + "_" + "skt" + HTML;
        this.unique_skt_info = URL_DOMAIN + unique_info + "_" + "skt" + HTML;
        this.telecom_skt_info = URL_DOMAIN + telecom_info + "_" + "skt" + HTML;
        this.safe_skt_info = URL_DOMAIN + safe_info + "_" + "skt" + HTML;

        this.common_agreement_info = URL_DOMAIN + common_agreement_info + HTML;
        this.smart_otp_info = URL_DOMAIN + smart_otp_info + HTML;

        this.nice_info = URL_DOMAIN + nice_info;
    }

    public void setNice_info(String nice_info) {
        this.nice_info = nice_info;
    }

    public String getNice_info() {
        return nice_info;
    }

    public void setPrivacy_kt_info(String privacy_kt_info) {
        this.privacy_kt_info = privacy_kt_info;
    }

    public String getPrivacy_kt_info() {
        return privacy_kt_info;
    }

    public void setPrivacy_lgt_info(String privacy_lgt_info) {
        this.privacy_lgt_info = privacy_lgt_info;
    }

    public void setPrivacy_skt_info(String privacy_skt_info) {
        this.privacy_skt_info = privacy_skt_info;
    }

    public void setUnique_kt_info(String unique_kt_info) {
        this.unique_kt_info = unique_kt_info;
    }

    public void setUnique_lgt_info(String unique_lgt_info) {
        this.unique_lgt_info = unique_lgt_info;
    }

    public void setUnique_skt_info(String unique_skt_info) {
        this.unique_skt_info = unique_skt_info;
    }

    public void setTelecom_kt_info(String telecom_kt_info) {
        this.telecom_kt_info = telecom_kt_info;
    }

    public void setTelecom_lgt_info(String telecom_lgt_info) {
        this.telecom_lgt_info = telecom_lgt_info;
    }

    public void setTelecom_skt_info(String telecom_skt_info) {
        this.telecom_skt_info = telecom_skt_info;
    }

    public void setCommon_agreement_info(String common_agreement_info) {
        this.common_agreement_info = common_agreement_info;
    }

    public void setSafe_kt_info(String safe_kt_info) {
        this.safe_kt_info = safe_kt_info;
    }

    public void setSafe_lgt_info(String safe_lgt_info) {
        this.safe_lgt_info = safe_lgt_info;
    }

    public void setSafe_skt_info(String safe_skt_info) {
        this.safe_skt_info = safe_skt_info;
    }

    public void setSmart_otp_info(String smart_otp_info) {
        this.smart_otp_info = smart_otp_info;
    }

    public String getPrivacy_lgt_info() {
        return privacy_lgt_info;
    }

    public String getPrivacy_skt_info() {
        return privacy_skt_info;
    }

    public String getUnique_kt_info() {
        return unique_kt_info;
    }

    public String getUnique_lgt_info() {
        return unique_lgt_info;
    }

    public String getUnique_skt_info() {
        return unique_skt_info;
    }

    public String getTelecom_kt_info() {
        return telecom_kt_info;
    }

    public String getTelecom_lgt_info() {
        return telecom_lgt_info;
    }

    public String getTelecom_skt_info() {
        return telecom_skt_info;
    }

    public String getCommon_agreement_info() {
        return common_agreement_info;
    }

    public String getSafe_kt_info() {
        return safe_kt_info;
    }

    public String getSafe_lgt_info() {
        return safe_lgt_info;
    }

    public String getSafe_skt_info() {
        return safe_skt_info;
    }

    public String getSmart_otp_info() {
        return smart_otp_info;
    }
}
