package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class EventInfo {

    @JsonProperty("event")
    private String event;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("app-exe")
    private String app_exe;

    public EventInfo() {
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("event-notice:{");
        sb.append("event='").append(event).append('\'');
        sb.append(", msg='").append(msg).append('\'');
        sb.append(", app-exe='").append(app_exe).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
