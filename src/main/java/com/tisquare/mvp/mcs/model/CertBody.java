package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.entity.EventPopup;
import org.apache.http.HttpStatus;
import org.codehaus.jackson.annotate.JsonIgnore;


import java.net.InetAddress;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class CertBody {

    @JsonProperty("ptoken")
    private String ptoken;

    @JsonProperty("iuid")
    private Long iuid;

    @JsonProperty("auth-key")
    private String authKey;

    @JsonProperty("subs-type")
    private int subsType;

    @JsonProperty("servers")
    private List<ServerConnInfo> serverList;

    @JsonProperty("notice")
    private NoticeInfo noticeInfo;

    @JsonProperty("help")
    private HelpInfo helpInfo;

    @JsonProperty("event-notice1")
    private EventInfo eventInfo;

    @JsonProperty("app-update")
    private AppUpdatetInfo app_update;

    @JsonProperty("new-notice-cnt")
    private Integer newNoticeCnt;

    @JsonProperty("event-notice")
    private EventPopup eventPopup;

    @JsonProperty("last-notice-time")
    private String lastNoticeTime;

    @JsonProperty("last-nid")
    private Long lastNid;

    @JsonProperty("user-info")
    private SvcUserInfo svcUserInfo;

    @JsonProperty("bt-info")
    private List<BtInfo> bt_info;

    @JsonProperty("url-info")
    private UrlInfo urlInfo;

    @JsonProperty("profile")
    private Profile profile;

    @JsonIgnore
    private String Trace;

    public void setPtoken(String ptoken) {
        this.ptoken = ptoken;
    }

    public String getPtoken() {
        return ptoken;
    }

    public void setUrlInfo(UrlInfo urlInfo) {
        this.urlInfo = urlInfo;
    }

    public UrlInfo getUrlInfo() {
        return urlInfo;
    }

    public void setBt_info(List<BtInfo> bt_info) {

        this.bt_info = bt_info;
    }

    public List<BtInfo> getBt_info() {
        return bt_info;
    }

    public void setSvcUserInfo(SvcUserInfo svcUserInfo) {
        this.svcUserInfo = svcUserInfo;
    }

    public SvcUserInfo getSvcUserInfo() {
        return svcUserInfo;
    }

    public String getTrace() {
        return Trace;
    }

    public void setTrace(String trace) {
        Trace = trace;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Profile getProfile() {

        return profile;
    }

    public void setLastNid(Long lastNid) {
        this.lastNid = lastNid;
    }

    public Long getLastNid() {

        return lastNid;
    }

    public void setNewNoticeCnt(Integer newNoticeCnt) {
        this.newNoticeCnt = newNoticeCnt;
    }

    public Integer getNewNoticeCnt() {

        return newNoticeCnt;
    }

    public void setLastNoticeTime(String lastNoticeTime) {
        this.lastNoticeTime = lastNoticeTime;
    }

    public String getLastNoticeTime() {

        return lastNoticeTime;
    }

    public void setEventPopup(EventPopup eventPopup) {
        this.eventPopup = eventPopup;
    }

    public EventPopup getEventPopup() {

        return eventPopup;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setSubsType(Integer subsType) {
        this.subsType = subsType;
    }

    public void setServerList(List<ServerConnInfo> serverList) {
        this.serverList = serverList;
    }

    public void setNoticeInfo(NoticeInfo noticeInfo) {
        this.noticeInfo = noticeInfo;
    }

    public void setHelpInfo(HelpInfo helpInfo) {
        this.helpInfo = helpInfo;
    }

    public void setEventInfo(EventInfo eventInfo) {
        this.eventInfo = eventInfo;
    }

    public void setApp_update(AppUpdatetInfo app_update) {
        this.app_update = app_update;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getAuthKey() {
        return authKey;
    }

    public Integer getSubsType() {
        return subsType;
    }

    public List<ServerConnInfo> getServerList() {
        return serverList;
    }

    public NoticeInfo getNoticeInfo() {
        return noticeInfo;
    }

    public HelpInfo getHelpInfo() {
        return helpInfo;
    }

    public EventInfo getEventInfo() {
        return eventInfo;
    }

    public AppUpdatetInfo getApp_update() {
        return app_update;
    }

    public void setSubsType(int subsType) {
        this.subsType = subsType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("iuid=").append(iuid);
        sb.append(", authKey=").append(authKey);
        sb.append(", subsType=").append(subsType);
        sb.append(", serverList=").append(serverList);
        sb.append(", notice=").append(noticeInfo);
        sb.append(", help=").append(helpInfo);
        sb.append(", event-notice=").append(eventInfo);
        sb.append(", app-update=").append(app_update);
        sb.append("]");
        return sb.toString();
    }


    public void RequestToString( String proto, String url, String telecomCode, int httpStatus) {

            final StringBuilder sb = new StringBuilder();

            sb.append(proto + "|");
            sb.append("ACS" + "|");
            sb.append(url + "|");
            if (telecomCode.equals("45005"))
                sb.append("자사" + "|");
            else
                sb.append("타사" + "|");
            sb.append(this.profile.getIuid() + "|");
            sb.append(this.getAuthKey() + "|");
            sb.append(svcUserInfo.getVas_status() + "|");
            sb.append(svcUserInfo.getCall_status() + "|");
            sb.append(svcUserInfo.getVolte_status() + "|");
            sb.append(svcUserInfo.getProduct_info() + "|");
            sb.append(httpStatus);
            if (httpStatus == HttpStatus.SC_OK)
                sb.append("(OK)" + "|");
            else
                sb.append("(FAIL)" + "|");

            Trace = sb.toString();

    }


    public void CspToString( String proto, String url, MCSResponse mcsResponse ) {
        final StringBuilder sb = new StringBuilder("");

        if( mcsResponse != null) {
            sb.append(proto + "|");
            sb.append("VASGW" + "|");
            sb.append(url + "|");
            sb.append(mcsResponse.getReturnCode() + "|");
            sb.append(mcsResponse.getCustomerMgntNum() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerVolte() + "|");
            sb.append(mcsResponse.getCustomerMainProdId() + "|");
            sb.append("200(OK)" + "|");
        }
        else if(mcsResponse == null) {
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("FAIL" + "|");
        }
        this.Trace += sb.toString();
    }

//    public String toJsonString() throws Exception {
//        return Json.toStringJson(this);
//    }
}