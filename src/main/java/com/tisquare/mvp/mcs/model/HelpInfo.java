package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class HelpInfo {

    @JsonProperty("new-help-cnt")
    private int new_help_cnt;

    @JsonProperty("last-hid")
    private Long last_hid;


    @JsonProperty("info")
    private String info;

    public HelpInfo() {
    }

    public HelpInfo(Long last_hid, int new_help_cnt, String info)
    {
        this.new_help_cnt = new_help_cnt;
        this.last_hid = last_hid;
        this.info = info;
    }

    public int getNew_help_cnt() {
        return new_help_cnt;
    }

    public Long getLast_hid() {
        return last_hid;
    }

    public String getInfo() {
        return info;
    }

    public void setNew_help_cnt(int new_help_cnt) {
        this.new_help_cnt = new_help_cnt;
    }

    public void setLast_hid(Long last_hid) {
        this.last_hid = last_hid;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("help:{");
        sb.append("new-help-cnt='").append(new_help_cnt).append('\'');
        sb.append("last-hid='").append(last_hid).append('\'');
        sb.append(", info='").append(info).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
