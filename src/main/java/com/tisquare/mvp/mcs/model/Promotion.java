package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.utils.Json;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Promotion {

    @JsonProperty("msg-id")
    private Long msg_id;

    @JsonProperty("new-start-date")
    private String new_start_date;

    @JsonProperty("new-end-date")
    private String new_end_date;

    @JsonProperty("start-date")
    private String start_date;

    @JsonProperty("end-date")
    private String end_date;

    @JsonProperty("img-url")
    private String img_url;

    @JsonProperty("target-url")
    private String target_url;

    @JsonProperty("title")
    private String title;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("promotion-type")
    private String promotion_type;

    @JsonProperty("landing-type")
    private String landing_type;

    @JsonProperty("landing-menu")
    private String landing_menu;

    @JsonProperty("landing-data")
    private String landing_data;

    public void setPromotion_type(String promotion_type) {
        this.promotion_type = promotion_type;
    }

    public void setLanding_type(String landing_type) {
        this.landing_type = landing_type;
    }

    public void setLanding_menu(String landing_menu) {
        this.landing_menu = landing_menu;
    }

    public void setLanding_data(String landing_data) {
        this.landing_data = landing_data;
    }

    public String getPromotion_type() {
        return promotion_type;
    }

    public String getLanding_type() {
        return landing_type;
    }

    public String getLanding_menu() {
        return landing_menu;
    }

    public String getLanding_data() {
        return landing_data;
    }

    public void setNew_start_date(String new_start_date) {
        this.new_start_date = new_start_date;
    }

    public void setNew_end_date(String new_end_date) {
        this.new_end_date = new_end_date;
    }

    public String getNew_start_date() {
        return new_start_date;
    }

    public String getNew_end_date() {
        return new_end_date;
    }

    public void setMsg_id(Long msg_id) {
        this.msg_id = msg_id;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public void setTarget_url(String target_url) {
        this.target_url = target_url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getMsg_id() {
        return msg_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getTarget_url() {
        return target_url;
    }

    public String getTitle() {
        return title;
    }

    public String getMsg() {
        return msg;
    }
}