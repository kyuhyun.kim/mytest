package com.tisquare.mvp.mcs.model;

/**
 * 요청 시 넘어오는 헤더
 *
 * @author leejs
 * @since 2015-03-05
 */
public class ClientAuthReqHeaders extends McsReqHeaders {

    public ClientAuthReqHeaders(Long iuid, String phoneNo, String appVersion, String authKey, Integer storeType, Integer subs_type) {

        super(iuid, phoneNo, authKey, appVersion, storeType, subs_type);
    }


    public ClientAuthReqHeaders(Long iuid, String phoneNo, String appVersion, String authKey, Integer storeType) {
        super(iuid, phoneNo, authKey, appVersion, storeType);
    }

}
