package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class MsgInfoResult {

    @JsonProperty("cause")
    private String cause;

    @JsonProperty("value")
    private int value;

    public void setCause(String cause) {
        this.cause = cause;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getCause() {
        return cause;
    }


    @Override
    public String toString() {
        if(this == null)
            return null;
        final StringBuilder sb = new StringBuilder("MsgInfoResult:{");
        sb.append("cause='").append(this.cause).append('\'');
        sb.append(", value='").append(this.value).append('\'');
        sb.append('}');
        return sb.toString();
    }
}