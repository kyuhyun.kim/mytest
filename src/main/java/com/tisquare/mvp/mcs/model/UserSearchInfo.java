package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UserSearchInfo {

    @JsonProperty("user-info")
    private UserSearch userSearch;

    @JsonProperty("user-list")
    private List<UserList> userLists;
/*
    @JsonProperty("user-list-mdn")
    private List<UserList> userListListMdn;*/

    public void setUserSearch(UserSearch userSearch) {
        this.userSearch = userSearch;
    }

    public void setUserLists(List<UserList> userLists) {
        this.userLists = userLists;
    }

    public UserSearch getUserSearch() {
        return userSearch;
    }

    public List<UserList> getUserLists() {
        return userLists;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }

}