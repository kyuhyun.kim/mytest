package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.*;
import com.tisquare.mvp.mcs.entity.EventPopup;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class PromotionList {


    @JsonProperty("promotion-info")
    private List<EventNotice> eventNotices;

    @JsonProperty("event-notice")
    private EventPopup eventPopup;

    @JsonProperty("url-info")
    private UrlInfo urlInfo;

    @JsonProperty("event-notice-list")
    private List<EventPopup> eventPopupList;

    public void setEventPopupList(List<EventPopup> eventPopupList) {
        this.eventPopupList = eventPopupList;
    }

    public List<EventPopup> getEventPopupList() {
        return eventPopupList;
    }

    public void setUrlInfo(UrlInfo urlInfo) {
        this.urlInfo = urlInfo;
    }

    public UrlInfo getUrlInfo() {
        return urlInfo;
    }

    public void setEventPopup(EventPopup eventPopup) {
        this.eventPopup = eventPopup;
    }

    public EventPopup getEventPopup() {
        return eventPopup;
    }

    public void setEventNotices(List<EventNotice> eventNotices) {
        this.eventNotices = eventNotices;
    }

    public List<EventNotice> getEventNotices() {
        return eventNotices;
    }
}