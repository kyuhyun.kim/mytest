package com.tisquare.mvp.mcs.model.pc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class NsSendSmsInfo {

    @JsonProperty("dstuser")
    public List<String> dstuser;

    @JsonProperty("msgtype")
    public String msgtype;

    @JsonProperty("callback")
    public String callback;

    @JsonProperty("subject")
    public String subject;

    @JsonProperty("text")
    public String text;

    public NsSendSmsInfo()
    {

    }

    public NsSendSmsInfo(PcAuthInfo pcAuthInfo)
    {
        dstuser = new ArrayList<>();
        dstuser.add(pcAuthInfo.getMdn());
        this.msgtype = "1";
        this.text = "[더 캠프] 아이디는 ["+pcAuthInfo.getUser_id()+"] 입니다.";
        this.callback = "0263945025";
    }

    public void setDstuser(List<String> dstuser) {
        this.dstuser = dstuser;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getDstuser() {
        return dstuser;
    }


    public String getCallback() {
        return callback;
    }

    public String getSubject() {
        return subject;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }



}