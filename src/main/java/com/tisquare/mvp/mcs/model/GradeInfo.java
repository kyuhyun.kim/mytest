package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class GradeInfo {

    @JsonProperty("grade")
    private String grade;

    @JsonProperty("grade-name")
    private String grade_name;

    @JsonProperty("unit-code")
    private String unit_code;

    @JsonProperty("description")
    private String description;

    @JsonProperty("group-id")
    private String group_id;

    @JsonProperty("status")
    private Integer status;

    @JsonProperty("mem-grade-info")
    private List<GradeInfo> mem_grade_infos;

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGroup_id() {
        return group_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setMem_grade_infos(List<GradeInfo> mem_grade_infos) {
        this.mem_grade_infos = mem_grade_infos;
    }

    public List<GradeInfo> getMem_grade_infos() {
        return mem_grade_infos;
    }

    public void setGrade_name(String grade_name) {
        this.grade_name = grade_name;
    }

    public String getGrade_name() {
        return grade_name;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public void setUnit_code(String unit_code) {
        this.unit_code = unit_code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGrade() {
        return grade;
    }

    public String getUnit_code() {
        return unit_code;
    }

    public String getDescription() {
        return description;
    }
}