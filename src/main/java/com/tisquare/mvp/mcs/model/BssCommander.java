package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BssCommander {

    private String cp_code;
    private String emp_id;
    private String commander_id;
    private String commander_pwd;
    private String cauth_key;
    private long iuid;

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public void setCommander_id(String commander_id) {
        this.commander_id = commander_id;
    }

    public void setCommander_pwd(String commander_pwd) {
        this.commander_pwd = commander_pwd;
    }

    public void setCauth_key(String cauth_key) {
        this.cauth_key = cauth_key;
    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public String getCp_code() {
        return cp_code;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public String getCommander_id() {
        return commander_id;
    }

    public String getCommander_pwd() {
        return commander_pwd;
    }

    public String getCauth_key() {
        return cauth_key;
    }

    public long getIuid() {
        return iuid;
    }
}