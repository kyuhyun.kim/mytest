package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Bssinfo {

    @JsonProperty("bss-id")
    private String bss_id;

    @JsonProperty("bss-pwd")
    private String bss_pwd;

    @JsonProperty("cp-code")
    private String cp_code;

    @JsonProperty("mdn")
    private String mdn;

    @JsonProperty("new-bss-pwd")
    private String new_bss_pwd;

    private long iuid;

    public void setNew_bss_pwd(String new_bss_pwd) {
        this.new_bss_pwd = new_bss_pwd;
    }

    public String getNew_bss_pwd() {
        return new_bss_pwd;
    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public long getIuid() {
        return iuid;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getMdn() {
        return mdn;
    }

    public void setBss_id(String bss_id) {
        this.bss_id = bss_id;
    }

    public void setBss_pwd(String bss_pwd) {
        this.bss_pwd = bss_pwd;
    }

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public String getBss_id() {
        return bss_id;
    }

    public String getBss_pwd() {
        return bss_pwd;
    }

    public String getCp_code() {
        return cp_code;
    }



    @Override
    public String toString() {
        if(this == null)
            return null;
        final StringBuilder sb = new StringBuilder("Bssinfo:{");
        sb.append("bss-id='").append(this.bss_id).append('\'');
        sb.append("bss-pwd='").append(this.bss_pwd).append('\'');
        sb.append(", cp-code='").append(this.cp_code).append('\'');
        sb.append('}');
        return sb.toString();
    }
}