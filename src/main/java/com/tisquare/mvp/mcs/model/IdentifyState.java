package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class IdentifyState {

    @JsonProperty("res-code")
    private int res_code;

    @JsonProperty("res-msg")
    private String res_msg;


    @JsonProperty("name")
    private String name;

    @JsonProperty("birth")
    private String birth;

    @JsonProperty("mobile-co")
    private String mobile_co;

    @JsonProperty("mdn")
    private String mdn;

    @JsonProperty("gender")
    private int gender;

    @JsonProperty("native")
    private int native_info;

    @JsonIgnore
    private Long iuid;

    public void setRes_code(int res_code) {
        this.res_code = res_code;
    }

    public void setRes_msg(String res_msg) {
        this.res_msg = res_msg;
    }

    public int getRes_code() {
        return res_code;
    }

    public String getRes_msg() {
        return res_msg;
    }

    public void setNative_info(int native_info) {
        this.native_info = native_info;
    }

    public int getNative_info() {
        return native_info;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setMobile_co(String mobile_co) {
        this.mobile_co = mobile_co;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public String getName() {
        return name;
    }

    public String getMobile_co() {
        return mobile_co;
    }

    public String getMdn() {
        return mdn;
    }

    public int getGender() {
        return gender;
    }

    public Long getIuid() {
        return iuid;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getBirth() {
        return birth;
    }
}