package com.tisquare.mvp.mcs.model;

/**
 * 가입 요청 시 넘어오는 쿼리 파라미터.
 *
 * @author leejs
 * @since 2015-03-05
 */
public class ClientAuthReqParams {

    public static final String SKT ="45005";
    public static final String KT ="45005";
    public static final String LGT ="45005";

    private String osType;
    private String osVersion;
    private String deviceId;
    private String telecomCode;
    private String telecomStr;
    private String netType;
    private Long nid;
    private Long hid;
    private Long sid;
    private String deviceModel;
    private Integer deviceWidth;
    private Integer deviceHeight;

    public ClientAuthReqParams() {
    }

    public ClientAuthReqParams(String osType, String osVersion, String deviceId, String telecomCode, String telecomStr, String netType, Long nid, Long hid, Long sid, String deviceModel) {
        this();
        this.osType = osType;
        this.osVersion = osVersion;
        this.deviceId = deviceId;
        this.telecomCode = telecomCode;
        this.telecomStr = telecomStr;
        this.netType = netType;
        this.nid = nid;
        this.hid = hid;
        this.sid = sid;
        this.deviceModel = deviceModel;

    }



    public ClientAuthReqParams(String osType, String osVersion, String deviceId, String telecomCode, String telecomStr, String netType, Long nid, Long hid,  String deviceModel) {
        this();
        this.osType = osType;
        this.osVersion = osVersion;
        this.deviceId = deviceId;
        this.telecomCode = telecomCode;
        this.telecomStr = telecomStr;
        this.netType = netType;
        this.nid = nid;
        this.hid = hid;
        this.deviceModel = deviceModel;

    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getSid() {
        return sid;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }

    public String getTelecomStr() {
        return telecomStr;
    }

    public void setTelecomStr(String telecomStr) {
        this.telecomStr = telecomStr;
    }

    public String getNetType() {
        return netType;
    }

    public void setNetType(String netType) {
        this.netType = netType;
    }

    public Long getNid() {
        return nid;
    }

    public void setNid(Long nid) {
        this.nid = nid;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public Integer getDeviceWidth() {
        return deviceWidth;
    }

    public void setDeviceWidth(Integer deviceWidth) {
        this.deviceWidth = deviceWidth;
    }

    public Integer getDeviceHeight() {
        return deviceHeight;
    }

    public void setDeviceHeight(Integer deviceHeight) {
        this.deviceHeight = deviceHeight;
    }

    public Long getHid() {

        return hid;
    }

    public void setHid(Long hid) {
        this.hid = hid;
    }

    /**
     * 요청 시 넘어온 정보로 UserDevice 객체를 생성한다.
     *
     * @return UserDevice
     */
    public UserDevice toUserDevice() {
        return new UserDevice(this.osType, this.osVersion, this.deviceId, this.telecomCode, this.telecomStr, this.netType, null);
    }
}
