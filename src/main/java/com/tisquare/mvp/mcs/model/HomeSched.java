package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class HomeSched {

    @JsonProperty("home-sched-info")
    private List<HomeSchedInfo> homeSchedInfoList;

    public HomeSched()
    {

    }

    public HomeSched(List<HomeSchedInfo> homeSchedInfoList)
    {
        this.homeSchedInfoList = homeSchedInfoList;
    }
    public void setHomeSchedInfoList(List<HomeSchedInfo> homeSchedInfoList) {
        this.homeSchedInfoList = homeSchedInfoList;
    }

    public List<HomeSchedInfo> getHomeSchedInfoList() {
        return homeSchedInfoList;
    }
}