package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Notice {

    @JsonProperty("new-notice-cnt")
    private Integer new_notice_cnt;

    @JsonProperty("last-notice-time")
    private String last_notice_time;

    @JsonProperty("last-nid")
    private long last_nid;


    public Notice() {
    }

    public void setNew_notice_cnt(Integer new_notice_cnt) {
        this.new_notice_cnt = new_notice_cnt;
    }

    public void setLast_notice_time(String last_notice_time) {
        this.last_notice_time = last_notice_time;
    }

    public void setLast_nid(long last_nid) {
        this.last_nid = last_nid;
    }

    public Integer getNew_notice_cnt() {

        return new_notice_cnt;
    }

    public String getLast_notice_time() {
        return last_notice_time;
    }

    public long getLast_nid() {
        return last_nid;
    }

    @Override

    public String toString() {
        final StringBuilder sb = new StringBuilder("notice:{");
        sb.append("new-notice-cnt='").append(new_notice_cnt).append('\'');
        sb.append(", last-notice-time='").append(last_notice_time).append('\'');
        sb.append(", last-nid=").append(last_nid);
        sb.append('}');
        return sb.toString();
    }
}
