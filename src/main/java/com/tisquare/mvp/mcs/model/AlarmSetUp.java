package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AlarmSetUp {

    @JsonProperty("type")
    private Integer type;

    @JsonProperty("value")
    private Integer value;

    public void setType(Integer type) {
        this.type = type;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getType() {
        return type;
    }

    public Integer getValue() {
        return value;
    }
}