package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.utils.Json;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Profile {

    @JsonProperty("iuid")
    private Long iuid;
    
    @JsonProperty("nickname")
    private String nickname;

    @JsonProperty("status-message")
    private String statusMessage;

    @JsonProperty("main-picture-picno")
    private int mainPicturePicNo;

    @JsonProperty("default-img-num")
    private int default_img_num;

    @JsonProperty("default-color-num")
    private int default_color_num;

    @JsonProperty("profile-tag")
    private String profile_tag;

    @JsonProperty("nick-count")
    private int nick_count;

    @JsonProperty("main-channel-alarm")
    private int main_channel_alarm;

    @JsonProperty("sub-channel-alarm")
    private int sub_channel_alarm;

    @JsonProperty("pic-type")
    private int pic_type;

    @JsonProperty("image")
    private String image;

    @JsonProperty("thumbnail")
    private String thumbnail;

    @JsonProperty("mime-type")
    private String mime_type;

    @JsonProperty("call-number")
    private Integer call_number;

    public void setCall_number(Integer call_number) {
        this.call_number = call_number;
    }

    public Integer getCall_number() {
        return call_number;
    }

    public void setNick_count(int nick_count) {
        this.nick_count = nick_count;
    }

    public void setMain_channel_alarm(int main_channel_alarm) {
        this.main_channel_alarm = main_channel_alarm;
    }

    public void setSub_channel_alarm(int sub_channel_alarm) {
        this.sub_channel_alarm = sub_channel_alarm;
    }

    public void setPic_type(int pic_type) {
        this.pic_type = pic_type;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public int getNick_count() {
        return nick_count;
    }

    public int getMain_channel_alarm() {
        return main_channel_alarm;
    }

    public int getSub_channel_alarm() {
        return sub_channel_alarm;
    }

    public int getPic_type() {
        return pic_type;
    }

    public String getImage() {
        return image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setProfile_tag(String profile_tag) {
        this.profile_tag = profile_tag;
    }

    public String getProfile_tag() {
        return profile_tag;
    }

    public Integer getDefault_color_num() {
        return default_color_num;
    }

    public void setDefault_color_num(Integer default_color_num) {
        this.default_color_num = default_color_num;
    }

    public Integer getDefault_img_num() {
        return default_img_num;
    }

    public void setDefault_img_num(Integer default_img_num) {
        this.default_img_num = default_img_num;
    }


    public Long getIuid() {
        return iuid;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Integer getMainPicturePicNo() {
        return mainPicturePicNo;
    }

    public void setMainPicturePicNo(Integer mainPicturePicNo) {
        this.mainPicturePicNo = mainPicturePicNo;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("iuid=").append(iuid);
        sb.append(", nickname='").append(nickname).append('\'');
        sb.append(", statusMessage='").append(statusMessage).append('\'');
        sb.append(", mainPicturePicNo=").append(mainPicturePicNo);
        sb.append("]");
        return sb.toString();
    }

    public String toJsonString() throws Exception {
        return Json.toStringJson(this);
    }



    /**
     * DB 업데이트 시 프로필에 포함되지 말아야할 항목을 제거한다.
     */
    public void doExcludeField() {
        this.nickname = null;
        this.statusMessage = null;
    }
}