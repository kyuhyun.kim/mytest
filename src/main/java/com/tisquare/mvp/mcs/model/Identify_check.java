package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by jhkim on 14. 11. 10.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Identify_check {

    @JsonProperty("unique-key")
    String unique_key;

    @JsonProperty("unique-no")
    String unique_no;

    @JsonProperty("sms-no")
    String sms_no;

    @JsonProperty("sub-key")
    String sub_key;

    public void setSub_key(String sub_key) {
        this.sub_key = sub_key;
    }

    public String getSub_key() {
        return sub_key;
    }

    public void setUnique_key(String unique_key) {
        this.unique_key = unique_key;
    }

    public void setUnique_no(String unique_no) {
        this.unique_no = unique_no;
    }

    public void setSms_no(String sms_no) {
        this.sms_no = sms_no;
    }

    public String getUnique_key() {
        return unique_key;
    }

    public String getUnique_no() {
        return unique_no;
    }

    public String getSms_no() {
        return sms_no;
    }

    @Override
    public String toString() {
        return "Identify{" +
                "unique-key='" + unique_key + '\'' +
                ", unique-no='" + unique_no + '\'' +
                ", sms-no='" + sms_no + '\'' +
                ", sub-key='" + sub_key + '\'' +
                '}';
    }
}
