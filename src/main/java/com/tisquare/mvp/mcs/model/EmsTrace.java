package com.tisquare.mvp.mcs.model;



import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.net.InetAddress;

public class EmsTrace {

    private String hostName;
    private String svcKey;
    private String code;
    private String time;

    private String phone_no;
    private String checksum;
    private String market;
    private String osType;
    private String version;
    private String device_id;
    private String telecomCode;
    private String model;
    private String commandType;
    private String success_yn;
    private String result_code;
    private String result_msg;
    private long elapse_time;

    private String tuid;
    private String suid;

    private Long iuid;
    private String cAuthKey;
    private String coupon_no;
    private String module;



    private long sid;



    public EmsTrace(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HttpStatus httpStatus, String Url, long elapse_time,  Bssinfo bssinfo, InitConfig initConfig)
    {
        try {

            this.hostName = InetAddress.getLocalHost().getHostName().toString();
            this.time =  DateUtil.toFormatString(System.currentTimeMillis(), "yyyyMMddHHmmss");
            this.phone_no = authReqHeaders.getPhoneNo();
            this.version = authReqHeaders.getAppVersion();
            this.market = String.valueOf( authReqHeaders.getStoreType() );
            this.osType = authReqParams.getOsType();
            this.telecomCode = authReqParams.getTelecomCode();
            this.device_id = authReqParams.getDeviceId();
            this.model = authReqParams.getDeviceModel();
            this.iuid = authReqHeaders.getIuid();
            this.cAuthKey = authReqHeaders.getcAuthKey();

            if (StringUtils.containsIgnoreCase(Url, "mvpinfo")) {
                this.code = "001";
            }
            else if (StringUtils.containsIgnoreCase(Url, "authuac")) {
                this.code = "002";
            }
            else if(StringUtils.containsIgnoreCase(Url, "authbss")) {

                this.tuid = bssinfo.getCp_code();
                this.suid = bssinfo.getBss_id();
                this.code = "003";
            }
            this.commandType = Url;
            this.svcKey = initConfig.getEms_log_svc_svrkey();
            this.module = initConfig.getProcess_name();
            this.result_code = httpStatus.toString();
            this.success_yn = "N";
            this.elapse_time = elapse_time;

            if (this.result_code.equals("200")) {
                this.success_yn = "Y";
                this.result_msg = "성공";
            } else if (this.result_code.equals("400")) {
                this.result_msg = "요청 메시지 오류";
            } else if (this.result_code.equals("403")) {
                this.result_msg = "AUTH-KEY 오류";
            } else if (this.result_code.equals("406")) {
                this.result_msg = "APP 강제 업데이트";
            } else if (this.result_code.equals("500")) {
                this.result_msg = "서버 오류";
            } else if (this.result_code.equals("503")) {
                this.result_msg = "시스템 작업";
            } else {
                this.result_msg = "인증 실패";
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public EmsTrace(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HttpStatus httpStatus, String Url, long elapse_time,  UsInfo usInfo, InitConfig initConfig)
    {
        try {

            this.hostName = InetAddress.getLocalHost().getHostName().toString();
            this.time =  DateUtil.toFormatString(System.currentTimeMillis(), "yyyyMMddHHmmss");
            this.phone_no = authReqHeaders.getPhoneNo();
            this.version = authReqHeaders.getAppVersion();
            this.market = String.valueOf( authReqHeaders.getStoreType() );
            this.osType = authReqParams.getOsType();
            this.telecomCode = authReqParams.getTelecomCode();
            this.device_id = authReqParams.getDeviceId();
            this.model = authReqParams.getDeviceModel();
            this.iuid = authReqHeaders.getIuid();
            this.cAuthKey = authReqHeaders.getcAuthKey();

            if (StringUtils.containsIgnoreCase(Url, "mvpinfo")) {
                this.code = "001";
            }
            else if (StringUtils.containsIgnoreCase(Url, "authuac")) {
                this.code = "002";
            }
            else if(StringUtils.containsIgnoreCase(Url, "authbss")) {

                this.suid = usInfo.getUser_id();
                this.code = "003";
            }
            this.commandType = Url;
            this.svcKey = initConfig.getEms_log_svc_svrkey();
            this.module = initConfig.getProcess_name();
            this.result_code = httpStatus.toString();
            this.success_yn = "N";
            this.elapse_time = elapse_time;

            if (this.result_code.equals("200")) {
                this.success_yn = "Y";
                this.result_msg = "성공";
            } else if (this.result_code.equals("400")) {
                this.result_msg = "요청 메시지 오류";
            } else if (this.result_code.equals("403")) {
                this.result_msg = "AUTH-KEY 오류";
            } else if (this.result_code.equals("406")) {
                this.result_msg = "APP 강제 업데이트";
            } else if (this.result_code.equals("500")) {
                this.result_msg = "서버 오류";
            } else if (this.result_code.equals("503")) {
                this.result_msg = "시스템 작업";
            } else {
                this.result_msg = "인증 실패";
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setTuid(String tuid) {
        this.tuid = tuid;
    }

    public void setSuid(String suid) {
        this.suid = suid;
    }

    public String getTuid() {
        return tuid;
    }

    public String getSuid() {
        return suid;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getMarket() {
        return market;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public String getModel() {
        return model;
    }

    public String getCode() {
        return code;
    }

    public String getSvcKey() {
        return svcKey;
    }

    public String getHostName() {
        return hostName;
    }

    public long getSid() {
        return sid;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setcAuthKey(String cAuthKey) {
        this.cAuthKey = cAuthKey;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }

    public void setCoupon_no(String coupon_no) {
        this.coupon_no = coupon_no;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public void setSuccess_yn(String success_yn) {
        this.success_yn = success_yn;
    }

    public void setCommandType(String commandType) {
        this.commandType = commandType;
    }

    public void setElapse_time(long elapse_time) {
        this.elapse_time = elapse_time;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getVersion() {
        return version;
    }

    public String getcAuthKey() {
        return cAuthKey;
    }

    public String getOsType() {
        return osType;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public String getCoupon_no() {
        return coupon_no;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public String getSuccess_yn() {
        return success_yn;
    }

    public String getCommandType() {
        return commandType;
    }

    public long getElapse_time() {
        return elapse_time;
    }

    public void setSvcKey(String svcKey) {
        this.svcKey = svcKey;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(this.hostName + "|");
        sb.append(this.module + "|");
        sb.append(this.svcKey + "|");
        sb.append(this.code + "|");
        sb.append(this.time + "|");
        sb.append("|");
        sb.append("|");
        sb.append(this.phone_no + "|");
        if(StringUtils.equals(this.code, "001"))
        {
            sb.append("|");
        }
        else if( StringUtils.equals(this.code, "002") )
        {
            sb.append(this.iuid +  "|");
            sb.append(this.cAuthKey +  "|");
            sb.append(this.checksum +  "|");
        }
        else if( StringUtils.equals(this.code, "003") )
        {
            sb.append(this.tuid +  "|");
            sb.append(this.suid +  "|");
            sb.append(this.iuid +  "|");
            sb.append(this.cAuthKey +  "|");
            sb.append(this.checksum +  "|");
        }

        sb.append(this.market + "|");
        sb.append(this.osType + "|");
        sb.append(this.version + "|");
        sb.append(this.device_id + "|");
        sb.append(this.telecomCode + "|");
        sb.append(this.model + "|");
        sb.append(this.commandType + "|");
        sb.append(this.success_yn + "|");
        sb.append(this.result_code + "|");
        sb.append(this.result_msg + "|");
        sb.append(elapse_time);

        return sb.toString();
    }




}

