package com.tisquare.mvp.mcs.model;


//{
//        "mdn":"+821012345678",
//        "statistic":[
//        {
//        "svctype":1,
//        "timestamp":1234,
//        "count":1
//        },
//        {
//        "svctype":1,
//        "timestamp":1234,
//        "count":1
//        }
//        ]
//}

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jhkim on 14. 2. 25.
 */
public class Statistic {


    @JsonProperty("service_type")
    String service_type;

    @JsonProperty("service_sub_type")
    String service_sub_type;

    @JsonProperty("share_type")
    String share_type;

    @JsonProperty("sender")
    String sender;

    @JsonProperty("recver")
    String recver;

    @JsonProperty("start_path")
    String start_path;

    @JsonProperty("call_state")
    String call_state;

    @JsonProperty("start_time")
    long start_time;

    @JsonProperty("end_time")
    long end_time;

    @JsonProperty("request_time")
    long request_time;

    @JsonProperty("file_size")
    long file_size;

    @JsonProperty("file_count")
    int file_count;

    Boolean isfind = Boolean.FALSE;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\"statistic\":[{\n");
        sb.append("\"service_type\":\""+        getService_type() + "\"\n");
        sb.append("\"service_sub_type\":\""+    getService_sub_type() + "\"\n");
        sb.append("\"share_type\":\""+          getShare_type() + "\"\n");
        sb.append("\"sender\":\""+              getSender() + "\"\n");
        sb.append("\"recver\":\""+              getRecver() + "\"\n");
        sb.append("\"start_path\":\""+          getStart_path() + "\"\n");
        sb.append("\"call_state\":\""+          getCall_state() + "\"\n");
        sb.append("\"start_time\":\""+          getStart_time() + "\"\n");
        sb.append("\"end_time\":\""+            getEnd_time() + "\"\n");
        sb.append("\"request_time\":\""+        getRequest_time() + "\"\n");
        sb.append("\"file_size\":\""+           getFile_size() + "\"\n");
        sb.append("\"file_count\":\""+          getFile_count() + "\"\n");
        sb.append("]}\n");
        return sb.toString();
    }

    public void setIsfind(Boolean isfind) {
        this.isfind = isfind;
    }

    public String getService_type() {
        return service_type;
    }

    public Boolean getIsfind() {
        return isfind;
    }

    public void setFile_size(long file_size) {
        this.file_size = file_size;
    }

    public void setFile_count(int file_count) {
        this.file_count = file_count;
    }

    public long getFile_size() {

        return file_size;
    }

    public int getFile_count() {
        return file_count;
    }

    public void setService_sub_type(String service_sub_type) {
        this.service_sub_type = service_sub_type;
    }

    public void setShare_type(String share_type) {
        this.share_type = share_type;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setRecver(String recver) {
        this.recver = recver;
    }

    public void setStart_path(String start_path) {
        this.start_path = start_path;
    }

    public void setCall_state(String call_state) {
        this.call_state = call_state;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public void setRequest_time(long request_time) {
        this.request_time = request_time;
    }

    public String getservice_type() {
        return service_type;
    }

    public String getService_sub_type() {
        return service_sub_type;
    }

    public String getShare_type() {
        return share_type;
    }

    public String getSender() {
        return sender;
    }

    public String getRecver() {
        return recver;
    }

    public String getStart_path() {
        return start_path;
    }

    public String getCall_state() {
        return call_state;
    }

    public long getStart_time() {
        return start_time;
    }

    public long getEnd_time() {
        return end_time;
    }

    public long getRequest_time() {
        return request_time;
    }
}
