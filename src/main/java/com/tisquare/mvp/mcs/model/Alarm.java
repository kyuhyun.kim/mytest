package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Alarm {

    @JsonProperty("alarm")
    private List<AlarmSetUp> alarmSetUps;


    public void setAlarmSetUps(List<AlarmSetUp> alarmSetUps) {
        this.alarmSetUps = alarmSetUps;
    }

    public List<AlarmSetUp> getAlarmSetUps() {
        return alarmSetUps;
    }
}