package com.tisquare.mvp.mcs.model;



import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.springframework.http.HttpStatus;

public class EmsStat {


    private String phone_no;
    private Long iuid;
    private String version;
    private String cAuthKey;
    private String osType;
    private String telecomCode;
    private String model;
    private String coupon_no;
    private String result_code;
    private String result_msg;
    private String success_yn;
    private String commandType;
    private long elapse_time;
    private String code;
    private String svcKey;
    private String hostName;
    private long sid;


    public EmsStat(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, CertBody certBody, HttpStatus httpStatus, InitConfig initConfig)
    {
        this.phone_no = authReqHeaders.getPhoneNo();
        this.version = authReqHeaders.getAppVersion();
        this.osType = authReqParams.getOsType();
        this.telecomCode = authReqParams.getTelecomCode();
        this.model = authReqParams.getDeviceModel();
        this.code = "001";
        this.commandType = "authauc";
        this.svcKey = initConfig.getProcess_name();

        this.result_code = httpStatus.toString();
        this.success_yn = "N";
        if(this.result_code.equals("200"))
        {
            this.success_yn = "Y";
            this.result_msg = "성공";
        }
        else if(this.result_code.equals("400"))
        {
            this.result_msg = "요청 메시지 오류";
        }
        else if(this.result_code.equals("403"))
        {
            this.result_msg = "AUTH-KEY 오류";
        }
        else if(this.result_code.equals("406"))
        {
            this.result_msg = "APP 강제 업데이트";
        }
        else if(this.result_code.equals("500"))
        {
            this.result_msg = "서버 오류";
        }
        else if(this.result_code.equals("503"))
        {
            this.result_msg = "시스템 작업";
        }
        else
        {
            this.result_msg = "인증 실패";
        }
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public String getModel() {
        return model;
    }

    public String getCode() {
        return code;
    }

    public String getSvcKey() {
        return svcKey;
    }

    public String getHostName() {
        return hostName;
    }

    public long getSid() {
        return sid;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setcAuthKey(String cAuthKey) {
        this.cAuthKey = cAuthKey;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }

    public void setCoupon_no(String coupon_no) {
        this.coupon_no = coupon_no;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public void setSuccess_yn(String success_yn) {
        this.success_yn = success_yn;
    }

    public void setCommandType(String commandType) {
        this.commandType = commandType;
    }

    public void setElapse_time(long elapse_time) {
        this.elapse_time = elapse_time;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getVersion() {
        return version;
    }

    public String getcAuthKey() {
        return cAuthKey;
    }

    public String getOsType() {
        return osType;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public String getCoupon_no() {
        return coupon_no;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public String getSuccess_yn() {
        return success_yn;
    }

    public String getCommandType() {
        return commandType;
    }

    public long getElapse_time() {
        return elapse_time;
    }

    public void setSvcKey(String svcKey) {
        this.svcKey = svcKey;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();


        sb.append( this.hostName + "|");
        sb.append( this.svcKey + "|");
        sb.append( this.code + "|");
        sb.append( DateUtil.toFormatString(System.currentTimeMillis(), "yyyyMMddHHmmss") + "|");
        sb.append( DateUtil.toFormatString(System.currentTimeMillis(), "yyyyMMddHHmmss") + this.code + this.svcKey + "|");
        sb.append( this.result_code +"|");
        sb.append( this.result_msg +"|");
        sb.append( this.phone_no + "|");
        sb.append( this.osType + "|");
        sb.append( this.version +"|");
        sb.append( this.telecomCode +"|");
        sb.append( this.model +"|");
        sb.append( this.commandType +"|");
        sb.append( this.success_yn+"|");
        sb.append( this.result_code +"|");
        sb.append( this.result_msg +"|");
        sb.append(elapse_time);

        return sb.toString();
    }




}

