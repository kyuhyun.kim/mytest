package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ConnectionInfo {

    @JsonProperty("protocol")
    private String protocol;

    @JsonProperty("ip")
    private String ipAddr;

    @JsonProperty("port")
    private Integer port;

    @JsonProperty("url")
    private String url;

    @JsonProperty("tag")
    private String tag;

    public ConnectionInfo() {
    }

    public ConnectionInfo(String protocol, String url, String tag) {
        this();
        this.protocol = protocol;
        this.url = url;
        this.tag = tag;
    }

    public ConnectionInfo(String protocol, String ipAddr, Integer port, String tag) {
        this();
        this.protocol = protocol;
        this.ipAddr = ipAddr;
        this.port = port;
        this.tag = tag;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ConnectionInfo{");
        sb.append("protocol='").append(protocol).append('\'');
        sb.append(", ipAddr='").append(ipAddr).append('\'');
        sb.append(", port=").append(port);
        sb.append(", url='").append(url).append('\'');
        sb.append(", tag='").append(tag).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
