package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.MasTrainee;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProfileDetail {

    @JsonIgnore
    private Long iuid;

    @JsonProperty("type")
    private int type;

    @JsonProperty("birth")
    private Long birth;

    @JsonProperty("gender")
    private Integer gender;

    @JsonProperty("name")
    private String name;

    @JsonProperty("service-num")
    private String service_num;

    @JsonProperty("enter-date")
    private Long enter_date;

    @JsonProperty("regiment")
    private String regiment;

    @JsonProperty("rank")
    private Integer rank;

    @JsonProperty("service")
    private Long service;

    @JsonProperty("group")
    private Long grp;

    @JsonProperty("upgrade-date")
    private Long upgrade_date;

    @JsonProperty("sm-id")
    private String sm_id;

    @JsonProperty("is-yn")
    private Integer is_yn;

    @JsonIgnore
    private String phone_no;

    public ProfileDetail()
    {

    }

    public ProfileDetail(Integer type)
    {
        this.type = type;
    }

    public void setMasTrainee(MasTrainee masTrainee)
    {
        this.enter_date = Long.valueOf(masTrainee.getTr_entrance_day());
        masTrainee.setCurr_date(System.currentTimeMillis());
        if (masTrainee.getTr_arr_day() != null && masTrainee.getCurr_date() >= masTrainee.getTr_arr_day()) /* 전속 부대 배치 여부 확인 */
            this.regiment = masTrainee.getTr_arr_unit();
        else if (masTrainee.getTr_sh_edu_start_day() != null && masTrainee.getCurr_date()  >= masTrainee.getTr_sh_edu_start_day()) /* 후반기 교육 여부 확인 */
            this.regiment = masTrainee.getTr_sh_edu_unit();
        else if (masTrainee.getTr_edu_start_day() != null && masTrainee.getCurr_date() >= masTrainee.getTr_edu_start_day()) /* 신병 교육 여부 확인 */
            this.regiment = masTrainee.getTr_edu_unit();
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setGrp(Long grp) {
        this.grp = grp;
    }

    public Long getGrp() {
        return grp;
    }

    public void setService(Long service) {
        this.service = service;
    }

    public void setUpgrade_date(Long upgrade_date) {
        this.upgrade_date = upgrade_date;
    }

    public Long getService() {
        return service;
    }

    public Long getUpgrade_date() {
        return upgrade_date;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public void setSm_id(String sm_id) {
        this.sm_id = sm_id;
    }

    public void setIs_yn(Integer is_yn) {
        this.is_yn = is_yn;
    }

    public Integer getGender() {
        return gender;
    }

    public String getSm_id() {
        return sm_id;
    }

    public Integer getIs_yn() {
        return is_yn;
    }

    public void setBirth(Long birth) {
        this.birth = birth;
    }

    public void setService_num(String service_num) {
        this.service_num = service_num;
    }

    public Long getBirth() {
        return birth;
    }

    public String getService_num() {
        return service_num;
    }

    public ProfileDetail(UserInfo userInfo)
    {
        this.iuid = userInfo.getIuid();
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setType(int type) {
        this.type = type;
    }



    public void setName(String name) {
        this.name = name;
    }


    public void setEnter_date(Long enter_date) {
        this.enter_date = enter_date;
    }

    public void setRegiment(String regiment) {
        this.regiment = regiment;
    }



    public Long getIuid() {
        return iuid;
    }

    public int getType() {
        return type;
    }


    public String getName() {
        return name;
    }


    public Long getEnter_date() {
        return enter_date;
    }

    public String getRegiment() {
        return regiment;
    }


    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getRank() {
        return rank;
    }
}