package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by jhkim on 14. 3. 4.
 */
public class HelpBody {



    @JsonProperty("result-code")
    int result_code;

    @JsonProperty("hid")
    int hid;

    @JsonProperty("title")
    String title;

    @JsonProperty("content")
    String content;

    @JsonProperty("date")
    String date;



    @JsonIgnore
    Date create_date;


    public int getResult_code() {
        return result_code;
    }

    public int getHid() {
        return hid;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }

    public void setHid(int hid) {
        this.hid = hid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public void setDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        this.create_date = Date.valueOf(date);
        this.date = date;
    }
    public String getDate(){
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        return format1.format(this.create_date);
    }
}
