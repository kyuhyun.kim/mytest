package com.tisquare.mvp.mcs.model;



import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class IdentifyResponse {

    @JsonProperty("res-code")
    int res_code;

    @JsonProperty("res-msg")
    String res_msg;

    @JsonProperty("unique-key")
    String unique_key;

    @JsonProperty("unique-no")
    String unique_no;

    @JsonProperty("rcpt-agr")
    String rcpt_agr;

    @JsonProperty("agree_yn")
    List<String> agree;

    public void setAgree(List<String> agree) {
        this.agree = agree;
    }

    public List<String> getAgree() {
        return agree;
    }

    public void setRcpt_agr(String rcpt_agr) {
        this.rcpt_agr = rcpt_agr;
    }

    public String getRcpt_agr() {
        return rcpt_agr;
    }

    public void setRes_code(int res_code) {
        this.res_code = res_code;
    }

    public void setRes_msg(String res_msg) {
        this.res_msg = res_msg;
    }

    public void setUnique_key(String unique_key) {
        this.unique_key = unique_key;
    }

    public void setUnique_no(String unique_no) {
        this.unique_no = unique_no;
    }

    public int getRes_code() {
        return res_code;
    }

    public String getRes_msg() {
        return res_msg;
    }

    public String getUnique_key() {
        return unique_key;
    }

    public String getUnique_no() {
        return unique_no;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("res-code=").append(res_code);
        sb.append("res-msg=").append(res_msg);
        sb.append(", unique-key=").append(unique_key);
        sb.append(", unique-no=").append(unique_no);
        sb.append("]");
        return sb.toString();
    }
}
