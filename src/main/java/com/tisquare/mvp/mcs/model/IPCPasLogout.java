package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class IPCPasLogout {

    @JsonProperty("content_type")
    private String content_type;

    @JsonProperty("from")
    private from from;

    public IPCPasLogout(String content_type, from from)
    {
        this.content_type = content_type;
        this.from = from;
    }

    public void setModel(String model) {
        this.content_type = model;
    }

    public void setFrom(from from) {
        this.from = from;
    }

    public String getModel() {
        return content_type;
    }

    public from getFrom() {
        return from;
    }
}