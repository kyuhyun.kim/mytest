package com.tisquare.mvp.mcs.model;


//{
//        "mdn":"+821012345678",
//        "statistic":[
//        {
//        "svctype":1,
//        "timestamp":1234,
//        "count":1
//        },
//        {
//        "svctype":1,
//        "timestamp":1234,
//        "count":1
//        }
//        ]
//}

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by jhkim on 14. 2. 25.
 */
public class Call {

    @JsonProperty("caller")
    String caller;

    @JsonProperty("callee")
    String callee;


    @JsonProperty("start_time")
    long start_time;

    @JsonProperty("end_time")
    long end_time;

    @JsonProperty("statistic")
    List<Statistic> stat;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\"call\":[{\n");
        sb.append("\"caller\":\""+      getCaller() + "\"\n");
        sb.append("\"callee\":\""+      getCallee() + "\"\n");
        sb.append("\"start_time\":\""+  getStart_time() + "\"\n");
        sb.append("\"end_time\":\""+    getEnd_time() + "\"\n");
        return sb.toString();
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }


    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public void setStat(List<Statistic> stat) {
        this.stat = stat;
    }

    public String getCaller() {

        return caller;
    }

    public String getCallee() {
        return callee;
    }

    public long getStart_time() {
        return start_time;
    }

    public long getEnd_time() {
        return end_time;
    }

    public List<Statistic> getStat() {
        return stat;
    }
}
