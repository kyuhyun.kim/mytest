package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.*;
import com.tisquare.mvp.mcs.entity.EventPopup;
import org.apache.http.HttpStatus;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class AuthInfoBody {



    @JsonProperty("reason")
    private Integer reason;

    @JsonProperty("subs-type")
    private int subsType;

    @JsonProperty("phone-no")
    private String phone_no;

    @JsonProperty("user-id")
    private String user_id;

    @JsonProperty("auth-key")
    private String authKey;

    @JsonProperty("user-type")
    private int user_type;

    @JsonProperty("ptoken")
    private String ptoken;

    @JsonProperty("servers")
    private List<ServerConnInfo> serverList;

    @JsonProperty("profile")
    private Profile profile;

    @JsonProperty("notice-info")
    private NoticeInfo noticeInfo;

    @JsonProperty("help")
    private HelpInfo helpInfo;

    @JsonProperty("event-notice")
    private EventPopup eventPopup;

    @JsonProperty("event-notice-list")
    private List<EventPopup> eventPopupList;

    @JsonProperty("app-update")
    private AppUpdatetInfo app_update;

   /* @JsonProperty("bt-info")
    private List<BtInfo> bt_info;
*/
    @JsonProperty("user-info")
    private SvcUserInfo svcUserInfo;

    @JsonProperty("url-info")
    private UrlInfo urlInfo;

    @JsonProperty("login-info")
    private Login_info login_info;

    @JsonProperty("bss-emp-info")
    private BssB2bInfo bssB2bInfo;

    @JsonProperty("config-info")
    private ConfigInfo configInfo;

    @JsonProperty("profile-detail")
    private ProfileDetail profileDetail;

    @JsonProperty("promotion-info")
    private List<EventNotice> eventNotices;

    @JsonProperty("terms-info")
    private List<Terms> termses;

    @JsonProperty("penalty-info")
    private List<CosCmmntyPenalty> cosCmmntyPenalties;

    @JsonProperty("privacy-info")
    private PrivacyInfo privacyInfo;

    @JsonProperty("home-sched-info")
    private HomeSched homeSched;

    @JsonProperty("grade-info")
    private GradeInfo gradeInfo;


    @JsonIgnore
    private String Trace;

    @JsonIgnore
    private String remoteIP;

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setEventPopupList(List<EventPopup> eventPopupList) {
        this.eventPopupList = eventPopupList;
    }

    public List<EventPopup> getEventPopupList() {
        return eventPopupList;
    }

    public void setGradeInfo(GradeInfo gradeInfo) {
        this.gradeInfo = gradeInfo;
    }

    public GradeInfo getGradeInfo() {
        return gradeInfo;
    }

    public void setHomeSched(HomeSched homeSched) {
        this.homeSched = homeSched;
    }

    public HomeSched getHomeSched() {
        return homeSched;
    }

    public void setPrivacyInfo(PrivacyInfo privacyInfo) {
        this.privacyInfo = privacyInfo;
    }

    public PrivacyInfo getPrivacyInfo() {
        return privacyInfo;
    }

    public void setCosCmmntyPenalties(List<CosCmmntyPenalty> cosCmmntyPenalties) {
        this.cosCmmntyPenalties = cosCmmntyPenalties;
    }

    public List<CosCmmntyPenalty> getCosCmmntyPenalties() {
        return cosCmmntyPenalties;
    }

    public void setTermses(List<Terms> termses) {
        this.termses = termses;
    }

    public List<Terms> getTermses() {
        return termses;
    }

    public void setEventNotices(List<EventNotice> eventNotices) {
        this.eventNotices = eventNotices;
    }

    public List<EventNotice> getEventNotices() {
        return eventNotices;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setProfileDetail(ProfileDetail profileDetail) {
        this.profileDetail = profileDetail;
    }

    public ProfileDetail getProfileDetail() {
        return profileDetail;
    }

    public AuthInfoBody(String remoteIP)
    {
        this.remoteIP = remoteIP;
    }

    public void setRemoteIP(String remoteIP) {
        this.remoteIP = remoteIP;
    }

    public String getRemoteIP() {
        return remoteIP;
    }

    public void setConfigInfo(ConfigInfo configInfo) {
        this.configInfo = configInfo;
    }

    public ConfigInfo getConfigInfo() {
        return configInfo;
    }



    public void setReason(Integer reason) {
        this.reason = reason;
    }

    public Integer getReason() {
        return reason;
    }

    public void setBssB2bInfo(BssB2bInfo bssB2bInfo) {
        this.bssB2bInfo = bssB2bInfo;
    }

    public BssB2bInfo getBssB2bInfo() {
        return bssB2bInfo;
    }

    public void setLogin_info(Login_info login_info) {
        this.login_info = login_info;
    }

    public Login_info getLogin_info() {
        return login_info;
    }


    public void setPtoken(String ptoken) {
        this.ptoken = ptoken;
    }

    public String getPtoken() {
        return ptoken;
    }


    public void setSubsType(int subsType) {
        this.subsType = subsType;
    }

    public void setServerList(List<ServerConnInfo> serverList) {
        this.serverList = serverList;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setNoticeInfo(NoticeInfo noticeInfo) {
        this.noticeInfo = noticeInfo;
    }

    public void setHelpInfo(HelpInfo helpInfo) {
        this.helpInfo = helpInfo;
    }

    public void setEventPopup(EventPopup eventPopup) {
        this.eventPopup = eventPopup;
    }

    public void setApp_update(AppUpdatetInfo app_update) {
        this.app_update = app_update;
    }



    public void setSvcUserInfo(SvcUserInfo svcUserInfo) {
        this.svcUserInfo = svcUserInfo;
    }

    public void setUrlInfo(UrlInfo urlInfo) {
        this.urlInfo = urlInfo;
    }

    public void setTrace(String trace) {
        Trace = trace;
    }


    public int getSubsType() {
        return subsType;
    }

    public List<ServerConnInfo> getServerList() {
        return serverList;
    }

    public Profile getProfile() {
        return profile;
    }

    public NoticeInfo getNoticeInfo() {
        return noticeInfo;
    }

    public HelpInfo getHelpInfo() {
        return helpInfo;
    }

    public EventPopup getEventPopup() {
        return eventPopup;
    }

    public AppUpdatetInfo getApp_update() {
        return app_update;
    }


    public SvcUserInfo getSvcUserInfo() {
        return svcUserInfo;
    }

    public UrlInfo getUrlInfo() {
        return urlInfo;
    }

    public String getTrace() {
        return Trace;
    }




    public void RequestToString( String proto, String url, String telecomCode, int httpStatus) {

        /*    final StringBuilder sb = new StringBuilder();

            sb.append(proto + "|");
            sb.append("MCS" + "|");
            sb.append(url + "|");
            if (telecomCode.equals("45005"))
                sb.append("자사" + "|");
            else
                sb.append("타사" + "|");
            sb.append(this.profile.getIuid() + "|");
            sb.append(svcUserInfo.getVas_status() + "|");
            sb.append(svcUserInfo.getCall_status() + "|");
            sb.append(svcUserInfo.getVolte_status() + "|");
            sb.append(svcUserInfo.getProduct_info() + "|");
            sb.append(httpStatus);
            if (httpStatus == HttpStatus.SC_OK)
                sb.append("(OK)" + "|");
            else
                sb.append("(FAIL)" + "|");

            Trace = sb.toString();*/

    }


    public void CspToString( String proto, String url, MCSResponse mcsResponse ) {
        final StringBuilder sb = new StringBuilder("");

        if( mcsResponse != null) {
            sb.append(proto + "|");
            sb.append("VASGW" + "|");
            sb.append(url + "|");
            sb.append(mcsResponse.getReturnCode() + "|");
            sb.append(mcsResponse.getCustomerMgntNum() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerVolte() + "|");
            sb.append(mcsResponse.getCustomerMainProdId() + "|");
            sb.append("200(OK)" + "|");
        }
        else if(mcsResponse == null) {
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("FAIL" + "|");
        }
        this.Trace += sb.toString();
    }

//    public String toJsonString() throws Exception {
//        return Json.toStringJson(this);
//    }
}