package com.tisquare.mvp.mcs.model;

import com.tisquare.mvp.mcs.db.ClientDao;
import com.tisquare.mvp.mcs.entity.SubscriberType;
import com.tisquare.mvp.mcs.type.ClientType;
import com.tisquare.mvp.mcs.utils.RandomString;
import com.tisquare.mvp.mcs.utils.UtilPhoneNo;
import com.tisquare.mvp.mcs.utils.Json;
import com.google.i18n.phonenumbers.NumberParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;


public class UserInfo {
    private static final Logger logger = LoggerFactory.getLogger(UserInfo.class);

    private static final int profileTagLen = 12;
    private static final int MAX_AUTH_KEY_LEN = 16;
    private static boolean sentCPathAlarm = false;

    private Long iuid;
    private String phoneNo;
    private String login_phoneNo;
    private String countryCode;
    private String regionCode;
    private String nationalFmt;
    private ClientType clientType;
    private String baseSvcDir;
    private String regDate;
    private String suid;
    private String suidPw;
    private String tuid;
    private Integer fTest;
    private Integer fActive;
    private SubscriberType subsType;
    private Integer svc_net_type;
    private Integer login_status;
    private Integer ch_auth;

    private BssCompany bssCompany;
    private BssEmp bssEmp;
    private Integer user_type;

    private Long home_sched_iuid;

    private Integer alarm_setup;
    private Integer alarm_setup_gr;
    private Integer alarm_setup_sched;
    private Integer alarm_setup_msg;
    private Integer alarm_setup_camp;


    public void setLogin_phoneNo(String login_phoneNo) {
        this.login_phoneNo = login_phoneNo;
    }

    public String getLogin_phoneNo() {
        return login_phoneNo;
    }

    public void setAlarm_setup_camp(Integer alarm_setup_camp) {
        this.alarm_setup_camp = alarm_setup_camp;
    }

    public Integer getAlarm_setup_camp() {
        return alarm_setup_camp;
    }

    public void setAlarm_setup(Integer alarm_setup) {
        this.alarm_setup = alarm_setup;
    }

    public void setAlarm_setup_gr(Integer alarm_setup_gr) {
        this.alarm_setup_gr = alarm_setup_gr;
    }

    public void setAlarm_setup_sched(Integer alarm_setup_sched) {
        this.alarm_setup_sched = alarm_setup_sched;
    }

    public void setAlarm_setup_msg(Integer alarm_setup_msg) {
        this.alarm_setup_msg = alarm_setup_msg;
    }

    public Integer getAlarm_setup() {
        return alarm_setup;
    }

    public Integer getAlarm_setup_gr() {
        return alarm_setup_gr;
    }

    public Integer getAlarm_setup_sched() {
        return alarm_setup_sched;
    }

    public Integer getAlarm_setup_msg() {
        return alarm_setup_msg;
    }

    public void setHome_sched_iuid(Long home_sched_iuid) {
        this.home_sched_iuid = home_sched_iuid;
    }

    public Long getHome_sched_iuid() {
        return home_sched_iuid;
    }

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    public Integer getUser_type() {
        return user_type;
    }

    public void setCh_auth(Integer ch_auth) {
        this.ch_auth = ch_auth;
    }

    public Integer getCh_auth() {
        return ch_auth;
    }

    public void setBssCompany(BssCompany bssCompany) {
        this.bssCompany = bssCompany;
    }

    public void setBssEmp(BssEmp bssEmp) {
        this.bssEmp = bssEmp;
    }

    public BssCompany getBssCompany() {
        return bssCompany;
    }

    public BssEmp getBssEmp() {
        return bssEmp;
    }

    public void setLogin_status(Integer login_status) {
        this.login_status = login_status;
    }

    public Integer getLogin_status() {
        return login_status;
    }

    public void setSvc_net_type(Integer svc_net_type) {
        this.svc_net_type = svc_net_type;
    }

    public Integer getSvc_net_type() {
        return svc_net_type;
    }

    public static void setSentCPathAlarm(boolean sentCPathAlarm) {
        UserInfo.sentCPathAlarm = sentCPathAlarm;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public void setPresence(Integer presence) {
        this.presence = presence;
    }

    public void setDeviceWidth(Integer deviceWidth) {
        this.deviceWidth = deviceWidth;
    }

    public static Logger getLogger() {

        return logger;
    }

    public static int getProfileTagLen() {
        return profileTagLen;
    }

    public static int getMaxAuthKeyLen() {
        return MAX_AUTH_KEY_LEN;
    }

    public static boolean isSentCPathAlarm() {
        return sentCPathAlarm;
    }

    public String getNickname() {
        return nickname;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public Integer getPresence() {
        return presence;
    }

    public Integer getDeviceWidth() {
        return deviceWidth;
    }

    public Integer getDeviceHeight() {
        return deviceHeight;
    }

    private String appVersion;
    private String cauthKey;
    private String pnotiToken;
    private String osType;
    private String osVersion;
    private String telecomStr;
    private String telecomCode;
    private String deviceId;
    private String profileTag;
    private Profile profile;
    private String deviceModel;

    private Integer device_chg_f;

    public void setDevice_chg_f(Integer device_chg_f) {
        this.device_chg_f = device_chg_f;
    }

    public Integer getDevice_chg_f() {
        return device_chg_f;
    }

    private ClientDao clientDao;

    private Integer storeType;
    //region 프로필에 포함되는 필드
    private String nickname;
    private String statusMessage;
    private Integer presence;
    //endregion
    private Integer deviceWidth;
    private Integer deviceHeight;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");

        sb.append("]");
        return sb.toString();
    }

    public UserInfo() {
        super();
    }

    public void setProfileFromString(String profileText) throws IOException {
        this.profile = Json.toObjectJson(profileText, Profile.class);
    }

    public UserInfo(String phoneNo, String regionCode, String tnVersion, UserDevice userDevice, Profile profile, Integer storeType, String deviceModel) throws NumberParseException {
        UtilPhoneNo utilPhoneNo = new UtilPhoneNo(phoneNo, regionCode);

        this.regionCode = utilPhoneNo.getRegionCode();
        this.nationalFmt = utilPhoneNo.getNationalFmt();
        this.phoneNo = utilPhoneNo.getE164Fmt();
        this.countryCode = utilPhoneNo.getCountryCode();

        this.clientType = ClientType.INDIVIDUAL_CLIENT;
        this.fTest = 0;
        this.fActive = 1;
        this.appVersion = tnVersion;
        if (userDevice != null) {
            this.pnotiToken = userDevice.getPnRegID();
            this.osType = userDevice.getOsType();
            this.osVersion = userDevice.getOsVersion();
            this.telecomStr = userDevice.getTelecomStr();
            this.telecomCode = userDevice.getTelecomCode();
            this.deviceId = userDevice.getDeviceID();
        }
        this.profile = profile;

        if (this.profile != null) {
            this.profileTag = this.getNewProfileTag();
        }

        if (storeType == null)
            this.setStoreType(new Integer(0));
        else
            this.setStoreType(storeType);

        this.deviceModel = deviceModel;
    }






    public ClientDao getClientDao() {
        return clientDao;
    }

    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    private String getNewProfileTag() {
        RandomString randomString = new RandomString();
        return randomString.getString(profileTagLen, "a1");
    }

    public Long getIuid() {
        return iuid;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getNationalFmt() {
        return nationalFmt;
    }

    public void setNationalFmt(String nationalFmt) {
        this.nationalFmt = nationalFmt;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    public String getBaseSvcDir() {
        return baseSvcDir;
    }

    public void setBaseSvcDir(String baseSvcDir) {
        this.baseSvcDir = baseSvcDir;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getSuid() {
        return suid;
    }

    public void setSuid(String suid) {
        this.suid = suid;
    }

    public String getSuidPw() {
        return suidPw;
    }

    public void setSuidPw(String suidPw) {
        this.suidPw = suidPw;
    }

    public String getTuid() {
        return tuid;
    }

    public void setTuid(String tuid) {
        this.tuid = tuid;
    }

    public Integer getfTest() {
        return fTest;
    }

    public void setfTest(Integer fTest) {
        this.fTest = fTest;
    }

    public Integer getfActive() {
        return fActive;
    }

    public void setfActive(Integer fActive) {
        this.fActive = fActive;
    }

    public SubscriberType getSubsType() {
        return subsType;
    }

    public void setSubsType(SubscriberType subsType) {
        this.subsType = subsType;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getCauthKey() {
        return cauthKey;
    }

    public void setCauthKey(String cauthKey) {
        this.cauthKey = cauthKey;
    }

    public String getPnotiToken() {
        return pnotiToken;
    }

    public void setPnotiToken(String pnotiToken) {
        this.pnotiToken = pnotiToken;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getTelecomStr() {
        return telecomStr;
    }

    public void setTelecomStr(String telecomStr) {
        this.telecomStr = telecomStr;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getProfileTag() {
        return profileTag;
    }

    public void setProfileTag(String profileTag) {
        this.profileTag = profileTag;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Integer getStoreType() {
        return storeType;
    }

    public void setStoreType(Integer storeType) {
        this.storeType = storeType;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }


    public void setDeviceHeight(Integer deviceHeight) {
        this.deviceHeight = deviceHeight;
    }


}
