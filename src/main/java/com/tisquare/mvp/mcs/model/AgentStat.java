package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

//{
//        "mdn":"+821034100114",
//        "count":3,                           //해당 MDN의 3회의 호 발생
//        "call":
//        [{
//        // 한가지의 서비스를 이용한 경우
//        "caller":"01034100114",
//        "callee":"01020951582",
//        "tphone_state":"T",                       // T 전화 우선여부
//        "start_time":1429199045348,               //전화 시작 시간
//        "end_time":1429199065095,                 // 전화 종료 시간
//        "statistic":[{
//        "service_type":"picture",             // 서비스
//        "service_sub_type":"none",            // 서브 서비스
//        "share_type":"share",
//        "sender":"01034100114",
//        "recver":"01020951582",
//        "start_path":"tphone",
//        "call_state":"calling",
//        "start_time":0,
//        "end_time":1429199059906,
//        "request_time":1429199055435,
//        "file_size":77082,
//        "file_count":1
//        }]},
//
//        // 아무것도 사용 하지 않은 경우
//        {"caller":"01034100114",
//        "callee":"01020951582",
//        "tphone_state":"T",
//        "start_time":1429199035570,
//        "end_time":1429199038261,
//        "statistic":[]},
//
//        // 두가지의 서비스를 이용한 경우
//        {"caller":"01034100114",
//        "callee":"01020951582",
//        "tphone_state":"T",
//        "start_time":1429198962602,
//        "end_time":1429199018729,
//        "statistic":[
//        {
//        "service_type":"picture",
//        "service_sub_type":"none",
//        "share_type":"share",
//        "sender":"01034100114",
//        "recver":"01020951582",
//        "start_path":"tphone",
//        "call_state":"calling",
//        "start_time":0,
//        "end_time":1429198988254,
//        "request_time":1429198982079,
//        "file_size":133617,
//        "file_count":2},
//
//        {"service_type":"web",
//        "service_sub_type":"none",
//        "share_type":"share",
//        "sender":"01034100114",
//        "recver":"01020951582",
//        "start_path":"tcalln",
//        "call_state":"calling",
//        "start_time":0,
//        "end_time":0,
//        "request_time":1429198999216,
//        "file_size":0,
//        "file_count":0}]
//        }]
//        }



public class AgentStat {

    @JsonProperty("mdn")
    String mdn;
    String nonE164Mdn;

    @JsonProperty("count")
    int count;


    @JsonProperty("call")
    List<Call> call;




    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\"mdn\":"+getMdn()+"\n");
        sb.append("\"count\":"+getCount()+"\n");

        for (Call s : getCall()) {
            sb.append(s.toString());
            for (Statistic statics : s.getStat()) {
                sb.append( statics.toString());
            }
        }


        return sb.toString();
    }

    public String toSimpleString(){
        StringBuilder sb = new StringBuilder();

        return sb.toString();
    }

    public String toFileLogString(){
        StringBuilder sb = new StringBuilder();

        return sb.toString();
    }

    public StatLog[] toStatLog(){
        StatLog[] statlog = new StatLog[10];


        return statlog;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public void setNonE164Mdn(String nonE164Mdn) {
        this.nonE164Mdn = nonE164Mdn;
    }

    public void setCount(int count) {
        this.count = count;
    }



    public String getMdn() {

        return mdn;
    }

    public String getNonE164Mdn() {
        return nonE164Mdn;
    }

    public int getCount() {
        return count;
    }

    public void setCall(List<Call> call) {
        this.call = call;
    }

    public List<Call> getCall() {

        return call;
    }
}
