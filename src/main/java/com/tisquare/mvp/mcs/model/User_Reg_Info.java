package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class User_Reg_Info {

    @JsonProperty("subs-type")
    private Integer subs_type;

    @JsonProperty("user-type")
    private Integer user_type;

    @JsonProperty("user-id")
    private String user_id;

    @JsonProperty("reg-date")
    private String reg_date;

    public User_Reg_Info()
    {

    }

    public User_Reg_Info(UserInfo userInfo, ProfileDetail profileDetail)
    {
        this.subs_type = userInfo.getSubsType().value();
        this.user_id = userInfo.getSuid();
        this.reg_date = userInfo.getRegDate();
        this.user_type = profileDetail.getType();
    }

    public Integer getSubs_type() {
        return subs_type;
    }

    public Integer getUser_type() {
        return user_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setSubs_type(Integer subs_type) {
        this.subs_type = subs_type;
    }

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }
}