package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SvcUserInfo {

    public static final int ENABLE = 1;
    public static final int DISENABLE = 0;
    public static final int DEFAULT = -1;

    @JsonProperty("vas-status")
    private int vas_status;

    @JsonProperty("call-status")
    private int call_status;

    @JsonProperty("volte-status")
    private int volte_status;

    @JsonProperty("product-info")
    private int product_info;

    @JsonProperty("mvoip-info")
    private int mvoip_info;

    public SvcUserInfo()
    {
        this.vas_status = DISENABLE;
        this.call_status = DISENABLE;
        this.volte_status = DISENABLE;
        this.product_info = DISENABLE;
        this.mvoip_info = ENABLE;
    }

    public void skt_init()
    {
        this.vas_status = DEFAULT;
        this.call_status = DEFAULT;
        this.volte_status = DEFAULT;
        this.product_info = DEFAULT;
        this.mvoip_info = ENABLE;
    }

    public void setMvoip_info(int mvoip_info) {
        this.mvoip_info = mvoip_info;
    }

    public int getMvoip_info() {
        return mvoip_info;
    }

    public void setVas_status(int vas_status) {
        this.vas_status = vas_status;
    }

    public void setCall_status(int call_status) {
        this.call_status = call_status;
    }

    public void setVolte_status(int volte_status) {
        this.volte_status = volte_status;
    }

    public void setProduct_info(int product_info) {
        this.product_info = product_info;
    }

    public int getVas_status() {
        return vas_status;
    }

    public int getCall_status() {
        return call_status;
    }

    public int getVolte_status() {
        return volte_status;
    }

    public int getProduct_info() {
        return product_info;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SvcUserInfo:{");
        sb.append("vas-status='").append(vas_status).append('\'');
        sb.append(", call-status='").append(call_status).append('\'');
        sb.append(", volte-status='").append(volte_status).append('\'');
        sb.append(", product_info='").append(product_info).append('\'');
        sb.append(", mvoip_info='").append(product_info).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
