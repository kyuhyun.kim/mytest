package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UsInfo {

    @JsonProperty("user-id")
    private String user_id;

    @JsonProperty("user-pwd")
    private String user_pwd;

    @JsonProperty("new-user-pwd")
    private String new_user_pwd;

    @JsonIgnore
    private long iuid;

    public UsInfo()
    {

    }
    public  UsInfo(Long iuid, String new_user_pwd)
    {
        this.iuid = iuid;
        this.new_user_pwd = new_user_pwd;
    }

    public void setNew_user_pwd(String new_user_pwd) {
        this.new_user_pwd = new_user_pwd;
    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public String getNew_user_pwd() {
        return new_user_pwd;
    }

    public long getIuid() {
        return iuid;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_pwd() {
        return user_pwd;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }
}