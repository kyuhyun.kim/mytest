package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class HomeSchedInfo {

    @JsonProperty("reg-iuid")
    private Long reg_iuid;

    @JsonProperty("chg-date")
    private String chg_date;

    @JsonProperty("iuid")
    private Long iuid;

    @JsonProperty("name")
    private String name;

    @JsonProperty("birth")
    private Long birth;

    @JsonProperty("enter-date")
    private Long enter_date;

    @JsonProperty("enter-unit")
    private String enter_unit;

    @JsonProperty("seq")
    private Integer seq;

    @JsonProperty("new-seq")
    private Integer new_seq;

    @JsonProperty("image")
    private String image;

    @JsonProperty("thumbnail")
    private String thumbnail;

    @JsonProperty("result")
    private Integer result;

    @JsonProperty("result-msg")
    private String result_msg;


    public void setResult(Integer result) {
        this.result = result;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public Integer getResult() {
        return result;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getImage() {
        return image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setNew_seq(Integer new_seq) {
        this.new_seq = new_seq;
    }

    public Integer getNew_seq() {
        return new_seq;
    }

    public void setReg_iuid(Long reg_iuid) {
        this.reg_iuid = reg_iuid;
    }

    public void setChg_date(String chg_date) {
        this.chg_date = chg_date;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirth(Long birth) {
        this.birth = birth;
    }

    public void setEnter_date(Long enter_date) {
        this.enter_date = enter_date;
    }

    public void setEnter_unit(String enter_unit) {
        this.enter_unit = enter_unit;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Long getReg_iuid() {
        return reg_iuid;
    }

    public String getChg_date() {
        return chg_date;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getName() {
        return name;
    }

    public Long getBirth() {
        return birth;
    }

    public Long getEnter_date() {
        return enter_date;
    }

    public String getEnter_unit() {
        return enter_unit;
    }

    public Integer getSeq() {
        return seq;
    }
}