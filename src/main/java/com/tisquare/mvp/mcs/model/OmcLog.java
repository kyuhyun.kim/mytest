package com.tisquare.mvp.mcs.model;

import com.tisquare.mvp.mcs.utils.DateUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;

import java.net.InetAddress;
import java.util.Objects;

public class OmcLog {

    public static final int SUCCESS = 0;
    public static final int FAIL = 1;

    public static final int ENABLE = 1;
    public static final String AUTHUAC_TYPE = "001";


    private String omc_log_base;
    private String servicename;
    private String Device_model;
    private String os_type;
    private String os_version;
    private String app_version;
    private String mdn;
    private int telecome_code;
    private String url_type;
    private String success_yn;
    private int result_code;
    private String result_msg;
    private long elapse_time;


    public  OmcLog( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HttpStatus httpStatus, String Url, long elapse_time, String omc_log_base )
    {

        this.omc_log_base = omc_log_base;
        this.url_type = Url;
        this.servicename="ACS";
        if(authReqParams.getDeviceModel() != null && !authReqParams.getDeviceModel().equals("") )
            this.Device_model=authReqParams.getDeviceModel();
        else
            this.Device_model = "-";
        this.os_type = authReqParams.getOsType();
        this.os_version = authReqParams.getOsVersion();
        this.app_version = authReqHeaders.getAppVersion();
        this.mdn = authReqHeaders.getPhoneNo();
        switch (authReqParams.getTelecomCode())
        {
            case "45005":
            case "45006":
            case "45008":
                this.telecome_code = Integer.valueOf(authReqParams.getTelecomCode());
                break;
            default:
                this.telecome_code = -1;
        }

        if(httpStatus == HttpStatus.OK)
            this.success_yn = "Y";
        else
            this.success_yn = "N";

        this.result_code = httpStatus.value();

        switch (this.result_code)
        {
            case 200:
                this.result_msg="성공";break;
            case 400:
                this.result_msg ="입력 파라메터 오류"; break;
            case 403:
                this.result_msg = "인증키 오류"; break;
            case 404:
                this.result_msg ="가입자 정보 없음"; break;
            case 405:
                this.result_msg = "가입자 정보 불 일치"; break;
            case 406:
                this.result_msg ="앱 버전 업그레이드 필요"; break;
            case 500:
                this.result_msg = "내부 서버 오류"; break;
            case 503:
                this.result_msg = "시스템 작업 중"; break;
            default:
                this.result_msg ="-";
        }


        this.elapse_time = elapse_time;


    }

    public String getOmc_log_base() {
        return omc_log_base;
    }


    public String getServicename() {
        return servicename;
    }

    public String getDevice_model() {
        return Device_model;
    }

    public String getOs_type() {
        return os_type;
    }

    public String getOs_version() {
        return os_version;
    }

    public String getApp_version() {
        return app_version;
    }

    public String getMdn() {
        return mdn;
    }

    public int getTelecome_code() {
        return telecome_code;
    }

    public String getUrl_type() {
        return url_type;
    }

    public String getSuccess_yn() {
        return success_yn;
    }

    public int getResult_code() {
        return result_code;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public long getElapse_time() {
        return elapse_time;
    }

    @Override
    public String toString() {

        String hostName = null;
        long time = System.currentTimeMillis();

        StringBuilder sb = new StringBuilder();

        try {



            hostName = InetAddress.getLocalHost().getHostName().toString();

            sb.append(hostName + ",");
            sb.append(this.servicename + ",");
            sb.append(AUTHUAC_TYPE + ",");
            sb.append( DateUtil.toFormatString(System.currentTimeMillis(), "yyyyMMddHHmmss") + "|");
            sb.append( DateUtil.toFormatString(System.currentTimeMillis(), "yyyyMMddHHmmss") + "|");
            sb.append(this.Device_model +",");
            sb.append(this.os_type +",");
            sb.append(this.os_version +",");
            sb.append(this.app_version +",");
            sb.append(this.mdn +",");
            sb.append(this.telecome_code +",");
            sb.append(this.url_type +",");
            sb.append(this.success_yn +",");
            sb.append(this.result_code +",");
            sb.append(this.result_msg +",");
            sb.append(this.elapse_time);


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }




}

