package com.tisquare.mvp.mcs.model;

/**
 * Created by hsna on 14. 8. 7.
 * SKT ICAS 시스템으로 부터 고객정보 및 에러코드를 전달하는 클래스
 */
public class MCSResponse {

    public   static  final int MCS_SC = 0;
    public   static  final int MCS_FA = 1;


    int returnCode;
    String customerName;
    String customerMgntNum;
    String customerGender;
    String customerType;
    int customerProdId_TWT;
    int customerProdId_CALL;
    String customerBirth;
    String optionalServiceRegDate;
    String customerBizNum;
    int customerVolte;
    String customerMainProdId;
    int customerMvoip;

    public void setCustomerMvoip(int customerMvoip) {
        this.customerMvoip = customerMvoip;
    }

    public int getCustomerMvoip() {
        return customerMvoip;
    }

    public void setCustomerProdId_TWT(int customerProdId_TWT) {
        this.customerProdId_TWT = customerProdId_TWT;
    }

    public void setCustomerProdId_CALL(int customerProdId_CALL) {
        this.customerProdId_CALL = customerProdId_CALL;
    }

    public void setCustomerVolte(int customerVolte) {
        this.customerVolte = customerVolte;
    }

    public int getCustomerProdId_TWT() {
        return customerProdId_TWT;
    }

    public int getCustomerProdId_CALL() {
        return customerProdId_CALL;
    }

    public int getCustomerVolte() {
        return customerVolte;
    }

    public String getCustomerMainProdId() {

        return customerMainProdId;
    }

    public void setCustomerMainProdId(String customerMainProdId) {
        this.customerMainProdId = customerMainProdId;
    }



    public String getCustomerBirth() {
        return customerBirth;
    }

    public void setCustomerBirth(String customerBirth) {
        this.customerBirth = customerBirth;
    }


    public String getOptionalServiceRegDate() {
        return optionalServiceRegDate;
    }

    public void setOptionalServiceRegDate(String optionalServiceRegDate) {
        this.optionalServiceRegDate = optionalServiceRegDate;
    }


    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMgntNum() {
        return customerMgntNum;
    }

    public void setCustomerMgntNum(String customerMgntNum) {
        this.customerMgntNum = customerMgntNum;
    }

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }




    public String getCustomerBizNum() {
        return customerBizNum;
    }

    public void setCustomerBizNum(String customerBizNum) {
        this.customerBizNum = customerBizNum;
    }


    @Override
    public String toString() {
        return "ICASResponse{" +
                "returnCode=" + returnCode +
                ", customerName='" + customerName + '\'' +
                ", customerMgntNum='" + customerMgntNum + '\'' +
                ", customerGender='" + customerGender + '\'' +
                ", customerType='" + customerType + '\'' +
                ", customerProdId_TWT='" + customerProdId_TWT + '\'' +
                ", customerProdId_CALL='" + customerProdId_CALL + '\'' +
                ", customerBirth='" + customerBirth + '\'' +
                ", optionalServiceRegDate='" + optionalServiceRegDate + '\'' +
                ", customerBizNum='" + customerBizNum + '\'' +
                ", customerMainProdId='" + customerMainProdId + '\'' +
                '}';
    }
}
