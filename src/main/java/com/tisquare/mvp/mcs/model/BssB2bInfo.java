package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BssB2bInfo {

    @JsonProperty("cp-code")
    private String cp_code;

    @JsonProperty("cp-name")
    private String cp_name;

    @JsonProperty("cp-eng-name")
    private String cp_eng_name;

    @JsonProperty("name-type")
    private int name_type;

    @JsonProperty("emp-mdn")
    private String emp_mdn;

    @JsonProperty("bss-id")
    private String emp_id;

    @JsonProperty("auth-key")
    private String auth_key;

    @JsonProperty("cp-tel")
    private String cp_tel;

    @JsonProperty("dp-tag")
    private String dp_tag;

    @JsonProperty("agency-code")
    private String agency_code;

    @JsonProperty("is-default-invite")
    private int is_default_invite;

    @JsonProperty("first-login")
    private String first_login;

    @JsonProperty("cp-org-manage-flag")
    private int cp_org_manage_flag;

    @JsonProperty("active-admin")
    private int active_admin;

    @JsonProperty("lc-use-yn")
    private long lc_use_yn;

    @JsonProperty("lc-collect-cycle")
    private long lc_collect_cycle;

    @JsonProperty("lc-report-cycle")
    private long lc_report_cycle;


    public BssB2bInfo()
    {

    }

    public BssB2bInfo(UserInfo userInfo)
    {
        this.cp_code = userInfo.getBssEmp().getCp_code();
        this.cp_name = userInfo.getBssCompany().getCp_name();
        this.cp_eng_name = userInfo.getBssCompany().getCp_eng_name();
        this.name_type = userInfo.getBssCompany().getName_type();
        this.emp_id = userInfo.getBssEmp().getEmp_id();
        this.auth_key = userInfo.getCauthKey();
        this.cp_tel = userInfo.getBssCompany().getCp_tel();
        this.dp_tag = userInfo.getBssCompany().getDp_tag();
        this.agency_code = userInfo.getBssEmp().getAgency_code();
        this.is_default_invite = userInfo.getBssEmp().getIs_default_invite();
        this.first_login = userInfo.getBssEmp().getFirst_login();
        this.cp_org_manage_flag = userInfo.getBssCompany().getCp_org_manage_flag();
        this.emp_mdn = userInfo.getBssEmp().getEmp_mdn();
        this.active_admin = userInfo.getBssCompany().getActive_admin();
        this.lc_use_yn = userInfo.getBssCompany().getLc_use_yn();
        this.lc_collect_cycle = userInfo.getBssCompany().getLc_collect_cycle();
        this.lc_report_cycle = userInfo.getBssCompany().getLc_report_cycle();
    }

    public void setEmp_mdn(String emp_mdn) {
        this.emp_mdn = emp_mdn;
    }

    public void setActive_admin(int active_admin) {
        this.active_admin = active_admin;
    }

    public void setLc_use_yn(long lc_use_yn) {
        this.lc_use_yn = lc_use_yn;
    }

    public void setLc_collect_cycle(long lc_collect_cycle) {
        this.lc_collect_cycle = lc_collect_cycle;
    }

    public void setLc_report_cycle(long lc_report_cycle) {
        this.lc_report_cycle = lc_report_cycle;
    }

    public String getEmp_mdn() {
        return emp_mdn;
    }

    public int getActive_admin() {
        return active_admin;
    }

    public long getLc_use_yn() {
        return lc_use_yn;
    }

    public long getLc_collect_cycle() {
        return lc_collect_cycle;
    }

    public long getLc_report_cycle() {
        return lc_report_cycle;
    }

    public void setCp_org_manage_flag(int cp_org_manage_flag) {
        this.cp_org_manage_flag = cp_org_manage_flag;
    }

    public int getCp_org_manage_flag() {
        return cp_org_manage_flag;
    }

    public void setCp_name(String cp_name) {
        this.cp_name = cp_name;
    }

    public void setCp_eng_name(String cp_eng_name) {
        this.cp_eng_name = cp_eng_name;
    }

    public void setName_type(int name_type) {
        this.name_type = name_type;
    }

    public void setFirst_login(String first_login) {
        this.first_login = first_login;
    }

    public String getCp_name() {
        return cp_name;
    }

    public String getCp_eng_name() {
        return cp_eng_name;
    }

    public int getName_type() {
        return name_type;
    }

    public String getFirst_login() {
        return first_login;
    }

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public void setCp_tel(String cp_tel) {
        this.cp_tel = cp_tel;
    }

    public void setDp_tag(String dp_tag) {
        this.dp_tag = dp_tag;
    }

    public void setAgency_code(String agency_code) {
        this.agency_code = agency_code;
    }

    public void setIs_default_invite(int is_default_invite) {
        this.is_default_invite = is_default_invite;
    }

    public String getCp_code() {
        return cp_code;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public String getCp_tel() {
        return cp_tel;
    }

    public String getDp_tag() {
        return dp_tag;
    }

    public String getAgency_code() {
        return agency_code;
    }

    public int getIs_default_invite() {
        return is_default_invite;
    }
}