package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class IPCPnsMsg {

    private MsgInfo msgInfo;

    public IPCPnsMsg(MsgInfo msgInfo)
    {
        this.msgInfo = msgInfo;
    }

    public void setMsgInfo(MsgInfo msgInfo) {
        this.msgInfo = msgInfo;
    }

    public MsgInfo getMsgInfo() {
        return msgInfo;
    }
}