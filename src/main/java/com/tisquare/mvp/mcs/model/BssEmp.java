package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class BssEmp {

    private String cp_code;
    private String emp_id;
    private String emp_pwd;
    private String emp_mdn;
    private int is_default_invite;
    private String agency_code;
    private String last_login;
    private String first_login;
    private int device_dup_f;


    public void setDevice_dup_f(int device_dup_f) {
        this.device_dup_f = device_dup_f;
    }

    public int getDevice_dup_f() {
        return device_dup_f;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public void setFirst_login(String first_login) {
        this.first_login = first_login;
    }

    public String getLast_login() {
        return last_login;
    }

    public String getFirst_login() {
        return first_login;
    }

    public String getEmp_pwd() {
        return emp_pwd;
    }

    public void setEmp_pwd(String emp_pwd) {
        this.emp_pwd = emp_pwd;
    }

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public void setEmp_mdn(String emp_mdn) {
        this.emp_mdn = emp_mdn;
    }

    public void setIs_default_invite(int is_default_invite) {
        this.is_default_invite = is_default_invite;
    }

    public void setAgency_code(String agency_code) {
        this.agency_code = agency_code;
    }

    public String getCp_code() {
        return cp_code;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public String getEmp_mdn() {
        return emp_mdn;
    }

    public int getIs_default_invite() {
        return is_default_invite;
    }

    public String getAgency_code() {
        return agency_code;
    }
}