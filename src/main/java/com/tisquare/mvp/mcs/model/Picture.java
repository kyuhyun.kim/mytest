package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Picture {
    @JsonProperty("picno")
    private Integer picNo;

    @JsonProperty("ppicid")
    private String ppicid;

    @JsonProperty("image")
    private String image;

    @JsonProperty("thumbnail")
    private String thumbnail;

    public Picture() {
        super();
    }

    public Picture(int picno, Long ppicid, String mediaUrl, String thumbnailUrl) {
        this();
        picNo = picno;
        image = mediaUrl;
        thumbnail = thumbnailUrl;
        this.ppicid = ppicid.toString();
    }

    public Integer getPicNo() {
        return picNo;
    }

    public void setPicNo(Integer picNo) {
        this.picNo = picNo;
    }

    public String getPpicid() {
        return ppicid;
    }

    public void setPpicid(String ppicid) {
        this.ppicid = ppicid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("picNo=").append(picNo);
        sb.append(", ppicid='").append(ppicid).append('\'');
        sb.append(", image='").append(image).append('\'');
        sb.append(", thumbnail='").append(thumbnail).append('\'');
        sb.append("]");
        return sb.toString();
    }

    public String getImagePath(String baseSvcDir) {
        String[] split = image.split("/", 6);
        return String.format("%s/%s", baseSvcDir, split[5]);
    }

    public String getThumbnailPath(String baseSvcDir) {
        String[] split = thumbnail.split("/", 6);
        return String.format("%s/%s", baseSvcDir, split[5]);
    }
}