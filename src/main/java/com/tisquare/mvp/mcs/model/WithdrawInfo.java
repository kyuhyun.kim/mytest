package com.tisquare.mvp.mcs.model;

public class WithdrawInfo {



    private String tn_req_user;
    private String tn_iuid;
    private String tn_auth_key;
    private String tn_version;
    private String tn_service;

    public WithdrawInfo( String tn_req_user, String tn_iuid, String tn_auth_key)
    {
        this.tn_auth_key = tn_auth_key;
        this.tn_iuid = tn_iuid;
        this.tn_req_user = tn_req_user;
    }

    public void setTn_service(String tn_service) {
        this.tn_service = tn_service;
    }

    public String getTn_service() {
        return tn_service;
    }

    public String getTn_req_user() {
        return tn_req_user;
    }

    public String getTn_iuid() {
        return tn_iuid;
    }

    public String getTn_auth_key() {
        return tn_auth_key;
    }

    public String getTn_version() {
        return tn_version;
    }

    public void setTn_req_user(String tn_req_user) {
        this.tn_req_user = tn_req_user;
    }

    public void setTn_iuid(String tn_iuid) {
        this.tn_iuid = tn_iuid;
    }

    public void setTn_auth_key(String tn_auth_key) {
        this.tn_auth_key = tn_auth_key;
    }

    public void setTn_version(String tn_version) {
        this.tn_version = tn_version;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "tn_req_user='" + tn_req_user + '\'' +
                ", tn_iuid='" + tn_iuid + '\'' +
                ", tn_auth_key='" + tn_auth_key + '\'' +
                ", tn_version=" + tn_version +
                '}';
    }
}

