package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class from {

    @JsonProperty("mdn")
    private String mdn;

    @JsonProperty("iuid")
    private long iuid;

    @JsonProperty("name")
    private String name;


    public from(String mdn, long iuid, String name)
    {
        this.mdn = mdn;
        this.iuid = iuid;
        this.name = name;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMdn() {
        return mdn;
    }

    public long getIuid() {
        return iuid;
    }

    public String getName() {
        return name;
    }
}