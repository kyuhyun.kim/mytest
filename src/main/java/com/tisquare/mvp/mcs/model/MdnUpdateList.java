package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class MdnUpdateList {

    private long iuid;

    private String phone_no;

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public long getIuid() {
        return iuid;
    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }
}