package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UserList {

    @JsonProperty("iuid")
    private Long iuid;

    @JsonProperty("type")
    private Integer type;

    @JsonProperty("phone-no")
    private String phone_no;

    @JsonProperty("name")
    private String name;

    @JsonProperty("birth")
    private Long birth;

    @JsonProperty("call-number")
    private Integer call_number;

    @JsonProperty("room-no")
    private Integer room_number;

    @JsonProperty("image")
    private String image;

    @JsonProperty("thumbnail")
    private String thumbnail;

    @JsonProperty("ad")
    private Integer ad;

    @JsonProperty("gender")
    private Integer gender;

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getGender() {
        return gender;
    }

    public void setBirth(Long birth) {
        this.birth = birth;
    }

    public Long getBirth() {
        return birth;
    }

    public void setAd(Integer ad) {
        this.ad = ad;
    }

    public Integer getAd() {
        return ad;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getImage() {
        return image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public Long getIuid() {
        return iuid;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getCall_number() {
        return call_number;
    }

    public Integer getRoom_number() {
        return room_number;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }


    public void setCall_number(Integer call_number) {
        this.call_number = call_number;
    }

    public void setRoom_number(Integer room_number) {
        this.room_number = room_number;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }

}