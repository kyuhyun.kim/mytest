package com.tisquare.mvp.mcs.model;

import org.springframework.web.bind.annotation.RequestHeader;

/**
 * 요청 시 넘어오는 헤더
 *
 * @author leejs
 * @since 2015-03-05
 */
public class NoticeReqHeaders  {



    public final String SKT = "45005";
    public final String KT = "45008";
    public final String LGT = "45006";

    private int start_sid;
    private int last_read_sid;
    private int notice_cnt;
    private String telecom_code;

    private long iuid;
    private String cAuthKey;

    public NoticeReqHeaders(int start_sid, int last_read_sid, int notice_cnt, String telecom_code, long iuid, String cAuthKey) {


        this.start_sid = start_sid;
        this.last_read_sid = last_read_sid;
        this.notice_cnt = notice_cnt;
        this.telecom_code = telecom_code;
        this.iuid = iuid;
        this.cAuthKey = cAuthKey;
    }

    public NoticeReqHeaders(int start_sid, int last_read_sid, int notice_cnt, String telecom_code) {


        this.start_sid = start_sid;
        this.last_read_sid = last_read_sid;
        this.notice_cnt = notice_cnt;
        this.telecom_code = telecom_code;

    }

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public void setcAuthKey(String cAuthKey) {
        this.cAuthKey = cAuthKey;
    }

    public long getIuid() {
        return iuid;
    }

    public String getcAuthKey() {
        return cAuthKey;
    }

    public int getStart_sid() {
        return start_sid;
    }

    public int getLast_read_sid() {
        return last_read_sid;
    }

    public int getNotice_cnt() {
        return notice_cnt;
    }

    public String getTelecom_code() {
        return telecom_code;
    }

    public void setStart_sid(int start_sid) {
        this.start_sid = start_sid;
    }

    public void setLast_read_sid(int last_read_sid) {
        this.last_read_sid = last_read_sid;
    }

    public void setNotice_cnt(int notice_cnt) {
        this.notice_cnt = notice_cnt;
    }

    public void setTelecom_code(String telecom_code) {
        this.telecom_code = telecom_code;
    }
}
