package com.tisquare.mvp.mcs.model;

/**
 * Created by asus on 2015-03-01.
 */
public class UrlShortData {
    String targetUrl;
    String svcType;

    public String getTargetUrl() { return targetUrl; }
    public void setTargetUrl(String targetUrl) { this.targetUrl = targetUrl; }

    public String getSvcType() { return svcType; }
    public void setSvcType(String svcType) { this.svcType = svcType; }

    @Override
    public String toString() {
        return "UrlShortData{" +
                "targetUrl='" + targetUrl + '\'' +
                ", svcType='" + svcType + '\'' +
                '}';
    }
}
