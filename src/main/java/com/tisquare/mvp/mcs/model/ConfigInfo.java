package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ConfigInfo {

    @JsonProperty("holding-time")
    private int holding_time;

    public ConfigInfo() {

    }

    public ConfigInfo(int holding_time) {
        this();
        this.holding_time = holding_time;

    }

    public void setHolding_time(int holding_time) {
        this.holding_time = holding_time;
    }

    public int getHolding_time() {
        return holding_time;
    }
}