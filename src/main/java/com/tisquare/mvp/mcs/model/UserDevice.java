package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class UserDevice {
    @JsonProperty("os-type")
    private String osType;

    @JsonProperty("os-version")
    private String osVersion;

    @JsonProperty("device-id")
    private String deviceID;

    @JsonProperty("telecom-code")
    private String telecomCode;

    @JsonProperty("telecom-str")
    private String telecomStr;

    @JsonProperty("net-type")
    private String netType;

    @JsonProperty("pn-reg-id")
    private String pnRegID;

    public UserDevice() {
    }

    public UserDevice(String osType, String osVersion, String deviceID, String telecomCode, String telecomStr, String netType, String pnRegID) {
        this();
        this.osType = osType;
        this.osVersion = osVersion;
        this.deviceID = deviceID;
        this.telecomCode = telecomCode;
        this.telecomStr = telecomStr;
        this.netType = netType;
        this.pnRegID = pnRegID;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getTelecomCode() {
        return telecomCode;
    }

    public void setTelecomCode(String telecomCode) {
        this.telecomCode = telecomCode;
    }

    public String getTelecomStr() {
        return telecomStr;
    }

    public void setTelecomStr(String telecomStr) {
        this.telecomStr = telecomStr;
    }

    public String getNetType() {
        return netType;
    }

    public void setNetType(String netType) {
        this.netType = netType;
    }

    public String getPnRegID() {
        return pnRegID;
    }

    public void setPnRegID(String pnRegID) {
        this.pnRegID = pnRegID;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("osType='").append(osType).append('\'');
        sb.append(", osVersion='").append(osVersion).append('\'');
        sb.append(", deviceID='").append(deviceID).append('\'');
        sb.append(", telecomCode='").append(telecomCode).append('\'');
        sb.append(", telecomStr='").append(telecomStr).append('\'');
        sb.append(", netType='").append(netType).append('\'');
        sb.append(", pnRegID='").append(pnRegID).append('\'');
        sb.append("]");
        return sb.toString();
    }
}