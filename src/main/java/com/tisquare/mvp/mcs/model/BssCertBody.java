package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.entity.EventPopup;
import org.apache.http.HttpStatus;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class BssCertBody {

    @JsonProperty("iuid")
    private Long iuid;

    @JsonProperty("phone-no")
    private String phone_no;

    @JsonProperty("auth-key")
    private String authKey;

    @JsonProperty("subs-type")
    private int subsType;

    @JsonProperty("ptoken")
    private String ptoken;

    @JsonProperty("servers")
    private List<ServerConnInfo> serverList;

    @JsonProperty("profile")
    private Profile profile;

    @JsonProperty("notice")
    private NoticeInfo noticeInfo;

    @JsonProperty("help")
    private HelpInfo helpInfo;

    @JsonProperty("event-notice")
    private EventPopup eventPopup;

    @JsonProperty("app-update")
    private AppUpdatetInfo app_update;

    @JsonProperty("bt-info")
    private List<BtInfo> bt_info;

    @JsonProperty("user-info")
    private SvcUserInfo svcUserInfo;

    @JsonProperty("url-info")
    private UrlInfo urlInfo;

    @JsonProperty("login-info")
    private Login_info login_info;

    @JsonIgnore
    private String Trace;

    public void setLogin_info(Login_info login_info) {
        this.login_info = login_info;
    }

    public Login_info getLogin_info() {
        return login_info;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPtoken(String ptoken) {
        this.ptoken = ptoken;
    }

    public String getPtoken() {
        return ptoken;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setSubsType(int subsType) {
        this.subsType = subsType;
    }

    public void setServerList(List<ServerConnInfo> serverList) {
        this.serverList = serverList;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setNoticeInfo(NoticeInfo noticeInfo) {
        this.noticeInfo = noticeInfo;
    }

    public void setHelpInfo(HelpInfo helpInfo) {
        this.helpInfo = helpInfo;
    }

    public void setEventPopup(EventPopup eventPopup) {
        this.eventPopup = eventPopup;
    }

    public void setApp_update(AppUpdatetInfo app_update) {
        this.app_update = app_update;
    }

    public void setBt_info(List<BtInfo> bt_info) {
        this.bt_info = bt_info;
    }

    public void setSvcUserInfo(SvcUserInfo svcUserInfo) {
        this.svcUserInfo = svcUserInfo;
    }

    public void setUrlInfo(UrlInfo urlInfo) {
        this.urlInfo = urlInfo;
    }

    public void setTrace(String trace) {
        Trace = trace;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getAuthKey() {
        return authKey;
    }

    public int getSubsType() {
        return subsType;
    }

    public List<ServerConnInfo> getServerList() {
        return serverList;
    }

    public Profile getProfile() {
        return profile;
    }

    public NoticeInfo getNoticeInfo() {
        return noticeInfo;
    }

    public HelpInfo getHelpInfo() {
        return helpInfo;
    }

    public EventPopup getEventPopup() {
        return eventPopup;
    }

    public AppUpdatetInfo getApp_update() {
        return app_update;
    }

    public List<BtInfo> getBt_info() {
        return bt_info;
    }

    public SvcUserInfo getSvcUserInfo() {
        return svcUserInfo;
    }

    public UrlInfo getUrlInfo() {
        return urlInfo;
    }

    public String getTrace() {
        return Trace;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("iuid=").append(iuid);
        sb.append(", authKey=").append(authKey);
        sb.append(", subsType=").append(subsType);
        sb.append(", serverList=").append(serverList);
        sb.append(", notice=").append(noticeInfo);
        sb.append(", help=").append(helpInfo);
        sb.append(", app-update=").append(app_update);
        sb.append("]");
        return sb.toString();
    }


    public void RequestToString( String proto, String url, String telecomCode, int httpStatus) {

            final StringBuilder sb = new StringBuilder();

            sb.append(proto + "|");
            sb.append("MCS" + "|");
            sb.append(url + "|");
            if (telecomCode.equals("45005"))
                sb.append("자사" + "|");
            else
                sb.append("타사" + "|");
            sb.append(this.profile.getIuid() + "|");
            sb.append(this.getAuthKey() + "|");
            sb.append(svcUserInfo.getVas_status() + "|");
            sb.append(svcUserInfo.getCall_status() + "|");
            sb.append(svcUserInfo.getVolte_status() + "|");
            sb.append(svcUserInfo.getProduct_info() + "|");
            sb.append(httpStatus);
            if (httpStatus == HttpStatus.SC_OK)
                sb.append("(OK)" + "|");
            else
                sb.append("(FAIL)" + "|");

            Trace = sb.toString();

    }


    public void CspToString( String proto, String url, MCSResponse mcsResponse ) {
        final StringBuilder sb = new StringBuilder("");

        if( mcsResponse != null) {
            sb.append(proto + "|");
            sb.append("VASGW" + "|");
            sb.append(url + "|");
            sb.append(mcsResponse.getReturnCode() + "|");
            sb.append(mcsResponse.getCustomerMgntNum() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerProdId_TWT() + "|");
            sb.append(mcsResponse.getCustomerVolte() + "|");
            sb.append(mcsResponse.getCustomerMainProdId() + "|");
            sb.append("200(OK)" + "|");
        }
        else if(mcsResponse == null) {
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("NULL" + "|");
            sb.append("FAIL" + "|");
        }
        this.Trace += sb.toString();
    }

//    public String toJsonString() throws Exception {
//        return Json.toStringJson(this);
//    }
}