package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by jhkim on 14. 11. 10.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Identify {

    @JsonProperty("mdn")
    String mdn;

    @JsonProperty("name")
    String name;

    @JsonProperty("birth")
    String birth;

    @JsonProperty("mobile-co")
    int mobile_co;

    @JsonProperty("gender")
    Integer gender;

    @JsonProperty("nativ")
    Integer nativ;

    @JsonProperty("sub-key")
    String sub_key;

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public void setNativ(Integer nativ) {
        this.nativ = nativ;
    }

    public Integer getGender() {
        return gender;
    }

    public Integer getNativ() {
        return nativ;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getBirth() {
        return birth;
    }

    public void setSub_key(String sub_key) {
        this.sub_key = sub_key;
    }

    public String getSub_key() {
        return sub_key;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getMdn() {
        return mdn;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setMobile_co(int mobile_co) {
        this.mobile_co = mobile_co;
    }

    public String getName() {
        return name;
    }


    public int getMobile_co() {
        return mobile_co;
    }



    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }
}
