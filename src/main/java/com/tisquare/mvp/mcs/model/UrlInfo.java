package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UrlInfo {

    @JsonProperty("agreement-info")
    private String agreement_info;

    @JsonProperty("privacy-info")
    private String privacy_info;

    @JsonProperty("third-info")
    private String third_info;

    @JsonProperty("ad-info")
    private String ad_info;

    @JsonProperty("oss-info")
    private String oss_info;

    @JsonProperty("privacy-op-info")
    private String privacy_op_info;

    @JsonProperty("privacy-md-info")
    private String privacy_md_info;

    @JsonProperty("location-info")
    private String location_info;

    @JsonProperty("nice-url-info")
    private NiceUrlInfo niceUrlInfo;

    public UrlInfo()
    {

    }

    public UrlInfo(String agreement_info, String privacy_info, String third_info, String ad_info, String oss_info, String privacy_op_info, String privacy_md_info, String location_info ) {

        this.agreement_info = agreement_info;
        this.privacy_info = privacy_info;
        this.third_info = third_info;
        this.ad_info = ad_info;
        this.oss_info = oss_info;
        this.privacy_op_info = privacy_op_info;
        this.privacy_md_info = privacy_md_info;
        this.location_info = location_info;
    }

    public void setNiceUrlInfo(NiceUrlInfo niceUrlInfo) {
        this.niceUrlInfo = niceUrlInfo;
    }

    public NiceUrlInfo getNiceUrlInfo() {
        return niceUrlInfo;
    }

    public void setLocation_info(String location_info) {
        this.location_info = location_info;
    }

    public String getLocation_info() {
        return location_info;
    }

    public void setAgreement_info(String agreement_info) {
        this.agreement_info = agreement_info;
    }

    public void setPrivacy_info(String privacy_info) {
        this.privacy_info = privacy_info;
    }

    public void setThird_info(String third_info) {
        this.third_info = third_info;
    }

    public void setAd_info(String ad_info) {
        this.ad_info = ad_info;
    }

    public void setOss_info(String oss_info) {
        this.oss_info = oss_info;
    }

    public void setPrivacy_op_info(String privacy_op_info) {
        this.privacy_op_info = privacy_op_info;
    }

    public void setPrivacy_md_info(String privacy_md_info) {
        this.privacy_md_info = privacy_md_info;
    }

    public String getAgreement_info() {
        return agreement_info;
    }

    public String getPrivacy_info() {
        return privacy_info;
    }

    public String getThird_info() {
        return third_info;
    }

    public String getAd_info() {
        return ad_info;
    }

    public String getOss_info() {
        return oss_info;
    }

    public String getPrivacy_op_info() {
        return privacy_op_info;
    }

    public String getPrivacy_md_info() {
        return privacy_md_info;
    }
}