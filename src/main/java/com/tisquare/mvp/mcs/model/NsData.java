package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class NsData {

    @JsonProperty("noti_title")
    private String noti_title;

    @JsonProperty("noti_body")
    private String noti_body;

    @JsonProperty("push_type")
    private Integer push_type;

    public NsData(String noti_title, String noti_body, Integer push_type)
    {
        this.noti_title = noti_title;
        this.noti_body = noti_body;
        this.push_type = push_type;
    }

    public void setNoti_title(String noti_title) {
        this.noti_title = noti_title;
    }

    public void setNoti_body(String noti_body) {
        this.noti_body = noti_body;
    }

    public void setPush_type(Integer push_type) {
        this.push_type = push_type;
    }

    public String getNoti_title() {
        return noti_title;
    }

    public String getNoti_body() {
        return noti_body;
    }

    public Integer getPush_type() {
        return push_type;
    }
}