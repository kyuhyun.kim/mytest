package com.tisquare.mvp.mcs.model;

import java.util.Calendar;

/**
 * Created by jhkim on 14. 2. 28.
 */
public class StatLog {
    String mdn;
    String year;
    String month;
    String day;
    String hour;
    String min;
    String sec;

    int svr_id;
    int svc_id;
    int sub_svc_id;
    int part_id;
    int receiver_type;
    int used_cnt;


    int svcType;
    long stamp;

//    static TimeZone tz = TimeZone.getTimeZone("KST"); //GMT +9
//    static TimeZone tz = TimeZone.getDefault(); // get time zone from local host
//    int count;

    public StatLog(){}
    public StatLog(String mdn, Statistic stat){
        this.mdn = mdn;




        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(this.stamp);

        this.setYear(String.format("%04d", cal.get(Calendar.YEAR)));
        this.setMonth(String.format("%02d", cal.get(Calendar.MONTH)+1));
        this.setDay(String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)));
//        this.setHour(String.format("%02d", cal.get(Calendar.HOUR))); // 12시 표기법 ㅡㅡ
        this.setHour(String.format("%02d", cal.get(Calendar.HOUR_OF_DAY))); // 24시 표기법
        //5분 통계라서 그에 맞는 값으로 바꿔준다.
        int min = cal.get(Calendar.MINUTE);
        this.setMin(String.format("%02d", (min/5)*5));
        this.setSec(String.format("%02d", cal.get(Calendar.SECOND)));
    }

    // MDN SVCTYPE TIMESTAMP COUNT
    public static StatLog fileToObject(String log){
        StatLog value = new StatLog();
        String[] nodes = log.split(" ");
        value.setMdn(nodes[0]);
        value.setSvcType(Integer.parseInt(nodes[1]));
        //value.setStamp(Timestamp.valueOf(nodes[2]));
        value.setStamp(Long.parseLong(nodes[2]));
        value.setUsed_cnt(Integer.parseInt(nodes[3]));

        // TimeStamp값 분해해서 입력
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(value.getStamp());
        //cal.setTimeZone(tz);

        value.setYear(String.format("%04d", cal.get(Calendar.YEAR)));
        value.setMonth(String.format("%02d", cal.get(Calendar.MONTH)+1));
        value.setDay(String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)));
//        value.setHour(String.format("%02d", cal.get(Calendar.HOUR)));  //12시 표기법 ㅡㅡ
        value.setHour(String.format("%02d", cal.get(Calendar.HOUR_OF_DAY))); //24시 표기법
        //5분 통계라서 그에 맞는 값으로 바꿔준다.
        int min = cal.get(Calendar.MINUTE);
        value.setMin(String.format("%02d", (min/5)*5));
        value.setSec(String.format("%02d", cal.get(Calendar.SECOND)));
        return value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MDN [" + mdn + "] ");
        sb.append("SVCTYPE [" + svcType + "] ");
        sb.append("TIMESTAMP [" + stamp + "] ");
        sb.append("COUNT [" + used_cnt + "] ");
        sb.append("Y [" + year + "] ");
        sb.append("M [" + month + "] ");
        sb.append("D [" + day + "]" );
        sb.append("H [" + hour + "] ");
        sb.append("m [" + min + "]" );
        sb.append("S [" + sec + "]");

        return sb.toString();
    }

    public String getMdn() { return mdn; }
    public void setMdn(String mdn) { this.mdn = mdn; }
//    public int getYear() { return year; }
//    public void setYear(int year) { this.year = year; }
//    public int getMonth() { return month; }
//    public void setMonth(int month) { this.month = month; }
//    public int getDay() { return day; }
//    public void setDay(int day) { this.day = day; }
//    public int getHour() { return hour; }
//    public void setHour(int hour) { this.hour = hour; }
//    public int getMin() { return min; }
//    public void setMin(int min) { this.min = min; }
//    public int getSec() { return sec; }
//    public void setSec(int sec) { this.sec = sec; }

    public String getYear() {            return year; }
    public void setYear(String year) {   this.year = year; }

    public int getSvr_id() { return svr_id; }
    public void setSvr_id(int svr_id) { this.svr_id = svr_id; }
    public int getSvc_id() { return svc_id; }
    public void setSvc_id(int svc_id) { this.svc_id = svc_id; }
    public int getSub_svc_id() { return sub_svc_id; }
    public void setSub_svc_id(int sub_svc_id) { this.sub_svc_id = sub_svc_id; }
    public int getPart_id() { return part_id; }
    public void setPart_id(int part_id) { this.part_id = part_id; }
    public int getReceiver_type() { return receiver_type; }
    public void setReceiver_type(int receiver_type) { this.receiver_type = receiver_type; }
    public String getMonth() {           return month; }
    public void setMonth(String month) { this.month = month; }
    public String getDay() {             return day; }
    public void setDay(String day) {     this.day = day; }
    public String getHour() {            return hour; }
    public void setHour(String hour) {   this.hour = hour; }
    public String getMin() {             return min; }
    public void setMin(String min) {     this.min = min; }
    public String getSec() {             return sec; }
    public void setSec(String sec) {     this.sec = sec; }
    public int getSvcType() {            return svcType; }
    public void setSvcType(int svcType){ this.svcType = svcType; }
    public long getStamp() { return stamp; }
    public void setStamp(long stamp) { this.stamp = stamp; }
    public int getUsed_cnt() { return used_cnt; }
    public void setUsed_cnt(int used_cnt) { this.used_cnt = used_cnt; }
}
