package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AlarmInfo {

    @JsonProperty("alarm-setup")
    private Integer alarm_setup;

    @JsonProperty("alarm-setup-gr")
    private Integer alarm_setup_gr;

    @JsonProperty("alarm-setup-sched")
    private Integer alarm_setup_sched;

    @JsonProperty("alarm-setup-msg")
    private Integer alarm_setup_msg;

    @JsonProperty("alarm-setup-camp")
    private Integer alarm_setup_camp;

    @JsonIgnore
    private Long iuid;

    public AlarmInfo()
    {

    }

    public AlarmInfo(UserInfo userInfo)
    {
        this.alarm_setup = userInfo.getAlarm_setup();
        this.alarm_setup_gr = userInfo.getAlarm_setup_gr();
        this.alarm_setup_msg = userInfo.getAlarm_setup_msg();
        this.alarm_setup_sched = userInfo.getAlarm_setup_sched();
        this.alarm_setup_camp = userInfo.getAlarm_setup_camp();
    }

    public AlarmInfo(UserInfo userInfo, Alarm alarm)
    {
        this.iuid = userInfo.getIuid();
        this.alarm_setup = userInfo.getAlarm_setup();
        this.alarm_setup_gr = userInfo.getAlarm_setup_gr();
        this.alarm_setup_msg = userInfo.getAlarm_setup_msg();
        this.alarm_setup_sched = userInfo.getAlarm_setup_sched();

        for(int nIdx = 0 ; nIdx < alarm.getAlarmSetUps().size(); nIdx++)
        {
            switch (alarm.getAlarmSetUps().get(nIdx).getType())
            {
                case 0: this.alarm_setup =alarm.getAlarmSetUps().get(nIdx).getValue() ; break;
                case 1: this.alarm_setup_gr =alarm.getAlarmSetUps().get(nIdx).getValue() ; break;
                case 2: this.alarm_setup_sched =alarm.getAlarmSetUps().get(nIdx).getValue() ; break;
                case 3: this.alarm_setup_msg =alarm.getAlarmSetUps().get(nIdx).getValue() ; break;
                case 4: this.alarm_setup_camp =alarm.getAlarmSetUps().get(nIdx).getValue() ; break;
                default: break;
            }
        }
    }


    public void setAlarm_setup_camp(Integer alarm_setup_camp) {
        this.alarm_setup_camp = alarm_setup_camp;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public Integer getAlarm_setup_camp() {
        return alarm_setup_camp;
    }

    public Long getIuid() {
        return iuid;
    }

    public void setAlarm_setup(Integer alarm_setup) {
        this.alarm_setup = alarm_setup;
    }

    public void setAlarm_setup_gr(Integer alarm_setup_gr) {
        this.alarm_setup_gr = alarm_setup_gr;
    }

    public void setAlarm_setup_sched(Integer alarm_setup_sched) {
        this.alarm_setup_sched = alarm_setup_sched;
    }

    public void setAlarm_setup_msg(Integer alarm_setup_msg) {
        this.alarm_setup_msg = alarm_setup_msg;
    }

    public Integer getAlarm_setup() {
        return alarm_setup;
    }

    public Integer getAlarm_setup_gr() {
        return alarm_setup_gr;
    }

    public Integer getAlarm_setup_sched() {
        return alarm_setup_sched;
    }

    public Integer getAlarm_setup_msg() {
        return alarm_setup_msg;
    }
}