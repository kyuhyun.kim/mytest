package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.entity.EventPopup;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class AuthPwdBody {





    @JsonProperty("subs-type")
    private int subsType;

    @JsonProperty("auth-key")
    private String authKey;

    @JsonProperty("user-type")
    private int user_type;

    @JsonProperty("iuid")
    private long iuid;

    public void setIuid(long iuid) {
        this.iuid = iuid;
    }

    public long getIuid() {
        return iuid;
    }

    public void setSubsType(int subsType) {
        this.subsType = subsType;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public int getSubsType() {
        return subsType;
    }

    public String getAuthKey() {
        return authKey;
    }

    public int getUser_type() {
        return user_type;
    }

    //    public String toJsonString() throws Exception {
//        return Json.toStringJson(this);
//    }
}