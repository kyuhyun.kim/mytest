package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.PromotionSamsung;
import com.tisquare.mvp.mcs.type.ResultCode;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({"result", "result_msg", "rel_phone_no", "rel_name", "rel_birth", "relationship", "name", "enter_date", "enter_unit", "reg_date", "rel_addr", "rel_addr_no" })

public class PromotionCampPackInfo {


    @JsonProperty("result")
    private int result;

    @JsonProperty("result-msg")
    private String result_msg;

    @JsonProperty("rel-phone-no")
    private String rel_phone_no;

    @JsonProperty("rel-name")
    private String rel_name;

    @JsonProperty("rel-birth")
    private Long rel_birth;

    @JsonProperty("rel-gender")
    private Integer rel_gender;

    @JsonProperty("relationship")
    private String relationship;

    @JsonProperty("name")
    private String name;

    @JsonProperty("birth")
    private Long birth;

    @JsonProperty("gender")
    private Integer gender;

    @JsonProperty("enter-date")
    private Long enter_date;

    @JsonProperty("enter-unit")
    private String enter_unit;

    @JsonProperty("reg-date")
    private String reg_date;

    @JsonProperty("rel-addr")
    private String rel_addr;

    @JsonProperty("rel-addr-no")
    private String rel_addr_no;

    @JsonProperty("rel-iuid")
    private Long rel_iuid;

    @JsonIgnore
    private String mdn;






    public PromotionCampPackInfo()
    {
        this.result = ResultCode.SUCCESS.getCode();
        this.result_msg = "SUCCESS";
    }

    public void PromotionCampPackInfo(PromotionCampPackInfo promotionCampPackInfo)
    {
        this.rel_phone_no = promotionCampPackInfo.getRel_phone_no();
        this.rel_name = promotionCampPackInfo.getRel_name();
        this.rel_birth = promotionCampPackInfo.getRel_birth();
        this.relationship = promotionCampPackInfo.getRelationship();
        this.name = promotionCampPackInfo.getName() ;
        this.birth = promotionCampPackInfo.getBirth();
        this.enter_date = promotionCampPackInfo.getEnter_date();
        this.enter_unit = promotionCampPackInfo.getEnter_unit();
        this.reg_date = promotionCampPackInfo.getReg_date();
        this.rel_addr = promotionCampPackInfo.getRel_addr();
        this.rel_addr_no = promotionCampPackInfo.getRel_addr_no();
        this.rel_gender = promotionCampPackInfo.getRel_gender();
        this.rel_iuid = promotionCampPackInfo.getRel_iuid();
        this.gender = promotionCampPackInfo.getGender();

    }

    public void PromotionCampPackInfoResult(PromotionCampPackInfo promotionCampPackInfo)
    {
        this.result = promotionCampPackInfo.getResult();
        this.result_msg = promotionCampPackInfo.getResult_msg();
    }


    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getGender() {
        return gender;
    }

    public void setRel_gender(Integer rel_gender) {
        this.rel_gender = rel_gender;
    }

    public Integer getRel_gender() {
        return rel_gender;
    }

    public void setBirth(Long birth) {
        this.birth = birth;
    }

    public Long getBirth() {
        return birth;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getMdn() {
        return mdn;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public void setRel_phone_no(String rel_phone_no) {
        this.rel_phone_no = rel_phone_no;
    }

    public void setRel_name(String rel_name) {
        this.rel_name = rel_name;
    }

    public void setRel_birth(Long rel_birth) {
        this.rel_birth = rel_birth;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnter_date(Long enter_date) {
        this.enter_date = enter_date;
    }

    public void setEnter_unit(String enter_unit) {
        this.enter_unit = enter_unit;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public void setRel_addr(String rel_addr) {
        this.rel_addr = rel_addr;
    }

    public void setRel_addr_no(String rel_addr_no) {
        this.rel_addr_no = rel_addr_no;
    }

    public void setRel_iuid(Long rel_iuid) {
        this.rel_iuid = rel_iuid;
    }

    public int getResult() {
        return result;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public String getRel_phone_no() {
        return rel_phone_no;
    }

    public String getRel_name() {
        return rel_name;
    }

    public Long getRel_birth() {
        return rel_birth;
    }

    public String getRelationship() {
        return relationship;
    }

    public String getName() {
        return name;
    }

    public Long getEnter_date() {
        return enter_date;
    }

    public String getEnter_unit() {
        return enter_unit;
    }

    public String getReg_date() {
        return reg_date;
    }

    public String getRel_addr() {
        return rel_addr;
    }

    public String getRel_addr_no() {
        return rel_addr_no;
    }

    public Long getRel_iuid() {
        return rel_iuid;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }
}