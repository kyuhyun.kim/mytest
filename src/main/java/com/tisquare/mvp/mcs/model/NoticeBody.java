package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by jhkim on 14. 3. 4.
 */
public class NoticeBody {



    @JsonProperty("result-code")
    int result_code;

    @JsonProperty("sid")
    int sid;

    @JsonProperty("title")
    String title;

    @JsonProperty("content")
    String content;

    @JsonProperty("date")
    String date;



    @JsonIgnore
    Date create_date;


    public int getSid() { return sid; }
    public void setSid(int sid) { this.sid = sid; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public int getResult_code() {
        return result_code;
    }

    public String getContent() {
        return content;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    @JsonIgnore
    public Date getCraete_date() {
        return create_date;
    }
    public void setCraete_date(Date craete_date) { this.create_date = craete_date; }

    public void setDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        this.create_date = Date.valueOf(date);
        this.date = date;
    }
    public String getDate(){
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        return format1.format(this.create_date);
    }
}
