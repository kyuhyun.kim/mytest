package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class NoticeInfo {

    @JsonProperty("notice")
    private Notice notice;

    @JsonProperty("bss-notice")
    private BssNotice bssNotice;



    public NoticeInfo() {
    }

    public NoticeInfo(Notice notice, BssNotice bssNotice) {
        this.notice = notice;
        this.bssNotice = bssNotice;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }

    public void setBssNotice(BssNotice bssNotice) {
        this.bssNotice = bssNotice;
    }

    public Notice getNotice() {
        return notice;
    }

    public BssNotice getBssNotice() {
        return bssNotice;
    }


}
