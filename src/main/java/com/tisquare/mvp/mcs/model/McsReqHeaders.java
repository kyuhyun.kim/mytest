package com.tisquare.mvp.mcs.model;

/**
 * SCS 에서 사용하는 공통적인 HTTP Headers
 *
 * @author leejs
 * @since 2015-03-06
 */
public class McsReqHeaders {
    private Long iuid;
    private String phoneNo;
    private String cAuthKey;
    private String appVersion;
    private Integer storeType;
    private Integer subs_type;

    public McsReqHeaders(Long iuid, String phoneNo, String cAuthKey, String appVersion, Integer storeType, Integer subs_type) {

        /*if(iuid == null) iuid = 0L;*/
        this.iuid = iuid;
        this.phoneNo = phoneNo;
        this.cAuthKey = cAuthKey;
        this.appVersion = appVersion;
        this.storeType = storeType;
        this.subs_type = subs_type;
    }

    public McsReqHeaders(Long iuid, String phoneNo, String cAuthKey, String appVersion, Integer storeType) {
        /*if(iuid == null) iuid = 0L;*/
        this.iuid = iuid;
        this.phoneNo = phoneNo;
        this.cAuthKey = cAuthKey;
        this.appVersion = appVersion;
        this.storeType = storeType;

    }

    public void setStoreType(Integer storeType) {
        this.storeType = storeType;
    }

    public void setSubs_type(Integer subs_type) {
        this.subs_type = subs_type;
    }

    public Integer getStoreType() {
        return storeType;
    }

    public Integer getSubs_type() {
        return subs_type;
    }

    public String getPhoneNo() {
        return phoneNo;
    }


    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCAuthKey() {
        return cAuthKey;
    }

    public void setCAuthKey(String authKey) {
        this.cAuthKey = authKey;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Long getIuid() {
        return iuid;
    }

    public String getcAuthKey() {
        return cAuthKey;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setcAuthKey(String cAuthKey) {
        this.cAuthKey = cAuthKey;
    }
}
