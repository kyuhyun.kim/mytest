package com.tisquare.mvp.mcs.model;




import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Created by jhkim on 14. 2. 25.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ShortUrlMap {

    @JsonProperty("target-url")
    String target_url;

    public void setTarget_url(String target_url) {
        this.target_url = target_url;
    }

    public String getTarget_url() {
        return target_url;
    }


}
