package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class HomeSchedResponse {

    @JsonProperty("home-sched-info")
    private List<HomeSchedInfo> homeSchedInfoList;

    @JsonProperty("home-sched-result")
    private List<HomeSchedInfo> homeSchedResultList;

    public HomeSchedResponse(List<HomeSchedInfo> homeSchedInfoList)
    {
        this.homeSchedInfoList = homeSchedInfoList;
    }

    public void setHomeSchedInfoList(List<HomeSchedInfo> homeSchedInfoList) {
        this.homeSchedInfoList = homeSchedInfoList;
    }

    public void setHomeSchedResultList(List<HomeSchedInfo> homeSchedResultList) {
        this.homeSchedResultList = homeSchedResultList;
    }

    public List<HomeSchedInfo> getHomeSchedResultList() {
        return homeSchedResultList;
    }

    public HomeSchedResponse(List<HomeSchedInfo> homeSchedInfoList, List<HomeSchedInfo> homeSchedResultList)
    {
        this.homeSchedInfoList = homeSchedInfoList;
        this.homeSchedResultList = homeSchedResultList;
    }

    public List<HomeSchedInfo> getHomeSchedInfoList() {
        return homeSchedInfoList;
    }
}