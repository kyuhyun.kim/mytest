package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by jhkim on 14. 3. 4.
 */
public class NoticeJh {
    @JsonProperty("sid")
    int sid;

    @JsonProperty("title")
    String title;

    @JsonProperty("date")
    String date;

    @JsonProperty("is-new")
    int is_new;

    @JsonIgnore
    Date create_date;


    public int getSid() { return sid; }
    public void setSid(int sid) { this.sid = sid; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    @JsonIgnore
    public Date getCraete_date() {
        return create_date;
    }
    public void setCraete_date(Date craete_date) { this.create_date = craete_date; }

    public int getIs_new() {
        return is_new;
    }

    public void setIs_new(int is_new) {
        this.is_new = is_new;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        this.create_date = Date.valueOf(date);
        this.date = date;
    }
    public String getDate(){
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        return format1.format(this.create_date);
    }
}
