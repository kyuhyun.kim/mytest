package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UserSearch {

    @JsonProperty("type")
    private Integer type;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("cp-code")
    private String cp_code;

    @JsonProperty("mask")
    private Integer mask;

    public UserSearch()
    {

    }
    public UserSearch(Integer type, String msg)
    {
        this.type = type;
        this.msg = msg;
    }

    public void setMask(Integer mask) {
        this.mask = mask;
    }

    public Integer getMask() {
        return mask;
    }

    public void setType(Integer type) {
        this.type = type;
    }


    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public Integer getType() {
        return type;
    }



    public String getCp_code() {
        return cp_code;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }

}