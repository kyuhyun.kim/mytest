package com.tisquare.mvp.mcs.model;

/**
 * 요청 시 넘어오는 헤더
 *
 * @author leejs
 * @since 2015-03-05
 */
public class HelpReqHeaders {


    private int start_hid;
    private int last_read_hid;
    private int help_cnt;

    public int getStart_hid() {
        return start_hid;
    }

    public int getLast_read_hid() {
        return last_read_hid;
    }

    public int getHelp_cnt() {
        return help_cnt;
    }

    public void setStart_hid(int start_hid) {
        this.start_hid = start_hid;
    }

    public void setLast_read_hid(int last_read_hid) {
        this.last_read_hid = last_read_hid;
    }

    public void setHelp_cnt(int help_cnt) {
        this.help_cnt = help_cnt;
    }

    public HelpReqHeaders(int start_hid, int last_read_hid, int help_cnt_cnt) {


        this.start_hid = start_hid;
        this.last_read_hid = last_read_hid;
        this.help_cnt = help_cnt_cnt;
    }

}
