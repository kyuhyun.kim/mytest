package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;



public class IotSiteInfo {

    @JsonProperty("site-id")
    int site_id;

    @JsonProperty("device-id")
    String device_id;

    @JsonProperty("container-id")
    String container_id;

    public void setSite_id(int site_id) {
        this.site_id = site_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setContainer_id(String container_id) {
        this.container_id = container_id;
    }

    public int getSite_id() {
        return site_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public String getContainer_id() {
        return container_id;
    }
}
