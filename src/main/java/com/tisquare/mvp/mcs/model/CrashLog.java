package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tisquare.mvp.mcs.db.domain.DevCrashLog;

/**
 * Created by jhkim on 14. 11. 10.
 */
public class CrashLog {

    String mdn;
    String iuid;
    String app_version;
    String os_version;
    String dev_model;
    String category;
    String stack_trace;

    public CrashLog(DevCrashLog devCrashLog )
    {
        this.mdn = devCrashLog.getMdn();
        this.iuid = devCrashLog.getIuid();
        this.app_version = devCrashLog.getApp_version();
        this.os_version = devCrashLog.getOs_ver();
        this.dev_model = devCrashLog.getDev_model();
        this.category = devCrashLog.getCategory();
        this.stack_trace = devCrashLog.getStack_trace();
    }
    public String getMdn() { return mdn; }
    public void setMdn(String mdn) { this.mdn = mdn; }
    public String getIuid() { return iuid; }
    public void setIuid(String iuid) { this.iuid = iuid; }
    public String getApp_version() { return app_version; }
    public void setApp_version(String app_version) { this.app_version = app_version; }

    public String getOs_ver() { return os_version; }
    public void setOs_ver(String os_ver) { this.os_version = os_ver; }
    public String getDev_model() { return dev_model; }
    public void setDev_model(String dev_model) { this.dev_model = dev_model; }
    public String getCategory() { return category; }
    public void setCategory(String category) { this.category = category; }
    public String getStack_trace() { return stack_trace; }
    public void setStack_trace(String stack_trace) { this.stack_trace = stack_trace; }
}
