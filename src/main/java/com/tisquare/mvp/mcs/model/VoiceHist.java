package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class VoiceHist {

    @JsonProperty("ch-name")
    private String ch_name;

    @JsonProperty("sid")
    private Long sid;

    @JsonProperty("stime")
    private Long stime;

    @JsonProperty("etime")
    private Long etime;

    @JsonProperty("stimestamp")
    private Long stimestamp;

    @JsonProperty("etimestamp")
    private Long etimestamp;

    @JsonProperty("bss-info")
    private Bssinfo bssinfo;

    public void setCh_name(String ch_name) {
        this.ch_name = ch_name;
    }

    public String getCh_name() {
        return ch_name;
    }

    public void setStimestamp(Long stimestamp) {
        this.stimestamp = stimestamp;
    }

    public void setEtimestamp(Long etimestamp) {
        this.etimestamp = etimestamp;
    }

    public Long getStimestamp() {
        return stimestamp;
    }

    public Long getEtimestamp() {
        return etimestamp;
    }

    public void setBssinfo(Bssinfo bssinfo) {
        this.bssinfo = bssinfo;
    }

    public Bssinfo getBssinfo() {
        return bssinfo;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public void setStime(Long stime) {
        this.stime = stime;
    }

    public void setEtime(Long etime) {
        this.etime = etime;
    }

    public Long getSid() {
        return sid;
    }

    public Long getStime() {
        return stime;
    }

    public Long getEtime() {
        return etime;
    }

    @Override
    public String toString() {
        if(this == null)
            return null;
        final StringBuilder sb = new StringBuilder("VoiceHist:{");
        sb.append("sid='").append(this.sid).append('\'');
        sb.append("stime='").append(this.stime).append('\'');
        sb.append(", etime='").append(this.etime).append('\'');
        sb.append('}');
        return sb.toString();
    }
}