package com.tisquare.mvp.mcs.model.pc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PcAuthInfo {

    @JsonProperty("iuid")
    public Long iuid;

    @JsonProperty("type")
    public Integer type;

    @JsonProperty("name")
    public String name;

    @JsonProperty("mdn")
    public String mdn;

    @JsonProperty("use-mdn")
    public String use_mdn;

    @JsonProperty("birth")
    public Long birth;

    @JsonProperty("user-id")
    public String user_id;

    @JsonProperty("user-pwd")
    public String user_pwd;

    @JsonProperty("cert-no")
    public String cert_no;

    @JsonProperty("user-info")
    PcAuthInfo pcAuthInfo;

    public PcAuthInfo()
    {

    }

    public PcAuthInfo( IdentifyPasswd identifyPasswd)
    {
        this.user_id = identifyPasswd.getUser_id();
        this.cert_no = identifyPasswd.getCert_no();
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setPcAuthInfo(PcAuthInfo pcAuthInfo) {
        this.pcAuthInfo = pcAuthInfo;
    }

    public PcAuthInfo getPcAuthInfo() {
        return pcAuthInfo;
    }

    public void setIuid(Long iuid) {
        this.iuid = iuid;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public void setUse_mdn(String use_mdn) {
        this.use_mdn = use_mdn;
    }

    public void setBirth(Long birth) {
        this.birth = birth;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public Long getIuid() {
        return iuid;
    }

    public Integer getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getMdn() {
        return mdn;
    }

    public String getUse_mdn() {
        return use_mdn;
    }

    public Long getBirth() {
        return birth;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_pwd() {
        return user_pwd;
    }



    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{ ");

        Map<String, Object> map = new TreeMap<>();
        for (int i = 0; i < getClass().getDeclaredFields().length; i++) {
            Field f = getClass().getDeclaredFields()[i];

            if (!Modifier.isStatic(f.getModifiers())) {
                try {
                    if (f.get(this) != null) {
                        map.put(f.getName(), f.get(this));
                    }
                } catch (IllegalAccessException e) {
                    // pass, don't print
                }
            }
        }

        if (!CollectionUtils.isEmpty(map)) {
            int i = 0;
            for (String key : map.keySet()) {
                if (i > 0 && i < map.size()) {
                    sb.append(", ");
                }
                sb.append(key).append("=").append(map.get(key));
                i++;
            }
        }
        sb.append(" }");
        return sb.toString();
    }



}