package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class MultiMsgInfo {

    @JsonProperty("msg-info")
    private MsgInfo msgInfo;

    @JsonProperty("bss-info")
    private Bssinfo bssinfo;

    public MsgInfo getMsgInfo() {
        return msgInfo;
    }

    public Bssinfo getBssinfo() {
        return bssinfo;
    }
}