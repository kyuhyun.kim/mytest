package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.entity.EventPopup;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class NoticeListBody {
    public static final int FLAG_IS_NEW = 0;       // 확인 공지.
    public static final int FLAG_IS_NOT_NEW = 1;   // 미 확인 공지.

    public static final int FLAG_IS_NOT_END = 0;   // 마지막 공지 사항 미 포함.
    public static final int FLAG_IS_END = 1;       // 마지막 공시 사항 포함.

    public static final int FLAG_IS_SUCCESS = 0;   // 조회 성공.
    public static final int FLAG_IS_FAIL = 1;      // 조회 실패.



    @JsonProperty("result-code")
    private int result_code;

    @JsonProperty("remain-cnt")
    private int remain_cnt;

    @JsonProperty("totcnt")
    private int totcnt;

    @JsonProperty("is-end")
    private int is_end;

    @JsonProperty("items")
    List<NoticeJh> items;


    public static int getFlagIsNew() {
        return FLAG_IS_NEW;
    }

    public static int getFlagIsNotNew() {
        return FLAG_IS_NOT_NEW;
    }

    public static int getFlagIsNotEnd() {
        return FLAG_IS_NOT_END;
    }

    public static int getFlagIsEnd() {
        return FLAG_IS_END;
    }

    public int getResult_code() {
        return result_code;
    }

    public int getRemain_cnt() {
        return remain_cnt;
    }

    public int getTotcnt() {
        return totcnt;
    }

    public int getIs_end() {
        return is_end;
    }

    public List<NoticeJh> getItems() {
        return items;
    }



    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }

    public void setRemain_cnt(int remain_cnt) {
        this.remain_cnt = remain_cnt;
    }

    public void setTotcnt(int totcnt) {
        this.totcnt = totcnt;
    }

    public void setIs_end(int is_end) {
        this.is_end = is_end;
    }

    public void setItems(List<NoticeJh> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("result-code=").append(result_code);
        sb.append(", remain-cnt=").append(remain_cnt);
        sb.append(", totcnt=").append(totcnt);
        sb.append(", is-end=").append(is_end);
        sb.append("]");
        return sb.toString();
    }



}