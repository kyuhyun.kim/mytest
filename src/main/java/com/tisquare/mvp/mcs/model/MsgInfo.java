package com.tisquare.mvp.mcs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class MsgInfo {

    @JsonProperty("dstuser")
    private String dstuser;

    @JsonProperty("dstusers")
    private String[] dstusers;

    @JsonProperty("msgtype")
    private String msgtype;

    @JsonProperty("callback")
    private String callback;

    @JsonProperty("text")
    private String text;

    public  MsgInfo()
    {
    }

    public MsgInfo(MsgInfo msgInfo)
    {
        this.dstuser = msgInfo.getDstuser();
        this.dstusers = msgInfo.getDstusers();
        this.msgtype = msgInfo.getMsgtype();
        this.callback = msgInfo.getCallback();
        this.text = msgInfo.getText();
    }

    public  MsgInfo(MultiMsgInfo multiMsgInfo)
    {
        this.dstuser = multiMsgInfo.getMsgInfo().getDstuser();
        this.dstusers = multiMsgInfo.getMsgInfo().getDstusers();
        this.msgtype = multiMsgInfo.getMsgInfo().getMsgtype();
        this.callback = multiMsgInfo.getMsgInfo().getCallback();
        this.text = multiMsgInfo.getMsgInfo().getText();
    }

    public void setDstuser(String dstuser) {
        this.dstuser = dstuser;
    }

    public void setDstusers(String[] dstusers) {
        this.dstusers = dstusers;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDstuser() {
        return dstuser;
    }

    public String[] getDstusers() {
        return dstusers;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public String getCallback() {
        return callback;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        if(this == null)
            return null;
        final StringBuilder sb = new StringBuilder("MsgInfo:{");
        sb.append("dstuser='").append(this.dstuser).append('\'');
        sb.append("dstusers=[");
        for(int nIdx = 0 ; nIdx < this.dstusers.length; nIdx++)
        {
            if(nIdx != 0)
                sb.append(",");
            sb.append("'").append(dstusers[nIdx]).append("'");
        }
        sb.append("],");
        sb.append("msgtype='").append(this.msgtype).append('\'');
        sb.append(", callback='").append(this.callback).append('\'');
        sb.append(", text='").append(this.text).append('\'');
        sb.append('}');
        return sb.toString();
    }
}