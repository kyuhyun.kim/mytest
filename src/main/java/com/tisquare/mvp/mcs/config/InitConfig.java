package com.tisquare.mvp.mcs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by jhkim on 14. 3. 3.
 */
@Configuration
@PropertySource("classpath:context.properties")
public class InitConfig {
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Value("${DB}")
    private String db;

    @Value("${LIST_NUM}")
    private String listNum;

    @Value("${SERVICE}")
    private String service;

    @Value("${SHORT_URL_PFX_DAMIN}")
    private String urlshortPfxDomain;

    @Value("${URL_PFX_DOMAIN}")
    private String urlPfxDomain;

    @Value("${URL_HELP_DOMAIN}")
    private String urlHelpDomain;

    @Value("${URL_DL_DOMAIN}")
    private String URL_DL_DOMAIN;

    @Value("${BSS_URL_AGREEMENT_DOMAIN}")
    private String url_AgreeDomain_bss;

    @Value("${BSS_URL_PRIVACY_DOMAIN}")
    private String url_Privacy_bss;

    @Value("${IOS_URL_AD_DOMAIN}")
    private String url_Ad_ios;

    @Value("${IOS_URL_TH_DOMAIN}")
    private String url_Th_ios;

    @Value("${IOS_URL_OSS_DOMAIN}")
    private String url_oss_ios;

    @Value("${URL_PRIVACY_OPTION_DOMAIN}")
    private String url_privacy_option_domain;

    @Value("${URL_PRIVACY_MANDATORY_DOMAIN}")
    private String url_privacy_mandatory_domain;

    @Value("${URL_PRIVACY_PAST_DOMAIN}")
    private String url_privacy_past_domain;

    @Value("${URL_DOMAIN}")
    private String url_domain;

    @Value("${URLMAP_DEFAULT_EXPIRE_VAL}")
    private Integer urlDefExpireVal;

    @Value("${URL_AGREEMENT_DOMAIN}")
    private String url_AgreeDomain;

    @Value("${URL_PRIVACY_DOMAIN}")
    private String url_Privacy;

    @Value("${URL_AD_DOMAIN}")
    private String url_Ad;

    @Value("${URL_TH_DOMAIN}")
    private String url_Th;

    @Value("${URL_OSS_DOMAIN}")
    private String url_oss;

    @Value("${URL_LOCATION_DOMAIN}")
    private String url_location;

    @Value("${trace.log.base}")
    private String trace_log_base;

    @Value("${trace.log.file.name}")
    private String trace_log_file_name;


    @Value("${event.log.base}")
    private String eventlog_base;

    @Value("${realtime.log.base}")
    private String realtime_log_base;

    @Value("${ems.log.svc.svrkey}")
    private  String ems_log_svc_svrkey;

    @Value("${SCS.WITHDRAW.URL}")
    private String scs_withdraw_url;

    @Value("${activeUser.compare.date}")
    private int activeUserCompareDate;

    @Value("${activeUser.trace.date}")
    private int activeUserTraceDate;

    @Value("${MAX_FCS_IDX}")
    private int MAX_FCS_IDX;

/*    @Value("${ems.log.svc.svrkey}")
    private String ems_log_svc_key;

    @Value("${ems.log.svc.svr_instance}")
    private String ems_log_svc_svr_instance;*/

    @Value("${PROCCESS_NAME}")
    private String process_name;

  /*  public String getEms_log_svc_key() {
        return ems_log_svc_key;
    }

    public String getEms_log_svc_svr_instance() {
        return ems_log_svc_svr_instance;
    }
*/
    public String getUrl_location() {
        return url_location;
    }



    public String getEventlog_base() {
        return eventlog_base;
    }


    public String getRealtime_log_base() {
        return realtime_log_base;
    }

    public String getEms_log_svc_svrkey() {
        return ems_log_svc_svrkey;
    }

    public String getURL_DL_DOMAIN() {
        return URL_DL_DOMAIN;
    }

    public String getProcess_name() {
        return process_name;
    }

    public String getUrl_domain() {
        return url_domain;
    }

    public String getUrl_privacy_past_domain() {
        return url_privacy_past_domain;
    }

    public String getUrl_privacy_option_domain() {
        return url_privacy_option_domain;
    }

    public String getUrl_privacy_mandatory_domain() {
        return url_privacy_mandatory_domain;
    }


    public String getUrl_AgreeDomain_bss() {
        return url_AgreeDomain_bss;
    }

    public String getUrl_Privacy_bss() {
        return url_Privacy_bss;
    }

    public String getUrl_Ad_ios() {
        return url_Ad_ios;
    }

    public String getUrl_Th_ios() {
        return url_Th_ios;
    }

    public String getUrl_oss_ios() {
        return url_oss_ios;
    }

    public int getMAX_FCS_IDX() {
        return MAX_FCS_IDX;
    }

    public int getActiveUserCompareDate() {
        return activeUserCompareDate;
    }

    public int getActiveUserTraceDate() {
        return activeUserTraceDate;
    }

    public String getUrl_oss() {
        return url_oss;
    }

    public String getScs_withdraw_url() {
        return scs_withdraw_url;
    }


    public String getUrlshortPfxDomain() {
        return urlshortPfxDomain;
    }

    public String getTrace_log_base() {
        return trace_log_base;
    }

    public String getTrace_log_file_name() {
        return trace_log_file_name;
    }

    public String getUrl_Privacy() {
        return url_Privacy;
    }

    public String getUrl_Ad() {
        return url_Ad;
    }

    public String getUrl_Th() {
        return url_Th;
    }

    public String getService() {
        return service;
    }

    public String getListNum() {
        return listNum;
    }

    public String getDb() {
        return db;
    }

    public String getUrlPfxDomain() { return urlPfxDomain; }
    public Integer getUrlDefExpireVal() { return urlDefExpireVal; }

    public String getUrlHelpDomain() {
        return urlHelpDomain;
    }

    public String getUrl_AgreeDomain() {
        return url_AgreeDomain;
    }
}
