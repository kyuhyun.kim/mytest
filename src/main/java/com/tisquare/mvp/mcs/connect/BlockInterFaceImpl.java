package com.tisquare.mvp.mcs.connect;


import com.tisquare.mvp.mcs.model.WithdrawInfo;
import org.apache.http.Header;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kimkyuhyun on 2015-05-12.
 */
@Service
public class BlockInterFaceImpl implements BlockInterFace {

    private static final Logger logger = LoggerFactory.getLogger(BlockInterFaceImpl.class);
    private final static String LINE = "\r\n";



    ObjectMapper mapper = new ObjectMapper();



    @Override
    public void HttpPostRequestLogPrint(String mdn, HttpPost httpPost)
    {
        Header[] headers = httpPost.getAllHeaders();

        String Print = LINE + "--------------------------------------------------------------------------------" + LINE +
                httpPost.toString()+LINE;
        for(int idx = 0; idx < headers.length; idx++)
        {
            Print += headers[idx].getName() +":" + headers[idx].getValue() + LINE;
        }

        Print += "================================================================================" +LINE;
        logger.info(Print);
    }




    @Override
    public void HttpGetRequestLogPrint(String mdn, HttpGet httpGet)
    {
        Header[] headers = httpGet.getAllHeaders();

        String Print = LINE + "--------------------------------------------------------------------------------" + LINE +
                httpGet.toString()+LINE;
        for(int idx = 0; idx < headers.length; idx++)
        {
            Print += headers[idx].getName() +":" + headers[idx].getValue() + LINE;
        }

        Print += "================================================================================" +LINE;

        logger.info(Print);

//        if(traceService.isTrace())
//            traceService.printTraceLog(mdn, Print);
    }
    @Override
    public void ScsInterFaceWithdraw(WithdrawInfo withdrawInfo, String uri)
    {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpPost httpPost = new HttpPost(uri);

        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setSocketTimeout(10000).build();
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-type", "application/json;charset=UTF-8");
        httpPost.addHeader("TN-REQ-USER",    withdrawInfo.getTn_req_user());
        httpPost.addHeader("TN-IUID",        withdrawInfo.getTn_iuid());
        httpPost.addHeader("TN-AUTH-KEY",    withdrawInfo.getTn_auth_key());


        HttpPostRequestLogPrint(withdrawInfo.getTn_req_user(), httpPost);


        try {

            CloseableHttpResponse response = httpClient.execute(httpPost);
            InputStreamReader is = new InputStreamReader(response.getEntity().getContent());
            BufferedReader br = new BufferedReader(is);
            String read = null;
            StringBuffer sb = new StringBuffer();
            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            is.close();
            response.close();
            httpPost.releaseConnection();

            String Message = LINE + "--------------------------------------------------------------------------------" + LINE +
                    response.toString()+LINE+
                    "================================================================================" +
                    LINE;


            logger.info(Message);

        }catch(Exception e){
            logger.error("[MCS -> SCS]",e);
        }
    }








}
