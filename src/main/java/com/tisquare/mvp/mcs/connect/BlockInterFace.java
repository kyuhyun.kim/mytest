package com.tisquare.mvp.mcs.connect;


import com.tisquare.mvp.mcs.model.WithdrawInfo;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

/**
 * Created by kimkyuhyun on 2015-05-12.
 */
public interface BlockInterFace {

    public void ScsInterFaceWithdraw(WithdrawInfo withdrawInfo, String uri);

    public void HttpPostRequestLogPrint(String mdn, HttpPost httpPost);

    public void HttpGetRequestLogPrint(String mdn, HttpGet httpGet);

}
