package com.tisquare.mvp.mcs.service;


import ch.qos.logback.classic.Logger;
import com.tisquare.mvp.mcs.model.TraceReq;
import org.springframework.http.ResponseEntity;

import java.util.Collection;

/**
 * 트레이스 서비스
 *
 * @author leejs
 * @since 2015-07-23
 */
public interface TraceService {
    /**
     * 로그 추적을 시작한다. 이미 시작 중일 경우 기존 정보를 초기화 후 새로운 정보로 반영한다.
     *
     * @param traceReq 요청 정보
     */
    ResponseEntity<String> startTrace(TraceReq traceReq);

    /**
     * 로그 추적을 중지한다.
     */
    void stopTrace();

    /**
     * 실행 여부를 가져온다.
     *
     * @return 실행 여부
     */
    boolean isTrace();

    /**
     * Trace 전용 Logger 를 가져온다.
     *
     * @return logger
     */
    Logger getTraceLogger();

    /**
     * 추첮ㄱ 대상 전화번호를 가져온다.
     *
     * @return 대상 전화번호
     */
    Collection<String> getTraceTarget();



    void printTraceLog(String mdn, String message);
}
