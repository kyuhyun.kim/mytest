package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.NoticeBody;
import com.tisquare.mvp.mcs.model.NoticeListBody;
import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import org.springframework.http.HttpStatus;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface BssNoticeControl {

    public HttpStatus NoticeList(NoticeListBody noticeListBody, NoticeReqHeaders noticeReqHeaders)
            throws Exception;

    public HttpStatus Notice(NoticeBody noticeBody, int sid, String telecomCode)
            throws Exception;

}
