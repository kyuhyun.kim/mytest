package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.domain.PromotionSamsung;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.PromotionControl;
import com.tisquare.mvp.mcs.service.PromotionService;
import com.tisquare.mvp.mcs.service.ResponseEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class PromotionServiceImpl extends ResponseEntityService implements PromotionService {

    private static final Logger logger = LoggerFactory.getLogger(PromotionServiceImpl.class);

    @Autowired
    private InitConfig config;


    @Autowired
    private PromotionControl promotionControl;


    public ResponseEntity<String> processPromotionCampPackInfoReg(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionCampPackInfo promotionCampPackInfo)
    {
        try
        {

            HttpStatus httpStatus = promotionControl.PromotionCampPackReg(promotionCampPackInfo, authReqHeaders, authReqParams);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }
            else if (httpStatus == HttpStatus.OK){
                return makeResponseEntityWithCheckBodySize(request, promotionCampPackInfo, httpStatus);
            }
            else
                return makeResponseEntity(httpStatus);


        } catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processPromotionCampPackInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionCampPackInfo promotionCampPackInfo )
    {

        try
        {

            HttpStatus httpStatus = promotionControl.PromotionCampPackInfo(promotionCampPackInfo, authReqHeaders, authReqParams);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }
            else if (httpStatus == HttpStatus.OK){
                return makeResponseEntityWithCheckBodySize(request, promotionCampPackInfo, httpStatus);
            }
            else
                return makeResponseEntity(httpStatus);


        } catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processPromotionSamSungInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
    {

        PromotionSamsungInfo promotionSamsungInfo = new PromotionSamsungInfo();

        try
        {

            HttpStatus httpStatus = promotionControl.PromotionSamSungInfo(promotionSamsungInfo, authReqHeaders, authReqParams);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }
            else if (httpStatus == HttpStatus.OK || httpStatus == HttpStatus.METHOD_NOT_ALLOWED){
                return makeResponseEntityWithCheckBodySize(request, promotionSamsungInfo, httpStatus);
            }
            else
                return makeResponseEntity(httpStatus);


        } catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    public ResponseEntity<String> processPromotionSamSungRegRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung)
    {

        try
        {


            HttpStatus httpStatus = promotionControl.PromotionSamSungReg( authReqHeaders, authReqParams, promotionSamsung);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if(httpStatus == HttpStatus.METHOD_NOT_ALLOWED)
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            else
                return makeResponseEntityWithCheckBodySize(request, promotionSamsung, httpStatus);


        } catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    public ResponseEntity<String> processPromotionPcListRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        try
        {

            PromotionList promotionList = new PromotionList();

            HttpStatus httpStatus = promotionControl.PromotionPcList(authReqHeaders, authReqParams, promotionList);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if(httpStatus == HttpStatus.METHOD_NOT_ALLOWED)
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            else
                return makeResponseEntityWithCheckBodySize(request, promotionList, httpStatus);


        } catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    public ResponseEntity<String> processPromotionListRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        try
        {

            PromotionList promotionList = new PromotionList();

            HttpStatus httpStatus = promotionControl.PromotionList( authReqHeaders, authReqParams, promotionList);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if(httpStatus == HttpStatus.METHOD_NOT_ALLOWED)
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            else
                return makeResponseEntityWithCheckBodySize(request, promotionList, httpStatus);


        } catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processPromotionSamSungOemRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung)
    {
        try
        {


            HttpStatus httpStatus = promotionControl.PromotionSamSungOem( authReqHeaders, authReqParams, promotionSamsung);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if(httpStatus == HttpStatus.METHOD_NOT_ALLOWED)
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            else
                return makeResponseEntityWithCheckBodySize(request, promotionSamsung, httpStatus);


        } catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processPromotionListRequest(HttpServletRequest request, String mdn, String os_type, String osVersion, String telecomCode )
    {
        PromotionListBody promotionListBody = new PromotionListBody();

        try
        {

            HttpStatus httpStatus = promotionControl.PromotionList(promotionListBody, mdn, os_type, osVersion, telecomCode);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            } else if(httpStatus == HttpStatus.NOT_FOUND){
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }
            else {
                return makeResponseEntityWithCheckBodySize(request, promotionListBody, httpStatus);
            }


        } catch (Exception e) {
        logger.error(e.toString());
        return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}


}
