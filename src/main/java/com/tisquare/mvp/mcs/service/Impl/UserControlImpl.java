package com.tisquare.mvp.mcs.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.connect.BlockInterFaceImpl;
import com.tisquare.mvp.mcs.db.*;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.db.domain.HelpNoticeCnt;
import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.entity.EventPopup;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.ActiveUserService;
import com.tisquare.mvp.mcs.service.IPCControl;
import com.tisquare.mvp.mcs.service.TraceService;
import com.tisquare.mvp.mcs.service.UserControl;
import com.tisquare.mvp.mcs.type.OS_TYPE;
import com.tisquare.mvp.mcs.type.SUBS_TYPE;
import com.tisquare.mvp.mcs.type.SubscriberNewType;
import com.tisquare.mvp.mcs.type.Telecom_code;
import com.tisquare.mvp.mcs.utils.RandomString;
import com.tisquare.mvp.mcs.utils.ValidCheck;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class UserControlImpl implements UserControl {

    private static final Logger logger = LoggerFactory.getLogger(UserControlImpl.class);

    private final static String LINE = "\r\n";

    private static int FCS_IDX =1;

    @Autowired
    private InitConfig config;

    @Autowired
    SystemInfoDao systemInfoDao;

    @Autowired
    ClientDao clientDao;

    @Autowired
    WebdbDao webdbDao;

    @Autowired
    ServerDao serverDao;

    @Autowired
    WebViewDao webViewDao;

    @Autowired
    PresenceDao presenceDao;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    TraceService traceService;

    @Autowired
    BlockInterFaceImpl blockInterFace;

    @Autowired
    IPCControl ipcControl;

    @Autowired
    BtinfoDao btinfoDao;


    ObjectMapper mapper = new ObjectMapper();



    public HttpStatus
    Install(AuthBody authBody,
                              String phoneNo,
                              String authKey,
                              String tnVersion,
                              UserDevice userDevice,
                              Long nid,
                              Long hid,
                              Integer storeType,
                              String deviceModel,
                              String telecomCode
                              ) throws Exception {


         /* 신규 공지 방식 변경 */
        EventPopup eventPopup = systemInfoDao.getEventPopup();
        if (eventPopup != null) {
            authBody.setEventPopup(eventPopup);

            int count = systemInfoDao.isSystemUnavailable();

            if(count > 0) {
                 /* 공지 시간 OK & 작업 시간은 OK */
                authBody.getEventPopup().setForceStop(EventPopup.FORCE_UPDATE);
                logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                return HttpStatus.SERVICE_UNAVAILABLE;

            }
        }


        UserInfo userInfo  = getUserInfoByIuidOrE164PhoneNo(phoneNo);

        if (userInfo == null) {
            authBody.setIs_subs(SubscriberNewType.IS_SUB_NEW.value());
        }
        else
        {
            /*if(!StringUtils.equals(userInfo.getTelecomCode(), telecomCode))
            {
                authBody.setIs_subs(SubscriberNewType.IS_SUB_NEW.value());
            }
            else
            {*/
                authBody.setIs_subs(SubscriberNewType.IS_SUB_REINSTALL.value());
                authBody.setAuthKey(userInfo.getCauthKey());
                authBody.setIuid(userInfo.getIuid());
            /*}*/
        }

        if (storeType == null) {
            storeType = userInfo.getStoreType();
            if (storeType == null) {
                storeType = 0;
            }
        }


        AppUpdatetInfo appUpdatetInfo = systemInfoDao.getAppUpdate(storeType, SUBS_TYPE.B2C.getCode());

        if (appUpdatetInfo != null) {
            authBody.setApp_update(appUpdatetInfo);
            if (appUpdatetInfo.needForceUpdate(tnVersion)) {
                logger.error("Not valid app version");
                appUpdatetInfo.setForceUpdate(appUpdatetInfo.FORCE_UPDATE);
                authBody.setApp_update(appUpdatetInfo);
                return HttpStatus.NOT_ACCEPTABLE;
            }
        }



       /* if (nid == null) {
            nid = 0L;
        }

        NoticeInfo noticeInfo = new NoticeInfo();
        Long lastNid;*/

        if(mcsConfig.getProperty("TELECOM.LIMIT").equals("1")) {
        /* telecom code check */
            switch (telecomCode.trim().substring(0, 3)) {
                case Telecom_code.TELECOM_CODE:
                    break;
                default:
                    return HttpStatus.BAD_REQUEST;
            }
        }


        if( FCS_IDX > config.getMAX_FCS_IDX())
            FCS_IDX = 1;

        authBody.setServerList(serverDao.findServers3(FCS_IDX++ ));

        StringBuilder sbUrlFix = new StringBuilder(config.getUrl_domain());
        for(int nIdx = 0; nIdx < authBody.getServerList().size(); nIdx++)
        {
            if(authBody.getServerList().get(nIdx).getName().toUpperCase().equals("APACHE"))
            {
                sbUrlFix.setLength(0);
                sbUrlFix.append(authBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toLowerCase());sbUrlFix.append("://");
                sbUrlFix.append(authBody.getServerList().get(nIdx).getConnections().get(0).getIpAddr().toLowerCase());sbUrlFix.append(":");
                sbUrlFix.append(authBody.getServerList().get(nIdx).getConnections().get(0).getPort());sbUrlFix.append("/");
                sbUrlFix.append(config.getProcess_name().toLowerCase());sbUrlFix.append("/");
            }
        }

        UrlInfo urlInfo = new UrlInfo();
        switch (userDevice.getOsType().toLowerCase())
        {
            case OS_TYPE.OS_TYPE_IOS:
                break;
            default:

                  /*  certBody.setBt_info(BtListGet(mcsConfig.getProperty("bt_info")));*/
                urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain());
                urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad());
                urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy());
                urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th());
                urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss());
                break;

            /*case OS_TYPE.OS_TYPE_IOS:
                urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain_ios());
                urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad_ios());
                urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy_ios());
                urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th_ios());
                urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss_ios());
                break;
            default:
                return HttpStatus.BAD_REQUEST;*/
        }

        urlInfo.setPrivacy_op_info(sbUrlFix.toString() + config.getUrl_privacy_option_domain());
        urlInfo.setPrivacy_md_info(sbUrlFix.toString() + config.getUrl_privacy_mandatory_domain());
        urlInfo.setLocation_info(sbUrlFix.toString() + config.getUrl_location());

        authBody.setUrl_info(urlInfo);

        return HttpStatus.OK;

    }

    public String HttpGetRequestLogPrint(String mdn, HttpGet httpGet)
    {
        Header[] headers = httpGet.getAllHeaders();

        String Print = LINE + "--------------------------------------------------------------------------------" + LINE +
                httpGet.toString()+LINE;
        for(int idx = 0; idx < headers.length; idx++)
        {
            Print += headers[idx].getName() +":" + headers[idx].getValue() + LINE;
        }

        Print += "================================================================================" +LINE;

        return Print;

    }




    public List<BtInfo> BtListGet(String ConfigName)
    {
        try {


            List<BtInfo> Bt = new ArrayList<>() ;

            String[] bt_info = ConfigName.split("\\|");
            for (int svr_idx = 0; svr_idx < bt_info.length; svr_idx++)
            {
                String[] bt_detail = bt_info[svr_idx].split("-");

                BtInfo btInfo = new BtInfo(bt_detail[0], bt_detail[1], bt_detail[2], mcsConfig.getProperty("bt_url"));
                Bt.add(btInfo);
            }
            return Bt;

        }catch (Exception e)
        {
            return  null;
        }

    }

    public HttpStatus Presence_info(Presence presence)
            throws Exception
    {
        String cAuth_Key = clientDao.selectIndividualClientIuid(presence);

        if(cAuth_Key == null)
        {
            return HttpStatus.NOT_FOUND;
        }
        else
        {
            if(!cAuth_Key.equals(presence.getcAuthKey()))
            {
                return HttpStatus.FORBIDDEN;
            }
            else
            {
                Integer presence_status = presenceDao.selectPresence(presence);
                if(presence_status == null)
                    return HttpStatus.NOT_FOUND;
                else
                {
                    presence.setPresence( presence_status);
                }
            }
        }
        return HttpStatus.OK;
    }

    public HttpStatus Presence( Presence presence)
            throws Exception
    {


        String cAuth_Key = clientDao.selectIndividualClientIuid(presence);
        if(cAuth_Key == null)
        {
            return HttpStatus.NOT_FOUND;
        }
        else
        {
            if(!cAuth_Key.equals(presence.getcAuthKey()))
            {
                return HttpStatus.FORBIDDEN;
            }

        }

        presenceDao.updatePresence(presence);

        return HttpStatus.OK;


    }

    public HttpStatus SetMode( String phoneNo,
                               Long iuid,
                               String authKey,
                               Integer mode)
            throws Exception
    {
        UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(phoneNo, iuid);


        try {
            if (userInfo != null) {
                // 폰번호, DEVICE ID, 이통사 정보 확인
                if (!StringUtils.equals(userInfo.getPhoneNo(), phoneNo)) {
                    logger.error("Request Mdn AND DB Mdn MisMatch");
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
                else  if( !userInfo.getIuid().equals(iuid) )
                {
                    logger.error("Request iuid AND DB iuid MisMatch");
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
                else if(!authKey.equals(userInfo.getCauthKey()))
                {
                    logger.error("Request AuthKey MisMatch");
                    return HttpStatus.FORBIDDEN;
                }
            } else {

                return HttpStatus.NOT_FOUND;
            }

            EventPopup eventPopup = systemInfoDao.getEventPopup();
            if (eventPopup != null) {

                if(eventPopup.getForceStop() == eventPopup.FORCE_UPDATE){
                    logger.debug("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                    return HttpStatus.SERVICE_UNAVAILABLE;
                }

            }


            Map<String, Object> map = new HashMap<String, Object>();
            map.put("iuid", String.valueOf(iuid));
            map.put("twt_mode", String.valueOf(mode));

            clientDao.updateIndividualTwtMode(map);

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }


    public HttpStatus
    Cert(CertBody certBody,
                              String phoneNo,
                              Long iuid,
                              String authKey,
                              String tnVersion,
                              UserDevice userDevice,
                              Long nid,
                              Long hid,
                              Integer storeType,
                              String deviceModel,
                              String telecomCode,
                              String deviceId
    ) throws Exception {


        /* 신규 공지 방식 변경 */
        EventPopup eventPopup = systemInfoDao.getEventPopup();
        if (eventPopup != null) {
            certBody.setEventPopup(eventPopup);

            int count = systemInfoDao.isSystemUnavailable();

            if(count > 0) {
                 /* 공지 시간 OK & 작업 시간은 OK */
                certBody.getEventPopup().setForceStop(EventPopup.FORCE_UPDATE);
                logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                return HttpStatus.SERVICE_UNAVAILABLE;

            }
        }

        UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(phoneNo, iuid);

        try {
            if (userInfo != null) {

                HttpStatus httpStatus = ValidCheck.MisMatchCheck(userInfo, phoneNo, iuid, authKey, telecomCode);
                if(httpStatus != HttpStatus.OK)
                    return httpStatus;

                /*if (userInfo.getPhoneNo() == null || !userInfo.getPhoneNo().equals(phoneNo)) {
                    logger.error("Request Mdn AND DB Mdn MisMatch");
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
                else if (!StringUtils.equals(userInfo.getTelecomCode(), telecomCode)) {
                    logger.error("Request Telecome_code AND DB Telecom_code MisMatch");
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
                else  if(!userInfo.getIuid().equals(iuid) )
                {
                    logger.error("Request iuid AND DB iuid MisMatch");
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
                else if(!StringUtils.equals(authKey, userInfo.getCauthKey()))
                {
                    logger.error("Request AuthKey MisMatch");
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }*/

            } else {

                return HttpStatus.NOT_FOUND;
            }

            certBody.setAuthKey(userInfo.getCauthKey());
            certBody.setPtoken(userInfo.getPnotiToken());

            /* CLIENT 정보 변경 여부 확인 후 변경 정보를 업데이트 한다 */
            if(!ValidCheck.InputParamChgCheck(userInfo,phoneNo, iuid, authKey, tnVersion, userDevice, nid, hid, storeType, deviceModel, telecomCode, deviceId ))
            {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("iuid", String.valueOf(iuid));
                map.put("app_version", tnVersion);
                map.put("os_type", userDevice.getOsType());
                map.put("os_version", userDevice.getOsVersion());
                map.put("device_id", userDevice.getDeviceID());
                map.put("device_chg_f", "0");
                clientDao.updateUserInfo(map);
            }

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("iuid", String.valueOf(iuid));
            clientDao.updateUserCert(map);


            if (storeType == null) {
                storeType = userInfo.getStoreType();
                if (storeType == null) {
                    storeType = 0;
                }
            }

            AppUpdatetInfo appUpdatetInfo = systemInfoDao.getAppUpdate(storeType,  SUBS_TYPE.B2C.getCode());

            if (appUpdatetInfo != null) {
                certBody.setApp_update(appUpdatetInfo);
                if (appUpdatetInfo.needForceUpdate(tnVersion)) {
                    logger.error("Not valid app version");
                    appUpdatetInfo.setForceUpdate(AppUpdatetInfo.FORCE_UPDATE);
                    certBody.setApp_update(appUpdatetInfo);
                    return HttpStatus.NOT_ACCEPTABLE;
                }
            }

            if (userInfo == null) {
                return HttpStatus.FORBIDDEN;
            }



            if (nid == null) {nid = 0L;}
            if (hid == null) {hid = 0L;}

            Notice notice = new Notice();
            Long lastNid;

            if(mcsConfig.getProperty("TELECOM.LIMIT").equals("1")) {
                /* telecom code check */
                switch (telecomCode.trim().substring(0, 3)) {
                    case Telecom_code.TELECOM_CODE:
                        break;
                    default:
                        return HttpStatus.BAD_REQUEST;
                }
            }

            if( FCS_IDX == config.getMAX_FCS_IDX())
                FCS_IDX = 0;

            certBody.setServerList(serverDao.findServers3(FCS_IDX++));

            StringBuilder sbUrlFix = new StringBuilder(config.getUrl_domain());
            for(int nIdx = 0; nIdx < certBody.getServerList().size(); nIdx++)
            {
                if(certBody.getServerList().get(nIdx).getName().toUpperCase().equals("WS") && certBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toUpperCase().equals("HTTPS"))
                {

                        sbUrlFix.setLength(0);
                        sbUrlFix.append(certBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toLowerCase());
                        sbUrlFix.append("://");
                        sbUrlFix.append(certBody.getServerList().get(nIdx).getConnections().get(0).getIpAddr().toLowerCase());
                        sbUrlFix.append(":");
                        sbUrlFix.append(certBody.getServerList().get(nIdx).getConnections().get(0).getPort());
                        sbUrlFix.append("/");
                        sbUrlFix.append(config.getProcess_name().toLowerCase());
                        sbUrlFix.append("/");

                }
            }


            /* OS_TYPE 체크 */
            UrlInfo urlInfo = new UrlInfo();
            switch (userDevice.getOsType().toLowerCase())
            {

                case OS_TYPE.OS_TYPE_IOS:
                    break;
                default:

                  /*  certBody.setBt_info(BtListGet(mcsConfig.getProperty("bt_info")));*/
                    urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain());
                    urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad());
                    urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy());
                    urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th());
                    urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss());
                    break;
              /*  case OS_TYPE.OS_TYPE_IOS:
                    *//*certBody.setBt_info(BtListGet(config.getIos_bt_info()));*//*
                    urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain_ios());
                    urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad_ios());
                    urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy_ios());
                    urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th_ios());
                    urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss_ios());
                    break;
*/
            }
            urlInfo.setPrivacy_op_info(sbUrlFix.toString() + config.getUrl_privacy_option_domain());
            urlInfo.setPrivacy_md_info(sbUrlFix.toString()  + config.getUrl_privacy_mandatory_domain());
            urlInfo.setLocation_info(sbUrlFix.toString() + config.getUrl_location());


            certBody.setProfile(userInfo.getProfile());

            long lastHid = 0;
            int newHelpCnt = webViewDao.selectHelpNewCnt(hid);

            /*if (newHelpCnt != 0 && hid != 0)*/
            lastHid = webViewDao.selectHelpLastHid();


            Integer newNoticeCnt = webdbDao.newNoticeCnt(nid);
            String lastNoticeTime = webdbDao.getLastNoticeTime();
            lastNid = webdbDao.getLastNid();


            if (newNoticeCnt != 0)
                notice.setNew_notice_cnt(newNoticeCnt);

            if (lastNoticeTime != null)
                notice.setLast_notice_time(lastNoticeTime);

            if (lastNid != null)
                notice.setLast_nid(lastNid);

            HelpInfo helpInfo = new HelpInfo();
            helpInfo.setLast_hid(lastHid);
            helpInfo.setNew_help_cnt(newHelpCnt);
            helpInfo.setInfo(sbUrlFix.toString() + config.getUrlHelpDomain());
            certBody.setHelpInfo(helpInfo);

            certBody.setBt_info(btinfoDao.selectBTinfo(storeType, sbUrlFix.toString()));
            certBody.setUrlInfo(urlInfo);
            certBody.setNoticeInfo(new NoticeInfo(notice, null));



        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;

    }

    public UserInfo getUserInfoByIuidOrE164PhoneNo(String phoneNo) throws IOException {
        UserInfo userInfo;


        if(phoneNo == null)
            return null;
//        logger.error( "getUserInfoByIuidOrE164 [PhoneNo]" + phoneNo);
        userInfo = clientDao.selectUserInfo(phoneNo);

        if (userInfo == null) {
            return null;
        }

//        logger.error( "==============================================================");
//        logger.error(  userInfo.toString());
//        logger.error( "==============================================================");
        return userInfo;
    }

    public UserInfo getUserCertByIuidOrE164PhoneNo(String phoneNo) throws IOException {
        UserInfo userInfo;


        userInfo = clientDao.selectUserCert(phoneNo);

        if (userInfo == null) {
            return null;
        }

        return userInfo;
    }

    public UserInfo getUserCertByIuidOrE164PhoneNo(String phoneNo, Long iuid) throws IOException {
        UserInfo userInfo;

//        if (iuid != null) {
//            userInfo = clientDao.selectUserInfo(iuid);
//        } else {
            userInfo = clientDao.selectUserInfo(phoneNo);
//        }

        if (userInfo == null) {
            return null;
        }

        userInfo.setClientDao(clientDao);
        return userInfo;
    }
}
