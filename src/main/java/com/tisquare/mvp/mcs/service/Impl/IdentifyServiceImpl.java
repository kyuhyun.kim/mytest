package com.tisquare.mvp.mcs.service.Impl;

import CheckPlus.nice.MCheckPlus;
import NiceID.Check.CPClient;
import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.ClientDao;
import com.tisquare.mvp.mcs.db.IdentifyDao;
import com.tisquare.mvp.mcs.db.domain.NiceInfo;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.BssUserControl;
import com.tisquare.mvp.mcs.service.IdentifyService;
import com.tisquare.mvp.mcs.service.ResponseEntityService;
import com.tisquare.mvp.mcs.type.NiceResponseCode;
import com.tisquare.mvp.mcs.type.ResultCode;
import com.tisquare.mvp.mcs.type.SKIP_TYPE;
import com.tisquare.mvp.mcs.utils.DateUtil;
import com.tisquare.mvp.mcs.utils.ValidCheck;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by kimkyuhyun on 2015-12-04.
 */
@Service
public class IdentifyServiceImpl extends ResponseEntityService implements IdentifyService {

    private static final Logger logger = LoggerFactory.getLogger(IdentifyServiceImpl.class);


    @Autowired
    private InitConfig config;


    @Autowired
    ClientDao clientDao;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    BssUserControl bssUserControl;

    @Autowired
    IdentifyDao identifyDao;


    public ResponseEntity<String> processIdentifyRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Identify param)
    {

        IdentifyResponse identifyResponse  = new IdentifyResponse();
        identifyResponse.setRes_code(ResultCode.FAIL.getCode());
        try
        {


            String ReqeustKey =  DateUtil.getMilisec();

            logger.info("[processIdentifyRequest][{}] NICE_SITE_CODE[{}] NICE_SITE_PWD[{}]", authReqHeaders.getPhoneNo(), mcsConfig.getProperty("NICE.SITE.CODE"), mcsConfig.getProperty("NICE.SITE.PWD"));
            logger.info("[processIdentifyRequest][{}] NDN[{}] : {}", authReqHeaders.getPhoneNo(), param.getMdn(), param.toString());

            MCheckPlus cpMobile = new MCheckPlus();


            if(StringUtils.equals(mcsConfig.getProperty("NICE.SKIP"), SKIP_TYPE.FALSE.getCode().toString()))
            {


                int jumin =  ValidCheck.juminGeneration(param.getBirth(), param.getNativ(), param.getGender());
                if( jumin < 0)
                {
                    logger.error("입력 정보 오류!!");
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }
                else
                {
                    param.setBirth(param.getBirth() + String.valueOf(jumin));
                    param.setBirth(param.getBirth().substring(2, 9));
                }

                int iReturn = cpMobile.fnRequestSafeAuth(mcsConfig.getProperty("NICE.SITE.CODE"), mcsConfig.getProperty("NICE.SITE.PWD"), param.getBirth(), URLEncoder.encode(param.getName(), "euc-kr"), String.valueOf(param.getMobile_co()), param.getMdn(), ReqeustKey);

                logger.info("[processIdentifyRequest] Response [{}]", iReturn);

                identifyResponse.setRes_code(ResultCode.FAIL.getCode());

            /* 인증 성공 */
                if (iReturn == ResultCode.SUCCESS.getCode()) {
                    logger.info("[processIdentifyRequest] [{}] NICE RETURN CODE[{}]", authReqHeaders.getPhoneNo(), cpMobile.getReturnCode());
                    logger.info("[processIdentifyRequest] [{}] NICE REQ SEQ[{}]", authReqHeaders.getPhoneNo(), cpMobile.getRequestSEQ());
                    logger.info("[processIdentifyRequest] [{}] NICE RES_SEQ[{}]", authReqHeaders.getPhoneNo(), cpMobile.getResponseSEQ());


                    if (cpMobile.getReturnCode().equals("0000")) {

                        identifyResponse.setRes_code(ResultCode.SUCCESS.getCode());
                        identifyResponse.setUnique_key(cpMobile.getRequestSEQ());
                        identifyResponse.setUnique_no(cpMobile.getResponseSEQ());
                    }


                    identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));


                }
            /* 네트워크 및 방화벽 관련 에러 및 connection error*/
                else if (iReturn == -7 || iReturn == -8) {
                    logger.error("Fire wall or network connection error!!");
                    identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));
                }
            /* 입력 데이터 에러 */
                else if (iReturn == -9 || iReturn == -10 || iReturn == 12) {
                    logger.error("invalid Input Data!!");
                    identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));
                }
            /* 기타 NICE 시스템에 문의 */
                else {
                    logger.error("NICE System question!! error!!");
                    identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));
                }
            }
            else
            {
                identifyResponse.setRes_code(ResultCode.SUCCESS.getCode());
                identifyResponse.setRes_msg("success");
                identifyResponse.setUnique_key(DateUtil.getMilisec());
                identifyResponse.setUnique_no("000000");
            }

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return makeResponseEntityWithCheckBodySize(request, identifyResponse, HttpStatus.OK);
    }

    public void processIdentifyFail(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        identifyDao.updateFailNiceStat();
    }

    public void processIdentifyresult(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, String EncodeData, Long iuid)
    {
        try {
            CPClient niceCheck = new CPClient();

            int iReturn = niceCheck.fnDecode("BE525", "jQWlJZX1noBG", EncodeData);
            if( iReturn == 0 ) {


                String session_sRequestNumber = (String) request.getAttribute("REQ_SEQ");

                /*identifyDao.insertNiceInfo(iuid, EncodeData);*/
                logger.info("IUID [{}] ENC_DATA[{}]", iuid, EncodeData);

                identifyDao.updateSuccNiceStat();
            }
        }
        catch (Exception e)
        {
            logger.error(e.toString());
        }
    }

    public ResponseEntity<String> processIdentifyStateRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {

        IdentifyState identifyState = new IdentifyState();

        try {


            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if (userInfo == null) {
                logger.error("가입자 정보 없음.");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()))
            {
                logger.error("가입자 정보 없음.");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }

            NiceInfo niceInfo = identifyDao.selectNiceInfo(userInfo);

            String sPlainData = "";

            identifyState.setRes_code(1);

            if( niceInfo != null )
            {
                CPClient niceCheck = new CPClient();
                int iReturn = niceCheck.fnDecode( mcsConfig.getProperty("NICE.SITE.CODE"), mcsConfig.getProperty("NICE.SITE.PWD"), niceInfo.getEnc_data());
                if( iReturn == 0 )
                {
                    sPlainData = niceCheck.getPlainData();


                    // 데이타를 추출합니다.
                    java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);
                    logger.info("REQ_SEQ[{}]  AUTH_TYPE[{}] NAME[{}] BIRTHDATE[{}] GENDER[{}] NATIONALINFO[{}] MOBILE_NO[{}] MOBILE_CO[{}] " ,
                            (String)mapresult.get("REQ_SEQ"), (String)mapresult.get("AUTH_TYPE"), (String)mapresult.get("NAME"), (String)mapresult.get("BIRTHDATE"),
                        (String)mapresult.get("GENDER"), (String)mapresult.get("NATIONALINFO"), (String)mapresult.get("MOBILE_NO"), (String)mapresult.get("MOBILE_CO"));

                    String session_sRequestNumber = (String)request.getAttribute("REQ_SEQ");
                    /*if(!sRequestNumber.equals(session_sRequestNumber))
                    {
                        sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
                        sResponseNumber = "";
                        sAuthType = "";
                    }*/

                    identifyState.setBirth((String)mapresult.get("BIRTHDATE"));
                    identifyState.setGender(Integer.parseInt((String)mapresult.get("GENDER")));
                    identifyState.setName((String)mapresult.get("NAME"));
                    identifyState.setMobile_co((String)mapresult.get("MOBILE_CO"));
                    identifyState.setMdn((String)mapresult.get("MOBILE_NO"));
                    identifyState.setNative_info(Integer.parseInt((String)mapresult.get("NATIONALINFO")));
                    identifyState.setRes_code(0);
                    identifyState.setRes_msg("success");
                }
                else
                {
                    identifyState.setRes_msg("fail");
                }

            }
            else
            {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            identifyDao.deleteNiceInfo(userInfo);

        }catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return makeResponseEntityWithCheckBodySize(request, identifyState, HttpStatus.OK);
    }

    public ResponseEntity<String> processIdentifyCheckRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Identify_check param)
    {
        IdentifyResponse identifyResponse  = new IdentifyResponse();

        identifyResponse.setRes_code(ResultCode.FAIL.getCode());

        try {

            logger.info("[processIdentifyCheckRequest] [{}] NICE_SITE_CODE[{}] NICE_SITE_PWD[{}]", authReqHeaders.getPhoneNo(), mcsConfig.getProperty("NICE.SITE.CODE"), mcsConfig.getProperty("NICE.SITE.PWD"));
            logger.info("[processIdentifyCheckRequest] [{}]  :{}", authReqHeaders.getPhoneNo(), param.toString());


            MCheckPlus cpMobile = new MCheckPlus();
            if (StringUtils.equals(mcsConfig.getProperty("NICE.SKIP"), SKIP_TYPE.FALSE.getCode().toString()))
            {

                int iReturn = -1;
            // Method 결과값(iReturn)에 따라, 프로세스 진행여부를 파악합니다.
            iReturn = cpMobile.fnRequestConfirm(mcsConfig.getProperty("NICE.SITE.CODE"), mcsConfig.getProperty("NICE.SITE.PWD"), param.getUnique_no(), param.getSms_no(), param.getUnique_key());

            /* 인증 성공 */
            if (iReturn == ResultCode.SUCCESS.getCode()) {

                logger.info("[processIdentifyCheckRequest] [" + authReqHeaders.getPhoneNo() + "] RETURN_CODE=" + cpMobile.getReturnCode());              // 응답코드
                logger.info("[processIdentifyCheckRequest] [" + authReqHeaders.getPhoneNo() + "] RETURN_MSG=" + NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));              // 응답메시지
                logger.info("[processIdentifyCheckRequest] [" + authReqHeaders.getPhoneNo() + "] CONFIRM_DATETIME=" + cpMobile.getConfirmDateTime());    // 인증 완료시간
                logger.info("[processIdentifyCheckRequest] [" + authReqHeaders.getPhoneNo() + "] REQ_SEQ=" + cpMobile.getRequestSEQ());                  // 요청 고유번호
                logger.info("[processIdentifyCheckRequest] [" + authReqHeaders.getPhoneNo() + "] RES_SEQ=" + cpMobile.getResponseSEQ());                 // 응답 고유번호
                logger.info("[processIdentifyCheckRequest] [" + authReqHeaders.getPhoneNo() + "] RES_CI=" + cpMobile.getResponseCI());                   // 아이핀 연결정보(CI)
                logger.info("[processIdentifyCheckRequest] [" + authReqHeaders.getPhoneNo() + "] RES_DI=" + cpMobile.getResponseDI());                   // 아이핀 중복가입확인정보(DI)

                if (!StringUtils.equals(cpMobile.getReturnCode(), "0000"))
                    identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));

            }
            /* 네트워크 및 방화벽 관련 에러 및 connection error*/
            else if (iReturn == -7 || iReturn == -8) {
                logger.error("Fire wall or network connection error!!");
                identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));
            }
            /* 입력 데이터 에러 */
            else if (iReturn == -9 || iReturn == -10 || iReturn == 12) {
                logger.error("invalid Input Data!!");
                identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));
            }
            /* 기타 NICE 시스템에 문의 */
            else {
                logger.error("NICE System question!! error!!");
                identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg(cpMobile.getReturnCode()));
            }

        }
        else
        {
            identifyResponse.setRes_msg(NiceResponseCode.getReturnMsg("0000"));
            identifyResponse.setRes_code(ResultCode.SUCCESS.getCode());
        }

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return makeResponseEntityWithCheckBodySize(request, identifyResponse, HttpStatus.OK);
    }
}
