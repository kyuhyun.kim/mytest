package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.pc.IdentifyPasswd;
import com.tisquare.mvp.mcs.service.AlarmService;
import com.tisquare.mvp.mcs.service.MailService;
import com.tisquare.mvp.mcs.service.ResponseEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class MailServiceImpl implements MailService {

    private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);


    @Autowired
    private JavaMailSender mailSender;

    @Override
    public boolean MailSend(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd)
    {
        /*String setfrom = "bite2058.kk@gmail.com";
        String tomail  = "kyuhyun.kim@tisquare.com";
        String title   = "제목";
        String content = "내용";*/

        try {

            Resource resource = new ClassPathResource("samsung.html");
            System.out.println("파일사이즈::"+resource.getFile().length());
            System.out.println("파일절대경로+파일명:"+resource.getURI().getPath().substring(1));


            MimeMessage message = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper
                    = new MimeMessageHelper(message, true, "UTF-8");

          /*  messageHelper.setFrom(setfrom);  // 보내는사람 생략하거나 하면 정상작동을 안함

            messageHelper.setTo(tomail);     // 받는사람 이메일

            messageHelper.setSubject(title); // 메일제목은 생략이 가능하다

            messageHelper.setText(content);  // 메일 내용
*/
            mailSender.send(message);

        } catch(Exception e){
            logger.error("메일 발송 실패 {}", e.toString());
            return false;

        }
        return true;
    }
}
