package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.db.domain.PromotionSamsung;
import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.HttpStatus;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface PromotionControl {

    public HttpStatus PromotionList(PromotionListBody promotionListBody, String phone_no, String os_type, String osVersion, String telecomCode)
            throws Exception;

    public HttpStatus PromotionPcList(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionList promotionList)
            throws Exception;

    public HttpStatus PromotionSamSungInfo(PromotionSamsungInfo promotionSamsungInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
            throws Exception;

    public HttpStatus PromotionSamSungReg( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung)
            throws Exception;

    public HttpStatus PromotionSamSungOem ( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung)
            throws Exception;

    public HttpStatus PromotionList ( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionList promotionList)
            throws Exception;

    public HttpStatus PromotionCampPackInfo(PromotionCampPackInfo promotionCampPackInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
            throws Exception;

    public HttpStatus PromotionCampPackReg(PromotionCampPackInfo promotionCampPackInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
            throws Exception;

}
