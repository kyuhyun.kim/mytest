package com.tisquare.mvp.mcs.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.connect.BlockInterFaceImpl;
import com.tisquare.mvp.mcs.db.*;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.entity.EventPopup;
import com.tisquare.mvp.mcs.interact.HttpAsyncClientWorker;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.IPCControl;
import com.tisquare.mvp.mcs.service.TraceService;
import com.tisquare.mvp.mcs.service.UserControl;
import com.tisquare.mvp.mcs.type.OS_TYPE;
import com.tisquare.mvp.mcs.type.SUBS_TYPE;
import com.tisquare.mvp.mcs.type.SubscriberNewType;
import com.tisquare.mvp.mcs.type.Telecom_code;
import com.tisquare.mvp.mcs.utils.DateUtil;
import com.tisquare.mvp.mcs.utils.Json;
import com.tisquare.mvp.mcs.utils.ValidCheck;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class IPCControlImpl implements IPCControl {

    private static final Logger logger = LoggerFactory.getLogger(IPCControlImpl.class);

    private final static String LINE = "\r\n";

    private static int FCS_IDX =1;

    @Autowired
    private InitConfig config;

    @Autowired
    SystemInfoDao systemInfoDao;

    @Autowired
    ClientDao clientDao;

    @Autowired
    WebdbDao webdbDao;

    @Autowired
    ServerDao serverDao;

    @Autowired
    WebViewDao webViewDao;

    @Autowired
    PresenceDao presenceDao;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    TraceService traceService;

    @Autowired
    BlockInterFaceImpl blockInterFace;

    @Autowired
    BtinfoDao btinfoDao;


    ObjectMapper mapper = new ObjectMapper();





    public String HttpGetRequestLogPrint(String mdn, HttpGet httpGet)
    {
        Header[] headers = httpGet.getAllHeaders();

        String Print = LINE + "--------------------------------------------------------------------------------" + LINE +
                httpGet.toString()+LINE;
        for(int idx = 0; idx < headers.length; idx++)
        {
            Print += headers[idx].getName() +":" + headers[idx].getValue() + LINE;
        }

        Print += "================================================================================" +LINE;

        return Print;

    }

    public void HttpPostRequestLogPrint(HttpPost httpPost, String body)
    {
        Header[] headers = httpPost.getAllHeaders();

        String Print = LINE + "--------------------------------------------------------------------------------" + LINE +
                httpPost.toString()+LINE;
        for(int idx = 0; idx < headers.length; idx++)
        {
            Print += headers[idx].getName() +":" + headers[idx].getValue() + LINE;
        }

        Print +=body +LINE;

        Print += "================================================================================" +LINE;
        logger.info(Print);

        System.out.println(Print);
    }

    /**
     * ACS => PAS 로그아웃 요청
     *
     * @param authInfoBody 사용자 인증 정보
     * @param authReqHeaders 사용자 인증 정보
     */
    @Async
    public void sendPasLogoutNotification(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) {
        String responseBody = null;
        try {
            ExecutorService pool = Executors.newSingleThreadExecutor();

            Map<String, String> headers = new HashMap<>();
            headers.put(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
            headers.put(HttpHeaders.ACCEPT_ENCODING, "ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4");
            headers.put("TN-IUID", String.valueOf(authInfoBody.getProfile().getIuid()));
            headers.put("TN-AUTH-KEY", authInfoBody.getBssB2bInfo().getAuth_key());
            headers.put("TN-APP-TYPE", "conference");
            headers.put("TN-REGION-CODE", "KR");

            logger.info(new String(mcsConfig.getProperty("NGINX.INTERNAL.VIP") + "/" + mcsConfig.getProperty("PAS.LOGOUT.URL")));

            responseBody = Json.toStringJson((Object)new IPCPasLogout( "mgr/control/logout/request", new from (authReqHeaders.getPhoneNo(), authInfoBody.getProfile().getIuid(), authInfoBody.getProfile().getNickname())));

            pool.execute(HttpAsyncClientWorker.create(new String(mcsConfig.getProperty("NGINX.INTERNAL.VIP") + "/" + mcsConfig.getProperty("PAS.LOGOUT.URL") ), headers, responseBody, ContentType.APPLICATION_JSON));
            pool.shutdown();
        } catch (Exception e) {
            logger.error(" => RMS|send withdraw notify failed. req-body: {}", responseBody, e);
        }
    }

    /**
     * ACS => PNS 동보 메시지 발송 요청
     *
     * @param multiMsgInfo 동보 전송 발송 대상자
     * @param authReqHeaders 사용자 인증 정보
     */
    @Async
    public void sendPnsMultiSms(MultiMsgInfo multiMsgInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) {
        String responseBody = null;
        try {
            ExecutorService pool = Executors.newSingleThreadExecutor();

            Map<String, String> headers = new HashMap<>();
            headers.put(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
            headers.put(HttpHeaders.ACCEPT_ENCODING, "ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4");
            headers.put("REQ-USER", authReqHeaders.getPhoneNo());

            logger.info(new String(mcsConfig.getProperty("NGINX.INTERNAL.VIP") +  mcsConfig.getProperty("PNS.MULTI.SMS.URL")));

            responseBody = Json.toStringJson((Object)new MsgInfo(multiMsgInfo));

            logger.info(responseBody);
            pool.execute(HttpAsyncClientWorker.create(new String(mcsConfig.getProperty("NGINX.INTERNAL.VIP") +  mcsConfig.getProperty("PNS.MULTI.SMS.URL") ), headers, responseBody, ContentType.APPLICATION_JSON));
            /*pool.execute(HttpAsyncClientWorker.create(new String(mcsConfig.getProperty("NGINX.INTERNAL.VIP") + "/" + mcsConfig.getProperty("PAS.LOGOUT.URL") ), headers, responseBody, ContentType.APPLICATION_JSON));*/
            pool.shutdown();
        } catch (Exception e) {
            logger.error(" ACS => PNS|send sms message notify failed. req-body: {}", responseBody, e.toString());
        }
    }



    public HttpStatus PNS_MultiSmsRequest(MultiMsgInfo multiMsgInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParamsm , MsgInfoResult msgInfoResult)  {

        URIBuilder bu = null;


        try {

            CloseableHttpClient httpClient = HttpClients.createDefault();

            bu = new URIBuilder(new URI(mcsConfig.getProperty("PNS.INTERNAL.IP")));

            bu.setPath(mcsConfig.getProperty("PNS.MULTI.SMS.URL"));

            HttpPost http = new HttpPost(bu.build());
            RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                    .setConnectTimeout(Integer.parseInt(mcsConfig.getProperty("IPC.TIME.OUT")))
                    .setSocketTimeout(Integer.parseInt(mcsConfig.getProperty("IPC.TIME.OUT"))).build();
            http.setConfig(requestConfig);
            http.addHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
            http.addHeader(HttpHeaders.ACCEPT_ENCODING,    "ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4");

            http.addHeader("REQ-USER", authReqHeaders.getPhoneNo());

            String responseBody = Json.toStringJson((Object) new MsgInfo( multiMsgInfo.getMsgInfo()));


            HttpPostRequestLogPrint(http, responseBody);

            http.setEntity(new StringEntity(responseBody, HTTP.UTF_8));
            CloseableHttpResponse response = httpClient.execute(http);
            InputStreamReader is = new InputStreamReader(response.getEntity().getContent());
            BufferedReader br = new BufferedReader(is);
            String read = null;
            StringBuffer sb = new StringBuffer();
            while ((read = br.readLine()) != null) {
                sb.append(read);
            }
            String result = sb.toString();

            br.close();
            is.close();
            response.close();
            http.releaseConnection();

            logger.info(String.valueOf( response.getStatusLine().getStatusCode()));

            String message = LINE + "--------------------------------------------------------------------------------" + LINE
                    +response.toString()+LINE+ result+LINE+"" +
                    "================================================================================" +
                    LINE;


            logger.info(message);


            if(response.getStatusLine().getStatusCode() != HttpStatus.OK.value()) {
                MsgInfoResult msgRs = mapper.readValue(result, MsgInfoResult.class);
                 msgInfoResult.setCause(msgRs.getCause());
                 msgInfoResult.setValue(msgRs.getValue());
            }

            return HttpStatus.valueOf(response.getStatusLine().getStatusCode());


        }catch(Exception e){
            logger.error("[ACS -> PNS] PNS_MultiSmsRequest Error" + e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;

        }


    }

    @Async
    public void PAS_LogoutRequest(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)  {

        URIBuilder bu = null;


        try {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        /*for(int nIdx = 0 ; nIdx < authInfoBody.getServerList().size(); nIdx++)
        {
            if(StringUtils.equals(authInfoBody.getServerList().get(nIdx).getName().toUpperCase(), "WS"))
            {
                if(StringUtils.equals(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toUpperCase(), "HTTP"))
                {
                    bu = new URIBuilder();
                    bu.setScheme(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toLowerCase())
                            .setHost(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getIpAddr())
                            .setPort(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getPort())
                            .setPath(mcsConfig.getProperty("PAS.LOGOUT.URL"));

                }
            }
        }*/


            bu = new URIBuilder(new URI(mcsConfig.getProperty("NGINX.INTERNAL.VIP")));

            bu.setPath(mcsConfig.getProperty("PAS.LOGOUT.URL"));

        HttpPost http = new HttpPost(bu.build());
        RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setConnectTimeout(Integer.parseInt(mcsConfig.getProperty("IPC.TIME.OUT")))
                .setSocketTimeout(Integer.parseInt(mcsConfig.getProperty("IPC.TIME.OUT"))).build();
            http.setConfig(requestConfig);
            http.addHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
            http.addHeader(HttpHeaders.ACCEPT_ENCODING,    "ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4");
            http.addHeader("TN-IUID", String.valueOf(authInfoBody.getProfile().getIuid()));
            http.addHeader("TN-AUTH-KEY", authInfoBody.getBssB2bInfo().getAuth_key());
            http.addHeader("TN-APP-TYPE", "conference");
            http.addHeader("TN-REGION-CODE", "KR");

            String responseBody = Json.toStringJson((Object)new IPCPasLogout( "mgr/control/logout/request", new from (authReqHeaders.getPhoneNo(), authInfoBody.getProfile().getIuid(), authInfoBody.getProfile().getNickname())));

            HttpPostRequestLogPrint(http, responseBody);

            http.setEntity(new StringEntity(responseBody, HTTP.UTF_8));
            CloseableHttpResponse response = httpClient.execute(http);
            InputStreamReader is = new InputStreamReader(response.getEntity().getContent());
            BufferedReader br = new BufferedReader(is);
            String read = null;
            StringBuffer sb = new StringBuffer();
            while ((read = br.readLine()) != null) {
                sb.append(read);
            }
            String result = sb.toString();

            br.close();
            is.close();
            response.close();
            http.releaseConnection();

            String message = LINE + "--------------------------------------------------------------------------------" + LINE
                    +response.toString()+LINE+ result+LINE+"" +
                    "================================================================================" +
                    LINE;


            logger.info(message);

        }catch(Exception e){
            logger.error("[ACS -> PAS] Logout Error" + e.toString());


        }

    }



}
