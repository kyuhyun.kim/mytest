package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.HttpStatus;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface HelpControl {

    public HttpStatus HelpList(HelpListBody helpListBody, HelpReqHeaders helpReqHeaders)
            throws Exception;

    public HttpStatus Help(HelpBody helpBody, int hid)
            throws Exception;

}
