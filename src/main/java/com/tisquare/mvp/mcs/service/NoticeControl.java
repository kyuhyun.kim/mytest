package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.HttpStatus;

import java.io.IOException;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface NoticeControl {

    public HttpStatus NoticeList(NoticeListBody noticeListBody, NoticeReqHeaders noticeReqHeaders)
            throws Exception;

    public HttpStatus Notice(NoticeBody noticeBody, int sid, String telecomCode)
            throws Exception;

}
