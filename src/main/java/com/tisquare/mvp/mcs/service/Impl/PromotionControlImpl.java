package com.tisquare.mvp.mcs.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.*;
import com.tisquare.mvp.mcs.db.domain.*;
import com.tisquare.mvp.mcs.entity.EventPopup;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.BssUserControl;
import com.tisquare.mvp.mcs.service.PromotionControl;
import com.tisquare.mvp.mcs.type.CampackRegResult;
import com.tisquare.mvp.mcs.type.CampackResult;
import com.tisquare.mvp.mcs.type.OS_TYPE;
import com.tisquare.mvp.mcs.type.USER_TYPE;
import com.tisquare.mvp.mcs.utils.AgeCalcurator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class PromotionControlImpl implements PromotionControl {

    private static final Logger logger = LoggerFactory.getLogger(PromotionControlImpl.class);

    private static int FCS_IDX = 1;

    @Autowired
    private InitConfig config;

    @Autowired
    PromotionDao promotionDao;

    @Autowired
    ClientDao clientDao;

    @Autowired
    ProfileDetailDao profileDetailDao;

    @Autowired
    TermsDao termsDao;

    @Autowired
    BssUserControl bssUserControl;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    MasDao masDao;

    @Autowired
    EventNoticeDao eventNoticeDao;

    @Autowired
    CampDao campDao;

    @Autowired
    SystemInfoDao systemInfoDao;

    @Autowired
    ServerDao serverDao;




    ObjectMapper mapper = new ObjectMapper();

    public HttpStatus PromotionCampPackReg(PromotionCampPackInfo promotionCampPackInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
            throws Exception
    {
        try {

            if(promotionCampPackInfo == null)
            {
                logger.error("캠프팩 신청 정보가 존재 하지 않습니다");
                return HttpStatus.BAD_REQUEST;
            }
            else
            {
                if(promotionCampPackInfo.getBirth() == null || promotionCampPackInfo.getName() == null || promotionCampPackInfo.getEnter_unit() == null || promotionCampPackInfo.getEnter_date() == null)
                {
                    logger.error("캠프팩 신청 정보누락");
                    return HttpStatus.BAD_REQUEST;
                }
                /* 캠프팩 신청 정보 확인 */
                if(AgeCalcurator.getAge(promotionCampPackInfo.getRel_birth().toString()) < Integer.valueOf( mcsConfig.getProperty("AGE.LIMIT")))
                {
                    logger.error("캠프팩 신청자의 나이가 만 19세 이상이 아닙니다.[{}]", promotionCampPackInfo.getRel_birth().toString());
                    promotionCampPackInfo.setResult(CampackResult.AGE_LIMIT.getCode());
                    promotionCampPackInfo.setResult_msg("신청자의 나이 입력 오류");
                    return HttpStatus.OK;
                }
                logger.info(promotionCampPackInfo.toString());
            }

            /* 사용자 인증 확인 */
            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return HttpStatus.NOT_FOUND;
            }

            if(!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return HttpStatus.FORBIDDEN;
            }

            promotionCampPackInfo.setMdn(promotionCampPackInfo.getRel_phone_no());

            /* 캠프팩 신청 정보 확인 */


            PromotionCampPackInfo promotionCampPackInfo1 = promotionDao.CampackRegHistoryValidCheck(promotionCampPackInfo);

            if(promotionCampPackInfo1 != null) {

                promotionCampPackInfo.PromotionCampPackInfoResult(promotionCampPackInfo1);
                return HttpStatus.OK;
            }




            logger.info("캠프팩 신청 이력 없음!");
            promotionCampPackInfo.setRel_iuid(authReqHeaders.getIuid());


            /* 군인 정보 조회 */
            if(campDao.selectCampInfo(promotionCampPackInfo) > 0)
                promotionDao.insertPromotionCampPackInfo(promotionCampPackInfo);
            else
            {
                promotionCampPackInfo.setResult(CampackRegResult.NOT_FOUND_SOLDEIR.getCode());
                promotionCampPackInfo.setResult_msg("군인정보 없음");
            }


        }catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    public HttpStatus PromotionCampPackInfo(PromotionCampPackInfo promotionCampPackInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
            throws Exception
    {
        try {

            if( promotionCampPackInfo == null)
            {
                logger.error("군인 정보 없음!");
                return HttpStatus.BAD_REQUEST;
            }
            else
            {
                if(promotionCampPackInfo.getBirth() == null)
                {
                    logger.error("생년월일 정보 없음!");
                    return HttpStatus.BAD_REQUEST;
                }

                if(promotionCampPackInfo.getName() == null)
                {
                    logger.error("군인 이름 정보 없음!");
                    return HttpStatus.BAD_REQUEST;
                }

                if(promotionCampPackInfo.getEnter_date() == null)
                {
                    logger.error("입대일 정보 없음!");
                    return HttpStatus.BAD_REQUEST;
                }

                if(promotionCampPackInfo.getEnter_unit() == null)
                {
                    logger.error("입대부대 정보 없음!");
                    return HttpStatus.BAD_REQUEST;
                }
            }

            /* 사용자 인증 확인 */
            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return HttpStatus.NOT_FOUND;
            }

            if(!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return HttpStatus.FORBIDDEN;
            }

            promotionCampPackInfo.setMdn(authReqHeaders.getPhoneNo());

            /* 캠프팩 신청 정보 확인 */
            PromotionCampPackInfo promotionCampPackInfo1 = promotionDao.CampackRegHistoryValidCheck(promotionCampPackInfo );

            if(promotionCampPackInfo1 != null) {
                logger.info("캠프팩 신청 이력 확인 됨!");
                if( promotionCampPackInfo1.getResult() == CampackResult.SUCCESS.getCode()) {
                    logger.info(promotionCampPackInfo.toString());
                    logger.info("본인이 신청한 이력으로 확인");
                }

                promotionCampPackInfo.PromotionCampPackInfo(promotionCampPackInfo1);
            }
            else
            {
                logger.info("캠프팩 신청 이력 없음!");
                promotionCampPackInfo.setResult(CampackResult.NOT_FOUND.getCode());
                promotionCampPackInfo.setResult_msg("캠프팩 신청 정보가 존재 하지 않음.");
            }



        }catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }



        return HttpStatus.OK;
    }

    public HttpStatus PromotionPcList( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionList promotionList)
            throws Exception
    {
        try {


        /* 프로모션 정보 확인 */

            EventPopup eventPopup = systemInfoDao.getEventPopup();
            if (eventPopup != null) {
                promotionList.setEventPopup(eventPopup);

                int count = systemInfoDao.isSystemUnavailable();

                if (count > 0) {
                 /* 공지 시간 OK & 작업 시간은 OK */
                    promotionList.getEventPopup().setForceStop(EventPopup.FORCE_UPDATE);
                    logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                    return HttpStatus.SERVICE_UNAVAILABLE;
                }
            }

            /* 신규 공지 방식 추가 */
           /* List<EventPopup> eventPopups = systemInfoDao.getEventPopupList();
            if (eventPopups != null) {
                promotionList.setEventPopupList(eventPopups);

                int count = systemInfoDao.isSystemUnavailableList();

                if (count > 0) {
                        *//* 공지 시간 OK & 작업 시간은 OK*//*
                    logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                    return HttpStatus.SERVICE_UNAVAILABLE;
                }
            }
*/
            List<EventNotice> eventNotices = eventNoticeDao.selectWebEventNotice(new ProfileDetail(USER_TYPE.NORMAL.getCode()));

            promotionList.setEventNotices(eventNotices);


            if (FCS_IDX == config.getMAX_FCS_IDX())
                FCS_IDX = 0;

            List<ServerConnInfo> serverConnInfos = serverDao.findServers3(FCS_IDX++);

            StringBuilder sbUrlFix = new StringBuilder(config.getUrl_domain());
            for (int nIdx = 0; nIdx < serverConnInfos.size(); nIdx++) {
                if (serverConnInfos.get(nIdx).getName().toUpperCase().equals("WS") && serverConnInfos.get(nIdx).getConnections().get(0).getProtocol().toUpperCase().equals("HTTPS")) {
                    sbUrlFix.setLength(0);
                    sbUrlFix.append(serverConnInfos.get(nIdx).getConnections().get(0).getProtocol().toLowerCase());
                    sbUrlFix.append("://");
                    sbUrlFix.append(serverConnInfos.get(nIdx).getConnections().get(0).getIpAddr().toLowerCase());
                    sbUrlFix.append(":");
                    sbUrlFix.append(serverConnInfos.get(nIdx).getConnections().get(0).getPort());
                    sbUrlFix.append("/");
                    sbUrlFix.append(config.getProcess_name().toLowerCase());
                    sbUrlFix.append("/");
                    break;
                }
            }


            /* OS_TYPE 체크 */
            UrlInfo urlInfo = new UrlInfo();
            urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain());
            urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad());


            urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy());
                    /*urlInfo.setPrivacy_info("https://okitalkie-pro.com/acs/pages/privacy_company.html");*/
            urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th());
            urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss());
            urlInfo.setPrivacy_op_info(sbUrlFix.toString() + config.getUrl_privacy_option_domain());
            urlInfo.setPrivacy_md_info(sbUrlFix.toString() + config.getUrl_privacy_mandatory_domain());
            urlInfo.setLocation_info(sbUrlFix.toString() + config.getUrl_location());

            /* 나이스 관련 약관 정보 세팅 */
            urlInfo.setNiceUrlInfo(new NiceUrlInfo(sbUrlFix.toString(),
                    mcsConfig.getProperty("URL_TELECOM_LIST"),
                    mcsConfig.getProperty("URL_NICE_PRIVACY_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_UNIQUE_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_TELECOM_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_AGREEMENT_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_SAFE_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_LGU_OTP_DOMAIN")
                    , mcsConfig.getProperty("URL_NICE_INFO")));

            promotionList.setUrlInfo(urlInfo);
        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    public HttpStatus PromotionList( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionList promotionList)
            throws Exception
    {
        try {


        /* 사용자 인증 확인 */
            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if (userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return HttpStatus.NOT_FOUND;
            }

            if (!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey())) {
                logger.error("인증 키 불일 치!!");
                return HttpStatus.FORBIDDEN;
            }

            ProfileDetail profileDetail = profileDetailDao.selectProfileDetail(userInfo);

            if (profileDetail.getType() == USER_TYPE.MILITARY.getCode()) {
                MasTrainee masTrainee = masDao.selectMasTraniee(userInfo.getIuid());
                if (masTrainee != null) {
                    profileDetail.setMasTrainee(masTrainee);
                }
                else
                    return HttpStatus.BAD_REQUEST;

            }

        /* 프로모션 정보 확인 */
            List<EventNotice> eventNotices = eventNoticeDao.selectEventNotice(profileDetail);

            if (profileDetail.getType() == USER_TYPE.MILITARY.getCode()) {
                if (eventNotices != null) {

                    for (int nIdx = 0; nIdx < eventNotices.size(); nIdx++) {
                        if (profileDetail.getEnter_date() < Long.valueOf(mcsConfig.getProperty("base.enter.date"))) {

                            if (org.apache.commons.lang.StringUtils.equals(eventNotices.get(nIdx).getLanding_menu(), "500"))
                                eventNotices.remove(nIdx);
                        }
                    }

                }
            }

            promotionList.setEventNotices(eventNotices);
        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    public HttpStatus PromotionSamSungOem ( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung)
            throws Exception
    {
        try {

            if(promotionSamsung == null)
            {
                logger.error("body Data is null!!");
                return HttpStatus.BAD_REQUEST;
            }
            else
            {
                if(promotionSamsung.getRel_phone_no() != null) {
                    if( !promotionSamsung.getRel_phone_no().contains( "+82")) {
                        logger.error("rel_phone no is not found +82 [{}]", promotionSamsung.getRel_phone_no());
                        return HttpStatus.BAD_REQUEST;
                    }
                    if(promotionSamsung.getRel_phone_no().contains(" "))
                    {
                        logger.error("rel phone no is found '-' [{}]", promotionSamsung.getPhone_no());
                        promotionSamsung.setRel_phone_no(promotionSamsung.getRel_phone_no().replace(" ", "") );
                    }

                    if(promotionSamsung.getRel_phone_no().contains("-"))
                    {
                        logger.error("rel phone no is found '-' [{}]", promotionSamsung.getPhone_no());
                        promotionSamsung.setRel_phone_no(promotionSamsung.getRel_phone_no().replace("-", "") );
                    }
                }
            }

            /* 사용자 인증 확인 */
            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return HttpStatus.NOT_FOUND;
            }

            if(!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return HttpStatus.FORBIDDEN;
            }

            /* 해당 사용자에 매핑된 프로모션 정보 확인 */

            if(StringUtils.equals(userInfo.getPhoneNo(), promotionSamsung.getRel_phone_no()))
            {
                logger.error("요청자의 폰 번호[{}]와 가족 등록 대상자[{}]의 폰번호가 동일 함!!", userInfo.getPhoneNo(), promotionSamsung.getRel_phone_no());
                return  HttpStatus.BAD_REQUEST;
            }


            ProfileDetail profileDetail = profileDetailDao.selectProfileDetail(userInfo);

            promotionSamsung.setIuid(userInfo.getIuid());

            promotionSamsung.setUser_type(profileDetail.getType());
            promotionSamsung.setS_id(profileDetail.getSm_id());

            /* 가입자 군인 여부 확인 */
            if(profileDetail.getType() == USER_TYPE.MILITARY.getCode())
            {

                UserInfo userInfo1 = null;
                /* 가족 등록 대상자 가입자 여부 확인 */

                userInfo1 = clientDao.selectUserInfo(promotionSamsung.getRel_phone_no());

                if( userInfo1 != null)
                    promotionSamsung.setRel_iuid(userInfo1.getIuid());
            }
            else
            {
                logger.error("요청자의 상태가 군인이 아님[{}]!!", userInfo.getIuid());
                return HttpStatus.METHOD_NOT_ALLOWED;
            }
        }catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;

    }

    public HttpStatus PromotionSamSungReg( ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung)
            throws Exception
    {

        try {

            if(promotionSamsung == null)
            {
                logger.error("body Data is null!!");
                return HttpStatus.BAD_REQUEST;
            }
            else
            {
                if(promotionSamsung.getRel_phone_no() != null) {

                    if (promotionSamsung.getRel_phone_no().trim().length() != 0) {

                        if (!promotionSamsung.getRel_phone_no().contains("+82")) {
                            logger.error("rel_phone no is not found +82 [{}]", promotionSamsung.getRel_phone_no());
                            return HttpStatus.BAD_REQUEST;
                        }
                        if (promotionSamsung.getRel_phone_no().contains(" ")) {
                            logger.error("rel phone no is found '-' [{}]", promotionSamsung.getPhone_no());
                            promotionSamsung.setRel_phone_no(promotionSamsung.getRel_phone_no().replace(" ", ""));
                        }

                        if (promotionSamsung.getRel_phone_no().contains("-")) {
                            logger.error("rel phone no is found '-' [{}]", promotionSamsung.getPhone_no());
                            promotionSamsung.setRel_phone_no(promotionSamsung.getRel_phone_no().replace("-", ""));
                        }

                    }
                }
            }

            /* 사용자 인증 확인 */
            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return HttpStatus.NOT_FOUND;
            }

            if(!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return HttpStatus.FORBIDDEN;
            }

            /* 해당 사용자에 매핑된 프로모션 정보 확인 */
            ProfileDetail profileDetail = profileDetailDao.selectProfileDetail(userInfo);

            if(StringUtils.equals(profileDetail.getPhone_no(), promotionSamsung.getRel_phone_no()))
            {
                logger.error("요청자의 폰 번호[{}]와 가족 등록 대상자[{}]의 폰번호가 동일 함!!", userInfo.getPhoneNo(), promotionSamsung.getRel_phone_no());
                promotionSamsung.setResult(5);
                return  HttpStatus.OK;
            }

            promotionSamsung.setIuid(userInfo.getIuid());
            promotionSamsung.setUser_type(profileDetail.getType());
            promotionSamsung.setS_id(profileDetail.getSm_id());
            promotionSamsung.setBirth(profileDetail.getBirth());
            promotionSamsung.setGender(profileDetail.getGender());
            promotionSamsung.setPhone_no(profileDetail.getPhone_no());
            promotionSamsung.setName(profileDetail.getName());
            if(promotionSamsung.getRel_addr() != null) {
                promotionSamsung.setGift_yn(1);

                /* 해당 사용자에 매핑된 프로모션 정보 확인 */

                PromotionCampPackInfo promotionCampPackInfo= profileDetailDao.selectProfileDetailMasTrainee(userInfo);



                promotionCampPackInfo = promotionDao.CampackHistoryValidCheck(promotionCampPackInfo);
                if( promotionCampPackInfo != null)
                {
                    logger.error("일반인이 캠프팩 신청한 정보가 존재 함!![{}]", userInfo.getIuid());
                    promotionSamsung.setResult(4);
                    return HttpStatus.OK;
                }
            }

            /* 가입 정책 위배 확인 */
            if(AgeCalcurator.getAge(promotionSamsung.getBirth().toString()) >  mcsConfig.getPropertyToInteger("AGE.SOLDIER.MAX.LIMIT"))
            {
                if(AgeCalcurator.getAge(promotionSamsung.getBirth().toString()) <  mcsConfig.getPropertyToInteger("AGE.SOLDIER.MIN.LIMIT") )
                logger.error("나이제한에 위배 됨! 나이[{}]", AgeCalcurator.getAge(promotionSamsung.getBirth().toString()));
                promotionSamsung.setResult(1);
                return  HttpStatus.OK;
            }

            if(promotionSamsung.getGender() != mcsConfig.getPropertyToInteger("SOLDER.GENDER"))
            {
                logger.error("성별제한에 위배 됨! [{}]", promotionSamsung.getGender());
                promotionSamsung.setResult(2);
                return  HttpStatus.OK;
            }

            /* 광고성 동의 여부 확인 */

            Integer ad_terms = termsDao.selectTermsAd(userInfo.getIuid());

            if(ad_terms == null)
                ad_terms = 0;

            promotionSamsung.setAd_yn(ad_terms);

            /* 가입자 군인 여부 확인 */
            if(profileDetail.getType() == USER_TYPE.MILITARY.getCode())
            {

                UserInfo userInfo1 = null;
                /* 입대일 확인 */
                MasTrainee masTrainee = masDao.selectMasTraniee(userInfo.getIuid());
                if(masTrainee != null)
                    promotionSamsung.setEnter_date(Long.parseLong( masTrainee.getTr_entrance_day()));
                /* 가족 등록 대상자 가입자 여부 확인 */
                if(promotionSamsung.getRel_iuid() == null)
                    userInfo1 = clientDao.selectUserInfo(promotionSamsung.getRel_phone_no());
                else {
                    userInfo1 = clientDao.selectUserInfo(promotionSamsung.getRel_iuid());
                    if(userInfo1 != null)
                        promotionSamsung.setRel_phone_no(userInfo1.getPhoneNo());
                    else
                        return HttpStatus.BAD_REQUEST;
                }

                /* 프로모션 등록 대상자 정보 선택 */
                if( userInfo1 != null) {

                    if(promotionSamsung.getIuid().equals(userInfo1.getIuid()))
                    {
                        logger.error("요청자의 폰 번호[{}]와 가족 등록 대상자[{}]의 IUID 가  동일 함!!", promotionSamsung.getIuid(), userInfo.getIuid());
                        promotionSamsung.setResult(5);
                        return  HttpStatus.OK;
                    }

                    ad_terms = termsDao.selectTermsAd(userInfo1.getIuid());

                    if(ad_terms == null)
                        ad_terms = 0;

                    promotionSamsung.setRel_ad_yn(ad_terms);


                    promotionSamsung.setRel_iuid(userInfo1.getIuid());
                    ProfileDetail Rel_profileDetail = profileDetailDao.selectProfileDetail(userInfo1);

                    if(Rel_profileDetail != null)
                    {
                        if(AgeCalcurator.getAge(Rel_profileDetail.getBirth().toString()) <  mcsConfig.getPropertyToInteger("AGE.PERSON.MIN.LIMIT") ) {
                            logger.error("수령자 나이제한에 위배 됨! 나이[{}]", AgeCalcurator.getAge(Rel_profileDetail.getBirth().toString()));
                            promotionSamsung.setResult(3);
                            return HttpStatus.OK;
                        }
                        promotionSamsung.setRel_name( Rel_profileDetail.getName());
                        promotionSamsung.setRel_birth(Rel_profileDetail.getBirth());
                        promotionSamsung.setRel_gender(Rel_profileDetail.getGender());


                    }
                }

                if(StringUtils.equals(promotionSamsung.getPhone_no(), promotionSamsung.getRel_phone_no()))
                {
                    logger.error("요청자의 폰 번호[{}]와 가족 등록 대상자[{}]의 폰번호가 동일 함!!", userInfo.getPhoneNo(), promotionSamsung.getRel_phone_no());
                    promotionSamsung.setResult(5);
                    return  HttpStatus.OK;
                }



                promotionDao.insertPromotionSamsung(promotionSamsung);

                PromotionSamsung promotionSamsung1 = promotionDao.selectPromotionSamsungInfo(authReqHeaders, authReqParams);

                if(promotionSamsung1 != null)
                    promotionSamsung.setResult(0);
                    promotionSamsung.setRel_addr(promotionSamsung1.getRel_addr());
                    promotionSamsung.setRel_addr_no(promotionSamsung1.getRel_addr_no());

            }
            else
            {
                logger.error("요청자의 상태가 군인이 아님[{}]!!", userInfo.getIuid());
                return HttpStatus.METHOD_NOT_ALLOWED;
            }
        }catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;

    }

    public HttpStatus PromotionSamSungInfo(PromotionSamsungInfo promotionSamsungInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams )
            throws Exception
    {
        try {

            /* 사용자 인증 확인 */
            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);



            promotionSamsungInfo.setSamsung_url(mcsConfig.getProperty("URL_DOMAIN") +  mcsConfig.getProperty("URL_SAMSUNG_DOMAIN"));

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return HttpStatus.NOT_FOUND;
            }

            if(!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return HttpStatus.FORBIDDEN;
            }




            PromotionSamsung  promotionSamsung = promotionDao.selectPromotionSamsungInfo(authReqHeaders, authReqParams);
            if(promotionSamsung != null)
            {
                promotionSamsungInfo.PromotionSamsungInfo(promotionSamsung);
                /* 프로필 정보 확인 */

                /*if( promotionSamsung.getRel_phone_no() != null)
                {
                    UserInfo userInfo1 = clientDao.selectUserInfo(promotionSamsung.getRel_phone_no());
                    if(userInfo1.getProfile() != null)
                    {
                        promotionSamsungInfo.setProfile(userInfo1.getProfile());
                    }
                }*/
            }
            else
            {
                return HttpStatus.METHOD_NOT_ALLOWED;
            }


        }catch (Exception e)
        {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }



        return HttpStatus.OK;

    }


    public HttpStatus PromotionList(PromotionListBody promotionListBody, String phone_no, String os_type, String osVersion, String telecomCode)
            throws Exception
    {


        return HttpStatus.OK;
    }

}
