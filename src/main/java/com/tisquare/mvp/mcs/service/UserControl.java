package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.HttpStatus;

import java.io.IOException;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface UserControl {

    public HttpStatus Install(AuthBody authBody,
                              String phoneNo,
                              String authKey,
                              String tnVersion,
                              UserDevice userDevice,
                              Long nid,
                              Long hid,
                              Integer storeType,
                              String deviceModel,
                              String telecomCode)
            throws Exception;

    /*public HttpStatus Cert(CertBody certBody,
                              String phoneNo,
                              Long iuid,
                              String authKey,
                              String tnVersion,
                              UserDevice userDevice,
                              Long nid,
                              Long hid,
                              Integer storeType,
                              String deviceModel,
                              String telecomCode,
                              String deviceId)
            throws Exception;
*/

    public HttpStatus SetMode( String phoneNo,
                           Long iuid,
                           String authKey,
                           Integer mode)
            throws Exception;

    public HttpStatus Presence(Presence presence)
            throws Exception;

    public HttpStatus Presence_info(Presence presence)
            throws Exception;

    public UserInfo getUserInfoByIuidOrE164PhoneNo(String phoneNo) throws IOException;
}
