package com.tisquare.mvp.mcs.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.*;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.db.domain.HelpNoticeCnt;
import com.tisquare.mvp.mcs.entity.EventPopup;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.NoticeControl;
import com.tisquare.mvp.mcs.service.UserControl;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class NoticeControlImpl implements NoticeControl {

    private static final Logger logger = LoggerFactory.getLogger(NoticeControlImpl.class);

    @Autowired
    private InitConfig config;

    @Autowired
    WebdbDao webdbDao;

    @Autowired
    WebViewDao webViewDao;

    ObjectMapper mapper = new ObjectMapper();

    public HttpStatus NoticeList(NoticeListBody noticeListBody, NoticeReqHeaders noticeReqHeaders)
            throws Exception {

        Map<String, Object> param = new HashMap<String, Object>();
        int remain_notice = 0;
        HelpNoticeCnt helpNoticeCnt;

        param.put("sid", noticeReqHeaders.getStart_sid());

        if( noticeReqHeaders.getNotice_cnt() >  Integer.valueOf( config.getListNum()))
            param.put("notice_cnt", Integer.valueOf( config.getListNum()));
        else
            param.put("notice_cnt", noticeReqHeaders.getNotice_cnt());

        param.put("l_read_id",  noticeReqHeaders.getLast_read_sid());


        helpNoticeCnt = webViewDao.selectNoticeTotCnt();

        if( noticeReqHeaders.getStart_sid() != 0 && noticeReqHeaders.getStart_sid() < helpNoticeCnt.getMin_cnt())
        {
            noticeListBody.setResult_code( NoticeListBody.FLAG_IS_FAIL );
            return HttpStatus.OK;
        }
        if( noticeReqHeaders.getStart_sid() == 0)
            param.put("nid", helpNoticeCnt.getMax_cnt() + 1);
        else
            param.put("nid", noticeReqHeaders.getStart_sid());


        List<NoticeJh> moreList;

        moreList = webViewDao.selectNoticeList(param);

        if( moreList.size() > 0 )
        {
            remain_notice =  webViewDao.selectNoticeRemainCnt( moreList.get(moreList.size() - 1).getSid() ) ;
        }

        noticeListBody.setResult_code( NoticeListBody.FLAG_IS_SUCCESS );
        noticeListBody.setRemain_cnt( remain_notice );
        noticeListBody.setTotcnt(helpNoticeCnt.getCnt());
        if( remain_notice > 0 )
            noticeListBody.setIs_end( NoticeListBody.FLAG_IS_NOT_END);
        else
            noticeListBody.setIs_end( NoticeListBody.FLAG_IS_END);


        noticeListBody.setItems( moreList );

        return HttpStatus.OK;

    }

    public HttpStatus Notice(NoticeBody noticeBody, int sid, String telecomCom)
            throws Exception {

        noticeBody.setResult_code( NoticeListBody.FLAG_IS_FAIL);
        NoticeBody notice;

        notice =  webViewDao.selectNotice(sid);


        if( notice != null ) {
            noticeBody.setResult_code(NoticeListBody.FLAG_IS_SUCCESS);
            noticeBody.setContent(notice.getContent());
            noticeBody.setCraete_date(notice.getCraete_date());
            noticeBody.setSid(notice.getSid());
            noticeBody.setTitle(notice.getTitle());
        }
        else
        {
            noticeBody.setContent("null");
            noticeBody.setSid(sid);
            noticeBody.setCraete_date(new Date(0));
            noticeBody.setTitle("null");

        }

        return HttpStatus.OK;

    }

}
