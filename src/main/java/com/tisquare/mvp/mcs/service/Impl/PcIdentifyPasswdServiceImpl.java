package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.ActiveUserDao;
import com.tisquare.mvp.mcs.entity.ActiveUser;
import com.tisquare.mvp.mcs.model.AuthInfoBody;
import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.EmsEvent;
import com.tisquare.mvp.mcs.model.pc.IdentifyPasswd;
import com.tisquare.mvp.mcs.model.pc.PcAuthInfo;
import com.tisquare.mvp.mcs.service.ActiveUserService;
import com.tisquare.mvp.mcs.service.PcIdentifyPasswdControl;
import com.tisquare.mvp.mcs.service.PcIdentifyPasswdService;
import com.tisquare.mvp.mcs.service.ResponseEntityService;
import com.tisquare.mvp.mcs.type.ActiveFlag;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
@Service
public class PcIdentifyPasswdServiceImpl extends ResponseEntityService implements PcIdentifyPasswdService {

    private static final Logger logger = LoggerFactory.getLogger(PcIdentifyPasswdServiceImpl.class);

    @Autowired
    public PcIdentifyPasswdControl pcIdentifyPasswdControl;


    @Override
    public ResponseEntity<String> processPcJoinMailCertResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo)
    {
        try {

            HttpStatus httpStatus = pcIdentifyPasswdControl.processPcjoinMailCertRequest(request, authReqHeaders, authReqParams, pcAuthInfo);

            return makeResponseEntity(httpStatus);

        } catch (Exception e) {
            logger.error("가입 메일 인증 확인 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processPcJoinMailResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo)
    {
        try {

            HttpStatus httpStatus = pcIdentifyPasswdControl.processPcjoinMailResetRequest(request, authReqHeaders, authReqParams, pcAuthInfo);

            return makeResponseEntity(httpStatus);

        } catch (Exception e) {
            logger.error("가입 메일 발송 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processPcId_sendResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo)
    {
        try {

            HttpStatus httpStatus = pcIdentifyPasswdControl.processPcId_sendResetRequest(request, authReqHeaders, authReqParams, pcAuthInfo);

            return makeResponseEntity(httpStatus);

        } catch (Exception e) {
            logger.error("가입자 아이디 발송 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processPcPasswdResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd)
    {
        try {

            HttpStatus httpStatus = pcIdentifyPasswdControl.processPcPasswdResetRequest(request, authReqHeaders, authReqParams, identifyPasswd);

            return makeResponseEntity(httpStatus);

        } catch (Exception e) {
            logger.error("인증번호 확인 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processPcPasswdCertRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd)
    {
        try {

            HttpStatus httpStatus = pcIdentifyPasswdControl.processPcPasswdCertRequest(request, authReqHeaders, authReqParams, identifyPasswd);


            if( httpStatus == HttpStatus.OK) {
                return makeResponseEntityWithCheckBodySize(request, identifyPasswd, HttpStatus.OK);
            } else {
                return makeResponseEntity(httpStatus);
            }

        } catch (Exception e) {
            logger.error("인증번호 확인 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processPcPasswdSmsRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd)
    {
        try {

            HttpStatus httpStatus = pcIdentifyPasswdControl.processPcPasswdSmsRequest(request, authReqHeaders, authReqParams, identifyPasswd);

            return makeResponseEntity(httpStatus);

        } catch (Exception e) {
            logger.error("인증번호 전송 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processPcIdRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd){

        try {

            HttpStatus httpStatus = pcIdentifyPasswdControl.processPcIdRequest(request, authReqHeaders, authReqParams, identifyPasswd);

            if( httpStatus == HttpStatus.OK) {
                return makeResponseEntityWithCheckBodySize(request, identifyPasswd, HttpStatus.OK);
            } else {
                return makeResponseEntity(httpStatus);
            }

        } catch (Exception e) {
            logger.error("Client ID 찾기 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
