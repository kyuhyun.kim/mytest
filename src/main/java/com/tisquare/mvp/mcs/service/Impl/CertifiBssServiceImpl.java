package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.*;
import com.tisquare.mvp.mcs.utils.AsyncProcess;
import com.tisquare.mvp.mcs.utils.OmcWrite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class CertifiBssServiceImpl extends ResponseEntityService implements CertifiBssService {

    private static final Logger logger = LoggerFactory.getLogger(CertifiBssServiceImpl.class);

    @Autowired
    private BssUserControl bssUserControl;

    @Autowired
    TraceService traceService;

    @Autowired
    OmcWrite omcWrite;

    @Autowired
    InitConfig initConfig;

    @Autowired
    AsyncProcess asyncProcess;

    @Autowired
    private ActiveUserService activeUserService;


    @Override
    public  ResponseEntity<String> processClientlogoutRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        try {

            HttpStatus httpStatus = bssUserControl.logout(
                    authReqHeaders,
                    authReqParams);



            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if(httpStatus == HttpStatus.OK){
                return makeResponseEntity(HttpStatus.OK);
            }
            else
            {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



   /* @Override
    public ResponseEntity<String> processClientChgPwdRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Bssinfo bssinfo)
    {
        try {

            HttpStatus httpStatus = bssUserControl.ChgPwd(
                    authReqHeaders.getIuid(),
                    authReqHeaders.getCAuthKey(), bssinfo);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if(httpStatus == HttpStatus.OK){
                return makeResponseEntity(HttpStatus.OK);
            }
            else
            {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

*/

    @Override
    public ResponseEntity<String> processClientMdnChgRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        try {

            HttpStatus httpStatus = bssUserControl.ChgMdn(
                    authReqHeaders,
                    authReqParams);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if(httpStatus == HttpStatus.OK){
                return makeResponseEntity(HttpStatus.OK);
            }
            else
            {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processClientPwdChgRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo)
    {
        try {

            HttpStatus httpStatus = bssUserControl.ChgPwd(
                    authReqHeaders,
                    authReqParams, usInfo);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if(httpStatus == HttpStatus.OK){
                return makeResponseEntity(HttpStatus.OK);
            }
            else
            {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processClientSearchIDRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, ProfileDetail profileDetail)
    {
        long startTime = System.currentTimeMillis();

        try {

            SearchIdResponse searchIdResponse = new SearchIdResponse();
            HttpStatus httpStatus = bssUserControl.SearchID(authReqHeaders, authReqParams, profileDetail, searchIdResponse);


            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if( httpStatus == HttpStatus.OK) {
                return makeResponseEntityWithCheckBodySize(request, searchIdResponse, httpStatus);
            }
            else {
                return makeResponseEntity(httpStatus.value());
            }

        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            e.printStackTrace();
            omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processClienPwdAuthRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo)
    {
        long startTime = System.currentTimeMillis();


        try {

            AuthPwdBody authPwdBody = new AuthPwdBody();
            HttpStatus httpStatus = bssUserControl.PwdAuth(authReqHeaders, authReqParams, usInfo, authPwdBody);


            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if( httpStatus == HttpStatus.OK) {
                return makeResponseEntityWithCheckBodySize(request, authPwdBody, httpStatus);
            }
            else {
                return makeResponseEntity(httpStatus.value());
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            e.printStackTrace();
            omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public ResponseEntity<String> processClientMvpInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo)
    {
        long startTime = System.currentTimeMillis();

        try {

            AuthInfoBody authInfoBody = new AuthInfoBody(request.getHeader("x-real-ip"));

            HttpStatus httpStatus = bssUserControl.Mvpinfo(authInfoBody, authReqHeaders, authReqParams, usInfo);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntityWithCheckBodySize(request, authInfoBody, httpStatus);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                if(authInfoBody.getReason() == null)
                    return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
                else
                    return makeResponseEntityWithCheckBodySize(request, authInfoBody, HttpStatus.METHOD_NOT_ALLOWED);
            }
            else {

                return makeResponseEntityWithCheckBodySize(request, authInfoBody, httpStatus);
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            e.printStackTrace();
            omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public ResponseEntity<String> processClientUserSearchRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UserSearchInfo userSearchInfo)
    {


        try {


            HttpStatus httpStatus = bssUserControl.UserSearch(authReqHeaders, authReqParams, userSearchInfo);


            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if(httpStatus == HttpStatus.OK) {

                return makeResponseEntityWithCheckBodySize(request, userSearchInfo, httpStatus);
            }
            else
            {
                return makeResponseEntity(httpStatus);
            }


        } catch (Exception e) {
            logger.error("사용자 검색 실패." + e.toString());
            e.printStackTrace();
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processClientActiveRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        long startTime = System.currentTimeMillis();

        try {


            HttpStatus httpStatus = bssUserControl.Active(authReqHeaders, authReqParams);




            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                    return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else {

                return makeResponseEntity(httpStatus);
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            e.printStackTrace();
            omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processClientPclogoutRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        try {

            HttpStatus httpStatus = bssUserControl.Pclogout(
                    authReqHeaders,
                    authReqParams);



            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            else if(httpStatus == HttpStatus.OK){
                return makeResponseEntity(HttpStatus.OK);
            }
            else
            {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> processClientPcAuthInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo)
    {
        long startTime = System.currentTimeMillis();

        try {

            AuthInfoBody authInfoBody = new AuthInfoBody(request.getHeader("x-real-ip"));

            HttpStatus httpStatus = bssUserControl.PcAuthinfo(authInfoBody, authReqHeaders, authReqParams, usInfo);

            if( httpStatus == HttpStatus.OK)
                asyncProcess.processLoginUserStat(authInfoBody, authReqHeaders, authReqParams);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                if(authInfoBody.getReason() == null)
                    return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
                else
                    return makeResponseEntityWithCheckBodySize(request, authInfoBody, HttpStatus.METHOD_NOT_ALLOWED);
            }
            else {

                return makeResponseEntityWithCheckBodySize(request, authInfoBody, httpStatus);
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            e.printStackTrace();
            omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @Override
    public ResponseEntity<String> processClientAuthInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo)
    {


        long startTime = System.currentTimeMillis();

        try {

            AuthInfoBody authInfoBody = new AuthInfoBody(request.getHeader("x-real-ip"));

            HttpStatus httpStatus = bssUserControl.Authinfo(authInfoBody, authReqHeaders, authReqParams, usInfo);

            if( httpStatus == HttpStatus.OK)
                asyncProcess.processLoginUserStat(authInfoBody, authReqHeaders, authReqParams);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if( httpStatus == HttpStatus.NOT_FOUND) {
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            } else if( httpStatus == HttpStatus.METHOD_NOT_ALLOWED) {
                if(authInfoBody.getReason() == null)
                    return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
                else
                    return makeResponseEntityWithCheckBodySize(request, authInfoBody, HttpStatus.METHOD_NOT_ALLOWED);
            }
            else {

                return makeResponseEntityWithCheckBodySize(request, authInfoBody, httpStatus);
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패." + e.toString());
            e.printStackTrace();
            omcWrite.EmsEventLog( new EmsEvent(authReqHeaders, authReqParams, HttpStatus.INTERNAL_SERVER_ERROR, request.getRequestURI(), (System.currentTimeMillis() - startTime), initConfig) );
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
