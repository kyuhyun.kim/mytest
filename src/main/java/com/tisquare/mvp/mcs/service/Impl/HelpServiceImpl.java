package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class HelpServiceImpl extends ResponseEntityService implements HelpService {

    private static final Logger logger = LoggerFactory.getLogger(HelpServiceImpl.class);

    @Autowired
    private HelpControl HelpControl;

    @Override
    public ResponseEntity<String> processHelpListRequest(HttpServletRequest request, HelpReqHeaders helpReqHeaders)
    {
        try {


            HelpListBody helpListBody = new HelpListBody();

            HttpStatus httpStatus = HelpControl.HelpList(helpListBody, helpReqHeaders);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else {
                return makeResponseEntityWithCheckBodySize(request, helpListBody, httpStatus);
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @Override
    public ResponseEntity<String> processHelpRequest(HttpServletRequest request, int hid)
    {
        try {

                HelpBody helpBody = new HelpBody();
                HttpStatus httpStatus = HelpControl.Help( helpBody, hid );




                if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                    return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
                } else if (httpStatus == HttpStatus.FORBIDDEN) {
                    return makeResponseEntity(HttpStatus.FORBIDDEN);
                } else {
                    return makeResponseEntityWithCheckBodySize(request, helpBody, httpStatus);
                }

            } catch (Exception e) {
                logger.error("Client 인증 실패.", e);
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }


    }



}
