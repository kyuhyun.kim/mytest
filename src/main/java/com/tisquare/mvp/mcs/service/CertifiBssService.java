package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface CertifiBssService {


    /**
     * BSS 클라이언트 인증 요청에 대한 처리를 한다.
     *
     * @param request        서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */

    ResponseEntity<String> processClientlogoutRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    ResponseEntity<String> processClientAuthInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo);

    ResponseEntity<String> processClientMvpInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo);

    ResponseEntity<String> processClientPwdChgRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo);

    ResponseEntity<String> processClientMdnChgRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    ResponseEntity<String> processClienPwdAuthRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo);

    ResponseEntity<String> processClientSearchIDRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, ProfileDetail profileDetail);

    ResponseEntity<String> processClientActiveRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    ResponseEntity<String> processClientUserSearchRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UserSearchInfo userSearchInfo);

    ResponseEntity<String> processClientPcAuthInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo);

    ResponseEntity<String> processClientPclogoutRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

}
