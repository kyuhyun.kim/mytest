package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface TestService {


    /**
     * SCS의 가입 알람 요청에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders   고객 정보

     * @return 가입 알람 요청 처리에 대한 응답
     */

    ResponseEntity<String> processTestRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders);

    ResponseEntity<String> processTestAllRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders);

}
