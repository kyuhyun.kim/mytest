package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.HttpStatus;

import java.io.IOException;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface IPCControl {

    public void sendPnsMultiSms(MultiMsgInfo multiMsgInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    public void PAS_LogoutRequest(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    public HttpStatus   PNS_MultiSmsRequest(MultiMsgInfo multiMsgInfo, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, MsgInfoResult msgInfoResult) ;
    /*public void sendPasLogoutNotification(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);*/


}
