package com.tisquare.mvp.mcs.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.BssCompanyDao;
import com.tisquare.mvp.mcs.db.SystemInfoDao;
import com.tisquare.mvp.mcs.db.WebViewDao;
import com.tisquare.mvp.mcs.db.WebdbDao;
import com.tisquare.mvp.mcs.db.domain.AppUpdatetInfo;
import com.tisquare.mvp.mcs.db.domain.HelpNoticeCnt;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.DownloadControl;
import com.tisquare.mvp.mcs.service.NoticeControl;
import com.tisquare.mvp.mcs.type.Cp_status;
import com.tisquare.mvp.mcs.type.SUBS_TYPE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class DownloadControlImpl implements DownloadControl {

    private static final Logger logger = LoggerFactory.getLogger(DownloadControlImpl.class);

    @Autowired
    private InitConfig config;

    @Autowired
    BssCompanyDao bssCompanyDao;

    @Autowired
    SystemInfoDao systemInfoDao;

    ObjectMapper mapper = new ObjectMapper();

    public ResponseEntity<String> Download(Download download)
            {

        HttpHeaders headers = new HttpHeaders();

                headers.add("Location",  config.getUrl_domain() + "pages/UrlShortner_404.html");
                /*headers.add("Location", "http://127.0.0.1:7100/acs/pages/UrlShortner_404.html");*/
        try {


            BssCompany bssCompany = bssCompanyDao.selectDownloadFlagBssCompany(download);

            if (bssCompany == null || bssCompany.getCp_status() != Cp_status.USE.getCode()) {
                return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
            }




        /* 앱 업데이트 정보 확인 */
            AppUpdatetInfo appUpdatetInfo = systemInfoDao.getAppUpdate(2, SUBS_TYPE.B2B.getCode());

            if (appUpdatetInfo != null) {


                headers.clear();
                headers.add("Location", config.getUrl_domain() + "pages/download_ok.html?agr="+ appUpdatetInfo.getDownloadUrl());

                try {

                    logger.info("RedirectUrl[{}]", headers.get("Location"));
                } catch (Exception pe) {
                    logger.error("설치 인증 페이지 연동 실패!", pe);
                }

            }

        }
        catch (Exception e)
        {

            logger.error(e.toString());
        }

        return new ResponseEntity(headers, HttpStatus.MOVED_PERMANENTLY);
    }



}
