package com.tisquare.mvp.mcs.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.connect.BlockInterFaceImpl;
import com.tisquare.mvp.mcs.db.*;
import com.tisquare.mvp.mcs.db.domain.*;
import com.tisquare.mvp.mcs.entity.EventPopup;
import com.tisquare.mvp.mcs.interact.HttpAsyncClientWorker;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.BssUserControl;
import com.tisquare.mvp.mcs.service.IPCControl;
import com.tisquare.mvp.mcs.service.TraceService;
import com.tisquare.mvp.mcs.type.*;
import com.tisquare.mvp.mcs.type.alarmType.AlarmType;
import com.tisquare.mvp.mcs.type.mas.Sched_type;
import com.tisquare.mvp.mcs.utils.AsyncProcess;
import com.tisquare.mvp.mcs.utils.DateUtil;
import com.tisquare.mvp.mcs.utils.ValidCheck;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class BssUserControlImpl implements BssUserControl {

    private static final Logger logger = LoggerFactory.getLogger(BssUserControlImpl.class);

    private final static String LINE = "\r\n";

    private static int FCS_IDX = 1;

    @Autowired
    IPCControl ipcControl;

    @Autowired
    private InitConfig config;

    @Autowired
    SystemInfoDao systemInfoDao;

    @Autowired
    ClientDao clientDao;

    @Autowired
    WebdbDao webdbDao;

    @Autowired
    BtinfoDao btinfoDao;

    @Autowired
    ServerDao serverDao;

    @Autowired
    WebViewDao webViewDao;

    @Autowired
    PresenceDao presenceDao;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    TraceService traceService;

    @Autowired
    BlockInterFaceImpl blockInterFace;

    @Autowired
    BssCompanyDao bssCompanyDao;

    @Autowired
    BssEmpDao bssEmpDao;

    @Autowired
    BssCmdDao bssCmdDao;

    @Autowired
    BssCpAdminDao bssCpAdminDao;

    @Autowired
    ProfileDetailDao profileDetailDao;

    @Autowired
    EventNoticeDao eventNoticeDao;

    @Autowired
    TermsDao termsDao;

    @Autowired
    Cos_CmmntyDao cos_cmmntyDao;

    @Autowired
    MasDao masDao;

    @Autowired
    AsyncProcess asyncProcess;

    @Autowired
    HomeSchedInfoDao homeSchedInfoDao;

    @Autowired
    TSbsDao sbsDao;

    @Autowired
    UserSearchDao userSearchDao;


    ObjectMapper mapper = new ObjectMapper();


    @Override
    public HttpStatus ChgPwd(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo)
            throws Exception {
        try {
            if (usInfo.getNew_user_pwd() == null)
                return HttpStatus.BAD_REQUEST;

            UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(authReqHeaders, authReqParams, usInfo);

            if (userInfo == null) {
                return HttpStatus.NOT_FOUND;
            }

            if (userInfo.getSubsType().value() != LoginType.NORMAL.getCode())
                return HttpStatus.METHOD_NOT_ALLOWED;


             /* PASSWORD 확인 */
             /* 비밀 번호 찾기/변경 여부 확인 현재 passwd 정보가 null이면 비밀번호 찾기*/
            if (usInfo.getUser_pwd() != null) {
                logger.info("계정 비밀번호 변경 호출 대상.");
                if (!StringUtils.equals(usInfo.getUser_pwd(), userInfo.getSuidPw())) {
                    logger.error("계정 비밀 번호 불일 치 ID[{}] DB_PWD[{}] REQ_PWD[{}]", userInfo.getSuid(), userInfo.getSuidPw(), usInfo.getUser_pwd());
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
            } else {
                logger.info("비밀번호 찾기 대상.");
            }

            usInfo.setIuid(userInfo.getIuid());
            clientDao.updatePasswd(usInfo);

        } catch (Exception e) {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    @Override
    public HttpStatus SearchID(
            ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, ProfileDetail profileDetail, SearchIdResponse searchIdResponse)
            throws Exception {
        try {

            if (profileDetail.getBirth() == null) {
                logger.error("가입자 생년월일 [{}]", profileDetail.getBirth());
                return HttpStatus.BAD_REQUEST;
            }

            UserInfo userInfo = getUserCertByPhoneNo(authReqHeaders, authReqParams);
            if (userInfo == null) {
                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

              /* 생년월일 정보 확인 */
            ProfileDetail profileDetail1 = profileDetailDao.selectProfileDetail(userInfo);

            if (profileDetail != null) {
                if (profileDetail.getBirth().longValue() == profileDetail1.getBirth().longValue()) {
                    searchIdResponse.setUser_reg_info(new User_Reg_Info(userInfo, profileDetail1));
                } else {
                    logger.error("생년월일 정보 불일 치 [{}][{}].", profileDetail.getBirth().longValue(), profileDetail1.getBirth().longValue());
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
            } else {
                logger.error("Profile Detail 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

        } catch (Exception e) {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    @Override
    public HttpStatus logout(
            ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
            throws Exception {

        try {


            UserInfo userInfo = getUserCertByIuid(authReqHeaders, authReqParams);
            if (userInfo == null) {
                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

            if (!StringUtils.equals(authReqHeaders.getCAuthKey(), userInfo.getCauthKey())) {
                logger.error("인증 키 불일 치.[{}][{}]", authReqHeaders.getCAuthKey(), userInfo.getCauthKey());
                return HttpStatus.FORBIDDEN;
            }

            if( authReqHeaders.getStoreType() == null)
                clientDao.updateIndividualClientLogout(userInfo.getIuid());
        } catch (Exception e) {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    @Override
    public HttpStatus Pclogout(
            ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
            throws Exception {

        try {


            UserInfo userInfo = getUserCertByIuid(authReqHeaders, authReqParams);
            if (userInfo == null) {
                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

            if (!StringUtils.equals(authReqHeaders.getCAuthKey(), userInfo.getCauthKey())) {
                logger.error("인증 키 불일 치.[{}][{}]", authReqHeaders.getCAuthKey(), userInfo.getCauthKey());
                return HttpStatus.FORBIDDEN;
            }

        } catch (Exception e) {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    @Override
    public HttpStatus UserSearch(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UserSearchInfo userSearchInfo) throws Exception
    {
        try {

            if (authReqHeaders.getIuid() == null || authReqHeaders.getCAuthKey() == null)
                return HttpStatus.BAD_REQUEST;

            if( userSearchInfo == null || userSearchInfo.getUserSearch() == null ||  userSearchInfo.getUserSearch().getMsg() == null)
            {
                logger.info("필수 항목 누락");
                return HttpStatus.BAD_REQUEST;
            }


                 /*한글 포함 여부 확인*/
                if(ValidCheck.CheckMdnSearch(userSearchInfo.getUserSearch().getMsg())) {

                    userSearchInfo.getUserSearch().setType(UserSearchType.E_164.getCode());
                }
                else
                {
                    userSearchInfo.getUserSearch().setType(UserSearchType.NICKNAME.getCode());
                }

                if (userSearchInfo.getUserSearch().getType() == UserSearchType.NICKNAME.getCode()) {
                    if (userSearchInfo.getUserSearch().getMsg().length() < mcsConfig.getPropertyToInteger("NAME.LIMIT.LENGTH")) {
                        logger.error("이름은 {}자리 이상 검색 가능.[{}][{}]", mcsConfig.getPropertyToInteger("NAME.LIMIT.LENGTH"), userSearchInfo.getUserSearch().getMsg(), userSearchInfo.getUserSearch().getMsg().length());
                        return HttpStatus.BAD_REQUEST;
                    }
                }
                if (userSearchInfo.getUserSearch().getType() == UserSearchType.E_164.getCode()) {
                    if (userSearchInfo.getUserSearch().getMsg().length() < mcsConfig.getPropertyToInteger("MDN.LIMIT.LENGTH")) {
                        logger.error("전화번호는 {}자리 이상 검색 가능.[{}][{}]", mcsConfig.getPropertyToInteger("MDN.LIMIT.LENGTH"),
                                userSearchInfo.getUserSearch().getMsg(), userSearchInfo.getUserSearch().getMsg().length());
                        return HttpStatus.BAD_REQUEST;
                    }
                    if (userSearchInfo.getUserSearch().getMsg().length() > mcsConfig.getPropertyToInteger("MDN.MAX.LIMIT.LENGTH")) {
                        logger.error("전화번호는 {}자리 이하 검색 가능.[{}][{}]", mcsConfig.getPropertyToInteger("MDN.MAX.LIMIT.LENGTH"),
                                userSearchInfo.getUserSearch().getMsg(), userSearchInfo.getUserSearch().getMsg().length());
                        return HttpStatus.BAD_REQUEST;
                    }

                }



            UserInfo userInfo = getUserCertByIuid(authReqHeaders, authReqParams);
            if (userInfo == null) {
                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

            if (!StringUtils.equals(authReqHeaders.getCAuthKey(), userInfo.getCauthKey())) {
                logger.error("인증 키 불일 치.[{}][{}]", authReqHeaders.getCAuthKey(), userInfo.getCauthKey());
                return HttpStatus.FORBIDDEN;
            }


            /*List<UserList> userListsMdn = userSearchDao.selectUserSearchByTypeMdn(userSearchInfo.getUserSearch());

            userSearchInfo.setUserListListMdn(userListsMdn);

            List<UserList> userListsName = userSearchDao.selectUserSearchByTypeName(userSearchInfo.getUserSearch());
*/

            userSearchInfo.getUserSearch().setType(UserSearchType.NICKNAME.getCode());
            List<UserList> userLists = userSearchDao.selectUserSearchByMaskType(userSearchInfo.getUserSearch());

            /*if(userSearchInfo.getUserSearch().getType() == UserSearchType.E_164.getCode())
            {
                List<UserList> userLists_name = userSearchDao.selectUserSearchByType(new UserSearch( UserSearchType.NICKNAME.getCode(), userSearchInfo.getUserSearch().getMsg()));


                for(UserList userList_name : userLists_name)
                {
                    boolean addflag = true;

                    for(UserList userList : userLists) {
                        if( userList.getIuid().equals(userList_name.getIuid()))
                        {
                           addflag = false;
                            break;
                        }
                    }
                    if(addflag)
                        userLists.add(userList_name);
                }
            }*/

            userSearchInfo.setUserLists(userLists);

        } catch (Exception e) {
            logger.error("사용자 검색 실패!!", e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    @Override
    public HttpStatus ChgMdn(
            ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
            throws Exception {

        try {

            if (authReqHeaders.getIuid() == null || authReqHeaders.getCAuthKey() == null)
                return HttpStatus.BAD_REQUEST;

            UserInfo userInfo = getUserCertByIuid(authReqHeaders, authReqParams);
            if (userInfo == null) {
                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

            if (!StringUtils.equals(authReqHeaders.getCAuthKey(), userInfo.getCauthKey())) {
                logger.error("인증 키 불일 치.[{}][{}]", authReqHeaders.getCAuthKey(), userInfo.getCauthKey());
                return HttpStatus.FORBIDDEN;
            }

            if (StringUtils.equals(authReqHeaders.getPhoneNo(), userInfo.getLogin_phoneNo())) {
                logger.error("현재 번호와 동일한 폰 정보.[{}][{}]", authReqHeaders.getPhoneNo(), userInfo.getLogin_phoneNo());
                return HttpStatus.METHOD_NOT_ALLOWED;
            }

            /* 로그인 단말 업데이트 */
            clientDao.updateChgLoginMdn(authReqHeaders, authReqParams);
            logger.info("단말 번호 변경 성공 [{}] ---> [{}]", userInfo.getLogin_phoneNo(), authReqHeaders.getPhoneNo());

            /* 번호 변경 알림 전송 */

            if (mcsConfig.getProperty("notification.target.chg.mdn") != null) {
                String[] TargetProcess = mcsConfig.getProperty("notification.target.chg.mdn").split("\\|");
                for (int nIdx = 0; nIdx < TargetProcess.length; nIdx++) {
                    String Url = mcsConfig.getProperty("NGINX.INTERNAL.VIP") + "/" + TargetProcess[nIdx].toLowerCase().toString() + "/" + AlarmType.CHG_MDN.getCode();
                    sendJoinNotification(Url, authReqHeaders, authReqParams);
                }
                logger.info("번호 변경 알림 요청 확인");
            }
        } catch (Exception e) {
            logger.error("단말번호 변경 실패!!", e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    @Override
    public HttpStatus ChgPwd(
            Long iuid,
            String authKey,
            Bssinfo bssinfo)
            throws Exception {
        try {

            if (bssinfo == null || bssinfo.getBss_pwd() == null || bssinfo.getNew_bss_pwd() == null)
                return HttpStatus.BAD_REQUEST;

            if (StringUtils.equals(bssinfo.getBss_pwd(), bssinfo.getNew_bss_pwd())) {
                return HttpStatus.OK;
            }

            /* 기존 번호를 확인 한다 */
            BssCommander bssCommander = bssCmdDao.selectCommanderUserPwd(bssinfo);
            if (bssCommander == null)
                return HttpStatus.METHOD_NOT_ALLOWED;
            /* 인증 키 불일치 여부 확인 */
            if (!StringUtils.equals(bssCommander.getCauth_key(), authKey))
                return HttpStatus.NOT_FOUND;
            /* IUID 불일치 여부 확인 */
            if (bssCommander.getIuid() != iuid)
                return HttpStatus.FORBIDDEN;

            /* 비밀번호 업데이트 */
            bssCmdDao.updateCommanderUserPwd(bssinfo);
        } catch (Exception e) {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

   /* @Override
    public HttpStatus ChgMdn(
            Long iuid,
            String authKey,
            Bssinfo bssinfo)
            throws Exception
    {
        try {

            if(bssinfo.getMdn() == null)
                return HttpStatus.BAD_REQUEST;

            UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(bssinfo);

            if (userInfo == null)
                return HttpStatus.METHOD_NOT_ALLOWED;

            if(!StringUtils.equals(userInfo.getCauthKey(), authKey))
                return HttpStatus.FORBIDDEN;

            if(userInfo.getIuid().equals(  iuid ))
                clientDao.updateChgMdnClient(bssinfo, iuid);
            else
                return HttpStatus.NOT_FOUND;


        }
        catch (Exception e)
        {
            logger.error(e.toString());
        }

        return HttpStatus.OK;
    }*/

    @Override
    public HttpStatus PwdAuth(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo, AuthPwdBody authPwdBody) throws Exception {
        try {
            logger.info(usInfo.toString());
            UserInfo userInfo = getUserCertIDPhoneNo(authReqHeaders, authReqParams, usInfo);

            if (userInfo == null) {
                return HttpStatus.NOT_FOUND;
            }

            if (!StringUtils.equals(userInfo.getPhoneNo(), authReqHeaders.getPhoneNo())) {
                logger.error("폰번호 불일 치!! [{}] [{}]", userInfo.getPhoneNo(), authReqHeaders.getPhoneNo());
                return HttpStatus.METHOD_NOT_ALLOWED;
            }

            authPwdBody.setAuthKey(userInfo.getCauthKey());
            authPwdBody.setSubsType(userInfo.getSubsType().value());
            authPwdBody.setIuid(userInfo.getIuid());
            authPwdBody.setUser_type(userInfo.getUser_type());


        } catch (Exception e) {
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }


    @Override
    public HttpStatus Mvpinfo(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws Exception {
        try {



            /* 신규 공지 방식 변경 */
            EventPopup eventPopup = systemInfoDao.getEventPopup();
            if (eventPopup != null) {
                authInfoBody.setEventPopup(eventPopup);

                int count = systemInfoDao.isSystemUnavailable();

                if (count > 0) {
                 /* 공지 시간 OK & 작업 시간은 OK */
                    authInfoBody.getEventPopup().setForceStop(EventPopup.FORCE_UPDATE);
                    logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                    return HttpStatus.SERVICE_UNAVAILABLE;
                }
            }


            switch (String.valueOf(authReqHeaders.getSubs_type())) {
                case SubsTypeList.NORMAL:
                case SubsTypeList.NAVER:
                case SubsTypeList.GOOGLE:
                case SubsTypeList.KAKAO:
                case SubsTypeList.FACEBOOK:
                    break;
                default:
                    logger.error("정의 되지 않은 인증 타입입니다.TN-SUBS-TYPE = {}", authReqHeaders.getSubs_type());
                    return HttpStatus.BAD_REQUEST;


            }

            /* 앱 업데이트 정보 확인 */
            AppUpdatetInfo appUpdatetInfo = systemInfoDao.getAppUpdate(authReqHeaders.getStoreType(), authReqHeaders.getSubs_type());

            if (appUpdatetInfo != null) {
                authInfoBody.setApp_update(appUpdatetInfo);
                if (appUpdatetInfo.needForceUpdate(authReqHeaders.getAppVersion())) {
                    logger.error("Not valid app version");
                    appUpdatetInfo.setForceUpdate(AppUpdatetInfo.FORCE_UPDATE);
                    authInfoBody.setApp_update(appUpdatetInfo);
                    return HttpStatus.NOT_ACCEPTABLE;
                }
            }


            /* 자동 로그인 여부 확인 */

            UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(authReqHeaders, authReqParams, usInfo);
            Login_info login_info = new Login_info();


            if (FCS_IDX == config.getMAX_FCS_IDX())
                FCS_IDX = 0;

            authInfoBody.setServerList(serverDao.findServers3(FCS_IDX++));

            StringBuilder sbUrlFix = new StringBuilder(config.getUrl_domain());
            for (int nIdx = 0; nIdx < authInfoBody.getServerList().size(); nIdx++) {
                if (authInfoBody.getServerList().get(nIdx).getName().toUpperCase().equals("WS") && authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toUpperCase().equals("HTTPS")) {
                    sbUrlFix.setLength(0);
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toLowerCase());
                    sbUrlFix.append("://");
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getIpAddr().toLowerCase());
                    sbUrlFix.append(":");
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getPort());
                    sbUrlFix.append("/");
                    sbUrlFix.append(config.getProcess_name().toLowerCase());
                    sbUrlFix.append("/");
                    break;
                }
            }


            /* OS_TYPE 체크 */
            UrlInfo urlInfo = new UrlInfo();
            urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain());
            urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad());


            urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy());
                    /*urlInfo.setPrivacy_info("https://okitalkie-pro.com/acs/pages/privacy_company.html");*/
            urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th());
            switch (authReqParams.getOsType().toLowerCase()) {

                case OS_TYPE.OS_TYPE_IOS:
                    urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss_ios());
                    break;
                default:

                    urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss());
                    break;
            }

            urlInfo.setPrivacy_op_info(sbUrlFix.toString() + config.getUrl_privacy_option_domain());
            urlInfo.setPrivacy_md_info(sbUrlFix.toString() + config.getUrl_privacy_mandatory_domain());
            urlInfo.setLocation_info(sbUrlFix.toString() + config.getUrl_location());

            /* 나이스 관련 약관 정보 세팅 */
            urlInfo.setNiceUrlInfo(new NiceUrlInfo(sbUrlFix.toString(),
                    mcsConfig.getProperty("URL_TELECOM_LIST"),
                    mcsConfig.getProperty("URL_NICE_PRIVACY_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_UNIQUE_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_TELECOM_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_AGREEMENT_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_SAFE_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_LGU_OTP_DOMAIN")
                    , mcsConfig.getProperty("URL_NICE_INFO")));

            authInfoBody.setUrlInfo(urlInfo);

            if (userInfo == null) {

                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;

    }

    @Override
    public HttpStatus Active(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws IOException {

        try {

            UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(authReqHeaders, authReqParams, null);


            if (userInfo == null) {
                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

            if( !StringUtils.equals(userInfo.getDeviceId(), authReqParams.getDeviceId()))
            {
                logger.error("디바이스 정보 불일치. (로그아웃 처리 대상자)");
                return HttpStatus.METHOD_NOT_ALLOWED;
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;

    }

    @Override
    public HttpStatus PcAuthinfo(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws Exception
    {
        try {

            /* 신규 공지 방식 변경 */
            EventPopup eventPopup = systemInfoDao.getEventPopup();
            if (eventPopup != null) {
                authInfoBody.setEventPopup(eventPopup);

                int count = systemInfoDao.isSystemUnavailable();

                if (count > 0) {
                 /* 공지 시간 OK & 작업 시간은 OK */
                    authInfoBody.getEventPopup().setForceStop(EventPopup.FORCE_UPDATE);
                    logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                    return HttpStatus.SERVICE_UNAVAILABLE;
                }
            }

            /* 신규 공지 방식 추가 */
          /*  List<EventPopup> eventPopups = systemInfoDao.getEventPopupList();
            if (eventPopups != null) {
                authInfoBody.setEventPopupList(eventPopups);

                int count = systemInfoDao.isSystemUnavailable();

                if (count > 0) {
                        *//* 공지 시간 OK & 작업 시간은 OK*//*
                    logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                    return HttpStatus.SERVICE_UNAVAILABLE;
                }
            }
*/

            switch (String.valueOf(authReqHeaders.getSubs_type())) {
                case SubsTypeList.NORMAL:
                case SubsTypeList.NAVER:
                case SubsTypeList.GOOGLE:
                case SubsTypeList.KAKAO:
                case SubsTypeList.FACEBOOK:
                    break;
                default:
                    logger.error("정의 되지 않은 인증 타입입니다.TN-SUBS-TYPE = {}", authReqHeaders.getSubs_type());
                    return HttpStatus.BAD_REQUEST;

            }

            /* 앱 업데이트 신규 정책 백업 (2018.06.04)*//*
            AppUpdatetInfo appUpdatetInfo = systemInfoDao.getAppUpdate(authReqHeaders.getStoreType(), authReqHeaders.getSubs_type());

            if (appUpdatetInfo != null) {

                if (appUpdatetInfo.needForceUpdate(authReqHeaders.getAppVersion())) {
                    logger.error("Not valid app version");
                    appUpdatetInfo.setForceUpdate(AppUpdatetInfo.FORCE_UPDATE);
                    authInfoBody.setApp_update(appUpdatetInfo);
                    return HttpStatus.NOT_ACCEPTABLE;
                }

                if (appUpdatetInfo.needUpdateIs(authReqHeaders.getAppVersion())) {
                    appUpdatetInfo = null;
                }

            }*/
            /*authInfoBody.setApp_update(appUpdatetInfo);*/

            /* 자동 로그인 여부 확인 */
            UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(authReqHeaders, authReqParams, usInfo);
            Login_info login_info = new Login_info();

            if (userInfo == null) {

                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

            /* 자동 로그인 여부 확인 */
            if (authReqHeaders.getCAuthKey() == null) {
                if (authReqHeaders.getSubs_type() == com.tisquare.mvp.mcs.entity.SubscriberType.NORMAL.value() && (!StringUtils.equals(usInfo.getUser_pwd(), userInfo.getSuidPw()))) {
                    logger.error("가입자[{}] 비밀번호 불일치.[{}][{}]", userInfo.getSuid(), usInfo.getUser_pwd(), userInfo.getSuidPw());
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
            } else {

                if (!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey())) {
                    logger.error("인증키 불일 치.[{}][{}]", userInfo.getCauthKey(), authReqHeaders.getCAuthKey());
                    return HttpStatus.FORBIDDEN;
                }
            }

            authInfoBody.setPhone_no(userInfo.getPhoneNo());
            authInfoBody.setUser_id(userInfo.getSuid());
            authInfoBody.setUser_type(userInfo.getUser_type());
            authInfoBody.setSubsType(userInfo.getSubsType().value());
            authInfoBody.setPtoken(userInfo.getPnotiToken());
            authInfoBody.setAuthKey(userInfo.getCauthKey());
            authInfoBody.setProfile(userInfo.getProfile());

            if (authReqParams.getNid() == null) {
                authReqParams.setNid(0L);
            }
            if (authReqParams.getHid() == null) {
                authReqParams.setHid(0L);
            }
            if (authReqParams.getSid() == null) {
                authReqParams.setSid(0L);
            }


            Long lastNid;

            if (FCS_IDX == config.getMAX_FCS_IDX())
                FCS_IDX = 0;

            authInfoBody.setServerList(serverDao.findServers3(FCS_IDX++));

            StringBuilder sbUrlFix = new StringBuilder(config.getUrl_domain());
            for (int nIdx = 0; nIdx < authInfoBody.getServerList().size(); nIdx++) {
                if (authInfoBody.getServerList().get(nIdx).getName().toUpperCase().equals("WS") && authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toUpperCase().equals("HTTPS")) {
                    sbUrlFix.setLength(0);
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toLowerCase());
                    sbUrlFix.append("://");
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getIpAddr().toLowerCase());
                    sbUrlFix.append(":");
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getPort());
                    sbUrlFix.append("/");
                    sbUrlFix.append(config.getProcess_name().toLowerCase());
                    sbUrlFix.append("/");
                    break;
                }
            }


            /* OS_TYPE 체크 */
            UrlInfo urlInfo = new UrlInfo();

            urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain());
            urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad());
            urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy());
            /*urlInfo.setPrivacy_info("https://okitalkie-pro.com/acs/pages/privacy_company.html");*/
            urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th());
            urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss());
                        urlInfo.setPrivacy_op_info(sbUrlFix.toString() + config.getUrl_privacy_option_domain());
            urlInfo.setPrivacy_md_info(sbUrlFix.toString() + config.getUrl_privacy_mandatory_domain());
            urlInfo.setLocation_info(sbUrlFix.toString() + config.getUrl_location());

            /* 나이스 관련 약관 정보 세팅 *//*
            urlInfo.setNiceUrlInfo(new NiceUrlInfo(sbUrlFix.toString(), mcsConfig.getProperty("URL_TELECOM_LIST"), mcsConfig.getProperty("URL_NICE_PRIVACY_DOMAIN"), mcsConfig.getProperty("URL_NICE_UNIQUE_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_TELECOM_DOMAIN"), mcsConfig.getProperty("URL_NICE_AGREEMENT_DOMAIN"), mcsConfig.getProperty("URL_NICE_SAFE_DOMAIN"), mcsConfig.getProperty("URL_NICE_LGU_OTP_DOMAIN")
                    , mcsConfig.getProperty("URL_NICE_INFO")));
*/
  /*          authInfoBody.setUrlInfo(urlInfo);
*/
            long lastHid = 0;
            int newHelpCnt = webViewDao.selectHelpNewCnt(authReqParams.getHid());


            lastHid = webViewDao.selectHelpLastHid();


            Integer newNoticeCnt = webdbDao.newNoticeCnt(authReqParams.getNid());
            String lastNoticeTime = webdbDao.getLastNoticeTime();
            lastNid = webdbDao.getLastNid();

            Notice notice = new Notice();

            if (newNoticeCnt != 0)
                notice.setNew_notice_cnt(newNoticeCnt);

            if (lastNoticeTime != null)
                notice.setLast_notice_time(lastNoticeTime);

            if (lastNid != null)
                notice.setLast_nid(lastNid);

            authInfoBody.setNoticeInfo(new NoticeInfo(notice, null));

            HelpInfo helpInfo = new HelpInfo(lastHid, newHelpCnt, sbUrlFix.toString() + config.getUrlHelpDomain());
            authInfoBody.setHelpInfo(helpInfo);



            /* 군인 정보 확인 */
            authInfoBody.setProfileDetail(profileDetailDao.selectProfileDetail(userInfo));

            if (authInfoBody.getProfileDetail().getType() == USER_TYPE.MILITARY.getCode()) {
                MasTrainee masTrainee = masDao.selectMasTraniee(userInfo.getIuid());
                if (masTrainee != null) {
                    authInfoBody.getProfileDetail().setMasTrainee(masTrainee);
                    /* 입대일 세팅 */

                }

                List<MasSched> masScheds = masDao.selectMasSched(userInfo.getIuid(), Sched_type.진급.getCode());

                if (masScheds != null && masScheds.size() != 0) {
                    /* 계급 확인*/
                    boolean advance_flag = false;

                    for (int nIdx = 0; nIdx < masScheds.size(); nIdx++) {
                         /* 진급일 세팅 */
                        if (masScheds.get(nIdx).getSCHEDULE_DATE() != null)
                            authInfoBody.getProfileDetail().setUpgrade_date(Long.valueOf(masScheds.get(nIdx).getSCHEDULE_DATE()));

                        Long diffDay = Long.valueOf(masScheds.get(nIdx).getSCHEDULE_DATE()) - Long.valueOf(DateUtil.toDATE_YYYYMMDD_String());
                        if (diffDay > -1) {
                            /* 진급 예정으로 판단 */
                            authInfoBody.getProfileDetail().setRank(masScheds.get(nIdx).getADVANCE_TYPE() - 1);
                            advance_flag = true;
                        }
                    }

                    if (!advance_flag) {
                        /* 진급 완료로 판단 */
                        authInfoBody.getProfileDetail().setRank(masScheds.get(0).getADVANCE_TYPE());
                        authInfoBody.getProfileDetail().setUpgrade_date(Long.valueOf(masScheds.get(0).getSCHEDULE_DATE()));
                    }
                }

            }
            /* 프로모션 정보 확인 */
            List<EventNotice> eventNotices = eventNoticeDao.selectWebEventNoticeType(authInfoBody);

            if (authInfoBody.getProfileDetail().getType() == USER_TYPE.MILITARY.getCode()) {
                if (eventNotices != null) {

                    for (int nIdx = 0; nIdx < eventNotices.size(); nIdx++) {
                        if (authInfoBody.getProfileDetail().getEnter_date() < Long.valueOf(mcsConfig.getProperty("base.enter.date"))) {

                            if (StringUtils.equals(eventNotices.get(nIdx).getLanding_menu(), "500"))
                                eventNotices.remove(nIdx);
                        }
                    }

                }
            }

            authInfoBody.setEventNotices(eventNotices);
            /* 약관 동의 정보 확인 */
            authInfoBody.setTermses(termsDao.selectTerms(userInfo));

            /* 페널티 부여 확인 */


            authInfoBody.setCosCmmntyPenalties(cos_cmmntyDao.selectCosCmmntyPenalty(userInfo));

            /* 개인 설정 정보 세팅 */
            if (userInfo.getHome_sched_iuid() != null)
                authInfoBody.setPrivacyInfo(new PrivacyInfo(userInfo));

  /*          *//* DEIVCE가 변경 되었고 최종 로그인 상태가 로그인이면 타 단말에 로그인 되어 있는 것은 로그아웃 시킨다 *//*
            if (authInfoBody.getLogin_info().getDevice_chg_f() == Device_chg_code.CHANGE.getCode() && authInfoBody.getLogin_info().getLogin_status() == Login_status.LOGIN.getCode()) {
                asyncProcess.NsInterFaceLogout(userInfo, mcsConfig.getProperty("NGINX.INTERNAL.VIP") + API_URL.NS_FCM);
            }
*/
            if (authReqHeaders.getIuid() == null)
                authReqHeaders.setIuid(userInfo.getIuid());

            List<HomeSchedInfo> homeSchedInfoList = homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);
            if (homeSchedInfoList == null || homeSchedInfoList.size() == 0) {
                logger.info("홈화면 등록 대상자 없음!![{}]", authReqHeaders.getIuid());
            } else {
                authInfoBody.setHomeSched(new HomeSched(homeSchedInfoList));
            }

            /* 등급 정보 전달 */
            GradeInfo gradeInfo = sbsDao.selectSbsAdmin(userInfo);

            List<GradeInfo> gradeInfos = sbsDao.selectCampMember(userInfo);
            if (gradeInfos != null && gradeInfos.size() != 0) {
                if (gradeInfo == null) {
                    gradeInfo = new GradeInfo();
                    gradeInfo.setMem_grade_infos(gradeInfos);
                } else {
                    gradeInfo.setMem_grade_infos(gradeInfos);
                }
            }

            if (gradeInfo != null)
                authInfoBody.setGradeInfo(gradeInfo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;

    }


    @Override
    public HttpStatus Authinfo(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws Exception {

        try {

            /* 신규 공지 방식 변경 */
            EventPopup eventPopup = systemInfoDao.getEventPopup();
            if (eventPopup != null) {
                authInfoBody.setEventPopup(eventPopup);

                int count = systemInfoDao.isSystemUnavailable();

                if (count > 0) {
/*                  공지 시간 OK & 작업 시간은 OK*/
                    authInfoBody.getEventPopup().setForceStop(EventPopup.FORCE_UPDATE);
                    logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                    return HttpStatus.SERVICE_UNAVAILABLE;
                }
            }
/*
            *//* 신규 공지 방식 추가 *//*
            List<EventPopup> eventPopups = systemInfoDao.getEventPopupList();
            if (eventPopups != null) {
                authInfoBody.setEventPopupList(eventPopups);

                    int count = systemInfoDao.isSystemUnavailable();

                    if (count > 0) {
                        *//* 공지 시간 OK & 작업 시간은 OK*//*
                        logger.info("Now System Unavaliable ..... !!!!!!!!!!!!!!!");
                        return HttpStatus.SERVICE_UNAVAILABLE;
                    }
            }*/


            switch (String.valueOf(authReqHeaders.getSubs_type())) {
                case SubsTypeList.NORMAL:
                case SubsTypeList.NAVER:
                case SubsTypeList.GOOGLE:
                case SubsTypeList.KAKAO:
                case SubsTypeList.FACEBOOK:
                    break;
                default:
                    logger.error("정의 되지 않은 인증 타입입니다.TN-SUBS-TYPE = {}", authReqHeaders.getSubs_type());
                    return HttpStatus.BAD_REQUEST;

            }

            /* 앱 업데이트 신규 정책 백업 (2018.06.04)*/
            AppUpdatetInfo appUpdatetInfo = systemInfoDao.getAppUpdate(authReqHeaders.getStoreType(), authReqHeaders.getSubs_type());

            if (appUpdatetInfo != null) {

                if (appUpdatetInfo.needForceUpdate(authReqHeaders.getAppVersion())) {
                    logger.error("Not valid app version");
                    appUpdatetInfo.setForceUpdate(AppUpdatetInfo.FORCE_UPDATE);
                    authInfoBody.setApp_update(appUpdatetInfo);
                    return HttpStatus.NOT_ACCEPTABLE;
                }

                if (appUpdatetInfo.needUpdateIs(authReqHeaders.getAppVersion())) {
                    appUpdatetInfo = null;
                }

            }
            authInfoBody.setApp_update(appUpdatetInfo);

            /* 자동 로그인 여부 확인 */
            UserInfo userInfo = getUserCertByIuidOrE164PhoneNo(authReqHeaders, authReqParams, usInfo);
            Login_info login_info = new Login_info();

            if (userInfo == null) {

                logger.error("가입자 정보 없음.");
                return HttpStatus.NOT_FOUND;
            }

            /* 자동 로그인 여부 확인 */
            if (authReqHeaders.getCAuthKey() == null) {
                if (authReqHeaders.getSubs_type() == com.tisquare.mvp.mcs.entity.SubscriberType.NORMAL.value() && (!StringUtils.equals(usInfo.getUser_pwd(), userInfo.getSuidPw()))) {
                    logger.error("가입자[{}] 비밀번호 불일치.[{}][{}]", userInfo.getSuid(), usInfo.getUser_pwd(), userInfo.getSuidPw());
                    return HttpStatus.METHOD_NOT_ALLOWED;
                }
            } else {

                if (!StringUtils.equals(userInfo.getCauthKey(), authReqHeaders.getCAuthKey())) {
                    logger.error("인증키 불일 치.[{}][{}]", userInfo.getCauthKey(), authReqHeaders.getCAuthKey());
                    return HttpStatus.FORBIDDEN;
                }
            }

            authInfoBody.setUser_type(userInfo.getUser_type());
            authInfoBody.setSubsType(userInfo.getSubsType().value());
            authInfoBody.setPtoken(userInfo.getPnotiToken());
            authInfoBody.setAuthKey(userInfo.getCauthKey());
            authInfoBody.setProfile(userInfo.getProfile());

            login_info.setLogin_status(userInfo.getLogin_status());

            /* 고객 정보 현행화를 위한 로그인 정보와 고객 정보 비교 */
            if (!ValidCheck.InputParamChgCheck(userInfo, authReqHeaders, authReqParams, usInfo)) {
                clientDao.updateBssUserInfo(userInfo, authReqHeaders, authReqParams);
                if (!StringUtils.equals(userInfo.getDeviceId(), authReqParams.getDeviceId()) && userInfo.getDeviceId() != null)
                    login_info.setDevice_chg_f(1);
            }

            clientDao.updateLoginStatus(userInfo, authReqHeaders, authReqParams);

            /* 로그인 번호 변경 여부 확인 */
            if (!StringUtils.equals(userInfo.getLogin_phoneNo(), authReqHeaders.getPhoneNo()))
                login_info.setPhone_chg_f(1);

            /* 로그인 시간 반영*/
            authInfoBody.setLogin_info(login_info);

            if (authReqHeaders.getStoreType() == null) {
                authReqHeaders.setStoreType(userInfo.getStoreType());
                if (authReqHeaders.getStoreType() == null) {
                    authReqHeaders.setStoreType(0);
                }
            }


            if (authReqParams.getNid() == null) {
                authReqParams.setNid(0L);
            }
            if (authReqParams.getHid() == null) {
                authReqParams.setHid(0L);
            }
            if (authReqParams.getSid() == null) {
                authReqParams.setSid(0L);
            }


            Long lastNid;

            if (mcsConfig.getProperty("TELECOM.LIMIT").equals("1")) {
            /* telecom code check */
                switch (authReqParams.getTelecomCode().trim().substring(0, 3)) {
                    case Telecom_code.TELECOM_CODE:
                        break;
                    default:
                        return HttpStatus.BAD_REQUEST;
                }
            }

            if (FCS_IDX == config.getMAX_FCS_IDX())
                FCS_IDX = 0;

            authInfoBody.setServerList(serverDao.findServers3(FCS_IDX++));

            StringBuilder sbUrlFix = new StringBuilder(config.getUrl_domain());
            for (int nIdx = 0; nIdx < authInfoBody.getServerList().size(); nIdx++) {
                if (authInfoBody.getServerList().get(nIdx).getName().toUpperCase().equals("WS") && authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toUpperCase().equals("HTTPS")) {
                    sbUrlFix.setLength(0);
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getProtocol().toLowerCase());
                    sbUrlFix.append("://");
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getIpAddr().toLowerCase());
                    sbUrlFix.append(":");
                    sbUrlFix.append(authInfoBody.getServerList().get(nIdx).getConnections().get(0).getPort());
                    sbUrlFix.append("/");
                    sbUrlFix.append(config.getProcess_name().toLowerCase());
                    sbUrlFix.append("/");
                    break;
                }
            }


            /* OS_TYPE 체크 */
            UrlInfo urlInfo = new UrlInfo();

            urlInfo.setAgreement_info(sbUrlFix.toString() + config.getUrl_AgreeDomain());
            urlInfo.setAd_info(sbUrlFix.toString() + config.getUrl_Ad());
            urlInfo.setPrivacy_info(sbUrlFix.toString() + config.getUrl_Privacy());
            /*urlInfo.setPrivacy_info("https://okitalkie-pro.com/acs/pages/privacy_company.html");*/
            urlInfo.setThird_info(sbUrlFix.toString() + config.getUrl_Th());
            switch (authReqParams.getOsType().toLowerCase()) {

                case OS_TYPE.OS_TYPE_IOS:
                    urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss_ios());
                    break;
                default:
                    urlInfo.setOss_info(sbUrlFix.toString() + config.getUrl_oss());
                    break;
            }

            urlInfo.setPrivacy_op_info(sbUrlFix.toString() + config.getUrl_privacy_option_domain());
            urlInfo.setPrivacy_md_info(sbUrlFix.toString() + config.getUrl_privacy_mandatory_domain());
            urlInfo.setLocation_info(sbUrlFix.toString() + config.getUrl_location());

            /* 나이스 관련 약관 정보 세팅 */
            urlInfo.setNiceUrlInfo(new NiceUrlInfo(sbUrlFix.toString(), mcsConfig.getProperty("URL_TELECOM_LIST"), mcsConfig.getProperty("URL_NICE_PRIVACY_DOMAIN"), mcsConfig.getProperty("URL_NICE_UNIQUE_DOMAIN"),
                    mcsConfig.getProperty("URL_NICE_TELECOM_DOMAIN"), mcsConfig.getProperty("URL_NICE_AGREEMENT_DOMAIN"), mcsConfig.getProperty("URL_NICE_SAFE_DOMAIN"), mcsConfig.getProperty("URL_NICE_LGU_OTP_DOMAIN")
                    , mcsConfig.getProperty("URL_NICE_INFO")));

            authInfoBody.setUrlInfo(urlInfo);

            long lastHid = 0;
            int newHelpCnt = webViewDao.selectHelpNewCnt(authReqParams.getHid());


            lastHid = webViewDao.selectHelpLastHid();


            Integer newNoticeCnt = webdbDao.newNoticeCnt(authReqParams.getNid());
            String lastNoticeTime = webdbDao.getLastNoticeTime();
            lastNid = webdbDao.getLastNid();

            Notice notice = new Notice();

            if (newNoticeCnt != 0)
                notice.setNew_notice_cnt(newNoticeCnt);

            if (lastNoticeTime != null)
                notice.setLast_notice_time(lastNoticeTime);

            if (lastNid != null)
                notice.setLast_nid(lastNid);

            authInfoBody.setNoticeInfo(new NoticeInfo(notice, null));

            HelpInfo helpInfo = new HelpInfo(lastHid, newHelpCnt, sbUrlFix.toString() + config.getUrlHelpDomain());
            authInfoBody.setHelpInfo(helpInfo);



            /* 군인 정보 확인 */
            authInfoBody.setProfileDetail(profileDetailDao.selectProfileDetail(userInfo));

            if (authInfoBody.getProfileDetail().getType() == USER_TYPE.MILITARY.getCode()) {
                MasTrainee masTrainee = masDao.selectMasTraniee(userInfo.getIuid());
                if (masTrainee != null) {
                    authInfoBody.getProfileDetail().setMasTrainee(masTrainee);
                    /* 입대일 세팅 */

                }

                List<MasSched> masScheds = masDao.selectMasSched(userInfo.getIuid(), Sched_type.진급.getCode());

                if (masScheds != null && masScheds.size() != 0) {
                    /* 계급 확인*/
                    boolean advance_flag = false;

                    for (int nIdx = 0; nIdx < masScheds.size(); nIdx++) {
                         /* 진급일 세팅 */
                        if (masScheds.get(nIdx).getSCHEDULE_DATE() != null)
                            authInfoBody.getProfileDetail().setUpgrade_date(Long.valueOf(masScheds.get(nIdx).getSCHEDULE_DATE()));

                        Long diffDay = Long.valueOf(masScheds.get(nIdx).getSCHEDULE_DATE()) - Long.valueOf(DateUtil.toDATE_YYYYMMDD_String());
                        if (diffDay > -1) {
                            /* 진급 예정으로 판단 */
                            authInfoBody.getProfileDetail().setRank(masScheds.get(nIdx).getADVANCE_TYPE() - 1);
                            advance_flag = true;
                        }
                    }

                    if (!advance_flag) {
                        /* 진급 완료로 판단 */
                        authInfoBody.getProfileDetail().setRank(masScheds.get(0).getADVANCE_TYPE());
                        authInfoBody.getProfileDetail().setUpgrade_date(Long.valueOf(masScheds.get(0).getSCHEDULE_DATE()));
                    }
                }

            }
            /* 프로모션 정보 확인 */
            List<EventNotice> eventNotices = eventNoticeDao.selectEventNotice(authInfoBody);

            if (authInfoBody.getProfileDetail().getType() == USER_TYPE.MILITARY.getCode()) {
                if (eventNotices != null) {

                    for (int nIdx = 0; nIdx < eventNotices.size(); nIdx++) {
                        if (authInfoBody.getProfileDetail().getEnter_date() < Long.valueOf(mcsConfig.getProperty("base.enter.date"))) {

                            if (StringUtils.equals(eventNotices.get(nIdx).getLanding_menu(), "500"))
                                eventNotices.remove(nIdx);
                        }
                    }

                }
            }

            authInfoBody.setEventNotices(eventNotices);
            /* 약관 동의 정보 확인 */
            authInfoBody.setTermses(termsDao.selectTerms(userInfo));

            /* 페널티 부여 확인 */


            authInfoBody.setCosCmmntyPenalties(cos_cmmntyDao.selectCosCmmntyPenalty(userInfo));

            /* 개인 설정 정보 세팅 */
            if (userInfo.getHome_sched_iuid() != null)
                authInfoBody.setPrivacyInfo(new PrivacyInfo(userInfo));

            /* DEIVCE가 변경 되었고 최종 로그인 상태가 로그인이면 타 단말에 로그인 되어 있는 것은 로그아웃 시킨다 */
            if (authInfoBody.getLogin_info().getDevice_chg_f() == Device_chg_code.CHANGE.getCode() && authInfoBody.getLogin_info().getLogin_status() == Login_status.LOGIN.getCode()) {
                asyncProcess.NsInterFaceLogout(userInfo, mcsConfig.getProperty("NGINX.INTERNAL.VIP") + API_URL.NS_FCM);
            }

            if (authReqHeaders.getIuid() == null)
                authReqHeaders.setIuid(userInfo.getIuid());

            List<HomeSchedInfo> homeSchedInfoList = homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);
            if (homeSchedInfoList == null || homeSchedInfoList.size() == 0) {
                logger.info("홈화면 등록 대상자 없음!![{}]", authReqHeaders.getIuid());
            } else {
                authInfoBody.setHomeSched(new HomeSched(homeSchedInfoList));
            }

            /* 등급 정보 전달 */
            GradeInfo gradeInfo = sbsDao.selectSbsAdmin(userInfo);

            List<GradeInfo> gradeInfos = sbsDao.selectCampMember(userInfo);
            if (gradeInfos != null && gradeInfos.size() != 0) {
                if (gradeInfo == null) {
                    gradeInfo = new GradeInfo();
                    gradeInfo.setMem_grade_infos(gradeInfos);
                } else {
                    gradeInfo.setMem_grade_infos(gradeInfos);
                }
            }

            if (gradeInfo != null)
                authInfoBody.setGradeInfo(gradeInfo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;

    }

    public UserInfo getUserCertByIuid(Long iuid) throws IOException {
        UserInfo userInfo = null;

        /* ID/PWD 인증을 위한 타입일 시 */
        /*if(authReqHeaders.getSubs_type() == SubscriberType.PAID_REGULAR_MEMBER.value())
        {*/
        userInfo = clientDao.selectIUID(iuid);
        /*}*/

        if (userInfo == null) {
            logger.error("계정 정보 없음!!");
            return null;
        }
        return userInfo;
    }

    public UserInfo getUserCertByIuid(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws IOException {

        UserInfo userInfo = null;

        /* ID/PWD 인증을 위한 타입일 시 */
        /*if(authReqHeaders.getSubs_type() == SubscriberType.PAID_REGULAR_MEMBER.value())
        {*/
        userInfo = clientDao.selectIUID(authReqHeaders);
        /*}*/

        if (userInfo == null) {
            logger.error("계정 정보 없음!!");
            return null;
        }
        return userInfo;
    }


    public UserInfo getUserCertByPhoneNo(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws IOException {

        UserInfo userInfo = null;

        /* ID/PWD 인증을 위한 타입일 시 */
        /*if(authReqHeaders.getSubs_type() == SubscriberType.PAID_REGULAR_MEMBER.value())
        {*/
        userInfo = clientDao.selectPhoneNo(authReqHeaders);
        /*}*/

        if (userInfo == null) {
            logger.error("계정 정보 없음!!");
            return null;
        }
        return userInfo;
    }


    public UserInfo getUserCertByIuidOrE164PhoneNo(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws IOException {

        UserInfo userInfo = null;

        /* ID/PWD 인증을 위한 타입일 시 */
        /*if(authReqHeaders.getSubs_type() == SubscriberType.PAID_REGULAR_MEMBER.value())
        {*/
        if (authReqHeaders.getCAuthKey() == null)
            userInfo = clientDao.selectUserID(authReqHeaders, usInfo);
        else
            userInfo = clientDao.selectIUID(authReqHeaders);
        /*}*/

        if (userInfo == null) {
            logger.error("계정 정보 없음!!");
            return null;
        }
        return userInfo;
    }

    public UserInfo getUserCertIDPhoneNo(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws IOException {

        UserInfo userInfo = null;

        /* ID/PWD 인증을 위한 타입일 시 */
        /*if(authReqHeaders.getSubs_type() == SubscriberType.PAID_REGULAR_MEMBER.value())
        {*/
        userInfo = clientDao.selectUserPwdID(authReqHeaders, usInfo);
        /*}*/

        if (userInfo == null) {
            logger.error("계정 정보 없음!!");
            return null;
        }


        return userInfo;
    }


    private void sendJoinNotification(final String url, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) {
        try {
            ExecutorService pool = Executors.newSingleThreadExecutor();

            Map<String, String> headers = new HashMap<>();
            headers.put("TN-REQ-USER", authReqHeaders.getPhoneNo());
            headers.put("TN-IUID", String.valueOf(authReqHeaders.getIuid()));


            pool.execute(HttpAsyncClientWorker.create(url, headers, Integer.valueOf(mcsConfig.getProperty("server.read.timeout"))));
            pool.shutdown();
        } catch (Exception e) {
            logger.error(" => send chg_mdn notify failed. IUID: {} PHONE_NO : {} | {}", authReqHeaders.getIuid(), authReqHeaders.getPhoneNo(), e);
        }
    }


}
