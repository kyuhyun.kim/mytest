package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.pc.IdentifyPasswd;
import com.tisquare.mvp.mcs.model.pc.PcAuthInfo;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface PcIdentifyPasswdService {


    /**
     * 아이디 찾기에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param identifyPasswd   고객 정보

     * @return 아이디 찾기 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPcIdRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd);

    /**
     * 비밀번호 찾기 시 인증 번호 요청에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param identifyPasswd   고객 정보

     * @return 아이디 찾기 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPcPasswdSmsRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd);


    /**
     * 비밀번호 찾기 시 인증 번호 확인에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders   고객 정보

     * @return 아이디 찾기 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPcPasswdCertRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd);

    /**
     * 비밀번호 변경 요청 처리에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders   고객 정보

     * @return 비밀번호 변경 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPcPasswdResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd);

    /**
     * 가입자 아이디 발송 요청 처리에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders   고객 정보

     * @return 가입자 아이디 발송 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPcId_sendResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo);

    /**
     * 가입 메일 발송 요청 처리에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders   고객 정보

     * @return 가입 메일 발송 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPcJoinMailResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo);


    /**
     * 가입 메일 발송 요청 처리에 대한 확인을 처리 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders   고객 정보

     * @return 가입 메일 발송 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPcJoinMailCertResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo);

}
