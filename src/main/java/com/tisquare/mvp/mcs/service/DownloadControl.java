package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.Download;
import com.tisquare.mvp.mcs.model.NoticeBody;
import com.tisquare.mvp.mcs.model.NoticeListBody;
import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface DownloadControl {

    ResponseEntity<String> Download(Download download);


}
