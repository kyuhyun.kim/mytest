package com.tisquare.mvp.mcs.service.Impl;

import CheckPlus.nice.MCheckPlus;
import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.ClientDao;
import com.tisquare.mvp.mcs.db.HomeSchedInfoDao;
import com.tisquare.mvp.mcs.db.MasDao;
import com.tisquare.mvp.mcs.db.ProfileDetailDao;
import com.tisquare.mvp.mcs.db.domain.MasTrainee;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.BssUserControl;
import com.tisquare.mvp.mcs.service.IdentifyService;
import com.tisquare.mvp.mcs.service.ResponseEntityService;
import com.tisquare.mvp.mcs.service.SetupService;
import com.tisquare.mvp.mcs.type.NiceResponseCode;
import com.tisquare.mvp.mcs.type.ResultCode;
import com.tisquare.mvp.mcs.type.USER_TYPE;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by kimkyuhyun on 2015-12-04.
 */
@Service
public class SetupServiceImpl extends ResponseEntityService implements SetupService {

    private static final Logger logger = LoggerFactory.getLogger(SetupServiceImpl.class);


    @Autowired
    private InitConfig config;


    @Autowired
    ClientDao clientDao;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    BssUserControl bssUserControl;

    @Autowired
    HomeSchedInfoDao homeSchedInfoDao;

    @Autowired
    ProfileDetailDao profileDetailDao;

    @Autowired
    MasDao masDao;

    public ResponseEntity<String> processHomeSchedSeq(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched)
    {
        try
        {

            if( authReqHeaders.getIuid() == null || authReqHeaders.getcAuthKey() == null)
            {
                logger.error("IUID [{}] AUTH-KEY[{}] 항목 누락!", authReqHeaders.getIuid(), authReqHeaders.getcAuthKey());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched.getHomeSchedInfoList().get(0).getReg_iuid() == null || homeSched.getHomeSchedInfoList().get(0).getSeq() == null)
            {
                logger.error("REG_IUID [{}] SEQ[{}] 항목 누락!", homeSched.getHomeSchedInfoList().get(0).getReg_iuid(), homeSched.getHomeSchedInfoList().get(0).getSeq());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched == null || homeSched.getHomeSchedInfoList().size() == 0)
            {
                logger.error("정렬 대상 항목 누락!");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched.getHomeSchedInfoList().size() > Integer.valueOf( mcsConfig.getProperty("home.sched.limit")))
            {
                logger.error("정렬 대상자 요청 갯수 초과[{}]!", homeSched.getHomeSchedInfoList().size());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }


            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }

            /* 가입자 정보 편집 */

            if( homeSched.getHomeSchedInfoList().size() != homeSchedInfoDao.selectCntHomeSchedInfo(authReqHeaders))
            {
                logger.error("변경 대상자의 갯수가 일치 하지 않음!!");
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }

            Collections.sort(homeSched.getHomeSchedInfoList(), new SeqDescCompare());

            for (HomeSchedInfo homeSchedInfo : homeSched.getHomeSchedInfoList()) {
                System.out.println(homeSchedInfo.getNew_seq());
                if(authReqHeaders.getIuid().equals(homeSchedInfo.getIuid()))
                {
                    logger.error("본인 정보가 포함되어 있음!!");
                    return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
                }
            }


            for(int nIdx = 0; nIdx < homeSched.getHomeSchedInfoList().size(); nIdx++)
            {
                homeSchedInfoDao.updateSeqHomeSchedInfo(homeSched.getHomeSchedInfoList().get(nIdx));
            }

            List<HomeSchedInfo> homeSchedInfoList= homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);

            return makeResponseEntityWithCheckBodySize(request, new HomeSchedResponse(homeSchedInfoList), HttpStatus.OK);

        }
        catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processHomeSchedDel(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched)
    {
        try
        {

            if( authReqHeaders.getIuid() == null || authReqHeaders.getcAuthKey() == null)
            {
                logger.error("IUID [{}] AUTH-KEY[{}] 항목 누락!", authReqHeaders.getIuid(), authReqHeaders.getcAuthKey());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched.getHomeSchedInfoList().get(0).getReg_iuid() == null || homeSched.getHomeSchedInfoList().get(0).getSeq() == null)
            {
                logger.error("REG_IUID [{}] SEQ[{}] 항목 누락!", homeSched.getHomeSchedInfoList().get(0).getReg_iuid(), homeSched.getHomeSchedInfoList().get(0).getSeq());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched == null || homeSched.getHomeSchedInfoList().size() == 0)
            {
                logger.error("삭제 대상 항목 누락!");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched.getHomeSchedInfoList().size() > 1)
            {
                logger.error("삭제 대상자 요청 갯수 초과[{}]!", homeSched.getHomeSchedInfoList().size());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }


            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }

            /* 가입자 정보 편집 */

            if( homeSchedInfoDao.selectDelHomeSchedInfo(homeSched.getHomeSchedInfoList().get(0)) == null)
            {
                logger.error("삭제 대상자가 존재 하지 않음!!");
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }

            /* 본인 정보 삭제 불가 */
            if( authReqHeaders.getIuid().equals(homeSched.getHomeSchedInfoList().get(0).getIuid()) )
            {
                logger.error("본인 정보는 삭제 할수 없음!!");
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }
            homeSchedInfoDao.deleteHomeSchedInfo(homeSched.getHomeSchedInfoList().get(0));

            List<HomeSchedInfo> homeSchedInfoList= homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);

            return makeResponseEntityWithCheckBodySize(request, new HomeSchedResponse(homeSchedInfoList), HttpStatus.OK);



        }
        catch (Exception e) {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processHomeSchedEdit(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched)
    {
        try
        {

            if( authReqHeaders.getIuid() == null || authReqHeaders.getcAuthKey() == null)
            {
                logger.error("IUID [{}] AUTH-KEY[{}] 항목 누락!", authReqHeaders.getIuid(), authReqHeaders.getcAuthKey());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched.getHomeSchedInfoList().get(0).getReg_iuid() == null || homeSched.getHomeSchedInfoList().get(0).getSeq() == null)
            {
                logger.error("REG_IUID [{}] SEQ[{}] 항목 누락!", homeSched.getHomeSchedInfoList().get(0).getReg_iuid(), homeSched.getHomeSchedInfoList().get(0).getSeq());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched == null || homeSched.getHomeSchedInfoList().size() == 0)
            {
                logger.error("편집 대상 항목 누락!");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched.getHomeSchedInfoList().size() > 1)
            {
                logger.error("편집 대상자 요청 갯수 초과[{}]!", homeSched.getHomeSchedInfoList().size());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            /* 생년월일 valid check */
            for(int nIdx = 0 ; nIdx < homeSched.getHomeSchedInfoList().size(); nIdx++)
            {
                if(homeSched.getHomeSchedInfoList().get(nIdx).getBirth() == null )
                {
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }

                if(!DateUtil.isValidDateStr( homeSched.getHomeSchedInfoList().get(nIdx).getBirth().toString()))
                {
                    logger.error("생년월일 포맷 에러 [{}]", homeSched.getHomeSchedInfoList().get(nIdx).getBirth().toString());
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }

                if(!DateUtil.isValidDateStr( homeSched.getHomeSchedInfoList().get(nIdx).getEnter_date().toString()))
                {
                    logger.error("입대일 포맷 에러 [{}]", homeSched.getHomeSchedInfoList().get(nIdx).getEnter_date().toString());
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }


            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }

            /* 가입자 정보 편집 */
            if( homeSchedInfoDao.selectUpdateHomeSchedInfo(homeSched.getHomeSchedInfoList().get(0)) != null)
            {
                homeSched.getHomeSchedInfoList().get(0).setResult(ResultCode.AGE_FAIL.getCode());
                homeSched.getHomeSchedInfoList().get(0).setResult_msg("대상 가입자는 편집이 불가능한 사용자 입니다.(직접 등록 대상이 아님).");

            }

            /* 중복 여부 확인 */
             if(homeSchedInfoDao.selectHomeSchedInfoDuplicate(homeSched.getHomeSchedInfoList().get(0)) > 0)
             {
                 homeSched.getHomeSchedInfoList().get(0).setResult(ResultCode.AGE_FAIL.getCode());
                 homeSched.getHomeSchedInfoList().get(0).setResult_msg("이미 등록된 사용자 입니다.");
             }
            else {

                /* 본인 정보는 편집 불가능 */
                 if (authReqHeaders.getIuid().equals(homeSched.getHomeSchedInfoList().get(0).getIuid())) {
                     homeSched.getHomeSchedInfoList().get(0).setResult(ResultCode.AGE_FAIL.getCode());
                     homeSched.getHomeSchedInfoList().get(0).setResult_msg("본인의 정보는 편집 불가능 합니다.");
                 } else {
                     homeSched.getHomeSchedInfoList().get(0).setResult(ResultCode.SUCCESS.getCode());
                     homeSched.getHomeSchedInfoList().get(0).setResult_msg("성공.");
                 }
             }
            if(homeSched.getHomeSchedInfoList().get(0).getResult() == ResultCode.SUCCESS.getCode())
                homeSchedInfoDao.updateHomeSchedInfo(homeSched.getHomeSchedInfoList().get(0));

            List<HomeSchedInfo> homeSchedInfoList= homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);

            return makeResponseEntityWithCheckBodySize(request, new HomeSchedResponse(homeSchedInfoList, homeSched.getHomeSchedInfoList()), HttpStatus.OK);



        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processHomeSchedReg(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched) {
        try
        {

            if( authReqHeaders.getIuid() == null || authReqHeaders.getcAuthKey() == null)
            {
                logger.error("IUID [{}] AUTH-KEY[{}] 항목 누락!", authReqHeaders.getIuid(), authReqHeaders.getcAuthKey());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if( homeSched == null || homeSched.getHomeSchedInfoList().size() == 0)
            {
                logger.error("등록 대상 항목 누락!");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            /* 생년월일 valid check */
            for(int nIdx = 0 ; nIdx < homeSched.getHomeSchedInfoList().size(); nIdx++)
            {
                if(homeSched.getHomeSchedInfoList().get(nIdx).getBirth() == null )
                {
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }

                if(!DateUtil.isValidDateStr( homeSched.getHomeSchedInfoList().get(nIdx).getBirth().toString()))
                {
                    logger.error("생년월일 포맷 에러 [{}]", homeSched.getHomeSchedInfoList().get(nIdx).getBirth().toString());
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }

                if(!DateUtil.isValidDateStr( homeSched.getHomeSchedInfoList().get(nIdx).getEnter_date().toString()))
                {
                    logger.error("입대일 포맷 에러 [{}]", homeSched.getHomeSchedInfoList().get(nIdx).getEnter_date().toString());
                    return makeResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }

            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }

            /* 가입자 홈 등록관리 정보 확인*/
            List<HomeSchedInfo> homeSchedInfoList= homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);
            if(homeSchedInfoList != null)
            {
                if(homeSchedInfoList.size() > Integer.valueOf( mcsConfig.getProperty("home.sched.limit")))
                {
                    logger.error(" 등록 건수 초과!! 현재 건수[{}]", homeSchedInfoList.size());
                    return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
                }

                if( (homeSchedInfoList.size() + homeSched.getHomeSchedInfoList().size()) > Integer.valueOf( mcsConfig.getProperty("home.sched.limit")))
                {
                    logger.error("등록 갯수 초과!! 현재 건수[{}] 요청 건수[{}]", homeSchedInfoList.size() , homeSched.getHomeSchedInfoList().size());
                    return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
                }
            }



            for(int nIdx =0; nIdx < homeSched.getHomeSchedInfoList().size(); nIdx++) {

                Long iuid = homeSched.getHomeSchedInfoList().get(nIdx).getIuid();

                if(iuid != null)
                {
                    ProfileDetail profileDetail = profileDetailDao.selectProfileDetail(iuid);
                    if( profileDetail == null)
                    {
                        homeSched.getHomeSchedInfoList().get(nIdx).setResult(ResultCode.AGE_FAIL.getCode());
                        homeSched.getHomeSchedInfoList().get(nIdx).setResult_msg("가입자 정보가 없습니다.");
                        continue;
                    }
                    else
                    {
                        if( profileDetail.getType() != USER_TYPE.MILITARY.getCode())
                        {
                            homeSched.getHomeSchedInfoList().get(nIdx).setResult(ResultCode.AGE_FAIL.getCode());
                            homeSched.getHomeSchedInfoList().get(nIdx).setResult_msg("해당 가입자는 군인이 아닙니다.");
                            continue;
                        }
                        else   /* 군인정보 입력 */
                        {
                            MasTrainee masTrainee = masDao.selectMasTraniee(iuid);
                            if(masTrainee == null)
                            {
                                homeSched.getHomeSchedInfoList().get(nIdx).setResult(ResultCode.AGE_FAIL.getCode());
                                homeSched.getHomeSchedInfoList().get(nIdx).setResult_msg("군인정보가 조회 되지 않습니다.");
                                continue;
                            }
                            else {
                                homeSched.getHomeSchedInfoList().get(nIdx).setBirth( Long.parseLong(masTrainee.getTr_birth()));
                                homeSched.getHomeSchedInfoList().get(nIdx).setName(masTrainee.getTr_name());
                                homeSched.getHomeSchedInfoList().get(nIdx).setEnter_date(Long.parseLong(masTrainee.getTr_entrance_day()));
                                homeSched.getHomeSchedInfoList().get(nIdx).setEnter_unit(masTrainee.getTr_entrance());
                            }
                        }
                    }

                }
                int isExist = homeSchedInfoDao.selectHomeSchedInfoDuplicate(homeSched.getHomeSchedInfoList().get(nIdx));
                if(isExist > 0)
                {
                    logger.error("등록 대상자 중복!! REG_IUID[{}] NAME[{}] BIRTH[{}] ENTER_DATE[{}], ENTER_UNIT[{}]",
                            homeSched.getHomeSchedInfoList().get(nIdx).getReg_iuid(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getName(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getBirth(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getEnter_date(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getEnter_unit()
                    );

                    homeSched.getHomeSchedInfoList().get(nIdx).setResult(ResultCode.AGE_FAIL.getCode());
                    homeSched.getHomeSchedInfoList().get(nIdx).setResult_msg("이미 등록된 사용자 입니다.");
                    continue;
                }
                else {

                    logger.error("홈화면에 등록 됨 REG_IUID[{}] NAME[{}] BIRTH[{}] ENTER_DATE[{}], ENTER_UNIT[{}]",
                            homeSched.getHomeSchedInfoList().get(nIdx).getReg_iuid(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getName(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getBirth(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getEnter_date(),
                            homeSched.getHomeSchedInfoList().get(nIdx).getEnter_unit()
                    );

                    homeSchedInfoDao.insertHomeSchedInfo(homeSched.getHomeSchedInfoList().get(nIdx));
                    homeSched.getHomeSchedInfoList().get(nIdx).setResult(ResultCode.SUCCESS.getCode());
                    homeSched.getHomeSchedInfoList().get(nIdx).setResult_msg("성공.");
                }
            }

            homeSchedInfoList= homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);

            return makeResponseEntityWithCheckBodySize(request, new HomeSchedResponse(homeSchedInfoList, homeSched.getHomeSchedInfoList()), HttpStatus.OK);



        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> processHomeSchedInfo(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)
    {
        try
        {

            if( authReqHeaders.getIuid() == null || authReqHeaders.getcAuthKey() == null)
            {
                logger.error("IUID [{}] AUTH-KEY[{}] 항목 누락!", authReqHeaders.getIuid(), authReqHeaders.getcAuthKey());
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }


            /* 가입자 홈 등록관리 정보 확인*/
            List<HomeSchedInfo> homeSchedInfoList= homeSchedInfoDao.selectHomeSchedInfo(authReqHeaders);
            if( homeSchedInfoList == null || homeSchedInfoList.size() == 0 ) {
                logger.info("홈화면 등록 대상자 없음!![{}]", authReqHeaders.getIuid());
            }
            return makeResponseEntityWithCheckBodySize(request, new HomeSchedResponse(homeSchedInfoList), HttpStatus.OK);



        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    public ResponseEntity<String> processSetupHomeIuid(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSchedIuid homeSchedIuid)
    {
        try
        {

            if( homeSchedIuid == null || homeSchedIuid.getTarget_iuid() == null)
            {
                logger.error("대상 정보 없음!");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }

            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }


            UserInfo target = bssUserControl.getUserCertByIuid(homeSchedIuid.getTarget_iuid());
            if(target == null)
            {
                logger.error("일정 등록 대상 가입자 없음!![{}]", homeSchedIuid.getTarget_iuid());
                return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
            }

            /* 설정 정보 세팅 */
            homeSchedIuid.setIuid(userInfo.getIuid());
            clientDao.updateIndividualClientHomeSchedIuid(homeSchedIuid);

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return makeResponseEntity(HttpStatus.OK);
    }



    public ResponseEntity<String> processSetupAlram(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Alarm param)
    {


        try
        {

            if( param == null || param.getAlarmSetUps().size() == 0)
            {
                logger.error("설정 정보 없음!");
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }
            else
            {
                for(int nIdx = 0 ; nIdx < param.getAlarmSetUps().size(); nIdx++)
                {
                    switch (param.getAlarmSetUps().get(nIdx).getValue())
                    {
                        case 0:  break;
                        case 1: break;
                        default:
                            logger.error("잘못 된 설정 정보 {}", param.getAlarmSetUps().get(nIdx).getType());
                            return makeResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
                    }
                }
            }

            UserInfo userInfo = bssUserControl.getUserCertByIuid(authReqHeaders, authReqParams);

            if(userInfo == null) {
                logger.error("가입자 정보 없음!!");
                return makeResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!StringUtils.equals( userInfo.getCauthKey(), authReqHeaders.getCAuthKey()) )
            {
                logger.error("인증 키 불일 치!!");
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            }


            /* 설정 정보 세팅 */

            clientDao.updateIndividualClientAlarm(new AlarmInfo(userInfo, param));

        }
        catch (Exception e)
        {
            logger.error(e.toString());
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return makeResponseEntity(HttpStatus.OK);
    }

    static class SeqDescCompare implements Comparator<HomeSchedInfo> {

        /**
         * 내림차순(DESC)
         */
        @Override
        public int compare(HomeSchedInfo arg0, HomeSchedInfo arg1) {
            // TODO Auto-generated method stub
            return arg0.getNew_seq().compareTo(arg1.getNew_seq());
        }


    }

}




