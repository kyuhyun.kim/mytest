package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface CertifiService {


    /**
     * 클라이언트 설치 인증 요청에 대한 처리를 한다.
     *
     * @param request        서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processClientInstallRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);


    /**
     * 클라이언트 인증 요청에 대한 처리를 한다.
     *
     * @param request        서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processClientCertRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);


    /**
     * 클라이언트 무전 연결 방식 설정 요청에 대한 처리를 한다.
     *
     * @param request        서블릿 요청 객체
     * @param phoneNo            요청 시 고객 번호
     * @param iuid           요청 고객 고유 번호
     * @param cAuthKey        요청 고객 인증키
     * @param mode           무전 연결 방식 설정
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processClientModeRequest(HttpServletRequest request, String phoneNo, long iuid, String cAuthKey, Integer mode);


    /**
     * 클라이언트 서비스 활성화 설정 요청에 대한 처리를 한다.
     *
     * @param request        서블릿 요청 객체
     * @param presence       요청 시 사용자 IUID, 발급 권한 Key, 서비스 활성화 여부
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processSvcPresenceRequest(HttpServletRequest request, Presence presence);

    /**
     * 클라이언트 서비스 활성화 조회 요청에 대한 처리를 한다.
     *
     * @param request        서블릿 요청 객체
     * @param presence       요청 시 사용자 IUID, 발급 권한 Key
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processSvcPresenceInfoRequest(HttpServletRequest request, Presence presence);


}
