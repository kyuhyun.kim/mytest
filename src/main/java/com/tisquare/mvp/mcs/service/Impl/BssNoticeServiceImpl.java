package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.model.NoticeBody;
import com.tisquare.mvp.mcs.model.NoticeListBody;
import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import com.tisquare.mvp.mcs.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class BssNoticeServiceImpl extends ResponseEntityService implements BssNoticeService {

    private static final Logger logger = LoggerFactory.getLogger(BssNoticeServiceImpl.class);

    @Autowired
    private BssNoticeControl bssNoticeControl;

    @Override
    public ResponseEntity<String> processBssNoticeListRequest(HttpServletRequest request, NoticeReqHeaders noticeReqHeaders)
    {
        try {

            NoticeListBody noticeListBody = new NoticeListBody();

            HttpStatus httpStatus = bssNoticeControl.NoticeList(noticeListBody, noticeReqHeaders);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }
            else {
                return makeResponseEntityWithCheckBodySize(request, noticeListBody, httpStatus);
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @Override
    public ResponseEntity<String> processBssNoticeRequest(HttpServletRequest request, int sid, String telecomCode)
    {
        try {

            NoticeBody noticeBody = new NoticeBody();

            HttpStatus httpStatus = bssNoticeControl.Notice(noticeBody, sid, telecomCode);

            if (httpStatus == HttpStatus.INTERNAL_SERVER_ERROR) {
                return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (httpStatus == HttpStatus.FORBIDDEN) {
                return makeResponseEntity(HttpStatus.FORBIDDEN);
            } else if  (httpStatus == HttpStatus.BAD_REQUEST) {
                return makeResponseEntity(HttpStatus.BAD_REQUEST);
            }
            else {
                return makeResponseEntityWithCheckBodySize(request, noticeBody, httpStatus);
            }


        } catch (Exception e) {
            logger.error("Client 인증 실패.", e);
            return makeResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



}
