package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.ActiveUserDao;
import com.tisquare.mvp.mcs.entity.ActiveUser;
import com.tisquare.mvp.mcs.service.ActiveUserService;
import com.tisquare.mvp.mcs.type.ActiveFlag;
import com.tisquare.mvp.mcs.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
@Service
public class ActiveUserServiceImpl implements ActiveUserService {

    private static final Logger logger = LoggerFactory.getLogger(ActiveUserServiceImpl.class);

    @Autowired
    private InitConfig initConfig;

    @Autowired
    ActiveUserDao activeUserDao;

    @Override
    public void updateActiveUser(Long iuid, String phoneNo, String tnVersion, Integer storeType) {
        ActiveUser activeUser = null;

        try {
            // T그룹on 2.0 사용자 대상으로 하기 때문에 1.0 사용자와 T전화 사용자는 제외 한다.
                boolean isTarget = false;
                Date currentDate = DateUtil.getAmountDateToDate(0);
                activeUser = activeUserDao.select(iuid, phoneNo);

                if (activeUser == null) {
                    activeUser = ActiveUser.create(iuid, phoneNo, ActiveFlag.INACTIVE, currentDate);
                } else {
                    float diffDays = DateUtil.diffDays(activeUser.getReg_date(), currentDate);
                    logger.debug("현재시간: {}, 마지막 인증시간: {}, 차이: {}", DateUtil.convertDateToString(currentDate, "yyyy-MM-dd HH:mm:ss"), DateUtil.convertDateToString(activeUser.getReg_date(), "yyyy-MM-dd HH:mm:ss"), diffDays);

                    if (diffDays > initConfig.getActiveUserCompareDate()) {
                        isTarget = true;
                        logger.info("ACTIVE_USER|활성 전환 대상자 입니다. active-user: {}", activeUser);
                        Date traceEndDate = DateUtil.getAmountDateToDate(initConfig.getActiveUserTraceDate());
                        activeUser.setActive_flag(ActiveFlag.ACTIVE.getCode());
                        activeUser.setTrace_stime(currentDate);
                        activeUser.setTrace_etime(traceEndDate);
                    }
                    // 공통적으로 인증 시간을 변경한다.
                    activeUser.setReg_date(currentDate);
                }

                if (!isTarget) {
                    logger.info("ACTIVE_USER|활성 전환 대상자가 아닙니다. active-user: {}", activeUser);
                }

                activeUserDao.insert(activeUser);

                logger.info("ACTIVE_USER|활성 사용자 업데이트 성공. active-user: {}", activeUser);

        } catch (Exception e) {
            logger.error("ACTIVE_USER|활성 사용자 업데이트 실패. active-user: {}", activeUser != null ? activeUser : String.format("{ iuid=%d, phone_no=%s }", iuid, phoneNo), e);
        }
    }

}
