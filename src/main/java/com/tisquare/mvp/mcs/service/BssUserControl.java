package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.db.domain.Presence;
import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.HttpStatus;

import java.io.IOException;


/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface BssUserControl {

    public HttpStatus UserSearch(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UserSearchInfo userSearchInfo) throws Exception;

    public HttpStatus ChgMdn(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws Exception;

    public HttpStatus ChgPwd(Long iuid, String authKey, Bssinfo bssinfo) throws Exception;

    public HttpStatus ChgPwd(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws Exception;

    public HttpStatus Mvpinfo(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws Exception;

    public HttpStatus Authinfo(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws Exception;

    public HttpStatus PwdAuth(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo, AuthPwdBody authPwdBody) throws Exception;

    public HttpStatus logout(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws Exception;

    public HttpStatus SearchID(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, ProfileDetail profileDetail, SearchIdResponse searchIdResponse) throws Exception;

    public UserInfo getUserCertByIuid(ClientAuthReqHeaders authReqHeaders , ClientAuthReqParams authReqParams) throws IOException;

    public UserInfo getUserCertByIuid(Long iuid) throws IOException;

    public HttpStatus Active(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams)throws IOException;

    public HttpStatus PcAuthinfo(AuthInfoBody authInfoBody, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, UsInfo usInfo) throws Exception;

    public HttpStatus Pclogout(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams) throws Exception;
}
