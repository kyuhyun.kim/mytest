package com.tisquare.mvp.mcs.service;


import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.Identify;
import com.tisquare.mvp.mcs.model.Identify_check;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface IdentifyService {


    /**
     * 클라이언트의 본인 인증 요청에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param param            본인 인증 요청 객체
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processIdentifyRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Identify param);

    /**
     * 클라이언트의 본인 확인 요청에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param param            본인 인증 확인 객체
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */

    ResponseEntity<String> processIdentifyCheckRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Identify_check param);

    ResponseEntity<String> processIdentifyStateRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    void processIdentifyresult(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, String EncodeData, Long iuid);

    void processIdentifyFail(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);



}
