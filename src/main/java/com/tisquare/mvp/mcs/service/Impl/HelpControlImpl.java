package com.tisquare.mvp.mcs.service.Impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.db.WebViewDao;
import com.tisquare.mvp.mcs.db.WebdbDao;
import com.tisquare.mvp.mcs.db.domain.HelpNoticeCnt;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.service.HelpControl;
import com.tisquare.mvp.mcs.service.NoticeControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class HelpControlImpl implements HelpControl {

    private static final Logger logger = LoggerFactory.getLogger(HelpControlImpl.class);

    @Autowired
    private InitConfig config;

    @Autowired
    WebdbDao webdbDao;

    @Autowired
    WebViewDao webViewDao;

    ObjectMapper mapper = new ObjectMapper();

    public HttpStatus HelpList(HelpListBody helpListBody, HelpReqHeaders helpReqHeaders)
            throws Exception {

        Map<String, Object> param = new HashMap<String, Object>();
        int  remain_notice = 0;

        HelpNoticeCnt helpNoticeCnt = webViewDao.selectHelpTotCnt();

        if(helpReqHeaders.getStart_hid() != 0 && helpReqHeaders.getStart_hid() < helpNoticeCnt.getMin_cnt())
        {
            helpListBody.setResult_code(NoticeListBody.FLAG_IS_FAIL);
            return HttpStatus.OK;
        }
        if(helpReqHeaders.getStart_hid() == 0 )
            param.put("hid", helpNoticeCnt.getMax_cnt() + 1);
        else
            param.put("hid", helpReqHeaders.getStart_hid());


        if( helpReqHeaders.getHelp_cnt() >  Integer.valueOf( config.getListNum()))
            param.put("help_cnt", Integer.valueOf( config.getListNum()));
        else
            param.put("help_cnt", helpReqHeaders.getHelp_cnt());

        param.put("l_read_id",  helpReqHeaders.getLast_read_hid());

        List<HelpJh> moreList = webViewDao.selectHelpList(param);

        if( moreList.size() > 0 )
        {
            int a = moreList.size();
            int ttt = moreList.get(a - 1).getHid();
            remain_notice =  webViewDao.selectHelpRemainCnt(moreList.get(moreList.size() - 1).getHid()) ;
        }

        helpListBody.setResult_code(NoticeListBody.FLAG_IS_SUCCESS);
        helpListBody.setRemain_cnt( remain_notice );
        helpListBody.setTotcnt(helpNoticeCnt.getCnt());
        if( remain_notice > 0 )
            helpListBody.setIs_end( NoticeListBody.FLAG_IS_NOT_END);
        else
            helpListBody.setIs_end( NoticeListBody.FLAG_IS_END);


        helpListBody.setItems( moreList );

        return HttpStatus.OK;

    }

    public HttpStatus Help(HelpBody helpBody, int hid)
            throws Exception {

        helpBody.setResult_code( NoticeListBody.FLAG_IS_FAIL);
        HelpBody help;


        help=  webViewDao.selectHelpHid(hid);

        if( help != null ) {
            helpBody.setResult_code(NoticeListBody.FLAG_IS_SUCCESS);
            helpBody.setContent(help.getContent());
            helpBody.setCreate_date(help.getCreate_date());
            helpBody.setHid(help.getHid());
            helpBody.setTitle(help.getTitle());
        }
        else
        {
            helpBody.setContent("null");
            helpBody.setHid(hid);
            helpBody.setCreate_date(new Date(0));
            helpBody.setTitle("null");

        }

        return HttpStatus.OK;

    }

}
