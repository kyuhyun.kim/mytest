package com.tisquare.mvp.mcs.service;


import com.tisquare.mvp.mcs.model.*;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface SetupService {


    /**
     * 클라이언트의 알람 설정에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param param            알람 설정 요청 객체
     * @return 클라이언트 요청 처리에 대한 응답
     */
    ResponseEntity<String> processSetupAlram(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, Alarm param);


    /**
     * 클라이언트의 홈 일정 대상 등록 설정에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param homeSchedIuid    일정 등록 요청 객체
     * @return 클라이언트 요청 처리에 대한 응답
     */
    ResponseEntity<String> processSetupHomeIuid(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSchedIuid homeSchedIuid);

    /**
     * 클라이언트의 홈 화면 등록 관리 대상 조회.
     *
     * @param request          서블릿 요청 객체
     * @return 클라이언트 요청 처리에 대한 응답
     */
    ResponseEntity<String> processHomeSchedInfo(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    /**
     * 클라이언트의 홈 화면 등록 요청.
     *
     * @param request          서블릿 요청 객체
     * @param homeSched        홈화면 등록 대상자
     * @return 클라이언트 요청 처리에 대한 응답
     */
    ResponseEntity<String> processHomeSchedReg(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched);

    /**
     * 클라이언트의 홈 화면 편집 요청.
     *
     * @param request          서블릿 요청 객체
     * @param homeSched        홈화면 편집 대상자
     * @return 클라이언트 요청 처리에 대한 응답
     */
    ResponseEntity<String> processHomeSchedEdit(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched);

    /**
     * 클라이언트의 홈 화면 삭제 요청.
     *
     * @param request          서블릿 요청 객체
     * @param homeSched        홈화면 삭제 대상자
     * @return 클라이언트 요청 처리에 대한 응답
     */
    ResponseEntity<String> processHomeSchedDel(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched);


    /**
     * 클라이언트의 홈 화면 재 정렬 요청.
     *
     * @param request          서블릿 요청 객체
     * @param homeSched        홈화면 재정렬 대상자
     * @return 클라이언트 요청 처리에 대한 응답
     */
    ResponseEntity<String> processHomeSchedSeq(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, HomeSched homeSched);
}
