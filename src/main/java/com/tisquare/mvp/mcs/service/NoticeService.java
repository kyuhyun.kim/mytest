package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface NoticeService {


    /**
     * 클라이언트 전체 공지 사항 리스트 요청에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param noticeReqHeaders 요청 시 헤더
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processNoticeListRequest(HttpServletRequest request, NoticeReqHeaders noticeReqHeaders);


    /**
     * 클라이언트 공지 사항 요청에 대한 처리를 한다.
     *
     * @param request       서블릿 요청 객체
     * @param sid           공지 사항 ID
     * @param telecomCode   이통사 코드
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processNoticeRequest(HttpServletRequest request, int sid, String telecomCode);



}
