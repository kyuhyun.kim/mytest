package com.tisquare.mvp.mcs.service.Impl;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import com.google.i18n.phonenumbers.NumberParseException;
import com.tisquare.mvp.mcs.collection.StringSet;
import com.tisquare.mvp.mcs.config.InitConfig;
import com.tisquare.mvp.mcs.model.TraceReq;
import com.tisquare.mvp.mcs.filter.TraceLogFilter;
import com.tisquare.mvp.mcs.service.TraceService;
import com.tisquare.mvp.mcs.utils.UtilPhoneNo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

@Service
public class TraceServiceImpl implements TraceService {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TraceServiceImpl.class);
    private static final String LOGGER_NAME = "TRACE";
    private static final String APPENDER_NAME = "TRC";




    @Autowired
    InitConfig initConfig;

    private boolean trace;
    private Logger trcLogger;
    private Collection<String> traceTarget = new StringSet();

    @Override
    public ResponseEntity<String> startTrace(TraceReq traceReq) {
        logger.info("trace-req: {}", traceReq);

        if (traceReq == null) {
            logger.error("trace-req is null.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (CollectionUtils.isEmpty(traceReq.getMdn())) {
            logger.error("trace mdn is null.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (traceTarget.size() >= 10 || traceReq.getMdn().size() > 10) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        Set<String> tmpSet = new TreeSet<>(traceTarget);
        tmpSet.addAll(traceReq.getMdn());
        if (tmpSet.size() > 10) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        for (String mdn : traceReq.getMdn()) {
            try {
                UtilPhoneNo utilPhoneNo = new UtilPhoneNo(mdn, null);
                traceTarget.add(utilPhoneNo.getE164Fmt());
            } catch (NumberParseException e) {
                logger.warn("trace phone-number parsing error. mdn={}", mdn);
            }
        }

        logger.info("trace target mdn: {}", traceTarget);

        if (trcLogger == null) {
            createLogger();
        } else {
            updateLogger();
        }

        if (!trace) {
            logger.info("trace on");
            this.trace = true;
        } else {
            logger.info("trace already on");
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public void stopTrace() {
        if (trace) {
            traceTarget.clear();

            if (trcLogger.getAppender(APPENDER_NAME) != null && trcLogger.getAppender(APPENDER_NAME).isStarted()) {
                trcLogger.getAppender(APPENDER_NAME).stop();
            }

            this.trace = false;
            logger.info("trace off");
        } else {
            logger.info("trace not started");
        }
    }

    @Override
    public boolean isTrace() {
        return trace;
    }

    @Override
    public Logger getTraceLogger() {
        return trcLogger;
    }

    @Override
    public Collection<String> getTraceTarget() {
        return traceTarget;
    }

    private void createLogger() {
        trcLogger = (Logger) LoggerFactory.getLogger(LOGGER_NAME);
        trcLogger.setLevel(Level.DEBUG);
        trcLogger.setAdditive(false);
        trcLogger.addAppender(createFileAppender());
        logger.info("trace appender created");
    }

    private void updateLogger() {
        if (trcLogger.getAppender(APPENDER_NAME) != null) {
            trcLogger.getAppender(APPENDER_NAME).clearAllFilters();
            trcLogger.getAppender(APPENDER_NAME).addFilter(TraceLogFilter.create(traceTarget));
            logger.debug("trace appender filer changed");
            if (!trcLogger.getAppender(APPENDER_NAME).isStarted()) {
                trcLogger.getAppender(APPENDER_NAME).start();
                logger.debug("trace appender restarted");
            }
        } else {
            trcLogger.addAppender(createFileAppender());
        }
    }

    private FileAppender<ILoggingEvent> createFileAppender() {

        try
        {
            String traceLogFile = initConfig.getTrace_log_base() + File.separator + initConfig.getTrace_log_file_name();
            LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
            PatternLayoutEncoder ple = new PatternLayoutEncoder();

            ple.setPattern("[%d{yyyy-MM-dd HH:mm:ss.SSS}] [%t] ["+ InetAddress.getLocalHost().getHostName().toString() +"] [MCS] [%X{phoneNo}] - %msg%n");
            ple.setContext(lc);
            ple.start();
            FileAppender<ILoggingEvent> fileAppender = new FileAppender<>();
            fileAppender.setName(APPENDER_NAME);
            fileAppender.setFile(traceLogFile);
            fileAppender.setEncoder(ple);
            fileAppender.setContext(lc);
            fileAppender.addFilter(TraceLogFilter.create(traceTarget));
            fileAppender.start();

            return fileAppender;
        }
        catch (Exception e)
        {
            logger.error(e.toString());
        }
        return null;
    }

    @Override
    public void printTraceLog(String mdn, String message) {

        try {
            UtilPhoneNo utilPhoneNo = new UtilPhoneNo(mdn, null);
            String targetMdn = utilPhoneNo.getE164Fmt();

            if (isTrace()) {
                boolean isTarget = false;
                for (String target : getTraceTarget()) {
                    if (StringUtils.equals(target, targetMdn)) {
                        isTarget = true;
                        break;
                    }
                }
                if (isTarget) {
                    try {
                        MDC.put("phoneNo", mdn);
                        String traceMessage = message;
                        getTraceLogger().debug(traceMessage);
                        MDC.clear();
                    } catch (Exception e) {
                        logger.error("trace log failed", e);
                    }
                }
            }
        }catch (Exception e)
        {
            logger.error(e.toString());
        }
    }

}
