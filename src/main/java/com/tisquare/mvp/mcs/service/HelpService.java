package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.HelpReqHeaders;
import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface HelpService {


    /**
     * 클라이언트 전체 도움말 리스트 요청에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param helpReqHeaders 요청 시 헤더
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processHelpListRequest(HttpServletRequest request, HelpReqHeaders helpReqHeaders);


    /**
     * 클라이언트 공지 사항 요청에 대한 처리를 한다.
     *
     * @param request       서블릿 요청 객체
     * @param hid           공지 사항 ID
     * @return 클라이언트 인증 요청 처리에 대한 응답
     */
    ResponseEntity<String> processHelpRequest(HttpServletRequest request, int hid);


}
