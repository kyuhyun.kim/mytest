package com.tisquare.mvp.mcs.service;

/**
 *  * 인증과 가입 시 인증 시간을 갱신하며 15일을 기준으로 활성/비활성을 체크하여 값을 저장한다.
 * Created by kimkyuhyun on 2015-12-02.
 */
public interface ActiveUserService {

    /**
     * 해당 iuid, 전화번호에 해당하는 사용자가 활성화 사용자가 아닐 경우 활성화하고 인증 및 추적 시간을 업데이트 한다.
     *
     * @param iuid    사용자 IUID
     * @param phoneNo 사용자 전화번호
     */
    void updateActiveUser(Long iuid, String phoneNo, String tnVersion, Integer storeType);
}
