package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.db.domain.PromotionSamsung;
import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.PromotionCampPackInfo;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface PromotionService {


    /**
     * 프로모션 리스트 요청에 대한 처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param mdn              요청 시 고객 번호
     * @param os_type          요청 시 고객 OS 타입
     * @param osVersion        요청 시 고객 OS 버전
     * @param telecomCode      요청 시 고객 이통사 코트
     * @return 클라이언트 프로모션 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionListRequest(HttpServletRequest request, String mdn, String os_type, String osVersion, String telecomCode );

    /**
     * 삼성생명 가족 요청에 대한 조회처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @return 클라이언트 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionSamSungInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams );

    /**
     * 캠프팩 가족 요청에 대한 조회처리를 한다. (일반인 기준)
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @return 클라이언트 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionCampPackInfoRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionCampPackInfo promotionCampPackInfo );

    /**
     * 캠프팩 가족 등록처리를 한다. (일반인 기준)
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @param promotionCampPackInfo  요청 시 캠프팩 등록 정보
     * @return 클라이언트 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionCampPackInfoReg(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionCampPackInfo promotionCampPackInfo);

    /**
     * 삼성생명 가족 등록 요청에 대한 조회처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @param promotionSamsung  등록 대상 정보
     * @return 클라이언트 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionSamSungRegRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung);

    /**
     * 삼성생명 가족 등록 시 OEM 주소록 조회 요청에 대한 조회처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @param promotionSamsung  조회 대상 정보
     * @return 클라이언트 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionSamSungOemRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PromotionSamsung promotionSamsung);


    /**
     * 프로모션 정보 조회에 대한 조회처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @return 클라이언트 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionListRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);

    /**
     * PC 프로모션 정보 조회에 대한 조회처리를 한다.
     *
     * @param request          서블릿 요청 객체
     * @param authReqHeaders 요청 시 헤더
     * @param authReqParams  요청 시 쿼리 파라미터
     * @return 클라이언트 요청 처리에 대한 응답
     */

    ResponseEntity<String> processPromotionPcListRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams);
}
