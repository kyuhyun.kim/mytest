package com.tisquare.mvp.mcs.service.Impl;


import com.tisquare.mvp.mcs.McsConfig;
import com.tisquare.mvp.mcs.db.ClientDao;
import com.tisquare.mvp.mcs.db.EmailKeyDao;
import com.tisquare.mvp.mcs.db.SmsDao;
import com.tisquare.mvp.mcs.db.UserSearchDao;
import com.tisquare.mvp.mcs.entity.ActiveUser;
import com.tisquare.mvp.mcs.entity.SubscriberType;
import com.tisquare.mvp.mcs.interact.HttpAsyncClientWorker;
import com.tisquare.mvp.mcs.model.*;
import com.tisquare.mvp.mcs.model.pc.IdentifyPasswd;
import com.tisquare.mvp.mcs.model.pc.PcAuthInfo;
import com.tisquare.mvp.mcs.service.MailService;
import com.tisquare.mvp.mcs.service.PcIdentifyPasswdControl;
import com.tisquare.mvp.mcs.service.PcIdentifyPasswdService;
import com.tisquare.mvp.mcs.type.API_URL;
import com.tisquare.mvp.mcs.type.FindIDType;
import com.tisquare.mvp.mcs.type.SUBS_TYPE;
import com.tisquare.mvp.mcs.type.pc.MailType;
import com.tisquare.mvp.mcs.type.pc.SenderType;
import com.tisquare.mvp.mcs.utils.AsyncProcess;
import com.tisquare.mvp.mcs.utils.RandomString;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by kimkyuhyun on 2015-12-02.
 */
@Service
public class PcIdentifyPasswdControlimpl implements PcIdentifyPasswdControl {

    private static final Logger logger = LoggerFactory.getLogger(PcIdentifyPasswdControlimpl.class);

    @Autowired
    UserSearchDao userSearchDao;

    @Autowired
    McsConfig mcsConfig;

    @Autowired
    MailService mailService;

    @Autowired
    SmsDao smsDao;

    @Autowired
    ClientDao clientDao;

    @Autowired
    EmailKeyDao emailKeyDao;


    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    AsyncProcess asyncProcess;

    @Override
    public HttpStatus processPcjoinMailCertRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo)
    {
        try {

            if (pcAuthInfo.getUser_id() == null || pcAuthInfo.getCert_no() == null)
                return HttpStatus.BAD_REQUEST;

            if( emailKeyDao.selectEmailKey(pcAuthInfo) > 0) {

                emailKeyDao.deleteEmailKey(pcAuthInfo);
                return HttpStatus.OK;
            }
            else
                return HttpStatus.FORBIDDEN;

        } catch (Exception e) {
        logger.error("가입자 인증 메일 확인 실패." + e.toString());
        return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

    @Override
    public HttpStatus processPcjoinMailResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo)
    {
        try {

            if (pcAuthInfo.getUser_id() == null || pcAuthInfo.getType() == null)
                return HttpStatus.BAD_REQUEST;


            if (pcAuthInfo.getType() == SenderType.EMAIL.getCode()) {

                String setfrom = mcsConfig.getProperty("EMAIL.SENDER");
                String title = "더 캠프 아이디 발송";
                String content = null;


                /*title = "더 캠프 인증 메일 발송";*/
                title = mcsConfig.getProperty("EMAIL.SUBJECT");


            /*    System.out.println("utf-8 -> euc-kr        : " + new String(title.getBytes("utf-8"), "euc-kr"));
                System.out.println("utf-8 -> ksc5601       : " + new String(title.getBytes("utf-8"), "ksc5601"));
                System.out.println("utf-8 -> x-windows-949 : " + new String(title.getBytes("utf-8"), "x-windows-949"));
                System.out.println("utf-8 -> iso-8859-1    : " + new String(title.getBytes("utf-8"), "iso-8859-1"));

                System.out.println("iso-8859-1 -> euc-kr        : " + new String(title.getBytes("iso-8859-1"), "euc-kr"));
                System.out.println("iso-8859-1 -> ksc5601       : " + new String(title.getBytes("iso-8859-1"), "ksc5601"));
                System.out.println("iso-8859-1 -> x-windows-949 : " + new String(title.getBytes("iso-8859-1"), "x-windows-949"));
                System.out.println("iso-8859-1 -> utf-8         : " + new String(title.getBytes("iso-8859-1"), "utf-8"));

                System.out.println("euc-kr -> utf-8         : " + new String(title.getBytes("euc-kr"), "utf-8"));
                System.out.println("euc-kr -> ksc5601       : " + new String(title.getBytes("euc-kr"), "ksc5601"));
                System.out.println("euc-kr -> x-windows-949 : " + new String(title.getBytes("euc-kr"), "x-windows-949"));
                System.out.println("euc-kr -> iso-8859-1    : " + new String(title.getBytes("euc-kr"), "iso-8859-1"));

                System.out.println("ksc5601 -> euc-kr        : " + new String(title.getBytes("ksc5601"), "euc-kr"));
                System.out.println("ksc5601 -> utf-8         : " + new String(title.getBytes("ksc5601"), "utf-8"));
                System.out.println("ksc5601 -> x-windows-949 : " + new String(title.getBytes("ksc5601"), "x-windows-949"));
                System.out.println("ksc5601 -> iso-8859-1    : " + new String(title.getBytes("ksc5601"), "iso-8859-1"));

                System.out.println("x-windows-949 -> euc-kr     : " + new String(title.getBytes("x-windows-949"), "euc-kr"));
                System.out.println("x-windows-949 -> utf-8      : " + new String(title.getBytes("x-windows-949"), "utf-8"));
                System.out.println("x-windows-949 -> ksc5601    : " + new String(title.getBytes("x-windows-949"), "ksc5601"));
                System.out.println("x-windows-949 -> iso-8859-1 : " + new String(title.getBytes("x-windows-949"), "iso-8859-1"));*/






                String ramdomKey = RandomString.getJoinMailKey();
                content = String.format(mcsConfig.getProperty("EMAIL.PASSWD.TEXT"), ramdomKey);
                pcAuthInfo.setCert_no(ramdomKey);

                MimeMessage message = mailSender.createMimeMessage();
                MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
                messageHelper.setFrom(setfrom);  // 보내는사람 생략하거나 하면 정상작동을 안함
                messageHelper.setTo(pcAuthInfo.getUser_id());     // 받는사람 이메일
                messageHelper.setSubject(title); // 메일제목은 생략이 가능하다
                messageHelper.setText(content);  // 메일 내용
                mailSender.send(message);

                emailKeyDao.insertEmailKey( pcAuthInfo );



            } else {

                logger.error("정의 되지 않은 타입 [{}]", pcAuthInfo.getType());
                return HttpStatus.BAD_REQUEST;
            }

        } catch (Exception e) {
            logger.error("가입자 아이디 발송 실패." + e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    @Override
    public HttpStatus processPcId_sendResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, PcAuthInfo pcAuthInfo) {
        try {

            if (pcAuthInfo.getUser_id() == null || pcAuthInfo.getType() == null)
                return HttpStatus.BAD_REQUEST;


            if (pcAuthInfo.getType() == SenderType.EMAIL.getCode()) {

                String setfrom = mcsConfig.getProperty("EMAIL.SENDER");
                String title = "더 캠프 아이디 발송";
                String content = null;


                title = "더 캠프 아이디 찾기";
                title = mcsConfig.getProperty("EMAIL.ID.FIND.SUBJECT");
                content = String.format(mcsConfig.getProperty("EMAIL.ID.TEXT"),  pcAuthInfo.getUser_id());




            /*Resource resource = new ClassPathResource("samsung.html");
            System.out.println("파일사이즈::" + resource.getFile().length());
            System.out.println("파일절대경로+파일명:" + resource.getURI().getPath().substring(1));
*/

                MimeMessage message = mailSender.createMimeMessage();

                MimeMessageHelper messageHelper
                        = new MimeMessageHelper(message, true, "UTF-8");

                messageHelper.setFrom(setfrom);  // 보내는사람 생략하거나 하면 정상작동을 안함

                messageHelper.setTo(pcAuthInfo.getUser_id());     // 받는사람 이메일


                messageHelper.setSubject(title); // 메일제목은 생략이 가능하다

                messageHelper.setText(content);  // 메일 내용

                mailSender.send(message);

            } else if (pcAuthInfo.getType() == SenderType.SMS.getCode()) {
                if( pcAuthInfo.getMdn() == null)
                    return HttpStatus.BAD_REQUEST;

                asyncProcess.NsInterFaceSMS(pcAuthInfo, mcsConfig.getProperty("NGINX.INTERNAL.VIP") + API_URL.NS_SMS);
            } else {

                logger.error("정의 되지 않은 타입 [{}]", pcAuthInfo.getType());
                return HttpStatus.BAD_REQUEST;
            }

        } catch (Exception e) {
            logger.error("가입자 아이디 발송 실패." + e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    @Override
    public HttpStatus processPcPasswdResetRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd) {
        try {

            if (identifyPasswd.getIuid() == null || identifyPasswd.getUser_pwd() == null)
                return HttpStatus.BAD_REQUEST;

            if (clientDao.selectExistIuid(identifyPasswd.getIuid()) == 0)
                return HttpStatus.NOT_FOUND;

            clientDao.updatePasswd(new UsInfo(identifyPasswd.getIuid(), identifyPasswd.getUser_pwd()));

        } catch (Exception e) {
            logger.error("인증 번호 확인 실패." + e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    @Override
    public HttpStatus processPcPasswdCertRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd) {
        try {

            if (identifyPasswd.getType() == null || identifyPasswd.getType() > FindIDType.BIRTH.getCode() || identifyPasswd.getName() == null || identifyPasswd.getCert_no() == null)
                return HttpStatus.BAD_REQUEST;

            if (identifyPasswd.getType() == FindIDType.MDN.getCode())
                if (identifyPasswd.getMdn() == null)
                    return HttpStatus.BAD_REQUEST;
            if (identifyPasswd.getType() == FindIDType.BIRTH.getCode())
                if (identifyPasswd.getBirth() == null)
                    return HttpStatus.BAD_REQUEST;


            IdentifyPasswd identifyPasswd_result = userSearchDao.selectSuidInfo(identifyPasswd);

            if (identifyPasswd_result == null || identifyPasswd_result.getIuid() == null)
                return HttpStatus.NOT_FOUND;

            identifyPasswd.setIuid(identifyPasswd_result.getIuid());

            if (identifyPasswd.getType() == FindIDType.MDN.getCode()) {
                if (!StringUtils.equals(identifyPasswd_result.getMdn(), identifyPasswd.getMdn())) {
                    logger.error("MDN 불일치!! [{}][{}]", identifyPasswd_result.getMdn(), identifyPasswd.getMdn());
                    return HttpStatus.FORBIDDEN;
                }
                if (!StringUtils.equals(identifyPasswd_result.getName(), identifyPasswd.getName())) {
                    logger.error("이름 불일치!! [{}][{}]", identifyPasswd_result.getName(), identifyPasswd.getName());
                    return HttpStatus.FORBIDDEN;
                }
            }

            if (identifyPasswd.getType() == FindIDType.BIRTH.getCode()) {
                if (!StringUtils.equals(identifyPasswd_result.getName(), identifyPasswd.getName())) {
                    logger.error("이름 불일치!! [{}][{}]", identifyPasswd_result.getName(), identifyPasswd.getName());
                    return HttpStatus.FORBIDDEN;
                }
                if (!identifyPasswd_result.getBirth().equals(identifyPasswd.getBirth())) {
                    logger.error("생년월일 불일치!! [{}][{}]", identifyPasswd_result.getBirth(), identifyPasswd.getBirth());
                    return HttpStatus.FORBIDDEN;
                }
            }

            identifyPasswd_result.setCert_no(identifyPasswd.getCert_no());


            if(identifyPasswd.getType() == FindIDType.MDN.getCode()) {

            /* 인증 번호 확인 */
                if (smsDao.selectSmsKey(identifyPasswd_result) == 0) {
                    logger.error("인증 결과 불일치 또는 인증 시간 초과!!");
                    return HttpStatus.FORBIDDEN;
                }
                smsDao.deleteSmsKey(identifyPasswd_result);
            }
            else
            {
                if( emailKeyDao.selectEmailKey(new PcAuthInfo(identifyPasswd_result)) > 0) {

                    emailKeyDao.deleteEmailKey(new PcAuthInfo(identifyPasswd_result));
                return HttpStatus.OK;
            }
            else
                return HttpStatus.FORBIDDEN;

            }


        } catch (Exception e) {
            logger.error("인증 번호 확인 실패." + e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    @Override
    public HttpStatus processPcPasswdSmsRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd) {
        try {

            if (identifyPasswd.getType() == null || identifyPasswd.getType() > FindIDType.BIRTH.getCode() || identifyPasswd.getName() == null)
                return HttpStatus.BAD_REQUEST;

            if (identifyPasswd.getType() == FindIDType.MDN.getCode())
                if (identifyPasswd.getMdn() == null)
                    return HttpStatus.BAD_REQUEST;
            if (identifyPasswd.getType().equals(FindIDType.BIRTH.getCode()))
                if (identifyPasswd.getBirth() == null)
                    return HttpStatus.BAD_REQUEST;


            IdentifyPasswd identifyPasswd_result = userSearchDao.selectSuidInfo(identifyPasswd);

            if (identifyPasswd_result == null)
                return HttpStatus.NOT_FOUND;

            if(identifyPasswd_result.getSubs_type() != SubscriberType.NORMAL.value())
            {
                logger.error("가입 유형이 E-MAIL 방식이 아닙니다. 유형[{}]", identifyPasswd_result.getSubs_type());
                return HttpStatus.METHOD_NOT_ALLOWED;
            }

            if (identifyPasswd.getType() == FindIDType.MDN.getCode()) {
                if (!StringUtils.equals(identifyPasswd_result.getMdn(), identifyPasswd.getMdn())) {
                    logger.error("[전화 번호 타입] 전화번호가 일치 하지 않습니다[{}][{}]",identifyPasswd_result.getMdn(), identifyPasswd.getMdn());
                    return HttpStatus.FORBIDDEN;
                }
                if (!StringUtils.equals(identifyPasswd_result.getName(), identifyPasswd.getName())) {
                    logger.error("[전화 번호 타입] 이름이 일치 하지 않습니다[{}][{}]",identifyPasswd_result.getName(), identifyPasswd.getName());
                    return HttpStatus.FORBIDDEN;
                }
                /* SMS 발송 */
                sendSms(authReqHeaders, authReqParams, identifyPasswd_result);
            }

            if (identifyPasswd.getType() == FindIDType.BIRTH.getCode()) {

                if (!StringUtils.equals(identifyPasswd_result.getName(), identifyPasswd.getName())) {
                    logger.error("[생년월일 타입] 이름이 일치 하지 않습니다[{}][{}]",identifyPasswd_result.getName(), identifyPasswd.getName());
                    return HttpStatus.FORBIDDEN;
                }
                if (!identifyPasswd_result.getBirth().equals(identifyPasswd.getBirth())) {
                    logger.error("[생년월일 타입] 생년월일이 일치 하지 않습니다[{}][{}]",identifyPasswd_result.getBirth(), identifyPasswd.getBirth());
                    return HttpStatus.FORBIDDEN;
                }
                String setfrom = mcsConfig.getProperty("EMAIL.SENDER");
                String title = "더 캠프 아이디 발송";
                String content = null;


                title = "더 캠프 아이디 찾기";
                title = String.format(mcsConfig.getProperty("EMAIL.PASSWD.FIND.SUBJECT"));
                /*title = new String(title.getBytes("iso-8859-1"), "utf-8");*/
                content = identifyPasswd_result.getUser_id();




            /*Resource resource = new ClassPathResource("samsung.html");
            System.out.println("파일사이즈::" + resource.getFile().length());
            System.out.println("파일절대경로+파일명:" + resource.getURI().getPath().substring(1));
*/

                String ramdomKey = RandomString.getJoinMailKey();
                content = String.format(mcsConfig.getProperty("EMAIL.PASSWD.TEXT"), ramdomKey);

                identifyPasswd_result.setCert_no(ramdomKey);

                MimeMessage message = mailSender.createMimeMessage();

                MimeMessageHelper messageHelper
                        = new MimeMessageHelper(message, true, "UTF-8");

                messageHelper.setFrom(setfrom);  // 보내는사람 생략하거나 하면 정상작동을 안함

                messageHelper.setTo(identifyPasswd_result.getUser_id());     // 받는사람 이메일


                messageHelper.setSubject(title); // 메일제목은 생략이 가능하다

                messageHelper.setText(content);  // 메일 내용

                mailSender.send(message);

                emailKeyDao.insertEmailKey( new PcAuthInfo(identifyPasswd_result) );

            }


        } catch (Exception e) {
            logger.error("인증 번호 발송 실패." + e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    @Override
    public HttpStatus processPcIdRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd) {


        try {

            List<IdentifyPasswd> identifyPasswds = userSearchDao.selectFindId(identifyPasswd);

            identifyPasswd.setIdentifyPasswds(identifyPasswds);

        } catch (Exception e) {
            logger.error("아이디 찾기 실패." + e.toString());

            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return HttpStatus.OK;
    }

    private void sendSms(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd) {
        try {
            ExecutorService pool = Executors.newSingleThreadExecutor();

            Map<String, String> headers = new HashMap<>();
            headers.put("TN-REQ-USER", identifyPasswd.getMdn());
            headers.put("TN-APP-MARKET", "0");
            headers.put("TN-VERSION", authReqHeaders.getAppVersion());

            pool.execute(HttpAsyncClientWorker.create(mcsConfig.getProperty("NGINX.INTERNAL.VIP") + mcsConfig.getProperty("SCS.SCS.REQ"), headers, Integer.valueOf(mcsConfig.getProperty("server.read.timeout"))));
            pool.shutdown();
        } catch (Exception e) {
            logger.error(" => send chg_mdn notify failed. PHONE_NO : {} | {}", authReqHeaders.getPhoneNo(), e);
        }
    }


    private void certSms(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd) {
        try {
            ExecutorService pool = Executors.newSingleThreadExecutor();

            Map<String, String> headers = new HashMap<>();
            headers.put("TN-REQ-USER", identifyPasswd.getMdn());
            headers.put("TN-APP-MARKET", "0");
            headers.put("TN-VERSION", authReqHeaders.getAppVersion());

            pool.execute(HttpAsyncClientWorker.create(mcsConfig.getProperty("NGINX.INTERNAL.VIP") + mcsConfig.getProperty("SCS.SCS.REQ"), headers, Integer.valueOf(mcsConfig.getProperty("server.read.timeout"))));
            pool.shutdown();
        } catch (Exception e) {
            logger.error(" => send chg_mdn notify failed. PHONE_NO : {} | {}", authReqHeaders.getPhoneNo(), e);
        }
    }

}
