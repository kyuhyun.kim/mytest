package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.db.TestDao;
import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.service.AlarmService;
import com.tisquare.mvp.mcs.service.ResponseEntityService;
import com.tisquare.mvp.mcs.service.TestService;
import org.apache.ibatis.io.ResolverUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class TestServiceImpl extends ResponseEntityService implements TestService {

    private static final Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);

    @Autowired
    TestDao testDao;

    @Override
    public ResponseEntity<String> processTestRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders)
    {
        Random rand = new Random();
        List<String> phone_no = new ArrayList<>();
        for(int nIdx = 0; nIdx < 2000; nIdx++)
        {
            int randomInteger = rand.nextInt(20000000) + 1000000;
            phone_no.add( "+8211" + String.format("%08d", randomInteger) );
        }
        List<Long> iuids = testDao.selectProfileSyn(phone_no);
        return makeResponseEntityWithCheckBodySize(request, iuids, HttpStatus.OK);

    }

    @Override
    public ResponseEntity<String> processTestAllRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders)
    {
        Random rand = new Random();
        List<String> phone_no = new ArrayList<>();
        for(int nIdx = 0; nIdx < 2000; nIdx++)
        {
            int randomInteger = rand.nextInt(20000000) + 1000000;
            phone_no.add( "+8211" + String.format("%08d", randomInteger) );
        }
        List<HashMap<Long, String>> iuids = testDao.selectAllProfile();

     /*   iuids.*/

        return makeResponseEntityWithCheckBodySize(request, null, HttpStatus.OK);

    }


}
