package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.utils.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import javax.servlet.http.HttpServletRequest;

public abstract class ResponseEntityService {

    private static Logger logger = LoggerFactory.getLogger(ResponseEntityService.class);

    public ResponseEntity<String> makeResponseEntity(HttpStatus httpStatus) {
        return new ResponseEntity<>(httpStatus);
    }

    public ResponseEntity<String> makeResponseEntity(Object content) {
        return makeResponseEntity(content, HttpStatus.OK);
    }

    public ResponseEntity<String> makeResponseEntity(HttpServletRequest request, Object content, HttpStatus status) {
        HttpHeaders headers = new HttpHeaders();

        String responseBody;
        if (content instanceof String) {
            responseBody = (String) content;
        } else {
            try {
                responseBody = Json.toStringJson(content);
                logger.info(responseBody);
            } catch (Exception e) {
                return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        request.setAttribute("content.response", responseBody);

        return new ResponseEntity<>(responseBody, headers, status);
    }

    public ResponseEntity<String> makeResponseEntityWithCheckBodySize(HttpServletRequest request, Object content, HttpStatus status) {
        String responseBody;

        if (content instanceof String) {
            responseBody = (String) content;
        } else {
            try {
                responseBody = Json.toStringJson(content);
            } catch (Exception e) {
                logger.error("", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }

        request.setAttribute("content.response", responseBody);



        if (responseBody.length() > 0) {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type", "application/json;charset=utf-8");
            return new ResponseEntity<>(responseBody, headers, status);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    public ResponseEntity<String> makeResponseEntityWithCheckBodySizeAndHeaders(HttpServletRequest request, HttpHeaders headers, Object content, HttpStatus status) {
        String responseBody;

        if (content instanceof String) {
            responseBody = (String) content;
        } else {
            try {
                responseBody = Json.toStringJson(content);
            } catch (Exception e) {
                logger.error("object to json convert error", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        request.setAttribute("content.response", responseBody);

        if (responseBody.length() > 0) {
            headers.set("Content-Type", "application/json;charset=utf-8");
            return new ResponseEntity<>(responseBody, headers, status);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    public <T> ResponseEntity<String> makeResponseEntity(T content, HttpStatus status) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=utf-8");

        String responseBody;
        if (content instanceof String) {
            responseBody = (String) content;
        } else {
            try {
                responseBody = Json.toStringJson(content);
                logger.info(responseBody);
            } catch (Exception e) {
                return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(responseBody, headers, status);
    }

    public ResponseEntity<String> makeResponseEntityWithHeaders(HttpHeaders headers, HttpStatus status) {
        return new ResponseEntity<>(headers, status);
    }

}
