package com.tisquare.mvp.mcs.service;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.ClientAuthReqParams;
import com.tisquare.mvp.mcs.model.pc.IdentifyPasswd;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
public interface MailService {


    /**
     * 메일 발송 요청에 대한 처리를 한다.
     *
     * @param identifyPasswd 고객 정보
     * @return 메일 발송 처리에 대한 응답
     */

    boolean MailSend(ClientAuthReqHeaders authReqHeaders, ClientAuthReqParams authReqParams, IdentifyPasswd identifyPasswd);
}

