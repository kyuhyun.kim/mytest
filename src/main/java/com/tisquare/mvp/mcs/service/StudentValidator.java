package com.tisquare.mvp.mcs.service;

/**
 * Created by kimkyuhyun on 2017-05-29.
 */
import com.tisquare.mvp.mcs.model.Download;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


public class StudentValidator implements Validator {

    public boolean supports(Class<?> paramClass) {
        return Download.class.equals(paramClass);
    }

    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "valid.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "valid.lastName");
    }
}
