package com.tisquare.mvp.mcs.service.Impl;

import com.tisquare.mvp.mcs.model.ClientAuthReqHeaders;
import com.tisquare.mvp.mcs.model.NoticeBody;
import com.tisquare.mvp.mcs.model.NoticeListBody;
import com.tisquare.mvp.mcs.model.NoticeReqHeaders;
import com.tisquare.mvp.mcs.service.AlarmService;
import com.tisquare.mvp.mcs.service.NoticeControl;
import com.tisquare.mvp.mcs.service.NoticeService;
import com.tisquare.mvp.mcs.service.ResponseEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kimkyuhyun on 2015-05-06.
 */
@Service
public class AlarmServiceImpl extends ResponseEntityService implements AlarmService {

    private static final Logger logger = LoggerFactory.getLogger(AlarmServiceImpl.class);

    @Override
    public ResponseEntity<String> processAlarmJoinRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders)
    {
            return makeResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> processAlarmWithdrawRequest(HttpServletRequest request, ClientAuthReqHeaders authReqHeaders)
    {
        return makeResponseEntity(HttpStatus.OK);
    }

}
